package com.brainmagic.pricolsalesorder;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.borax12.materialdaterangepicker.date.DatePickerDialog;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.TreeMap;

import Home.homevalues;
import Logout.logout;
import adapter.ViewHistoryAdapter;
import alertbox.Alertbox;
import alertbox.PasswordChangeAlert;
import alertbox.PrimaryOrder;
import network.NetworkConnection;
import persistance.ServerConnection;

public class TravelHistoryActivityRegion1 extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {
    private ListView listhistory;
    private ImageView menu,back,home,logout;
    private Alertbox box = new Alertbox(this);
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private Connection connection;
    private Button filterhistory;
    private Statement statement;
    private ResultSet resultSet;
    private int id;
    private ArrayList<String> DateList;
    private TreeMap<Integer,String> hashlist=new TreeMap<>(Collections.<Integer>reverseOrder());
    private boolean mAutoHighlight;
    private static final String TAG = "TravelHistoryActivity";
    private ImageView setting;
    private TextView title;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_travel_history_region1);
        myshare = getSharedPreferences("pricolsalesorder", MODE_PRIVATE);
        editor = myshare.edit();
        listhistory=(ListView)findViewById(R.id.listhistory);
        filterhistory=(Button)findViewById(R.id.filterhistory);
        home = (ImageView) findViewById(R.id.home);
        logout = (ImageView) findViewById(R.id.logout);
        id=myshare.getInt("idlistreg",0);
        hashlist.clear();
        title = (TextView) findViewById(R.id.title);
        title.setText("Travel History");
        menu=(ImageView)findViewById(R.id.menu) ;
        back=(ImageView)findViewById(R.id.back) ;
        setting=(ImageView)findViewById(R.id.setting);
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PopupMenu popupMenu = new PopupMenu(TravelHistoryActivityRegion1.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu pop) {
                        pop.dismiss();
                    }
                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.primary:
                                PrimaryOrder alert = new PrimaryOrder(TravelHistoryActivityRegion1.this);// new saleslogin().execute();
                                alert.showLoginboxtoggle();
                                return true;
                            case R.id.ch:
                                CheckInternetforpass();
                                return true;
                            case R.id.logout:
                                new logout(TravelHistoryActivityRegion1.this).log_outReg();
                                return true;
                        }
                        return false;
                    }
                });
                popupMenu.inflate(R.menu.pricolcms);
                Menu pop = popupMenu.getMenu();
                popupMenu.show();

            }
        });
//        back.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                onBackPressed();
//            }
//        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new homevalues(TravelHistoryActivityRegion1.this).home_io();
            }
        });
//        logout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                new logout(TravelHistoryActivityRegion1.this).log_outReg();
//            }
//        });
        filterhistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = com.borax12.materialdaterangepicker.date.DatePickerDialog.newInstance(
                        (DatePickerDialog.OnDateSetListener) TravelHistoryActivityRegion1.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );

                dpd.setAutoHighlight(mAutoHighlight);
                dpd.show(getFragmentManager(), "Datepickerdialog");
            }
        });


        checkInternet();
    }

    private void CheckInternetforpass() {
        NetworkConnection network = new NetworkConnection(TravelHistoryActivityRegion1.this);
        if (network.CheckInternet()) {
            PasswordChangeAlert alert = new PasswordChangeAlert(TravelHistoryActivityRegion1.this);// new saleslogin().execute();
            alert.showLoginbox();
        } else {
            final AlertDialog alertDialog = new AlertDialog.Builder(TravelHistoryActivityRegion1.this).create();
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.loginalert, null);
            alertDialog.setView(dialogView);
            TextView login =(TextView)dialogView.findViewById(R.id.login);
            login.setText("Please Check your Internet Connection !!");
            Button submit =(Button) dialogView.findViewById(R.id.submit);
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();

                }

            });
            alertDialog.show();
        }
    }

    private void checkInternet() {
        NetworkConnection networkConnection=new NetworkConnection(TravelHistoryActivityRegion1.this);
        if(networkConnection.CheckInternet()){
            new Travelhistory().execute("Select * from Regional_AttendanceTable where EmpId = '"+ id +"'");
        }else{
            final AlertDialog alertDialog = new AlertDialog.Builder(TravelHistoryActivityRegion1.this).create();
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.loginalert, null);
            alertDialog.setView(dialogView);
            TextView login =(TextView)dialogView.findViewById(R.id.login);
            login.setText("Please Check your Internet Connection !!");
            Button submit =(Button) dialogView.findViewById(R.id.submit);
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();

                }

            });
            alertDialog.show();
        }
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
        String date = "" + (monthOfYear + 1) + "You picked the following date: From- \" + dayOfMonth + \"//" + year + " To " + dayOfMonthEnd + "/" + (monthOfYearEnd+1) + "/" + yearEnd;
        String FromDate = "";
        String Todate = "";

        Toast.makeText(TravelHistoryActivityRegion1.this, date, Toast.LENGTH_LONG).show();

        if (monthOfYear+1 < 10)
            FromDate = year + "-0" + (monthOfYear + 1) + "-" + dayOfMonth;
        else {
            FromDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
        }

        if (monthOfYearEnd+1 < 10)
            Todate = yearEnd + "-0" + (monthOfYearEnd + 1) + "-" + dayOfMonthEnd;
        else {
            Todate = yearEnd + "-" + (monthOfYearEnd + 1) + "-" + dayOfMonthEnd;
        }

//        new GetTravelHistory().execute("select datetime from Travelcoordinates where S_id = '" + salesID + "' and convert(nvarchar,datetime) between '" + FromDate + "' and '" + Todate + "' and IsCancelled ='0' order by datetime desc");
//        new Travelhistory().execute("select  * from AttendanceTable where EmpId = '" + id + "' and date between '" + FromDate + "' and '" + Todate + "' order by date desc");
        new Travelhistory().execute("select id,date from Regional_AttendanceTable where EmpId = '" + id + "' and date between '" + FromDate + "' and '" + Todate + "' order by date desc");
    }


    private class Travelhistory extends AsyncTask<String, Void, String> {
        private ProgressDialog progressDialog;
        //        private Integer id;
        private SQLiteDatabase db;
        private List<String> namelist,datelist,intimelist,fromlocation,outtime,tolocation,fromlat,fromlong,tolat,tolong,daylist,empidlist,Designationlist;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(TravelHistoryActivityRegion1.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.show();
            DateList = new ArrayList<String>();
            datelist=new ArrayList<>();
            namelist=new ArrayList<>();
            intimelist=new ArrayList<>();
            fromlocation=new ArrayList<>();
            outtime=new ArrayList<>();
            tolocation=new ArrayList<>();
            fromlat=new ArrayList<>();
            fromlong=new ArrayList<>();
            tolat=new ArrayList<>();
            tolong=new ArrayList<>();
            daylist=new ArrayList<>();
            empidlist=new ArrayList<>();
            Designationlist=new ArrayList<>();
            hashlist.clear();



//            descriptiondy = (EditText) newView.findViewById(R.id.descriptiondynamic);
//            quantitydy = (EditText) newView.findViewById(R.id.quantitydynamic);
//            productdy = (MaterialSpinner) newView.findViewById(R.id.productdynname);


        }

        @Override
        protected String doInBackground(String... strings) {
            ServerConnection serverConnection = new ServerConnection();
            try {
                connection = serverConnection.getConnection();
                /*statement = connection.prepareStatement(selectquery);*/
                statement = connection.createStatement();
                String selectquery =  strings[0];
                resultSet = statement.executeQuery(selectquery);
                while (resultSet.next()) {
                    DateList.add(resultSet.getString("Date"));
                    hashlist.put(resultSet.getInt("id"),resultSet.getString("Date"));
                    Log.d(TAG, "doInBackground: datetime"+resultSet.getString("Date"));
                }
                for(Integer i:hashlist.keySet()) {
                    Log.v("query", selectquery);
                    String query2 = "select * from Regional_AttendanceTable where EmpId = '" + id + "' and date = '" + hashlist.get(i) + "'and id ='" + i + "' order by Date desc";
                    resultSet = statement.executeQuery(query2);
                    while (resultSet.next()) {
                        Log.d(TAG, "doInBackground: fromlatitude "+resultSet.getString("OutLatitude"));
                        Log.d(TAG, "doInBackground: fromongitude "+resultSet.getString("OutLongitude"));
//                        namelist.add(resultSet.getString("Name"));
                        datelist.add(resultSet.getString("Date"));
                        intimelist.add(resultSet.getString("InTime"));
                        outtime.add(resultSet.getString("OutTime"));
                        fromlocation.add(resultSet.getString("Address"));
                        tolocation.add(resultSet.getString("OutAddress"));
                        fromlat.add(resultSet.getString("InLatitude"));
                        fromlong.add(resultSet.getString("InLongitude"));
                        tolat.add(resultSet.getString("OutLatitude"));
                        tolong.add(resultSet.getString("OutLongitude"));
                        daylist.add(resultSet.getString("AttendDay"));
                        empidlist.add(resultSet.getString("EmpId"));
                        Designationlist.add(resultSet.getString("Designation"));

                    }
                }
                if (!datelist.isEmpty()) {
                    statement.close();
                    connection.close();
                    resultSet.close();
                    return "success";
                } else {
                    statement.close();
                    connection.close();
                    resultSet.close();
                    return "empty";
                }

            } catch (Exception e) {
                e.printStackTrace();
                return "error_insertion";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();

            if (s.equals("success")) {
                ViewHistoryAdapter viewAttendanceAdapter=new ViewHistoryAdapter(TravelHistoryActivityRegion1.this,namelist,datelist,intimelist,outtime,fromlocation,tolocation,fromlat,fromlong,tolat,tolong,daylist,empidlist,Designationlist);
                listhistory.setAdapter(viewAttendanceAdapter);

            } else if (s.equals("empty")) {
                final AlertDialog alertDialog = new AlertDialog.Builder(TravelHistoryActivityRegion1.this).create();
                LayoutInflater inflater = getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.loginalert, null);
                alertDialog.setView(dialogView);
                TextView login =(TextView)dialogView.findViewById(R.id.login);
                login.setText("No RegionNewOrder found!");
                Button submit =(Button) dialogView.findViewById(R.id.submit);
                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();

                    }

                });
                alertDialog.show();
            } else {
                final AlertDialog alertDialog = new AlertDialog.Builder(TravelHistoryActivityRegion1.this).create();
                LayoutInflater inflater = getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.loginalert, null);
                alertDialog.setView(dialogView);
                TextView login =(TextView)dialogView.findViewById(R.id.login);
                login.setText("Please Check your Internet Connection !!");
                Button submit =(Button) dialogView.findViewById(R.id.submit);
                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();

                    }

                });
                alertDialog.show();
            }
        }
    }

}
