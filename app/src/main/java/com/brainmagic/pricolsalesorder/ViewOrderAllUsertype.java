package com.brainmagic.pricolsalesorder;

import android.app.ProgressDialog;
import android.content.SharedPreferences;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

import Home.homevalues;
import Logout.logout;
import adapter.OrderPreviewAdapter1;
import adapter.ViewExecuteAllAdapter;
import alertbox.Alertbox;
import api.models.datewiseorderSe.DateWiseSeData;
import api.models.datewiseorderSe.Order_list;
import api.retrofit.APIService;
import api.retrofit.RetrofitClient;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewOrderAllUsertype extends AppCompatActivity {
    private ListView listsalesattendance;
    private ImageView menu, back,home,logout,setting;
    private Alertbox box = new Alertbox(this);
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private TextView title,name;
    private int regid;
    private List<Order_list> data;
    private Integer data1;
    private SearchView searchview;
    private ViewExecuteAllAdapter viewExecuteAdapter;
    private String empcode,fromdate,todate,customertype;
    private TextView fromdateText,todatetext;
    private OrderPreviewAdapter1 orderPreviewAdapter1;
    TextView Text2,Amount;
    private int executeid;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_order_all_usertype);
        myshare = getSharedPreferences("pricolsalesorder", MODE_PRIVATE);
        editor = myshare.edit();
        regid=myshare.getInt("idlistreg",0);
        listsalesattendance=(ListView)findViewById(R.id.listorderall);
        home = (ImageView) findViewById(R.id.home);
        logout = (ImageView) findViewById(R.id.logout);
        title=(TextView)findViewById(R.id.title);
        name=(TextView)findViewById(R.id.name);
        fromdateText=(TextView)findViewById(R.id.fromdateText);
        todatetext=(TextView)findViewById(R.id.todatetext);
        setting=(ImageView) findViewById(R.id.setting);
        Text2 = (TextView) findViewById(R.id.Title);
        Amount = (TextView) findViewById(R.id.amounttext);
        title.setText("Order History");
        executeid=myshare.getInt("idlist",0);
        empcode=getIntent().getStringExtra("salescode");
        fromdate=getIntent().getStringExtra("fromdate");
        todate=getIntent().getStringExtra("todate");
        customertype=getIntent().getStringExtra("usertypecus");
        name.setText(customertype);
        fromdateText.setText(fromdate);
        todatetext.setText(todate);
        CheckInternet();
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PopupMenu popupMenu = new PopupMenu(ViewOrderAllUsertype.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu pop) {
                        pop.dismiss();
                    }
                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.ch:
                                CheckInternetforpass();
                                return true;
                            case R.id.logout:
                                new logout(ViewOrderAllUsertype.this).log_outReg();
                                return true;
                        }
                        return false;
                    }
                });
                popupMenu.inflate(R.menu.pricolregion);
                Menu pop = popupMenu.getMenu();
                popupMenu.show();

            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new homevalues(ViewOrderAllUsertype.this).home_io();
            }
        });

    }

    private void CheckInternetforpass() {
        NetworkConnection network = new NetworkConnection(ViewOrderAllUsertype.this);
        if (network.CheckInternet()) {
            PasswordChangeAlertRegion alert = new PasswordChangeAlertRegion(ViewOrderAllUsertype.this);// new saleslogin().execute();
            alert.showLoginbox();
        } else {
            final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(ViewOrderAllUsertype.this).create();
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.loginalert, null);
            alertDialog.setView(dialogView);
            TextView login =(TextView)dialogView.findViewById(R.id.login);
            login.setText("Please Check Your internet Connection !");
            Button submit =(Button) dialogView.findViewById(R.id.submit);
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();

                }

            });
            alertDialog.show();
        }
    }

    private void CheckInternet() {
        NetworkConnection networkConnection=new NetworkConnection(ViewOrderAllUsertype.this);
        if(networkConnection.CheckInternet()){
            GetSalesAttendance();
        }else {
            final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(ViewOrderAllUsertype.this).create();
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.loginalert, null);
            alertDialog.setView(dialogView);
            TextView login =(TextView)dialogView.findViewById(R.id.login);
            login.setText("Please Check Your internet Connection !");
            Button submit =(Button) dialogView.findViewById(R.id.submit);
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();

                }

            });
            alertDialog.show();
        }
    }

    private void GetSalesAttendance() {
        try {

            final ProgressDialog loading = ProgressDialog.show(ViewOrderAllUsertype.this, getResources().getString(R.string.app_name), "Loading...", false, false);
            APIService service = RetrofitClient.getApiService();
            Call<DateWiseSeData> call = service.GetSalesdatewise(executeid,fromdate,todate,customertype);
            call.enqueue(new Callback<DateWiseSeData>() {
                @Override

                public void onResponse(Call<DateWiseSeData> call, Response<DateWiseSeData> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success")) {
                            data= response.body().getData().getOrder_list();
                            data1=response.body().getData().getTot();
                            orderPreviewAdapter1=new OrderPreviewAdapter1(ViewOrderAllUsertype.this,data);
                            listsalesattendance.setAdapter(orderPreviewAdapter1);
                            Amount.setText("₹"+String.valueOf(data1));

                        } else if(response.body().getResult().equals("NotSuccess")) {
                            final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(ViewOrderAllUsertype.this).create();
                            LayoutInflater inflater = getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.loginalert, null);
                            alertDialog.setView(dialogView);
                            TextView login =(TextView)dialogView.findViewById(R.id.login);
                            login.setText("No Order Found !");
                            Button submit =(Button) dialogView.findViewById(R.id.submit);
                            submit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    alertDialog.dismiss();

                                }

                            });
                            alertDialog.show();
                        }else
                        {
                            final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(ViewOrderAllUsertype.this).create();
                            LayoutInflater inflater = getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.loginalert, null);
                            alertDialog.setView(dialogView);
                            TextView login =(TextView)dialogView.findViewById(R.id.login);
                            login.setText("Something went wrong .Please try again later .");
                            Button submit =(Button) dialogView.findViewById(R.id.submit);
                            submit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    alertDialog.dismiss();

                                }

                            });
                            alertDialog.show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(ViewOrderAllUsertype.this).create();
                        LayoutInflater inflater = getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.loginalert, null);
                        alertDialog.setView(dialogView);
                        TextView login =(TextView)dialogView.findViewById(R.id.login);
                        login.setText("Something went wrong .Please try again later .");
                        Button submit =(Button) dialogView.findViewById(R.id.submit);
                        submit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                alertDialog.dismiss();

                            }

                        });
                        alertDialog.show();

                    }
                }

                @Override
                public void onFailure(Call<DateWiseSeData> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(ViewOrderAllUsertype.this).create();
                    LayoutInflater inflater = getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.loginalert, null);
                    alertDialog.setView(dialogView);
                    TextView login =(TextView)dialogView.findViewById(R.id.login);
                    login.setText("Something went wrong .Please try again later .");
                    Button submit =(Button) dialogView.findViewById(R.id.submit);
                    submit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();

                        }

                    });
                    alertDialog.show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }



    }
}
