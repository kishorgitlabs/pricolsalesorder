package com.brainmagic.pricolsalesorder;

import android.app.ProgressDialog;
import android.content.SharedPreferences;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

import Home.homevalues;
import Logout.logout;
import adapter.OrderRegionAdapterReg;
import alertbox.PrimaryOrder;
import api.models.datewisefilterregion.DateWiseFilterReData;
import api.models.datewisefilterregion.Order_list;
import api.retrofit.APIService;
import api.retrofit.RetrofitClient;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderWiseDateHistoryActivity extends AppCompatActivity {
    private TextView fromdateText,todatetext,amounttext;
    private ListView list;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private NetworkConnection networkConnection;
    private String fromdate,todate,codestring;
    private int regid;
    private List<Order_list>data;
    private Integer data1;
    private ImageView setting,home;
    private TextView code;
    private TextView tittle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_wise_date_history);
        myshare = getSharedPreferences("pricolsalesorder", MODE_PRIVATE);
        editor = myshare.edit();
        list=(ListView)findViewById(R.id.listorderregion);
        fromdateText=(TextView)findViewById(R.id.fromdateText);
        todatetext=(TextView)findViewById(R.id.todatetext);
        amounttext=(TextView)findViewById(R.id.amounttext);
        code=(TextView)findViewById(R.id.code);
        setting=(ImageView) findViewById(R.id.setting);
        tittle=(TextView) findViewById(R.id.title);
        fromdate=getIntent().getStringExtra("fromdate");
        todate=getIntent().getStringExtra("todate");
        codestring=getIntent().getStringExtra("salescode");
        regid=myshare.getInt("idlistreg",0);
        fromdateText.setText(fromdate);
        todatetext.setText(todate);
        tittle.setText("Order History");
        home = (ImageView) findViewById(R.id.home);
        code.setText(codestring);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new homevalues(OrderWiseDateHistoryActivity.this).home_io();
            }
        });
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PopupMenu popupMenu = new PopupMenu(OrderWiseDateHistoryActivity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu pop) {
                        pop.dismiss();
                    }
                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.primary:
                                PrimaryOrder alert = new PrimaryOrder(OrderWiseDateHistoryActivity.this);// new saleslogin().execute();
                                alert.showLoginboxtoggle();
                                return true;
                            case R.id.ch:
                                CheckInternetforpass();
                                return true;
                            case R.id.logout:
                                new logout(OrderWiseDateHistoryActivity.this).log_outReg();
                                return true;
                        }
                        return false;
                    }
                });
                popupMenu.inflate(R.menu.pricolcms);
                Menu pop = popupMenu.getMenu();
                popupMenu.show();

            }
        });
        checkInternet();

    }

    private void CheckInternetforpass() {
        NetworkConnection network = new NetworkConnection(OrderWiseDateHistoryActivity.this);
        if (network.CheckInternet()) {
            PasswordChangeAlertRegion alert = new PasswordChangeAlertRegion(OrderWiseDateHistoryActivity.this);// new saleslogin().execute();
            alert.showLoginbox();
        } else {
            final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(OrderWiseDateHistoryActivity.this).create();
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.loginalert, null);
            alertDialog.setView(dialogView);
            TextView login =(TextView)dialogView.findViewById(R.id.login);
            login.setText("Please Check Your internet Connection !");
            Button submit =(Button) dialogView.findViewById(R.id.submit);
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();

                }

            });
            alertDialog.show();
        }
    }

    private void checkInternet() {
        NetworkConnection networkConnection=new NetworkConnection(OrderWiseDateHistoryActivity.this);
        if(networkConnection.CheckInternet()){
            GetDateWiseDate();
        }else {
            final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(OrderWiseDateHistoryActivity.this).create();
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.loginalert, null);
            alertDialog.setView(dialogView);
            TextView login =(TextView)dialogView.findViewById(R.id.login);
            login.setText("Please Check Your internet Connection !");
            Button submit =(Button) dialogView.findViewById(R.id.submit);
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();

                }

            });
            alertDialog.show();
        }
    }

    private void GetDateWiseDate() {
        try {
//            if(ordertype.equals("Primary Order")){
            final ProgressDialog loading = ProgressDialog.show(OrderWiseDateHistoryActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);
            APIService service = RetrofitClient.getApiService();
            Call<DateWiseFilterReData> call = service.GetSalesdatewisere(regid,codestring,"",fromdate,todate);
            call.enqueue(new Callback<DateWiseFilterReData>() {
                @Override

                public void onResponse(Call<DateWiseFilterReData> call, Response<DateWiseFilterReData> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success")) {
                            data=response.body().getData().getOrder_list();
                            data1=response.body().getData().getTot();
                            OrderRegionAdapterReg orderRegionAdapterReg=new OrderRegionAdapterReg(OrderWiseDateHistoryActivity.this,data);
                            list.setAdapter(orderRegionAdapterReg);
                            amounttext.setText("₹"+String.valueOf(data1));


                        } else if(response.body().getResult().equals("NotSuccess")) {
                            final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(OrderWiseDateHistoryActivity.this).create();
                            LayoutInflater inflater = getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.loginalert, null);
                            alertDialog.setView(dialogView);
                            TextView login =(TextView)dialogView.findViewById(R.id.login);
                            login.setText("No Order Found !");
                            Button submit =(Button) dialogView.findViewById(R.id.submit);
                            submit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    alertDialog.dismiss();

                                }

                            });
                            alertDialog.show();
                        }else
                        {
                            final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(OrderWiseDateHistoryActivity.this).create();
                            LayoutInflater inflater = getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.loginalert, null);
                            alertDialog.setView(dialogView);
                            TextView login =(TextView)dialogView.findViewById(R.id.login);
                            login.setText("Something went wrong .Please try again later .");
                            Button submit =(Button) dialogView.findViewById(R.id.submit);
                            submit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    alertDialog.dismiss();

                                }

                            });
                            alertDialog.show();
//                                box.showAlertbox(getResources().getString(R.string.server_error));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(OrderWiseDateHistoryActivity.this).create();
                        LayoutInflater inflater = getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.loginalert, null);
                        alertDialog.setView(dialogView);
                        TextView login =(TextView)dialogView.findViewById(R.id.login);
                        login.setText("Something went wrong .Please try again later .");
                        Button submit =(Button) dialogView.findViewById(R.id.submit);
                        submit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                alertDialog.dismiss();

                            }

                        });
                        alertDialog.show();

                    }
                }

                @Override
                public void onFailure(Call<DateWiseFilterReData> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(OrderWiseDateHistoryActivity.this).create();
                    LayoutInflater inflater = getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.loginalert, null);
                    alertDialog.setView(dialogView);
                    TextView login =(TextView)dialogView.findViewById(R.id.login);
                    login.setText("Something went wrong .Please try again later .");
                    Button submit =(Button) dialogView.findViewById(R.id.submit);
                    submit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();

                        }

                    });
                    alertDialog.show();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
