package com.brainmagic.pricolsalesorder;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import Home.homevalues;
import Logout.logout;
import adapter.ViewAttendanceAdapter;
import alertbox.Alertbox;
import alertbox.PasswordChangeAlert;
import alertbox.PrimaryOrder;
import network.NetworkConnection;
import persistance.ServerConnection;

public class ViewAttendanceRegion extends AppCompatActivity implements com.borax12.materialdaterangepicker.date.DatePickerDialog.OnDateSetListener{
    private ListView listattendance;
    private ImageView menu, back,home,logout,setting;
    private Alertbox box = new Alertbox(this);
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private Connection connection;
    private Statement statement;
    private ResultSet resultSet;
    private Button filter;
    private TextView title;

    private boolean mAutoHighlight;
    private int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_attendance_region);
        myshare = getSharedPreferences("pricolsalesorder", MODE_PRIVATE);
        editor = myshare.edit();
        listattendance = (ListView) findViewById(R.id.listattendance);
        filter = (Button) findViewById(R.id.filter);
        menu = (ImageView) findViewById(R.id.menu);
        back = (ImageView) findViewById(R.id.back);
        home = (ImageView) findViewById(R.id.home);
        logout = (ImageView) findViewById(R.id.logout);
        setting = (ImageView) findViewById(R.id.setting);
        title=(TextView)findViewById(R.id.title);
        title.setText("View Attendance");
        id = myshare.getInt("idlistreg", 0);
//        back.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                onBackPressed();
//            }
//        });

        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PopupMenu popupMenu = new PopupMenu(ViewAttendanceRegion.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu pop) {
                        pop.dismiss();
                    }
                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.primary:
                                PrimaryOrder alert = new PrimaryOrder(ViewAttendanceRegion.this);// new saleslogin().execute();
                                alert.showLoginboxtoggle();
                                return true;
                            case R.id.ch:
                                CheckInternetforpass();
                                return true;
                            case R.id.logout:
                                new logout(ViewAttendanceRegion.this).log_outReg();
                                return true;
                        }
                        return false;
                    }
                });
                popupMenu.inflate(R.menu.pricolcms);
                Menu pop = popupMenu.getMenu();
                popupMenu.show();

            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new homevalues(ViewAttendanceRegion.this).home_io();
            }
        });
//        logout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                new logout(ViewAttendanceRegion.this).log_outReg();
//            }
//        });
        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                com.borax12.materialdaterangepicker.date.DatePickerDialog dpd = com.borax12.materialdaterangepicker.date.DatePickerDialog.newInstance(
                        (com.borax12.materialdaterangepicker.date.DatePickerDialog.OnDateSetListener) ViewAttendanceRegion.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.setAutoHighlight(mAutoHighlight);
                dpd.show(getFragmentManager(), "Datepickerdialog");
            }
        });


        checkInternet();
    }

    private void CheckInternetforpass() {
        NetworkConnection network = new NetworkConnection(ViewAttendanceRegion.this);
        if (network.CheckInternet()) {
            PasswordChangeAlert alert = new PasswordChangeAlert(ViewAttendanceRegion.this);// new saleslogin().execute();
            alert.showLoginbox();
        } else {
            final AlertDialog alertDialog = new AlertDialog.Builder(ViewAttendanceRegion.this).create();
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.loginalert, null);
            alertDialog.setView(dialogView);
            TextView login =(TextView)dialogView.findViewById(R.id.login);
            login.setText("Please Check your Internet Connection !");
            Button submit =(Button) dialogView.findViewById(R.id.submit);
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();

                }

            });
            alertDialog.show();
        }

    }


    private void checkInternet() {
        NetworkConnection networkConnection = new NetworkConnection(ViewAttendanceRegion.this);
        if (networkConnection.CheckInternet()) {
            new CheckAttendance().execute("Select * from Regional_AttendanceTable where EmpId = '"+ id +"' order by Date desc");
        } else {
            final AlertDialog alertDialog = new AlertDialog.Builder(ViewAttendanceRegion.this).create();
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.loginalert, null);
            alertDialog.setView(dialogView);
            TextView login =(TextView)dialogView.findViewById(R.id.login);
            login.setText("Please Check your Internet Connection !!");
            Button submit =(Button) dialogView.findViewById(R.id.submit);
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();

                }

            });
            alertDialog.show();
        }
    }



    @Override
    public void onDateSet(com.borax12.materialdaterangepicker.date.DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
        String date = "You picked the following date: From- " + dayOfMonth + "/" + (monthOfYear + 1) + "/" + year + " To " + dayOfMonthEnd + "/" + (monthOfYearEnd+1) + "/" + yearEnd;
        String FromDate = "";
        String Todate = "";

        Toast.makeText(ViewAttendanceRegion.this, date, Toast.LENGTH_LONG).show();

        if (monthOfYear < 10)
            FromDate = year + "-0" + (monthOfYear + 1) + "-" + dayOfMonth;

        if (monthOfYearEnd < 10)
            Todate = yearEnd + "-0" + (monthOfYearEnd + 1) + "-" + dayOfMonthEnd;

//        new GetTravelHistory().execute("select datetime from Travelcoordinates where S_id = '" + salesID + "' and convert(nvarchar,datetime) between '" + FromDate + "' and '" + Todate + "' and IsCancelled ='0' order by datetime desc");
        new CheckAttendance().execute("select id,date,Name,InTime,OutTime,Distance,AttendDay,Designation from Regional_AttendanceTable where EmpId = '" + id + "' and date between '" + FromDate + "' and '" + Todate + "' order by date desc");
    }


    private class CheckAttendance extends AsyncTask<String, Void, String> {
        private ProgressDialog progressDialog;
        private SQLiteDatabase db;
        private List<String> namelist, DateList, Intimelist, outtimelist,daylist,Distancelist,Designationlist;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(ViewAttendanceRegion.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.show();
            namelist = new ArrayList<>();
            DateList = new ArrayList<>();
            Intimelist = new ArrayList<>();
            outtimelist = new ArrayList<>();
            daylist = new ArrayList<>();
            Distancelist = new ArrayList<>();
            Designationlist = new ArrayList<>();


//            descriptiondy = (EditText) newView.findViewById(R.id.descriptiondynamic);
//            quantitydy = (EditText) newView.findViewById(R.id.quantitydynamic);
//            productdy = (MaterialSpinner) newView.findViewById(R.id.productdynname);


        }

        @Override
        protected String doInBackground(String... strings) {
            ServerConnection serverConnection = new ServerConnection();
            try {
                connection = serverConnection.getConnection();
                /*statement = connection.prepareStatement(selectquery);*/
                statement = connection.createStatement();
                String selectquery = strings[0];
                Log.v("query", selectquery);
                resultSet = statement.executeQuery(selectquery);
                while (resultSet.next()) {
                    namelist.add(resultSet.getString("Name"));
                    DateList.add(resultSet.getString("Date"));
                    Intimelist.add(resultSet.getString("InTime"));
                    outtimelist.add(resultSet.getString("OutTime"));
                    daylist.add(resultSet.getString("AttendDay"));
                    Distancelist.add(resultSet.getString("Distance"));
                    Designationlist.add(resultSet.getString("Designation"));

                }
                if (!namelist.isEmpty()) {
                    statement.close();
                    connection.close();
                    resultSet.close();
                    return "success";
                } else {
                    statement.close();
                    connection.close();
                    resultSet.close();
                    return "empty";
                }

            } catch (Exception e) {
                e.printStackTrace();
                return "error_insertion";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();

            if (s.equals("success")) {
                ViewAttendanceAdapter viewAttendanceAdapter=new ViewAttendanceAdapter(ViewAttendanceRegion.this,namelist,DateList,Intimelist,outtimelist,daylist,Distancelist,Designationlist);
                listattendance.setAdapter(viewAttendanceAdapter);

            } else if (s.equals("empty")) {
                final AlertDialog alertDialog = new AlertDialog.Builder(ViewAttendanceRegion.this).create();
                LayoutInflater inflater = getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.loginalert, null);
                alertDialog.setView(dialogView);
                TextView login =(TextView)dialogView.findViewById(R.id.login);
                login.setText("No Attendance Found!");
                Button submit =(Button) dialogView.findViewById(R.id.submit);
                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        alertDialog.dismiss();
                        finish();

                    }

                });
                alertDialog.show();
            } else {
                final AlertDialog alertDialog = new AlertDialog.Builder(ViewAttendanceRegion.this).create();
                LayoutInflater inflater = getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.loginalert, null);
                alertDialog.setView(dialogView);
                TextView login =(TextView)dialogView.findViewById(R.id.login);
                login.setText("Please Check your Internet Connection!");
                Button submit =(Button) dialogView.findViewById(R.id.submit);
                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                        alertDialog.dismiss();

                    }

                });
                alertDialog.show();
            }
        }
    }
    }

