package com.brainmagic.pricolsalesorder;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.AsyncTask;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.muddzdev.styleabletoastlibrary.StyleableToast;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.TransformerUtils;

import java.util.List;

import Home.homevalues;
import Logout.logout;
import SQL_Fields.SQLITE;
import alertbox.Alertbox;
import api.models.master.Citylist;
import api.models.master.Statelist;
import database.DBHelper;
import roomdb.database.AppDatabase;

import static roomdb.database.AppDatabase.getAppDatabase;

public class CreateRegionActivity extends AppCompatActivity {
    private EditText name_edit,address_edit,mobilenumber,email_edit;
    private MaterialSpinner state_spinner,city_edit;
    private TextView region;
    private Button create;
    Alertbox box = new Alertbox(CreateRegionActivity.this);
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private AppDatabase db1;
    private List<String> statelist,ciylist,regionlist;
    private String states,city,regions,retailername,address,mobile,email;
    private ProgressDialog progressDialog;
    private ImageView back,home,logout;
    private TextView title;
    private int Salesid;
    private String regionstr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_region);
        myshare = getSharedPreferences("pricolsalesorder", MODE_PRIVATE);
        editor = myshare.edit();
        db1=getAppDatabase(CreateRegionActivity.this);
        regionstr=myshare.getString("regName","");
        name_edit=(EditText)findViewById(R.id.name_edit);
        address_edit=(EditText)findViewById(R.id.address_edit);
        mobilenumber=(EditText)findViewById(R.id.mobilenumber1);
        email_edit=(EditText)findViewById(R.id.email_edit);
        state_spinner=(MaterialSpinner) findViewById(R.id.state_spinner);
        city_edit=(MaterialSpinner) findViewById(R.id.city_edit);
        region=(TextView) findViewById(R.id.region);
        create=(Button) findViewById(R.id.create);
        home = (ImageView) findViewById(R.id.home);
        logout = (ImageView) findViewById(R.id.logout);
        back=(ImageView)findViewById(R.id.back) ;
        title=(TextView)findViewById(R.id.title);
        title.setText("Create Region");
        Salesid=myshare.getInt("idlist",0);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new homevalues(CreateRegionActivity.this).home_io();
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new logout(CreateRegionActivity.this).log_out();
            }
        });
       region.setText(regionstr);


        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 if(name_edit.getText().toString().equals("")){
                     StyleableToast st = new StyleableToast(CreateRegionActivity.this, "Enter name", Toast.LENGTH_SHORT);
                     st.setBackgroundColor(CreateRegionActivity.this.getResources().getColor(R.color.colorPrimary));
                     st.setTextColor(Color.WHITE);
                     st.setMaxAlpha();
                     st.show();

                 }else if(state_spinner.getText().toString().equals("Select State")){
                     StyleableToast st = new StyleableToast(CreateRegionActivity.this, "Select State", Toast.LENGTH_SHORT);
                     st.setBackgroundColor(CreateRegionActivity.this.getResources().getColor(R.color.colorPrimary));
                     st.setTextColor(Color.WHITE);
                     st.setMaxAlpha();
                     st.show();

                 }else if(city_edit.getText().toString().equals("Select City")) {
                     StyleableToast st = new StyleableToast(CreateRegionActivity.this, "Select City", Toast.LENGTH_SHORT);
                     st.setBackgroundColor(CreateRegionActivity.this.getResources().getColor(R.color.colorPrimary));
                     st.setTextColor(Color.WHITE);
                     st.setMaxAlpha();
                     st.show();

                 }else if(address_edit.getText().toString().equals("")){
                     StyleableToast st = new StyleableToast(CreateRegionActivity.this, "Enter Address", Toast.LENGTH_SHORT);
                     st.setBackgroundColor(CreateRegionActivity.this.getResources().getColor(R.color.colorPrimary));
                     st.setTextColor(Color.WHITE);
                     st.setMaxAlpha();
                     st.show();

                 }else if(mobilenumber.getText().toString().equals("")){
                     StyleableToast st = new StyleableToast(CreateRegionActivity.this, "Enter Mobile Number", Toast.LENGTH_SHORT);
                     st.setBackgroundColor(CreateRegionActivity.this.getResources().getColor(R.color.colorPrimary));
                     st.setTextColor(Color.WHITE);
                     st.setMaxAlpha();
                     st.show();

                 }else {
                      new GetRetailes().execute();

                 }
            }
        });

        GetStateLsit(db1.adhereDao().GetStatelist());
        //GetCityList(db1.adhereDao().GetCitylist());
        state_spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                states=item.toString();
                GetCItystrings(db1.adhereDao().GetCitystring(states));
                
            }
        });
        city_edit.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                city=item.toString();
            }
        });

    }

    private void GetCItystrings(List<Citylist> citylists) {
        List<Object>cities=(List<Object>) CollectionUtils.collect(citylists, TransformerUtils.invokerTransformer("getTown1"));
        cities.add(0,"Select City");
        city_edit.setItems(cities);
    }


    private void GetCityList(List<Citylist> citylists) {
        List<Object>cities=(List<Object>) CollectionUtils.collect(citylists, TransformerUtils.invokerTransformer("getTown1"));
        cities.add(0,"Select City");
        city_edit.setItems(cities);
    }

    private void GetStateLsit(List<Statelist> statelists) {
        List<Object>states=(List<Object>) CollectionUtils.collect(statelists, TransformerUtils.invokerTransformer("getState1"));
        states.add(0,"Select State");
        state_spinner.setItems(states);
        
    }

    private class GetRetailes extends AsyncTask<String, Void, String> {


        ContentValues values = new ContentValues();
        private long success;
        SQLiteDatabase db;
        private int a;

        @SuppressLint("InflateParams")
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(CreateRegionActivity.this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Saving local...");
            progressDialog.show();
            retailername=name_edit.getText().toString();
            address=address_edit.getText().toString();
            mobile=mobilenumber.getText().toString();
            email=email_edit.getText().toString();
        }

        @Override
        protected String doInBackground(String... args0) {

            try {
                DBHelper dbHelper = new DBHelper(CreateRegionActivity.this);
                db = dbHelper.getWritableDatabase();
               // db.execSQL("DROP TABLE  IF EXISTS "+ SQLITE.TABLE_Create_Profile);
                values.put(SQLITE.COLUMN_Create_retsalesid,Salesid);
                values.put(SQLITE.COLUMN_Create_retname, retailername);
                values.put(SQLITE.COLUMN_Create_retstate, states);
                values.put(SQLITE.COLUMN_Create_retcity, city);
                values.put(SQLITE.COLUMN_Create_retaddress, address);
                values.put(SQLITE.COLUMN_Create_retmobile, mobile);
                values.put(SQLITE.COLUMN_Create_retregion, regionstr);


                dbHelper.CreateRetailerProfile(db);
                success = db.insert(SQLITE.TABLE_Create_Profile, null, values);
                if(success!=-1)
                {
                    db.close();
                    return "received";
                }
                db.close();
                return "not Success";

            } catch (Exception e) {
                Log.v("Error in Add local db ", "catch is working");
                Log.v("Error in Add local db", e.getMessage());
                return "notsuccess";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            if (result.equals("received")) {
                Log.v("success", Double.toString(success));
              startActivity(new Intent(CreateRegionActivity.this, SalesOrderExistingActivity.class).putExtra("retailername",retailername));

                Toast.makeText(getApplicationContext(), "Profile Created for " + retailername, Toast.LENGTH_LONG).show();

            } else {
                box.showAlertbox("Error in adding to local");
            }
        }
    }
}
