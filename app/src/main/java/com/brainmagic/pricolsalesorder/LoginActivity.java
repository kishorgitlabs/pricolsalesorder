package com.brainmagic.pricolsalesorder;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.jaredrummler.materialspinner.MaterialSpinner;

import java.util.ArrayList;
import java.util.List;

import alertbox.Alertbox;
import api.models.LoginData;
import api.models.User_exist;
import api.models.User_exist1;
import api.models.forgotpassword.ForgotPasswordData;
import api.models.login;
import api.retrofit.APIService;
import api.retrofit.RetrofitClient;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    private EditText edlogname, edpassword;
    private Button submits;
    private TextView txforgotpassword;
    Alertbox box = new Alertbox(LoginActivity.this);
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private AlertDialog Alertdialog;
    private MaterialSpinner usertype;
    private TextView email;
    private List<String> usertypelist;
    private String Designation,getUserName,getPassword;
    private AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        myshare = getSharedPreferences("pricolsalesorder", MODE_PRIVATE);
        editor = myshare.edit();
        edlogname = (EditText) findViewById(R.id.edlogname);
        edpassword = (EditText) findViewById(R.id.edpassword);
        submits= (Button) findViewById(R.id.logbtn);
        txforgotpassword = (TextView) findViewById(R.id.txforgotpassword);


        submits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edlogname.getText().toString().equals("")) {
                    edlogname.setError("Enter Employee Code");
//                    StyleableToast st = new StyleableToast(LoginActivity.this, "Enter username", Toast.LENGTH_SHORT);
//                    st.setBackgroundColor(LoginActivity.this.getResources().getColor(R.color.colorPrimary));
//                    st.setTextColor(Color.WHITE);
//                    st.setMaxAlpha();
//                    st.show();

                } else if (edpassword.getText().toString().equals("")) {
                    edpassword.setError("Enter Password");
//                    StyleableToast st = new StyleableToast(LoginActivity.this, "Enter Password", Toast.LENGTH_SHORT);
//                    st.setBackgroundColor(LoginActivity.this.getResources().getColor(R.color.colorPrimary));
//                    st.setTextColor(Color.WHITE);
//                    st.setMaxAlpha();
//                    st.show();
                } else {
                    checkInternet();


                }
            }
        });

        txforgotpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GetLoginData();
            }
        });
    }

    private void GetLoginData() {
        alertDialog = new AlertDialog.Builder(
                LoginActivity.this).create();

        LayoutInflater inflater = ((Activity) LoginActivity.this).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.forhotpasswordalert, null);
        alertDialog.setView(dialogView);

        email = (TextView) dialogView.findViewById(R.id.email);
        usertype = (MaterialSpinner) dialogView.findViewById(R.id.usertype);
        usertypelist = new ArrayList<>();
        usertypelist.add(0, "Select Designation");
        usertypelist.add("SalesExecutive");
        usertypelist.add("RegionalManager");
        usertype.setItems(usertypelist);
        usertype.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                Designation = item.toString();
            }
        });
        Button submt = (Button) dialogView.findViewById(R.id.submt);
        submt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (email.getText().toString().equals("")) {
                    email.setError("Enter Email Id");
//                    StyleableToast st = new StyleableToast(LoginActivity.this, "Enter Email", Toast.LENGTH_SHORT);
//                    st.setBackgroundColor(LoginActivity.this.getResources().getColor(R.color.colorPrimary));
//                    st.setTextColor(Color.WHITE);
//                    st.setMaxAlpha();
//                    st.show();
                } else if(usertype.getText().toString().equals("Select Designation")) {
                    usertype.setError("Select Designation");

                }else {
                    GetPasword();
                }

            }
        });
        alertDialog.show();


    }

    private void GetPasword() {
        try {

            final ProgressDialog loading = ProgressDialog.show(LoginActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);
            APIService service = RetrofitClient.getApiService();

            Call<ForgotPasswordData> call = service.forgot(email.getText().toString(), Designation);
            call.enqueue(new Callback<ForgotPasswordData>() {
                @Override

                public void onResponse(Call<ForgotPasswordData> call, Response<ForgotPasswordData> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success")) {
                            alertDialog.dismiss();
                            final AlertDialog alertDialog = new AlertDialog.Builder(LoginActivity.this).create();
                            LayoutInflater inflater = getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.loginalert, null);
                            alertDialog.setView(dialogView);
                            TextView login =(TextView)dialogView.findViewById(R.id.login);
                            login.setText("Password  Send To Your Email Id!");
                            Button submi =(Button) dialogView.findViewById(R.id.submit);
                            submi.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    alertDialog.dismiss();

                                }

                            });
                            alertDialog.show();
                           // box.showAlertbox("Password Send To Your Emailid");


                        } else if (response.body().getResult().equals("NotSuccess")) {
                            alertDialog.dismiss();
                            final AlertDialog alertDialog = new AlertDialog.Builder(LoginActivity.this).create();
                            LayoutInflater inflater = getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.loginalert, null);
                            alertDialog.setView(dialogView);
                            TextView login =(TextView)dialogView.findViewById(R.id.login);
                            login.setText("The given Email Id does not match with designation!");
                            Button submit =(Button) dialogView.findViewById(R.id.submit);
                            submit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    alertDialog.dismiss();

                                }

                            });
                            alertDialog.show();

                         //   box.showAlertbox("Password Not  Send To Your Emailid");
                        } else {
                            alertDialog.dismiss();
                            final AlertDialog alertDialog = new AlertDialog.Builder(LoginActivity.this).create();
                            LayoutInflater inflater = getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.loginalert, null);
                            alertDialog.setView(dialogView);
                            TextView login =(TextView)dialogView.findViewById(R.id.login);
                            login.setText("Something went wrong .Please try again later .!!");
                            Button submit =(Button) dialogView.findViewById(R.id.submit);
                            submit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    alertDialog.dismiss();

                                }

                            });
                            alertDialog.show();
                          //  box.showAlertbox(getResources().getString(R.string.server_error));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        alertDialog.dismiss();
                        final AlertDialog alertDialog = new AlertDialog.Builder(LoginActivity.this).create();
                        LayoutInflater inflater = getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.loginalert, null);
                        alertDialog.setView(dialogView);
                        TextView login =(TextView)dialogView.findViewById(R.id.login);
                        login.setText("Something went wrong .Please try again later .!!");
                        Button submit =(Button) dialogView.findViewById(R.id.submit);
                        submit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                alertDialog.dismiss();

                            }

                        });
                        alertDialog.show();

                    }
                }

                @Override
                public void onFailure(Call<ForgotPasswordData> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    final AlertDialog alertDialog = new AlertDialog.Builder(LoginActivity.this).create();
                    LayoutInflater inflater = getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.loginalert, null);
                    alertDialog.setView(dialogView);
                    TextView login =(TextView)dialogView.findViewById(R.id.login);
                    login.setText("Something went wrong .Please try again later .!!");
                    Button submit =(Button) dialogView.findViewById(R.id.submit);
                    submit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();

                        }

                    });
                    alertDialog.show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private void checkInternet() {
        NetworkConnection networkConnection = new NetworkConnection(LoginActivity.this);
        if (networkConnection.CheckInternet()) {
            GetLogin();
        } else {
            final AlertDialog alertDialog = new AlertDialog.Builder(LoginActivity.this).create();
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.loginalert, null);
            alertDialog.setView(dialogView);
            TextView login =(TextView)dialogView.findViewById(R.id.login);
            login.setText("Please Check Your internet Connection!");
            Button submit =(Button) dialogView.findViewById(R.id.submit);
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();

                }

            });
            alertDialog.show();
        }
    }

    private void GetLogin() {

        try {
            getUserName=edlogname.getText().toString();
            getPassword=edpassword.getText().toString();
            final ProgressDialog loading = ProgressDialog.show(LoginActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);
            APIService service = RetrofitClient.getApiService();

            Call<LoginData> call = service.Login(getUserName, getPassword);
            call.enqueue(new Callback<LoginData>() {
                @Override

                public void onResponse(Call<LoginData> call, Response<LoginData> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success")) {
                            if (response.body().getData().getUser_exist() == null) {
                                SuccessLogin1((User_exist1) response.body().getData().getUser_exist1());

                            } else {
                                SuccessLogin((User_exist) response.body().getData().getUser_exist());
                            }


                        } else if (response.body().getResult().equals("InvalidUser!")) {
                            final AlertDialog alertDialog = new AlertDialog.Builder(LoginActivity.this).create();
                            LayoutInflater inflater = getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.loginalert, null);
                            alertDialog.setView(dialogView);
                            TextView login =(TextView)dialogView.findViewById(R.id.login);
                            Button submit =(Button) dialogView.findViewById(R.id.submit);
                            submit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                 alertDialog.dismiss();
                                }
                            });
                            login.setText("Invalid User!");
                            alertDialog.show();
                        } else {
                            final AlertDialog alertDialog = new AlertDialog.Builder(LoginActivity.this).create();
                            LayoutInflater inflater = getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.loginalert, null);
                            alertDialog.setView(dialogView);
                            TextView login =(TextView)dialogView.findViewById(R.id.login);
                            login.setText("Please check your Employee Code or Password!");
                            Button submit =(Button) dialogView.findViewById(R.id.submit);
                            submit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                  alertDialog.dismiss();
                                }
                            });

                            alertDialog.show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        final AlertDialog alertDialog = new AlertDialog.Builder(LoginActivity.this).create();
                        LayoutInflater inflater = getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.loginalert, null);
                        alertDialog.setView(dialogView);
                        TextView login =(TextView)dialogView.findViewById(R.id.login);
                        login.setText(" Slow!");
                        Button subm =(Button) dialogView.findViewById(R.id.submit);
                        subm.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                               alertDialog.dismiss();
                            }
                        });
                        alertDialog.show();

                    }
                }

                @Override
                public void onFailure(Call<LoginData> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    final AlertDialog alertDialog = new AlertDialog.Builder(LoginActivity.this).create();
                    LayoutInflater inflater = getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.loginalert, null);
                    alertDialog.setView(dialogView);
                    TextView login =(TextView)dialogView.findViewById(R.id.login);
                    login.setText("Something went wrong .Please try again later .!");
                    Button subm =(Button) dialogView.findViewById(R.id.submit);
                    subm.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();
                        }
                    });
                    alertDialog.show();

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void SuccessLogin1(final User_exist1 data) {
        final AlertDialog alertDialog = new AlertDialog.Builder(LoginActivity.this).create();
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.loginalert, null);
        alertDialog.setView(dialogView);
        TextView login =(TextView)dialogView.findViewById(R.id.login);
        login.setText("Login Successfully!");
        Button submit =(Button) dialogView.findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editor.putBoolean("isLoginreg", true);
                editor.putString("username", data.getUserName());
                editor.putString("password", data.getPassword());
                editor.putString("emailidreg", data.getEmailId());
                editor.putString("mobilereg", data.getMobileNumber());
                editor.putInt("idlistreg", data.getR_Id());
                editor.putString("regcode",data.getManagercode());
                editor.putString("namereg",data.getManagername());
                editor.putString("empcodereg",data.getManagercode());
                editor.putString("userType",data.getEmployeetype());
                editor.putBoolean("isLogin", false);
                editor.commit();
                Intent i = new Intent(LoginActivity.this, RegionHomeActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);

            }

        });
        alertDialog.show();


//
//final AlertDialog dialog = new AlertDialog.Builder(LoginActivity.this).create();
//        dialog.setTitle(R.string.app_name);
//        dialog.setMessage("Login Successfully");
//        dialog.setButton("Okay", new DialogInterface.OnClickListener() {
//
//            public void onClick(DialogInterface dialog, int which) {
//                editor.putBoolean("isLoginreg", true);
//                editor.putString("username", data.getUserName());
//                editor.putString("password", data.getPassword());
//                editor.putString("emailidreg", data.getEmailId());
//
//                editor.putString("mobilereg", data.getMobileNumber());
//                editor.putInt("idlistreg", data.getR_Id());
//                editor.putString("namereg",data.getManagername());
//                editor.putString("empcodereg",data.getManagercode());
//                editor.putBoolean("isLogin", false);
//
//
//                editor.commit();
//                Intent i = new Intent(LoginActivity.this, RegionHomeActivity.class);
//                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(i);
//            }
//        });
//        dialog.show();

    }


    private void SuccessLogin(final User_exist data) {
        final AlertDialog alertDialog = new AlertDialog.Builder(LoginActivity.this).create();
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.loginalert, null);
        alertDialog.setView(dialogView);
        TextView login =(TextView)dialogView.findViewById(R.id.login);
        login.setText("Login Successfully!");
        Button submit =(Button) dialogView.findViewById(R.id.submit);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editor.putBoolean("isLogin", true);
                editor.putString("username", data.getUserName());
                editor.putString("password", data.getPassword());
                editor.putString("emailid", data.getEmailId());
                editor.putString("mobile", data.getMobileNo());
                editor.putInt("idlist", data.getId());
                editor.putString("name",data.getName());
                editor.putString("usertype",data.getUserType());
                editor.putString("empcode",data.getCustomerCode());
                editor.putString("regid",data.getRegid());
                editor.putString("regName",data.getRegion());
                editor.putBoolean("isLoginreg", false);
                editor.putString("userType", "Sales");
                editor.commit();
                Intent i = new Intent(LoginActivity.this, HomeActivty.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            }
        });
        alertDialog.show();





//        final AlertDialog dialog = new AlertDialog.Builder(LoginActivity.this).create();
//        dialog.setTitle(R.string.app_name);
//        dialog.setMessage("Login Successfully");
//        dialog.setButton("Okay", new DialogInterface.OnClickListener() {
//
//            public void onClick(DialogInterface dialog, int which) {
//                editor.putBoolean("isLogin", true);
//                editor.putString("username", data.getUserName());
//                editor.putString("password", data.getPassword());
//                editor.putString("emailid", data.getEmailId());
//                editor.putString("mobile", data.getMobileNo());
//                editor.putInt("idlist", data.getId());
//                editor.putString("name",data.getName());
//                editor.putString("usertype",data.getUserType());
//                editor.putString("empcode",data.getCustomerCode());
//                editor.putString("regid",data.getRegid());
//                editor.putString("regName",data.getRegion());
//                editor.putBoolean("isLoginreg", false);
//
//                editor.commit();
//                Intent i = new Intent(LoginActivity.this, HomeActivty.class);
//                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(i);
//            }
//        });
////        dialog.show();
    }
}
