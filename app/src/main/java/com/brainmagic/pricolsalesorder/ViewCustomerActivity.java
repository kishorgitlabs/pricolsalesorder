package com.brainmagic.pricolsalesorder;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import java.util.ArrayList;
import java.util.List;

import Logout.logout;
import adapter.CuslistAdapter2;
import alertbox.Alertbox;
import alertbox.PasswordChangeAlert;
import alertbox.PrimaryOrder;
import api.models.deal;
import network.NetworkConnection;
import roomdb.database.AppDatabase;

import static roomdb.database.AppDatabase.getAppDatabase;

public class ViewCustomerActivity extends AppCompatActivity {
    private static final int PERMISSION_REQUEST = 100;
    private List<String> Namelist,CuscodeList,BranchcodeList,EmailList,MobileList,CusNamelist,CustomercodeList;
    ImageView back,home,logout;
    ListView gridview;
    private TextView title;
    HorizontalScrollView horizontalScrollView;

    Spinner Customer_spinner;
    Button Search;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    String SalesName,salesID,Customername,Code;
    Integer Position;
    NetworkConnection network = new NetworkConnection(ViewCustomerActivity.this);
    Alertbox alert = new Alertbox(ViewCustomerActivity.this);
    private AppDatabase db;
    private ImageView setting;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_customer);
        myshare = getSharedPreferences("pricolsalesorder", ViewCustomerActivity.this.MODE_PRIVATE);
        editor = myshare.edit();
        salesID = myshare.getString("salesID", "");
        SalesName = myshare.getString("name", "");
        title=(TextView)findViewById(R.id.title);
        gridview = (ListView) findViewById(R.id.customers_listview);
        Customer_spinner = (Spinner)findViewById(R.id.customer_spinner);
        Search = (Button)findViewById(R.id.search);
        // initial views
        home = (ImageView) findViewById(R.id.home);
        setting = (ImageView) findViewById(R.id.setting);

        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PopupMenu popupMenu = new PopupMenu(ViewCustomerActivity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu pop) {
                        pop.dismiss();
                    }
                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.primary:
                                PrimaryOrder alert = new PrimaryOrder(ViewCustomerActivity.this);// new saleslogin().execute();
                                alert.showLoginboxtoggle();
                                return true;
                            case R.id.ch:
                                CheckInternetforpass();
                                return true;
                            case R.id.logout:
                                new logout(ViewCustomerActivity.this).log_out();
                                return true;
                        }
                        return false;
                    }
                });
                popupMenu.inflate(R.menu.pricolcms);
                Menu pop = popupMenu.getMenu();
                popupMenu.show();

            }
        });
      //  back = (ImageView) findViewById(R.id.back);
      //  logout = (ImageView) findViewById(R.id.logout);
        title.setText("View Customer");
        db = getAppDatabase(ViewCustomerActivity.this);
//        logout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                new logout(ViewCustomerActivity.this).log_out();
//            }
//        });
//        back.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                onBackPressed();
//            }
//        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent a = new Intent(ViewCustomerActivity.this, HomeActivty.class);
                a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(a);

            }
        });
        //checkInternet();
        //menu=(ImageView)findViewById(R.id.menubar);



        Namelist = new ArrayList<String>();
        CuscodeList = new ArrayList<String>();
        BranchcodeList = new ArrayList<String>();
        EmailList = new ArrayList<String>();
        MobileList = new ArrayList<String>();
        CusNamelist = new ArrayList<String>();
        CustomercodeList = new ArrayList<String>();





        //checkInternet();

        Search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Customername = Customer_spinner.getSelectedItem().toString();
                if (Customername.equals("All")) {
                    customers();
                } else {
                    selectedcustomers(Customername);
                }

            }
        });

        Getpermissionforsms();

    }

    private void CheckInternetforpass() {
        NetworkConnection network = new NetworkConnection(ViewCustomerActivity.this);
        if (network.CheckInternet()) {
            PasswordChangeAlert alert = new PasswordChangeAlert(ViewCustomerActivity.this);// new saleslogin().execute();
            alert.showLoginbox();
        } else {
            Alertbox alert = new Alertbox(ViewCustomerActivity.this);
            alert.showAlertbox("Kindly check your Internet Connection");
        }

    }

    private void selectedcustomers(String customername) {
        List<deal> customer_lists =  db.adhereDao().GetCustomerlist(customername);
        CuslistAdapter2 adapter = new CuslistAdapter2(ViewCustomerActivity.this, customer_lists);
        gridview.setAdapter(adapter);
    }

    private void customers() {
        List<deal> customer_lists =  db.adhereDao().GetDeal();
        CuslistAdapter2 adapter = new CuslistAdapter2(ViewCustomerActivity.this, customer_lists);
        gridview.setAdapter(adapter);
    }

    private void Getpermissionforsms() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                if (shouldShowRequestPermissionRationale(android.Manifest.permission.CALL_PHONE)) {
                    Toast.makeText(getApplicationContext(),"You need to give permission",Toast.LENGTH_SHORT).show();
                } else {
                    requestPermissions(new String[]{android.Manifest.permission.CALL_PHONE}, PERMISSION_REQUEST);
                }
            } else {
                GetDealerNamesForSpinner();
            }
        } else
        {
            GetDealerNamesForSpinner();

        }

    }

    private void GetDealerNamesForSpinner() {

        Namelist =  db.adhereDao().GetCustomersie();
        Namelist.add(0,"All");
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(ViewCustomerActivity.this,R.layout.simple_spinner_item,Namelist);
        spinnerAdapter.setNotifyOnChange(true);
        spinnerAdapter.setDropDownViewResource(R.layout.simple_spinner_item);
        Customer_spinner.setAdapter(spinnerAdapter);

    }
    }
