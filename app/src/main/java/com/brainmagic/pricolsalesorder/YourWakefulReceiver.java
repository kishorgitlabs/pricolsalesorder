package com.brainmagic.pricolsalesorder;

import android.content.Context;
import android.content.Intent;


import androidx.legacy.content.WakefulBroadcastReceiver;

import service.BGServiceRegion;

public class YourWakefulReceiver extends WakefulBroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent service = new Intent(context, BGServiceRegion.class);
        startWakefulService(context, service);
    }
}

