package com.brainmagic.pricolsalesorder;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Places;
import com.jaredrummler.materialspinner.MaterialSpinner;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.TransformerUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import javax.net.ssl.HttpsURLConnection;

import Home.homevalues;
import Logout.logout;
import SQL_Fields.SQLITE;
import adapter.ExistingPreviewAdapter;
import alertbox.Alertbox;
import api.models.master.Distributor_list;
import api.models.orderexist.request.ExistOrderReq;
import api.models.orderexist.request.Exist_Orders_List;
import api.models.orderexist.response.OrderResult;
import api.retrofit.APIService;
import api.retrofit.RetrofitClient;
import database.DBHelper;
import database.DBHelpersync;
import network.NetworkConnection;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Response;
import roomdb.database.AppDatabase;
import static roomdb.database.AppDatabase.getAppDatabase;

public class ExistingCustomerCart extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, android.location.LocationListener, com.google.android.gms.location.LocationListener
        ,EasyPermissions.PermissionCallbacks,ResultCallback<LocationSettingsResult> {
    private ListView list;
    private Button order;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private ArrayList<String> customerlist, productseglist, productcatagorylist, datelist, quantitylist, packagelsist, ordertypelist,discountlist,unpacklist,distlist,dealerlist,customlist,customertypelist,mobilenumberlist;
    private ArrayList<Integer> IDList;
    private ProgressDialog progressDialog;
    private SQLiteDatabase db;
    private Alertbox box = new Alertbox(ExistingCustomerCart.this);
    private String distiid,distiname,distimobile,distiaddress;
    List<Exist_Orders_List> existOrderslist;
    private String SalesName,regid;
    private Alertbox alertbox=new Alertbox(ExistingCustomerCart.this);
    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;
    private final static int REQUEST_CHECK_SETTINGS_GPS=100;
    private final static int REQUEST_ID_MULTIPLE_PERMISSIONS=0x2;
    private static final String TAG = "ExistingCustomerCart";
    private LocationManager locationManager;
    private Location mylocation;
    private ProgressDialog mProgressDialog;
    private boolean loc=false;
    private boolean alreadyCalled=false;
    private Location mLastLocation;
    private Double latitude,longitude;
    public static final String APIKEY =  "AIzaSyBRmYkOOy9QhrI53Fp3h_Tt8t7amWNa4Q0";
    String address, address1, City, state, country, postalCode,orginaladdress;
    private TextView grandtotla;
    String Grandtotal;
    private ImageView back,home,logout;
    private TextView title;
    private List<String>statelist,distributer,citylsut,namelsit,idlist;
    private AppDatabase db1;
    private MaterialSpinner selectstate,selectcity,selectdis;
    private List<Object>stateobject;
    private List<Object>distributerobject;
    private List<Object>distributercity;
    private List<Object>distributername;
    private List<Object>distributerid;
    private String states,namestring,idstring;
    private Button placeorder;
    private String mobilenumber;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_confirm_dealer);
        myshare = getSharedPreferences("pricolsalesorder", MODE_PRIVATE);
        editor = myshare.edit();
        db1=getAppDatabase(ExistingCustomerCart.this);
        distiid=getIntent().getStringExtra("disid");
        distiname=getIntent().getStringExtra("distiname");
        distimobile=getIntent().getStringExtra("distimobile");
        distiaddress=getIntent().getStringExtra("disaddress");
        SalesName=myshare.getString("name","");
        regid=myshare.getString("regid","");
        title=(TextView)findViewById(R.id.title);
        title.setText("Confirm Order");
        list = (ListView) findViewById(R.id.listview);
        order = (Button) findViewById(R.id.order);
        grandtotla=(TextView)findViewById(R.id.grandtotla);
        home = (ImageView) findViewById(R.id.home);
        logout = (ImageView) findViewById(R.id.logout);
        back=(ImageView)findViewById(R.id.back) ;
        mobilenumber= SQLITE.COLUMN_AddMobileNO;
        progressDialog = new ProgressDialog(ExistingCustomerCart.this);
        customerlist = new ArrayList<>();
        productseglist = new ArrayList<>();
        productseglist = new ArrayList<>();
        productcatagorylist = new ArrayList<>();
        datelist = new ArrayList<>();
        quantitylist = new ArrayList<>();
        packagelsist = new ArrayList<>();
        ordertypelist = new ArrayList<>();
        discountlist = new ArrayList<>();
        unpacklist = new ArrayList<>();
        IDList = new ArrayList<>();
        distlist = new ArrayList<>();
        dealerlist = new ArrayList<>();
        customlist = new ArrayList<>();
        customertypelist = new ArrayList<>();
        mobilenumberlist = new ArrayList<>();
        new GetListView().execute();
        getNetworkStatus();
        NetworkConnection network = new NetworkConnection(ExistingCustomerCart.this);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new homevalues(ExistingCustomerCart.this).home_io();
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new logout(ExistingCustomerCart.this).log_out();
            }
        });
        if (network.CheckInternet())
        {
            AskLocationPermission();
//            mProgressDialog= ProgressDialog.show(this,"Location","Getting Your Location",false,false);
        }
        else {
            Alertbox alert = new Alertbox(ExistingCustomerCart.this);
            alert.showAlertbox("Kindly check your Internet Connection");
        }



        order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog alertDialog = new AlertDialog.Builder(ExistingCustomerCart.this).create();
                LayoutInflater inflater = getLayoutInflater();

//                distributer.add("chennai");
//                distributer.add("trichy");
                View dialogView = inflater.inflate(R.layout.selectalert, null);
                selectstate=dialogView.findViewById(R.id.selectstate);
                selectcity=dialogView.findViewById(R.id.selectcity);
                selectdis=dialogView.findViewById(R.id.selectdis);
                placeorder=dialogView.findViewById(R.id.placeorder) ;
                selectstate.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                        states=item.toString();
                        GetDisti(db1.adhereDao().GetCityDistilist1(states));
                    }
                });
                selectdis.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                        namestring=item.toString();
//                        List<Integer> ids=new ArrayList<>(db1.adhereDao().GetIdDistilist1(namestring));
////                        idlist  =new ArrayList<>();
//                        for(Integer ides:ids)
//                        {
//                            idlist.add(ides+"");
//                        }
                        idstring=idlist.get(position).toString();
                    }
                });
                selectcity.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                        String city=item.toString();

//                        namelsit  =new ArrayList<>(db1.adhereDao().GetNameDistilist1(city));
//                        namelsit.add(0,"Select Name");
                        GetNameIdDisti(db1.adhereDao().GetNameDistilist1(city));
//                        selectdis.setItems(namelsit);
                    }
                });
//                GetState(db1.adhereDao().GetDistilist());
//                statelist=new ArrayList<String>(db1.adhereDao().GetStateDistilist());
//                statelist.add(0,"Select State");
//                selectstate.setItems(statelist);
                // selectdis=dialogView.findViewById(R.id.selectdis);
                final SpinnerDialog spinnerDialog=new SpinnerDialog(ExistingCustomerCart.this,"Select Distributer");

//                selectdis.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        spinnerDialog.SetItems((ArrayList<String>) distributer);
//                        spinnerDialog.showSpinerDialog();
//
//                    }
//                });
                placeorder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                        new GetOrder().execute();
                    }
                });
                alertDialog.setView(dialogView);
                alertDialog.show();

                spinnerDialog.bindOnSpinerListener(new OnSpinerItemClick() {
                    @Override
                    public void onClick(String s, int var2) {
                        selectdis.setText(s);
                    }
                });
            }


        });
    }

//    private void GetDisti(List<Distributor_list> distributor_lists) {
//        distributer  =new ArrayList<>();
//        citylsut  =new ArrayList<>();
//        namelsit  =new ArrayList<>();
//        idlist  =new ArrayList<>();
//        distributerobject=(List<Object>) CollectionUtils.collect(distributor_lists, TransformerUtils.invokerTransformer("getAddress"));
//        distributercity=(List<Object>) CollectionUtils.collect(distributor_lists, TransformerUtils.invokerTransformer("getTownnme"));
//        distributername=(List<Object>) CollectionUtils.collect(distributor_lists, TransformerUtils.invokerTransformer("getDisname"));
//        distributerid=(List<Object>) CollectionUtils.collect(distributor_lists, TransformerUtils.invokerTransformer("getP_Id"));
//        for (Object object : distributerobject) {
//            if (object!=null)
//                distributer.add(Objects.toString(object, null));
//        }
//        for (Object object : distributercity) {
//            if (object!=null)
//                citylsut.add(Objects.toString(object, null));
//        }
//        for (Object object : distributername) {
//            if (object!=null)
//                namelsit.add(Objects.toString(object, null));
//        }
//        for (Object object : distributerid) {
//            if (object!=null)
//                idlist.add(Objects.toString(object, null));
//        }
//        citylsut.add(0,"Select City");
//        namelsit.add(0,"Select Name");
//        idlist.add(0,"Select id");
//        selectcity.setItems(citylsut);
//        selectdis.setItems(namelsit);
//
////        final SpinnerDialog spinnerDialog=new SpinnerDialog(ExistingCustomerCart.this,"Select Distributer");
////        spinnerDialog.SetItems((ArrayList<String>) distributer);
//    }

    private void GetDisti(List<String> distributor_lists) {
        distributer  =new ArrayList<>();
        citylsut  =new ArrayList<>(distributor_lists);


//        distributerobject=(List<Object>) CollectionUtils.collect(distributor_lists, TransformerUtils.invokerTransformer("getAddress"));
//        distributercity=(List<Object>) CollectionUtils.collect(distributor_lists, TransformerUtils.invokerTransformer("getTownnme"));
//        distributername=(List<Object>) CollectionUtils.collect(distributor_lists, TransformerUtils.invokerTransformer("getDisname"));
//        distributerid=(List<Object>) CollectionUtils.collect(distributor_lists, TransformerUtils.invokerTransformer("getP_Id"));
//        for (Object object : distributerobject) {
//            if (object!=null)
//                distributer.add(Objects.toString(object, null));
//        }
//        for (Object object : distributercity) {
//            if (object!=null)
//                citylsut.add(Objects.toString(object, null));
//        }
//        for (Object object : distributername) {
//            if (object!=null)
//                namelsit.add(Objects.toString(object, null));
//        }
//        for (Object object : distributerid) {
//            if (object!=null)
//                idlist.add(Objects.toString(object, null));
//        }
        citylsut.add(0,"Select City");


        selectcity.setItems(citylsut);


//        final SpinnerDialog spinnerDialog=new SpinnerDialog(ExistingCustomerCart.this,"Select Distributer");
//        spinnerDialog.SetItems((ArrayList<String>) distributer);
    }

    private void GetNameIdDisti(List<Distributor_list> distributor_lists) {
//        distributer  =new ArrayList<>();
        namelsit  =new ArrayList<>();
        idlist=new ArrayList<>();
//        citylsut  =new ArrayList<>();




        List<Object> idList=(List<Object>) CollectionUtils.collect(distributor_lists, TransformerUtils.invokerTransformer("getP_Id"));
        List<Object> nameLists=(List<Object>) CollectionUtils.collect(distributor_lists, TransformerUtils.invokerTransformer("getDisname"));
//        distributercity=(List<Object>) CollectionUtils.collect(distributor_lists, TransformerUtils.invokerTransformer("getTownnme"));
//        distributername=(List<Object>) CollectionUtils.collect(distributor_lists, TransformerUtils.invokerTransformer("getDisname"));
//        distributerid=(List<Object>) CollectionUtils.collect(distributor_lists, TransformerUtils.invokerTransformer("getP_Id"));
        for (Object object : idList) {
            if (object!=null)
                idlist.add(Objects.toString(object, null));
        }
        for (Object object : nameLists) {
            if (object!=null)
                namelsit.add(Objects.toString(object, null));
        }
//        for (Object object : distributername) {
//            if (object!=null)
//                namelsit.add(Objects.toString(object, null));
//        }
//        for (Object object : distributerid) {
//            if (object!=null)
//                idlist.add(Objects.toString(object, null));
//        }
//        citylsut.add(0,"Select City");
        idlist.add(0,"Select id");
        namelsit.add(0,"Select Name");
        selectdis.setItems(namelsit);
        selectcity.setItems(citylsut);


//        final SpinnerDialog spinnerDialog=new SpinnerDialog(ExistingCustomerCart.this,"Select Distributer");
//        spinnerDialog.SetItems((ArrayList<String>) distributer);
    }

    private void GetState(List<String> statelists1) {
        statelist=new ArrayList<String>();
        stateobject=(List<Object>) CollectionUtils.collect(statelists1, TransformerUtils.invokerTransformer("getStatename"));
        for (Object object : stateobject) {
            if (object!=null)
                statelist.add(Objects.toString(object, null));
        }
        statelist.add(0,"Select State");
        selectstate.setItems(statelist);
    }

    private void AskLocationPermission() {
        if (EasyPermissions.hasPermissions(ExistingCustomerCart.this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Have permission, do the thing!
            if (CheckLocationIsEnabled()) {
                // if location is enabled show place picker activity to use
                startLocationUpdates();

            } else {

                LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                        .addLocationRequest(locationRequest);

                PendingResult<LocationSettingsResult> result =
                        LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
                result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                    @SuppressLint("LongLogTag")
                    @Override
                    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
                        final Status status = locationSettingsResult.getStatus();
                        switch (status.getStatusCode()) {
                            case LocationSettingsStatusCodes.SUCCESS:
                                startLocationUpdates();
                                break;
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                try {
                                    status.startResolutionForResult(
                                            ExistingCustomerCart.this,
                                            REQUEST_CHECK_SETTINGS_GPS);
                                } catch (IntentSender.SendIntentException e) {
                                    Log.e(TAG, "Exception : " + e.getMessage());
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                Log.e(TAG, "Location settings are not satisfied.");
                                break;
                        }
                    }
                });


            }
        } else {
            // Request one permission
            EasyPermissions.requestPermissions(this, getString(R.string.rationale_location), REQUEST_ID_MULTIPLE_PERMISSIONS, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION);

        }
    }

    @SuppressLint("LongLogTag")
    private void startLocationUpdates() {
        Log.d(TAG, "startLocationUpdates: ");
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                    if(googleApiClient.isConnected())
                    {
                        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
                        mylocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                    }
//                    if (mylocation == null)
                    {
                        mProgressDialog = ProgressDialog.show(this, "Location", "Getting Your Location", false, false);
                        locationManager.requestLocationUpdates(
                                LocationManager.NETWORK_PROVIDER,
                                1000,
                                500, this);
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                1000,
                                500, this);
                    }
//                    else {
//                        mProgressDialog = ProgressDialog.show(this, "Location", "Getting Your Location", false, false);
//                        locationManager.requestLocationUpdates(
//                                LocationManager.NETWORK_PROVIDER,
//                                1000,
//                                500, this);
//                    }
//                CheckInternet();
//                mProgressDialog=ProgressDialog.show(this,"Location","Getting Your Location",false,false);
                    Log.d(TAG, "startLocationUpdates: ");
                } else {
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            REQUEST_ID_MULTIPLE_PERMISSIONS);
                    Log.d(TAG, "Permission Not Granted");
                }

            } else {
                locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                if (googleApiClient.isConnected()) {
                    Log.d(TAG, "startLocationUpdates: else");

                    LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
                    mylocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                }
//                if (mylocation == null)
                {
                    mProgressDialog = ProgressDialog.show(this, "Location", "Getting Your Location", false, false);
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            1000,
                            500, this);
                    locationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            1000,
                            500, this);
                }
//                else {
//                    mProgressDialog = ProgressDialog.show(this, "Location", "Getting Your Location", false, false);
//                    AskLocationPermission();
//                }

            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private boolean CheckLocationIsEnabled() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

        }
        if(googleApiClient!=null)
            mylocation= LocationServices.FusedLocationApi
                    .getLastLocation(googleApiClient);

        if (mylocation == null) {
            return false;
        } else {
            return true;
        }
    }

    private void getNetworkStatus() {
        NetworkConnection network = new NetworkConnection(ExistingCustomerCart.this);
        if (network.CheckInternet())
        {
            setUpGClient();
            getLastLocation();
//            mProgressDialog= ProgressDialog.show(this,"Location","Getting Your Location",false,false);
        }
        else {
            Alertbox alert = new Alertbox(ExistingCustomerCart.this);
            alert.showAlertbox("Kindly check your Internet Connection");

        }
    }

    public void getLastLocation()
    {
        int permissionLocation = ContextCompat.checkSelfPermission(ExistingCustomerCart.this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
//            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
//            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//                    LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
//            mylocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
            locationRequest = new LocationRequest();
            locationRequest.setInterval(100);
            locationRequest.setFastestInterval(100);
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setSmallestDisplacement(10);
            loc=true;
        }
    }

    private synchronized void setUpGClient() {
        try {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .addApi(Places.GEO_DATA_API)
                    .addApi(Places.PLACE_DETECTION_API)
                    .enableAutoManage(this, this).build();
//            googleApiClient.connect();
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @SuppressLint("LongLogTag")
    @Override
    protected void onPause() {
        super.onPause();
        try {
            if(googleApiClient!=null)
                if(googleApiClient.isConnected()) {
                    googleApiClient.disconnect();
                }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    @SuppressLint("LongLogTag")
    @Override
    protected void onResume() {
        super.onResume();
        if(googleApiClient!=null)
            if(!alreadyCalled && googleApiClient.isConnected())
            {
                getNetworkStatus();
            }
        alreadyCalled=false;

    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS_GPS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        startLocationUpdates();
                        break;
                    case Activity.RESULT_CANCELED:
                        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(ExistingCustomerCart.this);
                        alertDialog.setMessage("If you don't enable GPS, your travel cannot be tracked. Do you want to close the app?");
                        alertDialog.setTitle("Pricol Sales Order");
                        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                finish();
                            }
                        });

                        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                AskLocationPermission();
                            }
                        });

                        alertDialog.show();
                        break;
                    default:
                        finish();
                        break;
                }
                break;
        }
    }
    @SuppressLint("LongLogTag")
    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop: ");
        if(googleApiClient!=null)
            if (googleApiClient.isConnected()) {
                googleApiClient.disconnect();
            }
    }

    @SuppressLint("LongLogTag")
    private void CheckInternet() {

        Log.d(TAG, "CheckInternet: lat and long");
        NetworkConnection network = new NetworkConnection(ExistingCustomerCart.this);
        if (network.CheckInternet()) {
            // relativeLayout.setVisibility(View.VISIBLE);
            Log.d(TAG, "CheckInternet: RelativeLayout");
            ShowCurrentLocationMarker();
        } else {
            Alertbox alert = new Alertbox(ExistingCustomerCart.this);
            alert.showAlertbox("Kindly check your Internet Connection");
            //  retry.setVisibility(View.VISIBLE);
            //relativeLayout.setVisibility(View.GONE);
        }
    }
    @SuppressLint("LongLogTag")
    private void ShowCurrentLocationMarker() {
        if (mLastLocation != null) {
            Log.d(TAG, "ShowCurrentLocationMarker: latitude "+mLastLocation.getLatitude()+" logitude "+mLastLocation.getLongitude());

            new GeocodeAsyncTask().execute(mLastLocation.getLatitude(), mLastLocation.getLongitude());     // Sets the center of the map to location user
        }
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onLocationChanged(Location location) {
        mProgressDialog.dismiss();
        // check_button.setVisibility(View.VISIBLE);
        if (location != null) {
            mylocation = location;
            latitude=mylocation.getLatitude();
            longitude=mylocation.getLongitude();
            editor.putString("latitude",latitude.toString());
            editor.putString("longitude",longitude.toString());
            Log.d(TAG, "onLocationChanged: "+mylocation.getLatitude()+" long "+mylocation.getLongitude());
            editor.commit();
            new GeocodeAsyncTask().execute(mylocation.getLatitude(), mylocation.getLongitude());
            //Or Do whatever you want with your location
        }
        else if(mylocation!=null) {
            latitude=mylocation.getLatitude();
            longitude=mylocation.getLongitude();
            editor.putString("latitude",latitude.toString());
            editor.putString("longitude",longitude.toString());
            Log.d(TAG, "onLocationChanged: "+mylocation.getLatitude()+" long "+mylocation.getLongitude());
            editor.commit();
            new GeocodeAsyncTask().execute(mylocation.getLatitude(), mylocation.getLongitude());
        }

    }



    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {

    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

    }

    private class GeocodeAsyncTask extends AsyncTask<Double, Void, Address> {

        String errorMessage = "";


        @SuppressLint("LongLogTag")
        @Override
        protected Address doInBackground(Double... latlang) {
            Geocoder geocoder = new Geocoder(ExistingCustomerCart.this, Locale.getDefault());
            List<Address> addresses = null;
            if (geocoder.isPresent()) {
                try {
                    addresses = geocoder.getFromLocation(latlang[0], latlang[1], 1);
                    Log.d(TAG, "doInBackground: ************");
                } catch (IOException ioException) {
                    errorMessage = "Service Not Available";
                    Log.e(TAG, errorMessage, ioException);
                } catch (IllegalArgumentException illegalArgumentException) {
                    errorMessage = "Invalid Latitude or Longitude Used";
                    Log.e(TAG, errorMessage + ". " +
                            "Latitude = " + latlang[0] + ", Longitude = " +
                            latlang[1], illegalArgumentException);
                }

                if (addresses != null && addresses.size() > 0)
                    return addresses.get(0);
            } else {
                new GetGeoCodeAPIAsynchTask().execute(latlang[0], latlang[1]);
            }

            return null;
        }

        @SuppressLint("LongLogTag")
        protected void onPostExecute(Address addresss) {

            if (addresss == null) {
                new GetGeoCodeAPIAsynchTask().execute(mylocation.getLatitude(), mylocation.getLongitude());
                Log.d(TAG, "onPostExecute: *****");
            } else {
//                progressBar.setVisibility(View.GONE);
                address = addresss.getAddressLine(0);
                editor.putString("ToAddress", address);
                editor.commit();
                editor.apply();
                City = addresss.getLocality();
                Log.d(TAG, "onPostExecute: **************************" + City);
                String city = addresss.getLocality();
                String state = addresss.getAdminArea();
                //create your custom title
                String title = city + "-" + state;
                orginaladdress=(address +
                        "\n"
                        + title);
                editor.putString("FromAddress", address);
                Geocoder geocoder = new Geocoder(ExistingCustomerCart.this);
                try {
                    ArrayList<Address> addresses = (ArrayList<Address>) geocoder.getFromLocationName("karur", 50);
                    for (Address address3 : addresses) {
                        double lat = address3.getLatitude();
                        double lon = address3.getLongitude();
//                        address2.setText(lat +
//                                "\n"
//                                + lon);

                    }
                } catch (IOException e) {
                    e.printStackTrace();

                }
                //create your custom title
//                String title = city + "-" + state;
//                Alertbox alertbox=new Alertbox(MainActivity.this);
//                alertbox.showAlertboxwithback("Your Current location is "+city);


            }
        }
    }

    private class GetGeoCodeAPIAsynchTask extends AsyncTask<Double, Void, String[]> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String[] doInBackground(Double... latlang) {
            String response;
            try {
                String URL = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + latlang[0] + "," + latlang[1] + "&key=" + APIKEY;
                Log.v("URL", URL);
                response = getLatLongByURL(URL);
                return new String[]{response};
            } catch (Exception e) {
                return new String[]{"error"};
            }
        }

        @SuppressLint("LongLogTag")
        @Override
        protected void onPostExecute(String... result) {
            try {
                JSONObject jsonObject = new JSONObject(result[0]);

                address = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONArray("address_components").getJSONObject(0).getString("long_name");

                editor.putString("FromAddress",address);
                editor.putString("ToAddress",address);
                editor.commit();
                editor.apply();

                City = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONArray("address_components").getJSONObject(2).getString("long_name");

                String state = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONArray("address_components").getJSONObject(4).getString("long_name");

                Log.d(TAG, "onPostExecute: "+City);

//                String title = city + "-" + state;

//                Alertbox alertbox=new Alertbox(MainActivity.this);
//                alertbox.showAlertbox("Your Current location is "+city);

//                if (fetchType == FROMADDRESS) {
//
//                    mPrimaryAddress.setText(address);
//                    mSecondaryAddress.setText(title);
//                    mDropText.requestFocus();
//                    fromaddress = address;
//                    mPickupText.setText(address);
//
//                    editor.putString("FromAddress", fromaddress);
//                    editor.apply();
//                    editor.commit();
//
//                    Log.i("FromAddress1", address);
//                    Log.i("FromAddress2", title);
//                } else {
//                    mPrimaryAddress.setText(address);
//                    mSecondaryAddress.setText(title);
//                    mDropText.setText(address);
//                    toaddress = address;
//                    editor.putString("ToAddress", toaddress).commit();
//
//                    Log.i("ToAddress1", address);
//                    Log.i("ToAddress2", title);
//
//                }


                Log.d("Address", "" + address);
            } catch (JSONException e) {
                e.printStackTrace();
            }
//            progressBar.setVisibility(View.GONE);

        }
    }

    public String getLatLongByURL(String requestURL) {
        URL url;
        String response = "";
        try {
            url = new URL(requestURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");
            conn.setDoOutput(true);
            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = "";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }


    private class GetListView extends AsyncTask<String, Void, String>

    {
        DBHelpersync dbhelper = new DBHelpersync(ExistingCustomerCart.this);
        Cursor cursor;

        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Loading...");
            progressDialog.show();
            db = dbhelper.getWritableDatabase();

        }

        @SuppressLint("Range")
        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            Log.v("OFF Line", "SQLITE Table");
            try {
                //String query ="select * from Orderpreview order by formattedDate asc";
//                String query="select * from '"+SalesOrderExistingActivity.TABLE_Additem+"' where OrderType ='"+getIntent().getStringExtra("ordertypevalues")+"'";
                String query="select * from ExistingOrderpreview where OrderType ='"+getIntent().getStringExtra("ordertypevalues")+"'";
//                String query = "select * from " + SQLITE.TABLE_Additems + " order by formattedDate asc";
                Log.v("Dealer Name", query);
                cursor = db.rawQuery(query, null);
                if (cursor.moveToFirst()) {
                    do {
                        customerlist.add(cursor.getString(cursor.getColumnIndex("colours")));
                        productseglist.add(cursor.getString(cursor.getColumnIndex("ProductName")));
                        productcatagorylist.add(cursor.getString(cursor.getColumnIndex("ProductCatagory")));
                        datelist.add(cursor.getString(cursor.getColumnIndex("formattedDate")));
                        quantitylist.add(cursor.getString(cursor.getColumnIndex("quantity")));
                        packagelsist.add(cursor.getString(cursor.getColumnIndex("Package")));
                        ordertypelist.add(cursor.getString(cursor.getColumnIndex("OrderType")));
                        IDList.add(cursor.getInt(cursor.getColumnIndex("id")));
                        discountlist.add(cursor.getString(cursor.getColumnIndex("discount")));
                        unpacklist.add(cursor.getString(cursor.getColumnIndex("Unpackage")));
                        distlist.add(cursor.getString(cursor.getColumnIndex("AddNames")));
                        dealerlist.add(cursor.getString(cursor.getColumnIndex("colours")));
                        dealerlist.add(cursor.getString(cursor.getColumnIndex("colours")));
                        customlist.add(cursor.getString(cursor.getColumnIndex("CustomerNameDealer")));
                        customertypelist.add(cursor.getString(cursor.getColumnIndex("CustomerType")));
                        mobilenumberlist.add(cursor.getString(cursor.getColumnIndex("MobileNO")));
                    } while (cursor.moveToNext());
                    cursor.close();
                    db.close();
                    return "received";
                } else {
                    cursor.close();
                    db.close();
                    return "nodata";
                }
            } catch (Exception e) {
                if(cursor!=null)
                    cursor.close();
                db.close();
                Log.v("Error in get date", "catch is working");
                Log.v("Error in Incentive", e.getMessage());
                return "nodata";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();

            switch (result) {
                case "received" :
                    int overallAmount=0;
                    String size=String.valueOf(discountlist.size());
                    for(int i=0;i<discountlist.size();i++)
                    {
                        String amt=discountlist.get(i);
                        //String[] split = amt.split("\\.");
                        //int mrp=Integer.parseInt(split[0].trim());
                        // int quantity = Integer.parseInt(discountlist.get(i));
                        int totalamount=Integer.valueOf(amt);


                        // int overallAmountTemp = Integer.parseInt(discountlist.get(i));
                        overallAmount += totalamount;
                    }
                    Grandtotal= String.valueOf(overallAmount);
                    grandtotla.setText("₹"+" "+Grandtotal);

                    ExistingPreviewAdapter previewAdapter = new ExistingPreviewAdapter(ExistingCustomerCart.this, customerlist, productseglist, productcatagorylist, datelist, quantitylist, packagelsist, ordertypelist,IDList,discountlist,unpacklist,distlist,dealerlist,customlist,customertypelist,getIntent().getStringExtra("salesType"));
                    list.setAdapter(previewAdapter);
                    break;
                case "nodata":
                    box.showAlertbox("No Orders Found");
                    break;
                default:
                    //alert.showAlertbox("Error in Preview");
                    break;
            }

        }
    }

    private class GetOrder extends AsyncTask<String, Void, String> {

        DBHelpersync dbhelper = new DBHelpersync(ExistingCustomerCart.this);
        ExistOrderReq existOrdersList = new ExistOrderReq();
        String Result;

        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Placing Order...");
            progressDialog.show();
            existOrderslist=new ArrayList<Exist_Orders_List>();

            // Calling JSON
        }

        @SuppressLint("Range")
        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            Log.v("OFF Line", "SQLITE Table");
            try {
                String query="select * from ExistingOrderpreview where OrderType ='"+ getIntent().getStringExtra("ordertypevalues")+"'";
//                String query = "select * from " + SQLITE.TABLE_Additems;
                Log.v("Query order detail code", query);
                Log.v("OFF Line", "SQLITE Table");

                db = dbhelper.getWritableDatabase();
                Cursor curr = db.rawQuery(query, null);
                if (curr.moveToFirst()) {
                    do {
                        Exist_Orders_List existOrder = new Exist_Orders_List();
                        existOrder.setExecutivename(SalesName);
                        existOrder.setDealername(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_Addstring_ordercolors)));
                        existOrder.setSegment(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_Addproductname)));
                        existOrder.setOrderType(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_AddOrderType)));
                        //existOrder.setOrderMode(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_AddOrderType)));
                        existOrder.setProductname(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_Productcat)));
                        existOrder.setPartNo(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_Addpackage)));
                        existOrder.setQuantity(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_Addquantity)));
                        //existOrder.setAmount(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_Adddiscount)));
                        existOrder.setAmount(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_Unpackage)));
                        existOrder.setDate(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_Addcurrentdate)));
                        existOrder.setExcuteid(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_Addstring_mail)));
                        existOrder.setDealermobile(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_AddMobileNO)));
                        existOrder.setDealerstate(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_RetState)));
                        existOrder.setAddress(orginaladdress);
                        existOrder.setUnitprice(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_Unpackage)));
                        existOrder.setDealercity(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_RetCity)));
                        existOrder.setDealeraddress(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_RetAddress)));
                        existOrder.setDealeremail(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_RetEmail)));
                        existOrder.setDealerregion(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_RetRegion)));
                        existOrder.setStatus("Order Pending");
                        existOrder.setDistributorName(namestring);
                        existOrder.setDisid(idstring);
                        existOrder.setDealerusertype(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_AddcustomerType)));
                        existOrder.setCusName(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_CUSTOMERNAME)));
                        existOrder.setCusCode(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_Addcustomercode)));
                        existOrder.setRemarks(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_AddRemarks)));
                        existOrder.setGrandtotal(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_Adddiscount)));
                        existOrderslist.add(existOrder);
                    } while (curr.moveToNext());
                    curr.close();
                    db.close();
                }

                existOrdersList.setExist_Orders_List(existOrderslist);
                APIService service = RetrofitClient.getApiService();
                // Calling JSON
                Call<OrderResult> call = service.EXIST_ORDERS_CALL(existOrdersList);
                Response<OrderResult> response = call.execute();
                return response.body().getResult();

            } catch (Exception e) {

                return "Failure";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();

            switch (result) {
                case "Success":
                    final AlertDialog dialog = new AlertDialog.Builder(ExistingCustomerCart.this).create();
                    dialog.setTitle(R.string.app_name);
                    dialog.setMessage("Order Placed Successfully");
                    dialog.setButton("Okay", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int which) {
                            DBHelpersync dbHelper = new DBHelpersync(ExistingCustomerCart.this);
                            SQLiteDatabase db = dbHelper.getWritableDatabase();
                            try {
                                db1.adhereDao().DeleteAllsecondary();
                                db1.adhereDao().DeleteAllExistingCustomer();
                                db.execSQL("DELETE FROM  ExistingOrderpreview WHERE OrderType='"+ getIntent().getStringExtra("ordertypevalues")+"'");
                                db.close();
                                editor.putString("customertypestring","");
                                editor.commit();
                                //drop the new register
                                for (int i=0 ;i<mobilenumberlist.size();i++){
                                    db1.adhereDao().DeleteAllCreate(mobilenumberlist.get(i));
                                }
                                DBHelper dbHelper1=new DBHelper(ExistingCustomerCart.this);
                                SQLiteDatabase db1=dbHelper1.getWritableDatabase();

                            } catch (Exception e) {
                                db.close();
                                Log.v("DeleteAging", "Delete");
                            }
                            Intent i = new Intent(ExistingCustomerCart.this, SalesOrderType.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);
                        }
                    });
                    dialog.show();


                    break;

                case "Failure":

                    alertbox.showAlertbox("Error ");
                    break;
                default:

                    alertbox.showAlertbox("Something went wrong .Please try again later .");
                    break;
            }


        }
    }

}
