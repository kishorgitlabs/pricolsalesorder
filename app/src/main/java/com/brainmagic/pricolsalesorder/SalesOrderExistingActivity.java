package com.brainmagic.pricolsalesorder;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.AsyncTask;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.muddzdev.styleabletoastlibrary.StyleableToast;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.TransformerUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import Home.homevalues;
import Logout.logout;
import SQL_Fields.SQLITE;
import alertbox.Alertbox;
import alertbox.PasswordChangeAlert;
import alertbox.PrimaryOrder;
import api.models.deal;
import api.models.master.Citylist;
import api.models.master.Customer_list;
import api.models.master.Distributor_list;
import api.models.master.Productlist;
import api.models.master.createlist;
import database.DBHelper;
import database.DBHelpersync;
import network.NetworkConnection;
import roomdb.database.AppDatabase;
import roomdb.entity.ExistingCustomerList;
import roomdb.entity.SecondaryOrderDetails;

import static SQL_Fields.SQLITE.TABLE_Additems;
import static roomdb.database.AppDatabase.destroyInstance;
import static roomdb.database.AppDatabase.getAppDatabase;

public class SalesOrderExistingActivity extends AppCompatActivity {
    //private SearchableSpinner dealerspinner;
    private Spinner dealerspinner;
    private AutoCompleteTextView productcatagory,productname,packag,mobilenumberstatus;
    private TextView catagory,segments,customspinner;
    private EditText qty,remarks,discount;
    private MaterialSpinner ordertype,distributname,dealername,dealernametext,customerspinner,comname;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private Button add,view;
    private TextView mrp1;
    SQLiteDatabase db;
    AppDatabase db1;
    DBHelper dbHelper;
    Cursor c;
    private TextView shopName;
    private static final String TAG = "SalesOrderExisting";
    private List<String> productsegment;
    private List<String> productcatagorylist;
    private List<String> partlist;
    private List<String> ordertypelist;
    private List<String> dealerNameList,dealermobilelist,statelist,citylist,regionlist,addresslist,emaillist,distributeliststring,distributidlist,customerslist;
    private String segmentstring,ordertypestring,catagorystring,partnostring;
    private ImageView menu;
    public ProgressDialog progressDialog;
    private Alertbox box = new Alertbox(SalesOrderExistingActivity.this);
    // Table Additems
    public static final String TABLE_Additem = "ExistingOrderpreview";
    public static final String COLUMN_Addexecutive = "SalesName";
    public static final String COLUMN_AddcustomerType = "CustomerType";
    public static final String COLUMN_Addproductname = "ProductName";
    public static final String COLUMN_Addpackage = "Package";
    public static final String COLUMN_Addquantity = "quantity";
    public static final String COLUMN_Adddiscount = "discount";
    public static final String COLUMN_AddOrderType="OrderType";
    public static final String COLUMN_Addcustomercode="Customercode";
    public static final String COLUMN_Addcurrentdate="formattedDate";
    public static final String COLUMN_Productcat="ProductCatagory";
    public static final String COLUMN_AddMobileNO="MobileNO";
    public static final String COLUMN_Addstring_mail="string_mail";
    public static final String COLUMN_Addstring_ordermode="order_mode";
    private static final String COLUMN_AddNames="AddNames";
    private static final String COLUMN_AddRemarks="Remarks";
    private static final String COLUMN_Addstring_ordernumber="numbers";
    private static final String COLUMN_Addstring_ordercolors="colours";
    private static final String COLUMN_Unpackage="Unpackage";
    private static final String COLUMN_RetState="retailerstate";
    private static final String COLUMN_RetCity="retailercity";
    private static final String COLUMN_RetAddress="Retaddress";
    private static final String COLUMN_RetEmail="Retemail";
    private static final String COLUMN_RetRegion="Retregion";
    public static final String COLUMN_CUSTOMERNAME="CustomerNameDealer";
    //    public static final String COLUMN_Execute_id="salesExecuteId";
//    public static final String COLUMN_Distirbute_id="distributeId";
    private String formattedDate;
    private static final int PERMISSION_REQUEST = 100;
    private List<String>Namelist;
    private List<String>productcatlist;
    private List<Productlist>partnolist;
    private String customers;
    private String salesname,quantity,totalvalues;
    private int totqty,executeid;
    private int totvalues;
    List<Object>mrptcat;
    private TableRow distributrow,dealerrow,dealerrowtext,cusrow,customertype,mobilenumberrow,companyname,shopNameView;
    private String distiname,retailername1,distid;
    private ImageView addretailer,addretailer1;
    private String retailename = "";
    private Boolean addretailerbol=false;
    List<Object>distributor,dealerObjectNameList,dealerObjectmobilelist,stateObjectlist,cityObjectlist,regionObjectlist,addressObjectlist,emailObjectlist,segmentobjectlist,catagoryobjectlist,customerobject,customercodeobject;
    List<String> list = new ArrayList<String>();

    private String retmobilenumber,retstate,retcity,retaddress,retemail,retregion,distiid,customers1,customercode,customerstring="";
    private ArrayList<String>Distilistname=new ArrayList<String>();
    private List<String>partlistauto,segmentlist,catagorylist,customerlist,customercodelist=new ArrayList<String>();
    private List<String> mobilelistlist=new ArrayList<String>();
    private List<String> cuslist=new ArrayList<String>();
    private List<String> shopNameList=new ArrayList<String>();
    private List<String> typeoffacclist=new ArrayList<String>();
    private List<String> regionliststring=new ArrayList<String>();
    private List<String> stateliststring=new ArrayList<String>();
    private List<String> cityliststring=new ArrayList<String>();
    private List<String> mobilenolist=new ArrayList<String>();
    private List<String> addressllist=new ArrayList<String>();
    private List<String>companylist=new ArrayList<>();
    private TableRow selectordertype,secondaryorder;
    private String mob,companynamestring;
    private String customertypestring = "",choosen="";
    List<Object>customername,typeofaccount,region,state,city,mobileno,address;
    private String customernamestring;
    List<ExistingCustomerList> detail;
    private String Companynamestring,customertypestring1,ordertypeString;
    private List<String>partlistauto1;

    //new
    private AutoCompleteTextView mobileedit;
    private LinearLayout shplinear;
    private EditText shopname,contactperson,emailid,addressedit,gst;
    private TextView region1;
    private MaterialSpinner state_spinner,city_edit;
    private String regionstr,states;

    //Table row
    private LinearLayout staterow,cityrow,addressrow;
    private ImageView home,logout,setting;
    private TextView title;
    private EditText othecityedit;
    private LinearLayout othercity;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_order_existing);
        shopNameView=findViewById(R.id.shop_name_view);
        shopName=findViewById(R.id.shop_name);
        myshare = getSharedPreferences("pricolsalesorder", MODE_PRIVATE);
        editor = myshare.edit();
        choosen=getIntent().getStringExtra("chooseactivity");
        addretailerbol=myshare.getBoolean("addretailerbol",addretailerbol);
        customertypestring=myshare.getString("customertypestring","");
        salesname=myshare.getString("name","");
        executeid=myshare.getInt("idlist",0);
        regionstr=myshare.getString("regName","");
        //new Fields
        Companynamestring=getIntent().getStringExtra("companynamae");
        customertypestring1=getIntent().getStringExtra("customertype");
        ordertypeString=getIntent().getStringExtra("ordertypes");
        partlistauto1=new ArrayList<>();
        statelist=new ArrayList<>();
        mobileedit=(AutoCompleteTextView)findViewById(R.id.mobileedit);
        home = (ImageView) findViewById(R.id.home);
        title=(TextView)findViewById(R.id.title);
        othercity=(LinearLayout) findViewById(R.id.othercity);
        othecityedit=(EditText) findViewById(R.id.othecityedit);
        setting=(ImageView) findViewById(R.id.setting);
        shplinear=(LinearLayout) findViewById(R.id.shplinear);
        shopname=(EditText)findViewById(R.id.shopname);
        contactperson=(EditText)findViewById(R.id.contactperson);
        emailid=(EditText)findViewById(R.id.emailid);
        addressedit=(EditText)findViewById(R.id.addressedit);
        gst=(EditText)findViewById(R.id.gst);
        region1=(TextView)findViewById(R.id.region) ;
        state_spinner=(MaterialSpinner)findViewById(R.id.state_spinner);
        city_edit=(MaterialSpinner)findViewById(R.id.city_edit);
        staterow=(LinearLayout) findViewById(R.id.staterow);
        cityrow=(LinearLayout)findViewById(R.id.cityrow);
        addressrow=(LinearLayout)findViewById(R.id.addressrow);
        logout = (ImageView) findViewById(R.id.logout);
        db1 = getAppDatabase(SalesOrderExistingActivity.this);
        dealerspinner=(Spinner) findViewById(R.id.dealerspinner);
        packag=(AutoCompleteTextView) findViewById(R.id.packagespinner);
        qty=(EditText)findViewById(R.id.quantityedittext);
        discount=(EditText) findViewById(R.id.discountedittext);
        //comname=(MaterialSpinner) findViewById(R.id.comname);
        mrp1=(TextView) findViewById(R.id.mrp1);
        //dealernametext=(MaterialSpinner) findViewById(R.id.dealernametext);
        remarks=(EditText)findViewById(R.id.remark);
        //mobilenumberstatus=(AutoCompleteTextView) findViewById(R.id.mobilenumberstatus);
        //ordertype=(MaterialSpinner)findViewById(R.id.ordertypespinner);
        segments=(TextView) findViewById(R.id.segments);
        catagory=(TextView) findViewById(R.id.catagory);
        distributname=(MaterialSpinner) findViewById(R.id.distribute);
        dealername=(MaterialSpinner) findViewById(R.id.dealername);
        customspinner=(TextView) findViewById(R.id.customspinner);
       // customerspinner=(MaterialSpinner) findViewById(R.id.customerspinner);
        distributrow=(TableRow)findViewById(R.id.distributrow);
        dealerrow=(TableRow)findViewById(R.id.dealerrow) ;
        cusrow=(TableRow)findViewById(R.id.cusrow) ;
        customertype=(TableRow)findViewById(R.id.customertype) ;
        dealerrowtext=(TableRow)findViewById(R.id.dealerrowtext) ;
        mobilenumberrow=(TableRow)findViewById(R.id.mobilenumberrow) ;
        companyname=(TableRow)findViewById(R.id.companyname) ;
        addretailer=(ImageView) findViewById(R.id.retailer) ;
        addretailer1=(ImageView) findViewById(R.id.retailer1) ;
        selectordertype=findViewById(R.id.selectordertype);
        secondaryorder=findViewById(R.id.secondaryorder);
        detail=db1.adhereDao().existingCustomerDetails();
        add=(Button)findViewById(R.id.add);
        view=(Button)findViewById(R.id.order);
        title.setText("Sales Order");
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new homevalues(SalesOrderExistingActivity.this).home_io();

            }
        });
        city_edit.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                if(item.toString().equals("Other")){
                    othercity.setVisibility(View.VISIBLE);
                }else {
                    othercity.setVisibility(View.GONE);
                }
            }
        });
        packag.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (packag.length()==0){
                    mrp1.setText("");
                    qty.setText("");
                    segments.setText("");
                }
            }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PopupMenu popupMenu = new PopupMenu(SalesOrderExistingActivity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu pop) {
                        pop.dismiss();
                    }
                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.primary:
                                PrimaryOrder alert = new PrimaryOrder(SalesOrderExistingActivity.this);// new saleslogin().execute();
                                alert.showLoginboxtoggle();
                                return true;
                            case R.id.ch:
                                CheckInternetforpass();
                                return true;
                            case R.id.logout:
                                new logout(SalesOrderExistingActivity.this).log_out();
                                return true;
                        }
                        return false;
                    }
                });
                popupMenu.inflate(R.menu.pricolcms);
                Menu pop = popupMenu.getMenu();
                popupMenu.show();

            }
        });
        if(Companynamestring.equals("Pricol")){
            String type="Spares";
            GetPartnumber(type);
        }else if(Companynamestring.equals("PEIL")){
            String type="PEIL";
            GetPartnumber(type);

        }else if(Companynamestring.equals("Xenos")){
            String type="Accessories";
            GetPartnumber(type);
        }
        GetStatelist(db1.adhereDao().sGetRegion(regionstr));
        GetMobileNumber(db1.adhereDao().GetCustomermobileexisting(customertypestring1));

        state_spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                states=item.toString();
                GetCItystrings(db1.adhereDao().GetCitystring(states));

            }
        });
        mobileedit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.length() == 10){
                    if(db1.adhereDao().GetCustomermobileexisting(customertypestring1).equals(mobileedit.getText().toString())){
                            mobileedit.setEnabled(false);
                            // adapterView.getSelectedItem().toString();
                            if(db1.adhereDao().GetShopname(mobileedit.getText().toString()).equals("")){
                                shopname.setText("Empty");
                                shopname.setEnabled(true);
                            }else {
                                shopname.setText(db1.adhereDao().GetShopname(mobileedit.getText().toString()));
                                shopname.setEnabled(true);
                            }
                            if(db1.adhereDao().GetCpn(mobileedit.getText().toString()).equals("")){
                                contactperson.setText("Empty");
                                contactperson.setEnabled(true);
                            }else {
                                contactperson.setText(db1.adhereDao().GetCpn(mobileedit.getText().toString()));
                                contactperson.setEnabled(true);
                            }
                            if(db1.adhereDao().Getemail(mobileedit.getText().toString()).equals("")){
                                emailid.setText("Empty");
                                emailid.setEnabled(true);
                            }else {
                                emailid.setText(db1.adhereDao().Getemail(mobileedit.getText().toString()));
                                emailid.setEnabled(true);
                            }
                            if(db1.adhereDao().GetGst(mobileedit.getText().toString()).equals("")){
                                gst.setText("Empty");
                                gst.setEnabled(true);
                            }else {
                                emailid.setText(db1.adhereDao().GetGst(mobileedit.getText().toString()));
                                emailid.setEnabled(true);
                            }

                            statelist.add(0,db1.adhereDao().GetState(mobileedit.getText().toString()));
                            state_spinner.setItems(statelist);
                            state_spinner.setEnabled(false);
                            citylist.add(0,db1.adhereDao().GetCity(mobileedit.getText().toString()));
                            city_edit.setItems(citylist);
                            city_edit.setEnabled(false);

                            if(db1.adhereDao().GetAddress(mobileedit.getText().toString()).equals("")){
                                addressedit.setText("Empty");
                                addressedit.setEnabled(true);
                            }else {
                                addressedit.setText(db1.adhereDao().GetAddress(mobileedit.getText().toString()));
                                addressedit.setEnabled(true);
                            }




                    }else {
                        staterow.setVisibility(View.VISIBLE);
                        cityrow.setVisibility(View.VISIBLE);
                        addressrow.setVisibility(View.VISIBLE);
                        shopname.setEnabled(true);
                        contactperson.setEnabled(true);
                        state_spinner.setEnabled(true);
                        city_edit.setEnabled(true);
                        emailid.setEnabled(true);
                        addressedit.setEnabled(true);
                    }
                }else{

                }

            }
        });



        mobileedit.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                if(!db1.adhereDao().GetCustomermobileexisting(customertypestring1).equals(mobileedit.getText().toString())){
                    if(mobileedit.length() == 10){
                        mobileedit.setEnabled(false);
                       // adapterView.getSelectedItem().toString();
                        if(db1.adhereDao().GetShopname(mobileedit.getText().toString()).equals("")){
                            shopname.setText("N/A");
                            shopname.setEnabled(true);
                        }else {
                            shopname.setText(db1.adhereDao().GetShopname(mobileedit.getText().toString()));
                            shopname.setEnabled(true);
                        }
                        if(db1.adhereDao().GetCpn(mobileedit.getText().toString()).equals("")){
                            contactperson.setText("N/A");
                            contactperson.setEnabled(true);
                        }else {
                            contactperson.setText(db1.adhereDao().GetCpn(mobileedit.getText().toString()));
                            contactperson.setEnabled(true);
                        }
                        if(db1.adhereDao().Getemail(mobileedit.getText().toString()).equals("")){
                            emailid.setText("N/A");
                            emailid.setEnabled(true);
                        }else {
                            emailid.setText(db1.adhereDao().Getemail(mobileedit.getText().toString()));
                            emailid.setEnabled(true);
                        }
                        if(db1.adhereDao().GetGst(mobileedit.getText().toString()).equals("")){
                            gst.setText("N/A");
                            gst.setEnabled(true);
                        }else {
                            emailid.setText(db1.adhereDao().GetGst(mobileedit.getText().toString()));
                            emailid.setEnabled(true);
                        }
                        staterow.setVisibility(View.GONE);
                        cityrow.setVisibility(View.GONE);
                        addressrow.setVisibility(View.GONE);
                        statelist.add(0,db1.adhereDao().GetState(mobileedit.getText().toString()));
                        state_spinner.setItems(statelist);
                        state_spinner.setEnabled(false);
                        citylist.add(0,db1.adhereDao().GetCity(mobileedit.getText().toString()));
                        city_edit.setItems(citylist);
                        city_edit.setEnabled(false);

                        if(db1.adhereDao().GetAddress(mobileedit.getText().toString()).equals("")){
                            addressedit.setText("N/A");
                            addressedit.setEnabled(true);
                        }else {
                            addressedit.setText(db1.adhereDao().GetAddress(mobileedit.getText().toString()));
                            addressedit.setEnabled(true);
                        }

                    }else {
                        shopname.setEnabled(true);
                        contactperson.setEnabled(true);
                        state_spinner.setEnabled(true);
                        city_edit.setEnabled(true);
                        emailid.setEnabled(true);
                        addressedit.setEnabled(true);
                    }
                }else {
                    staterow.setVisibility(View.VISIBLE);
                    cityrow.setVisibility(View.VISIBLE);
                    addressrow.setVisibility(View.VISIBLE);
                }



            }
        });
        if(customertypestring1.equals("Retailer")||customertypestring1.equals("Mechanic")|| customertypestring1.equals("Electrician") ){
            shplinear.setVisibility(View.VISIBLE);
        }else {
            shplinear.setVisibility(View.GONE);
        }
        Date d = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(d);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formattedDate = df.format(c.getTime());
        dealerNameList=new ArrayList<>();
        dealermobilelist=new ArrayList<>();
        statelist=new ArrayList<>();
        citylist=new ArrayList<>();
        regionlist=new ArrayList<>();
        addresslist=new ArrayList<>();
        emaillist=new ArrayList<>();
        distributeliststring=new ArrayList<>();
        distributidlist=new ArrayList<>();

//        List<SecondaryOrderDetails>details=db1.adhereDao().customerDetails();
//
//        if(details.size()!=0)
//        {
//            if(details.get(0).getMobileNo().contains(getIntent().getStringExtra("mobilestring"))){
//                // no action
//
//
//            }else {
//                final android.app.AlertDialog dialog = new android.app.AlertDialog.Builder(SalesOrderExistingActivity.this).create();
//                dialog.setTitle(R.string.app_name);
//                dialog.setMessage("Product have been added for this customer "+details.get(0).getCustomerName()+". Please place order for this customer or move to local and then add Products for another customer");
//                dialog.setButton("Okay", new DialogInterface.OnClickListener() {
//
//                    public void onClick(DialogInterface dialog, int which) {
//                        Intent i =new Intent(SalesOrderExistingActivity.this,SalesConfirmDealerActivity.class).putExtra("ordertypevalues", myshare.getString("customertypestring","")).putExtra("salesType","New");
//                        startActivity(i);
//                        // finish();
//                    }
//                });
//                dialog.show();
//            }
//
//        }
//        else {
//            // no action
//
//        }

//        companylist.add(0,"Select Company Name");
//        companylist.add("Pricol");
//        companylist.add("PEIL");
//        comname.setItems(companylist);

//        comname.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
//                companynamestring=item.toString();
//            }
//        });

//        customerspinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
//                customerstring =item.toString();
//                if(customerstring.equals("Retailer")||customerstring.equals("Mechanic"))
//                {
//                    shopNameView.setVisibility(View.VISIBLE);
//                }
//                else {
//                    shopNameView.setVisibility(View.GONE);
//                }
//                GetValues(db1.adhereDao().GetDeallist(customerstring));
//            }
//        });


        new GetDealerNamesForSpinner().execute();
//        //  new GetMobilenumberspinner().execute();
//        if(addretailerbol){
//            dealerrowtext.setVisibility(View.GONE);
//        }else {
//            dealerrowtext.setVisibility(View.VISIBLE);
//        }
//        ordertype.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
//                ordertypestring=item.toString();
//
//            }
//        });
//        home.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                new homevalues(SalesOrderExistingActivity.this).home_io();
//            }
//        });
//        logout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                new logout(SalesOrderExistingActivity.this).log_out();
//            }
//        });

//        back.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                onBackPressed();
//            }
//        });
//        dealernametext.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
//                retailername1=item.toString();
//                retmobilenumber=dealermobilelist.get(position).toString();
//                retstate=statelist.get(position).toString();
//                retcity=citylist.get(position).toString();
//                retaddress=addresslist.get(position).toString();
//                retemail=emaillist.get(position).toString();
//                retregion=regionlist.get(position).toString();
//            }
//        });

//        addretailer1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent i =new Intent(SalesOrderExistingActivity.this,CreateRegionActivity.class);
//                startActivity(i);
//            }
//        });

//        distributname.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
//                distiname=item.toString();
//                distiid=distributidlist.get(position).toString();
//
//
//
//                //distid=
//            }
//        });
//        dealerspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                customers = parent.getItemAtPosition(position).toString();
//
//                if(customers.equals("Primary Order")){
//                    distributrow.setVisibility(View.GONE);
//                    dealerrowtext.setVisibility(View.GONE);
//                    cusrow.setVisibility(View.GONE);
//                }else if(customers.equals("Secondary Order")){
//                    distributrow.setVisibility(View.GONE);
//                    dealerrowtext.setVisibility(View.GONE);
//                    cusrow.setVisibility(View.VISIBLE);
//                    customertype.setVisibility(View.VISIBLE);
//                    mobilenumberrow.setVisibility(View.VISIBLE);
//                    companyname.setVisibility(View.VISIBLE);
//                    addretailerbol=true;
//                    editor.putBoolean("addretailerbol",addretailerbol);
//                    editor.commit();
//                }else {
//
////                    distributrow.setVisibility(View.GONE);
////                    dealerrowtext.setVisibility(View.GONE);
////                    addretailerbol=false;
////                    editor.putBoolean("addretailerbol",addretailerbol);
////                    editor.commit();
//                }
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });

        qty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.length()>0){
                    try {
                        quantity=editable.toString();
                    totqty=Integer.parseInt(quantity);
                    String mrpValue=mrp1.getText().toString();
                    StringBuilder mrp;
                    if(mrpValue.contains("."))
                    {
                        String mrpList[]=mrpValue.split("\\.");
                        mrp=new StringBuilder(mrpList[0]);
                    }
                    else {
                        mrp=new StringBuilder(mrpValue);
                    }
                    totvalues= Integer.parseInt(mrp.toString());
                    totalvalues= String.valueOf((totqty *totvalues));
                    discount.setText(totalvalues);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else{
                    discount.setText("");
                }

            }
        });

//        segments.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                segmentstring=adapterView.getItemAtPosition(i).toString();
//                 productcat();
//               // new productCatagory().execute();
//            }
//        });
//        catagory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                catagorystring=adapterView.getItemAtPosition(i).toString();
//                partnolist=db1.adhereDao().Getpartno(segmentstring,catagorystring);
//                List<Object>productcat=(List<Object>) CollectionUtils.collect(partnolist, TransformerUtils.invokerTransformer("getPartno"));
//                mrptcat=(List<Object>) CollectionUtils.collect(partnolist, TransformerUtils.invokerTransformer("getMrp"));
//                packag.setText((CharSequence) productcat.get(0));
//                mrp1.setText((CharSequence) mrptcat.get(0));
//            }
//        });
//        Namelist = new ArrayList<String>();
//        segmentlist = new ArrayList<String>();
//        partnolist = new ArrayList<Productlist>();
//        ordertypelist=new ArrayList<>();
//        ordertypelist.add(0,"Select Order Type");
//        ordertypelist.add("Credit Bill");
//        ordertypelist.add("CashBill");
//        ordertypelist.add("Sample");
//        ordertype.setItems(ordertypelist);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isTableExists1()){
                    Intent i =new Intent(SalesOrderExistingActivity.this,SalesConfirmDealerActivity.class);
                    i.putExtra("ordertypevalues",myshare.getString("customertypestring","")).putExtra("salesType","Old");
                    startActivity(i);

                }else {
                    final AlertDialog alertDialog = new AlertDialog.Builder(SalesOrderExistingActivity.this).create();
                    LayoutInflater inflater = getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.loginalert, null);
                    alertDialog.setView(dialogView);
                    TextView login =(TextView)dialogView.findViewById(R.id.login);
                    login.setText("No Order Found!");
                    Button submit =(Button) dialogView.findViewById(R.id.submit);
                    submit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();

                        }

                    });
                    alertDialog.show();
                }

//                        final AlertDialog alertDialog = new AlertDialog.Builder(SalesOrderExistingActivity.this).create();
//
//                        LayoutInflater inflater = getLayoutInflater();
//                        View dialogView = inflater.inflate(R.layout.order_type, null);
//                        alertDialog.setView(dialogView);
//
//                        TextView primaryorder = (TextView) dialogView.findViewById(R.id.text2);
//                        TextView secondaryorder = (TextView) dialogView.findViewById(R.id.text3);
//
//                        primaryorder.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View arg0) {
//                                // TODO Auto-generated method stub
//                                alertDialog.dismiss();
//                                if(isTableExists()){
//                                    Intent i =new Intent(SalesOrderExistingActivity.this,SalesConfirmOrder.class);
//                                    i.putExtra("ordertype","primary");
//                                    startActivity(i);
//                                }else {
//                                    StyleableToast st = new StyleableToast(SalesOrderExistingActivity.this, "No Orders Found", Toast.LENGTH_SHORT);
//                                    st.setBackgroundColor(SalesOrderExistingActivity.this.getResources().getColor(R.color.colorPrimary));
//                                    st.setTextColor(Color.WHITE);
//                                    st.setMaxAlpha();
//                                    st.show();
//                                }
//                                alertDialog.dismiss();
//                            }
//                        });
//
//                        secondaryorder.setOnClickListener(new View.OnClickListener() {
//
//                            @Override
//                            public void onClick(View arg0) {
//                                // TODO Auto-generated method stub
//                                if(isTableExists()){
//
//                                }else {
//                                    StyleableToast st = new StyleableToast(SalesOrderExistingActivity.this, "No Orders Found", Toast.LENGTH_SHORT);
//                                    st.setBackgroundColor(SalesOrderExistingActivity.this.getResources().getColor(R.color.colorPrimary));
//                                    st.setTextColor(Color.WHITE);
//                                    st.setMaxAlpha();
//                                    st.show();
//                                }
//                                // /startActivity(new Intent(Sales_segments_Activity.this,Sales_OrderNew_Activity.class));
//                                alertDialog.dismiss();
//                            }
//                        });
//
//                        alertDialog.show();
//                        //startActivity(new Intent(SalesOrderExistingActivity.this, SelectDistributorActivity.class));
            }
        });
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(customertypestring1.equals("Retailer")||customertypestring1.equals("Mechanic")|| customertypestring1.equals("Electrician") ){
                        List<SecondaryOrderDetails>details=db1.adhereDao().customerDetails();
                    if(details.size()!=0)
                    {
                        if(details.get(0).getMobileNo().contains(mobileedit.getText().toString())){

                            if(mobileedit.getText().toString().equals("")){
                                mobileedit.setError("Enter Mobile Number");
//                                StyleableToast st = new StyleableToast(SalesOrderExistingActivity.this, "Enter Mobile Number", Toast.LENGTH_SHORT);
//                                st.setBackgroundColor(SalesOrderExistingActivity.this.getResources().getColor(R.color.colorPrimary));
//                                st.setTextColor(Color.WHITE);
//                                st.setMaxAlpha();
//                                st.show();

                            }else if(shopname.getText().toString().equals("")){
                                shopname.setError("Enter Shop Name");
//                                StyleableToast st = new StyleableToast(SalesOrderExistingActivity.this, "Enter Shop Name", Toast.LENGTH_SHORT);
//                                st.setBackgroundColor(SalesOrderExistingActivity.this.getResources().getColor(R.color.colorPrimary));
//                                st.setTextColor(Color.WHITE);
//                                st.setMaxAlpha();
//                                st.show();

                            }else if(contactperson.getText().toString().equals("")){
                                contactperson.setError("Enter Contact Person Name");
//                                StyleableToast st = new StyleableToast(SalesOrderExistingActivity.this, "Enter Contact Person Name", Toast.LENGTH_SHORT);
//                                st.setBackgroundColor(SalesOrderExistingActivity.this.getResources().getColor(R.color.colorPrimary));
//                                st.setTextColor(Color.WHITE);
//                                st.setMaxAlpha();
//                                st.show();

                            }else if(state_spinner.getText().toString().equals("Select State")){
                                state_spinner.setError("Select State");
//                                StyleableToast st = new StyleableToast(SalesOrderExistingActivity.this, "Select State", Toast.LENGTH_SHORT);
//                                st.setBackgroundColor(SalesOrderExistingActivity.this.getResources().getColor(R.color.colorPrimary));
//                                st.setTextColor(Color.WHITE);
//                                st.setMaxAlpha();
//                                st.show();

                            }else if(city_edit.getText().toString().equals("Select City")){
                                city_edit.setError("Select City");
//                                StyleableToast st = new StyleableToast(SalesOrderExistingActivity.this, "Select City", Toast.LENGTH_SHORT);
//                                st.setBackgroundColor(SalesOrderExistingActivity.this.getResources().getColor(R.color.colorPrimary));
//                                st.setTextColor(Color.WHITE);
//                                st.setMaxAlpha();
//                                st.show();

                            }else if(addressedit.getText().toString().equals("")){
                                addressedit.setError("Enter Address");
//                                StyleableToast st = new StyleableToast(SalesOrderExistingActivity.this, "Enter Address", Toast.LENGTH_SHORT);
//                                st.setBackgroundColor(SalesOrderExistingActivity.this.getResources().getColor(R.color.colorPrimary));
//                                st.setTextColor(Color.WHITE);
//                                st.setMaxAlpha();
//                                st.show();

                            }else if(packag.getText().toString().equals("")){
                                packag.setError("Enter Part No");
//                                StyleableToast st = new StyleableToast(SalesOrderExistingActivity.this, "Enter Part No", Toast.LENGTH_SHORT);
//                                st.setBackgroundColor(SalesOrderExistingActivity.this.getResources().getColor(R.color.colorPrimary));
//                                st.setTextColor(Color.WHITE);
//                                st.setMaxAlpha();
//                                st.show();

                            }else if(segments.getText().toString().equals("")){
                                StyleableToast st = new StyleableToast(SalesOrderExistingActivity.this, "Enter Segments", Toast.LENGTH_SHORT);
                                st.setBackgroundColor(SalesOrderExistingActivity.this.getResources().getColor(R.color.colorPrimary));
                                st.setTextColor(Color.WHITE);
                                st.setMaxAlpha();
                                st.show();

                            }else if(qty.getText().toString().equals("")){
                                addressedit.setError("Enter Qty");
//                                StyleableToast st = new StyleableToast(SalesOrderExistingActivity.this, "Enter Qty", Toast.LENGTH_SHORT);
//                                st.setBackgroundColor(SalesOrderExistingActivity.this.getResources().getColor(R.color.colorPrimary));
//                                st.setTextColor(Color.WHITE);
//                                st.setMaxAlpha();
//                                st.show();

                            }else if(discount.getText().toString().equals("")){
                                discount.setError("Enter Mrp");
//                                StyleableToast st = new StyleableToast(SalesOrderExistingActivity.this, "Enter Mrp", Toast.LENGTH_SHORT);
//                                st.setBackgroundColor(SalesOrderExistingActivity.this.getResources().getColor(R.color.colorPrimary));
//                                st.setTextColor(Color.WHITE);
//                                st.setMaxAlpha();
//                                st.show();

                            }else{
                                new Additems1().execute();
                            }


                        }else {

                            final AlertDialog alertDialog = new AlertDialog.Builder(SalesOrderExistingActivity.this).create();
                            LayoutInflater inflater = getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.loginalertvaldiate, null);
                            alertDialog.setView(dialogView);
                            TextView login =(TextView)dialogView.findViewById(R.id.login);
                            login.setText("Product have been added for this customer "+details.get(0).getCustomerName()+". Please place order for this customer and then add Products for another customer!");
                            Button submit =(Button) dialogView.findViewById(R.id.submit);
                            submit.setOnClickListener( new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    alertDialog.dismiss();
                                    Intent i =new Intent(SalesOrderExistingActivity.this,SalesConfirmDealerActivity.class).putExtra("ordertypevalues", myshare.getString("customertypestring","")).putExtra("salesType","New");
                                    startActivity(i);

                                }

                            });
                            alertDialog.show();
//                            final android.app.AlertDialog dialog = new android.app.AlertDialog.Builder(SalesOrderExistingActivity.this).create();
//                            dialog.setTitle(R.string.app_name);
//                            dialog.setMessage("Product have been added for this customer "+details.get(0).getCustomerName()+". Please place order for this customer or move to local and then add Products for another customer");
//                            dialog.setButton("Okay", new DialogInterface.OnClickListener() {
//
//                                public void onClick(DialogInterface dialog, int which) {
//                                    Intent i =new Intent(SalesOrderExistingActivity.this,SalesConfirmDealerActivity.class).putExtra("ordertypevalues", myshare.getString("customertypestring","")).putExtra("salesType","New");
//                                    startActivity(i);
//                                    // finish();
//                                }
//                            });
//                            dialog.show();
                        }

                    }
                    else {

                        if(mobileedit.getText().toString().equals("")){
                            mobileedit.setError("Enter Mobile Number");
//                            StyleableToast st = new StyleableToast(SalesOrderExistingActivity.this, "Enter Mobile Number", Toast.LENGTH_SHORT);
//                            st.setBackgroundColor(SalesOrderExistingActivity.this.getResources().getColor(R.color.colorPrimary));
//                            st.setTextColor(Color.WHITE);
//                            st.setMaxAlpha();
//                            st.show();

                        }else if(shopname.getText().toString().equals("")){
                            shopname.setError("Enter Shop Number");
//                                StyleableToast st = new StyleableToast(SalesOrderExistingActivity.this, "Enter Shop Name", Toast.LENGTH_SHORT);
//                                st.setBackgroundColor(SalesOrderExistingActivity.this.getResources().getColor(R.color.colorPrimary));
//                                st.setTextColor(Color.WHITE);
//                                st.setMaxAlpha();
//                                st.show();

                        }else if(contactperson.getText().toString().equals("")){
                            contactperson.setError("Enter Contact PersonName");
//                            StyleableToast st = new StyleableToast(SalesOrderExistingActivity.this, "Enter Contact Person Name", Toast.LENGTH_SHORT);
//                            st.setBackgroundColor(SalesOrderExistingActivity.this.getResources().getColor(R.color.colorPrimary));
//                            st.setTextColor(Color.WHITE);
//                            st.setMaxAlpha();
//                            st.show();

                        }else if(state_spinner.getText().toString().equals("Select State")){
                            state_spinner.setError("Select State");
//                            StyleableToast st = new StyleableToast(SalesOrderExistingActivity.this, "Enter State", Toast.LENGTH_SHORT);
//                            st.setBackgroundColor(SalesOrderExistingActivity.this.getResources().getColor(R.color.colorPrimary));
//                            st.setTextColor(Color.WHITE);
//                            st.setMaxAlpha();
//                            st.show();

                        }else if(city_edit.getText().toString().equals("Select City")){
                            city_edit.setError("Select City");
//                            StyleableToast st = new StyleableToast(SalesOrderExistingActivity.this, "Enter City", Toast.LENGTH_SHORT);
//                            st.setBackgroundColor(SalesOrderExistingActivity.this.getResources().getColor(R.color.colorPrimary));
//                            st.setTextColor(Color.WHITE);
//                            st.setMaxAlpha();
//                            st.show();

                        }else if(addressedit.getText().toString().equals("")){
                            addressedit.setError("Enter Address");
//                            StyleableToast st = new StyleableToast(SalesOrderExistingActivity.this, "Enter Address", Toast.LENGTH_SHORT);
//                            st.setBackgroundColor(SalesOrderExistingActivity.this.getResources().getColor(R.color.colorPrimary));
//                            st.setTextColor(Color.WHITE);
//                            st.setMaxAlpha();
//                            st.show();

                        }else if(packag.getText().toString().equals("")){
                            packag.setError("Enter Part No");
//                            StyleableToast st = new StyleableToast(SalesOrderExistingActivity.this, "Enter Part No", Toast.LENGTH_SHORT);
//                            st.setBackgroundColor(SalesOrderExistingActivity.this.getResources().getColor(R.color.colorPrimary));
//                            st.setTextColor(Color.WHITE);
//                            st.setMaxAlpha();
//                            st.show();

                        }else if(segments.getText().toString().equals("")){
                            StyleableToast st = new StyleableToast(SalesOrderExistingActivity.this, "Enter Segments", Toast.LENGTH_SHORT);
                            st.setBackgroundColor(SalesOrderExistingActivity.this.getResources().getColor(R.color.colorPrimary));
                            st.setTextColor(Color.WHITE);
                            st.setMaxAlpha();
                            st.show();

                        }else if(qty.getText().toString().equals("")){
                            qty.setError("Enter Qty");
//                            StyleableToast st = new StyleableToast(SalesOrderExistingActivity.this, "Enter Qty", Toast.LENGTH_SHORT);
//                            st.setBackgroundColor(SalesOrderExistingActivity.this.getResources().getColor(R.color.colorPrimary));
//                            st.setTextColor(Color.WHITE);
//                            st.setMaxAlpha();
//                            st.show();

                        }else if(discount.getText().toString().equals("")){
                            discount.setError("Enter Mrp");
//                            StyleableToast st = new StyleableToast(SalesOrderExistingActivity.this, "Enter Mrp", Toast.LENGTH_SHORT);
//                            st.setBackgroundColor(SalesOrderExistingActivity.this.getResources().getColor(R.color.colorPrimary));
//                            st.setTextColor(Color.WHITE);
//                            st.setMaxAlpha();
//                            st.show();

                        }else{
                            new Additems1().execute();
                        }
                    }



                }else {

                    List<SecondaryOrderDetails> details = db1.adhereDao().customerDetails();

                    if (details.size() != 0) {
                        if (details.get(0).getMobileNo().contains(mobileedit.getText().toString())) {
                            // no action
                            if(mobileedit.getText().toString().equals("")) {
                                mobileedit.setError("Enter Mobile Number");
//                            StyleableToast st = new StyleableToast(SalesOrderExistingActivity.this, "Enter Mobile Number", Toast.LENGTH_SHORT);
//                            st.setBackgroundColor(SalesOrderExistingActivity.this.getResources().getColor(R.color.colorPrimary));
//                            st.setTextColor(Color.WHITE);
//                            st.setMaxAlpha();
//                            st.show();

                            }else if(contactperson.getText().toString().equals("")){
                                contactperson.setError("Enter Contact PersonName");
//                            StyleableToast st = new StyleableToast(SalesOrderExistingActivity.this, "Enter Contact Person Name", Toast.LENGTH_SHORT);
//                            st.setBackgroundColor(SalesOrderExistingActivity.this.getResources().getColor(R.color.colorPrimary));
//                            st.setTextColor(Color.WHITE);
//                            st.setMaxAlpha();
//                            st.show();

                            }else if(state_spinner.getText().toString().equals("Select State")){
                                  state_spinner.setError("Select State");
//                                StyleableToast st = new StyleableToast(SalesOrderExistingActivity.this, "Enter State", Toast.LENGTH_SHORT);
//                                st.setBackgroundColor(SalesOrderExistingActivity.this.getResources().getColor(R.color.colorPrimary));
//                                st.setTextColor(Color.WHITE);
//                                st.setMaxAlpha();
//                                st.show();

                            }else if(city_edit.getText().toString().equals("Select City")){
                                city_edit.setError("Select City");
//                                StyleableToast st = new StyleableToast(SalesOrderExistingActivity.this, "Enter City", Toast.LENGTH_SHORT);
//                                st.setBackgroundColor(SalesOrderExistingActivity.this.getResources().getColor(R.color.colorPrimary));
//                                st.setTextColor(Color.WHITE);
//                                st.setMaxAlpha();
//                                st.show();

                            }else if(addressedit.getText().toString().equals("")){
                                addressedit.setError("Enter Address");
//                            StyleableToast st = new StyleableToast(SalesOrderExistingActivity.this, "Enter Address", Toast.LENGTH_SHORT);
//                            st.setBackgroundColor(SalesOrderExistingActivity.this.getResources().getColor(R.color.colorPrimary));
//                            st.setTextColor(Color.WHITE);
//                            st.setMaxAlpha();
//                            st.show();

                            }else if(packag.getText().toString().equals("")){
                                packag.setError("Enter Part No");
//                            StyleableToast st = new StyleableToast(SalesOrderExistingActivity.this, "Enter Part No", Toast.LENGTH_SHORT);
//                            st.setBackgroundColor(SalesOrderExistingActivity.this.getResources().getColor(R.color.colorPrimary));
//                            st.setTextColor(Color.WHITE);
//                            st.setMaxAlpha();
//                            st.show();

                            }else if(segments.getText().toString().equals("")){
                                StyleableToast st = new StyleableToast(SalesOrderExistingActivity.this, "Enter Segments", Toast.LENGTH_SHORT);
                                st.setBackgroundColor(SalesOrderExistingActivity.this.getResources().getColor(R.color.colorPrimary));
                                st.setTextColor(Color.WHITE);
                                st.setMaxAlpha();
                                st.show();

                            }else if(qty.getText().toString().equals("")){
                                qty.setError("Enter Qty");
//                            StyleableToast st = new StyleableToast(SalesOrderExistingActivity.this, "Enter Qty", Toast.LENGTH_SHORT);
//                            st.setBackgroundColor(SalesOrderExistingActivity.this.getResources().getColor(R.color.colorPrimary));
//                            st.setTextColor(Color.WHITE);
//                            st.setMaxAlpha();
//                            st.show();

                            }else if(discount.getText().toString().equals("")){
                                discount.setError("Enter Mrp");
//                            StyleableToast st = new StyleableToast(SalesOrderExistingActivity.this, "Enter Mrp", Toast.LENGTH_SHORT);
//                            st.setBackgroundColor(SalesOrderExistingActivity.this.getResources().getColor(R.color.colorPrimary));
//                            st.setTextColor(Color.WHITE);
//                            st.setMaxAlpha();
//                            st.show();

                            }else{
                                new Additems1().execute();
                            }


                        }else {

                            final AlertDialog alertDialog = new AlertDialog.Builder(SalesOrderExistingActivity.this).create();
                            LayoutInflater inflater = getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.loginalertvaldiate, null);
                            alertDialog.setView(dialogView);
                            TextView login =(TextView)dialogView.findViewById(R.id.login);
                            login.setText("Product have been added for this customer " + details.get(0).getCustomerName() + ". Please place order for this customer or move to local and then add Products for another customer");
                            Button submit =(Button) dialogView.findViewById(R.id.submit);
                            submit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    alertDialog.dismiss();
                                    Intent i =new Intent(SalesOrderExistingActivity.this,SalesConfirmDealerActivity.class).putExtra("ordertypevalues", myshare.getString("customertypestring","")).putExtra("salesType","New");
                                    startActivity(i);

                                }

                            });
                            alertDialog.show();
//                            final android.app.AlertDialog dialog = new android.app.AlertDialog.Builder(SalesOrderExistingActivity.this).create();
//                            dialog.setTitle(R.string.app_name);
//                            dialog.setMessage("Product have been added for this customer " + details.get(0).getCustomerName() + ". Please place order for this customer or move to local and then add Products for another customer");
//                            dialog.setButton("Okay", new DialogInterface.OnClickListener() {
//
//                                public void onClick(DialogInterface dialog, int which) {
//                                    Intent i = new Intent(SalesOrderExistingActivity.this, SalesConfirmDealerActivity.class).putExtra("ordertypevalues", myshare.getString("customertypestring", "")).putExtra("salesType", "New");
//                                    startActivity(i);
//                                    // finish();
//                                }
//                            });
//                            dialog.show();
                       }


                    }else {
                        if(mobileedit.getText().toString().equals("")){
                            mobileedit.setError("Enter Mobile Number");
//                            StyleableToast st = new StyleableToast(SalesOrderExistingActivity.this, "Enter Mobile Number", Toast.LENGTH_SHORT);
//                            st.setBackgroundColor(SalesOrderExistingActivity.this.getResources().getColor(R.color.colorPrimary));
//                            st.setTextColor(Color.WHITE);
//                            st.setMaxAlpha();
//                            st.show();

                        }else if(contactperson.getText().toString().equals("")){
                            contactperson.setError("Enter Contact PersonName");
//                            StyleableToast st = new StyleableToast(SalesOrderExistingActivity.this, "Enter Contact Person Name", Toast.LENGTH_SHORT);
//                            st.setBackgroundColor(SalesOrderExistingActivity.this.getResources().getColor(R.color.colorPrimary));
//                            st.setTextColor(Color.WHITE);
//                            st.setMaxAlpha();
//                            st.show();

                        }else if(state_spinner.getText().toString().equals("Select State")){
                            state_spinner.setError("Select State");
//                            StyleableToast st = new StyleableToast(SalesOrderExistingActivity.this, "Enter State", Toast.LENGTH_SHORT);
//                            st.setBackgroundColor(SalesOrderExistingActivity.this.getResources().getColor(R.color.colorPrimary));
//                            st.setTextColor(Color.WHITE);
//                            st.setMaxAlpha();
//                            st.show();

                        }else if(city_edit.getText().toString().equals("Select City")){
                            city_edit.setError("Select City");
//                            StyleableToast st = new StyleableToast(SalesOrderExistingActivity.this, "Enter City", Toast.LENGTH_SHORT);
//                            st.setBackgroundColor(SalesOrderExistingActivity.this.getResources().getColor(R.color.colorPrimary));
//                            st.setTextColor(Color.WHITE);
//                            st.setMaxAlpha();
//                            st.show();

                        }else if(addressedit.getText().toString().equals("")){
                            addressedit.setError("Enter Address");
//                            StyleableToast st = new StyleableToast(SalesOrderExistingActivity.this, "Enter Address", Toast.LENGTH_SHORT);
//                            st.setBackgroundColor(SalesOrderExistingActivity.this.getResources().getColor(R.color.colorPrimary));
//                            st.setTextColor(Color.WHITE);
//                            st.setMaxAlpha();
//                            st.show();

                        }else if(packag.getText().toString().equals("")){
                            packag.setError("Enter Part No");
//                            StyleableToast st = new StyleableToast(SalesOrderExistingActivity.this, "Enter Part No", Toast.LENGTH_SHORT);
//                            st.setBackgroundColor(SalesOrderExistingActivity.this.getResources().getColor(R.color.colorPrimary));
//                            st.setTextColor(Color.WHITE);
//                            st.setMaxAlpha();
//                            st.show();

                        }else if(segments.getText().toString().equals("")){
                            StyleableToast st = new StyleableToast(SalesOrderExistingActivity.this, "Enter Segments", Toast.LENGTH_SHORT);
                            st.setBackgroundColor(SalesOrderExistingActivity.this.getResources().getColor(R.color.colorPrimary));
                            st.setTextColor(Color.WHITE);
                            st.setMaxAlpha();
                            st.show();

                        }else if(qty.getText().toString().equals("")){
                            qty.setError("Enter Qty");
//                            StyleableToast st = new StyleableToast(SalesOrderExistingActivity.this, "Enter Qty", Toast.LENGTH_SHORT);
//                            st.setBackgroundColor(SalesOrderExistingActivity.this.getResources().getColor(R.color.colorPrimary));
//                            st.setTextColor(Color.WHITE);
//                            st.setMaxAlpha();
//                            st.show();

                        }else if(discount.getText().toString().equals("")){
                            discount.setError("Enter Mrp");
//                            StyleableToast st = new StyleableToast(SalesOrderExistingActivity.this, "Enter Mrp", Toast.LENGTH_SHORT);
//                            st.setBackgroundColor(SalesOrderExistingActivity.this.getResources().getColor(R.color.colorPrimary));
//                            st.setTextColor(Color.WHITE);
//                            st.setMaxAlpha();
//                            st.show();

                        }else{
                            new Additems1().execute();
                        }
                    }

                    }




//                customernamestring=myshare.getString("orders","");
//                if(customernamestring.equals("")){
//                    if (dealerspinner.equals("Secondary Order")) {
//                        if (customertypestring.equals("Primary Order")) {
//                            final android.app.AlertDialog dialog = new android.app.AlertDialog.Builder(SalesOrderExistingActivity.this).create();
//                            dialog.setTitle(R.string.app_name);
//                            dialog.setMessage("You have Primary Order in the cart Please Place the Order Or Move to Local");
//                            dialog.setButton("Okay", new DialogInterface.OnClickListener() {
//
//                                public void onClick(DialogInterface dialog, int which) {
//
//                                    Intent i = new Intent(SalesOrderExistingActivity.this, SalesConfirmDealerActivity.class).putExtra("ordertypevalues", myshare.getString("customertypestring", "Select"));
//                                    startActivity(i);
//                                }
//                            });
//                            dialog.show();
//                        }
//                        if (packag.getText().toString().equals("")) {
//                            StyleableToast st = new StyleableToast(SalesOrderExistingActivity.this, "Select PartNumber", Toast.LENGTH_SHORT);
//                            st.setBackgroundColor(SalesOrderExistingActivity.this.getResources().getColor(R.color.colorPrimary));
//                            st.setTextColor(Color.WHITE);
//                            st.setMaxAlpha();
//                            st.show();
//                        } else if (qty.getText().toString().equals("")) {
//                            StyleableToast st = new StyleableToast(SalesOrderExistingActivity.this, "Select Quantity", Toast.LENGTH_SHORT);
//                            st.setBackgroundColor(SalesOrderExistingActivity.this.getResources().getColor(R.color.colorPrimary));
//                            st.setTextColor(Color.WHITE);
//                            st.setMaxAlpha();
//                            st.show();
//                        } else {
//                            new Additems1().execute();
//                        }
//
//
//                    } else {
//                        new Additems1().execute();
//                    }
//                } else if(customernamestring.contains(customerstring)){
//                    if (dealerspinner.equals("Secondary Order")) {
//                        if (customertypestring.equals("Primary Order")) {
//                            final android.app.AlertDialog dialog = new android.app.AlertDialog.Builder(SalesOrderExistingActivity.this).create();
//                            dialog.setTitle(R.string.app_name);
//                            dialog.setMessage("You have Primary Order in the cart Please Place the Order Or Move to Local");
//                            dialog.setButton("Okay", new DialogInterface.OnClickListener() {
//
//                                public void onClick(DialogInterface dialog, int which) {
//
//                                    Intent i = new Intent(SalesOrderExistingActivity.this, SalesConfirmDealerActivity.class).putExtra("ordertypevalues", myshare.getString("customertypestring", "Select"));
//                                    startActivity(i);
//                                }
//                            });
//                            dialog.show();
//                        }
//                        if (packag.getText().toString().equals("")) {
//                            StyleableToast st = new StyleableToast(SalesOrderExistingActivity.this, "Select PartNumber", Toast.LENGTH_SHORT);
//                            st.setBackgroundColor(SalesOrderExistingActivity.this.getResources().getColor(R.color.colorPrimary));
//                            st.setTextColor(Color.WHITE);
//                            st.setMaxAlpha();
//                            st.show();
//                        } else if (qty.getText().toString().equals("")) {
//                            StyleableToast st = new StyleableToast(SalesOrderExistingActivity.this, "Select Quantity", Toast.LENGTH_SHORT);
//                            st.setBackgroundColor(SalesOrderExistingActivity.this.getResources().getColor(R.color.colorPrimary));
//                            st.setTextColor(Color.WHITE);
//                            st.setMaxAlpha();
//                            st.show();
//                        } else {
//                            new Additems1().execute();
//                        }
//
//
//                    } else {
//                        new Additems1().execute();
//                    }
//
//                }else {
//                    final android.app.AlertDialog dialog = new android.app.AlertDialog.Builder(SalesOrderExistingActivity.this).create();
//                    dialog.setTitle(R.string.app_name);
//                    dialog.setMessage("You have "+customernamestring+" in the cart Please Place the Order Or Move to Local");
//                    dialog.setButton("Okay", new DialogInterface.OnClickListener() {
//
//                        public void onClick(DialogInterface dialog, int which) {
//
//                            Intent i = new Intent(SalesOrderExistingActivity.this, SalesConfirmDealerActivity.class).putExtra("ordertypevalues", myshare.getString("customertypestring", "Select"));
//                            startActivity(i);
//                        }
//                    });
//                    dialog.show();
//                }
////                if (dealerspinner.equals("Select Order Type")) {
////                    StyleableToast st = new StyleableToast(SalesOrderExistingActivity.this, "Select Order", Toast.LENGTH_SHORT);
////                    st.setBackgroundColor(SalesOrderExistingActivity.this.getResources().getColor(R.color.colorPrimary));
////                    st.setTextColor(Color.WHITE);
////                    st.setMaxAlpha();
////                    st.show();
////
////                } else if (customers.equals("Primary Order")) {
////                    if (customertypestring.equals("Secondary Order")) {
////                        final android.app.AlertDialog dialog = new android.app.AlertDialog.Builder(SalesOrderExistingActivity.this).create();
////                        dialog.setTitle(R.string.app_name);
////                        dialog.setMessage("You have Secondary Order in the cart Please Place the Order Or Move to Local");
////                        dialog.setButton("Okay", new DialogInterface.OnClickListener() {
////
////                            public void onClick(DialogInterface dialog, int which) {
////                                Intent i = new Intent(SalesOrderExistingActivity.this, SalesConfirmDealerActivity.class).putExtra("ordertypevalues", myshare.getString("customertypestring", "Select"));
////                                startActivity(i);
////                            }
////                        });
////                        dialog.show();
////                    }
////                    if (packag.getText().toString().equals("")) {
////                        StyleableToast st = new StyleableToast(SalesOrderExistingActivity.this, "Select PartNumber", Toast.LENGTH_SHORT);
////                        st.setBackgroundColor(SalesOrderExistingActivity.this.getResources().getColor(R.color.colorPrimary));
////                        st.setTextColor(Color.WHITE);
////                        st.setMaxAlpha();
////                        st.show();
////                    } else if (qty.getText().toString().equals("")) {
////                        StyleableToast st = new StyleableToast(SalesOrderExistingActivity.this, "Select Quantity", Toast.LENGTH_SHORT);
////                        st.setBackgroundColor(SalesOrderExistingActivity.this.getResources().getColor(R.color.colorPrimary));
////                        st.setTextColor(Color.WHITE);
////                        st.setMaxAlpha();
////                        st.show();
////                    } else {
////                        new Additems1().execute();
////                    }
////
////
////                } else

            }

        });


        catagory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Partnumber().execute();
            }
        });
        // new segment().execute();
        // segment1();
        //GetPartnumber();
     //   GetCustomerData(db1.adhereDao().GetCustomer_list());//no need
//        if(choosen!=null) {
//            if (choosen.equals("fromchoosed")) {
//                createdcustomer();
//                getcreatecustomer();
//                mobilenumberstatus.setText(getIntent().getStringExtra("mobilestring"));
//                String customertye = getIntent().getStringExtra("ordertype");
//                if (customertye.equals("Retailer")) {
//                    //shopname is customername
//                    customspinner.setText(getIntent().getStringExtra("shopname"));
//                } else {
//                    customspinner.setText(getIntent().getStringExtra("name"));
//                }
//
//            }
//        }else {
////            GetOrder();//primary or secondary
////            getcustomertype();//customer type
//        }

//        if(detail.size()!=0)
//        {
//
//          //  GetOrder();
//            customerspinner.setText(detail.get(0).getCustomerType());
//            mobilenumberstatus.setText(detail.get(0).getMobileNo());
//            customspinner.setText(detail.get(0).getCustomerName()+"N/A");
//            //get created list
////            if(details.get(0).getMobileNo().contains(getIntent().getStringExtra("mobilestring"))){
////                // no action
////
////
////            }else {
////                final android.app.AlertDialog dialog = new android.app.AlertDialog.Builder(SalesOrderExistingActivity.this).create();
////                dialog.setTitle(R.string.app_name);
////                dialog.setMessage("Product have been added for this customer "+details.get(0).getCustomerName()+". Please place order for this customer or move to local and then add Products for another customer");
////                dialog.setButton("Okay", new DialogInterface.OnClickListener() {
////
////                    public void onClick(DialogInterface dialog, int which) {
////
////                        finish();
////                    }
////                });
////                dialog.show();
////            }
//
//        }
//        else {
//            // create list
//          //  GetOrder();
//           // getcustomertype();
//
//        }


//        GetDitribut(db1.adhereDao().GetDistilist());//no  need
        packag.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                partnostring=adapterView.getItemAtPosition(i).toString();
                GetProductsegcat(db1.adhereDao().Getprducts(partnostring));

            }
        });




//        mobilenumberstatus.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                mob=adapterView.getItemAtPosition(i).toString();
//               // GetCustname(db1.adhereDao().GetCrestemobile(mob));
//
//                GetCustomervalues(db1.adhereDao().GetCrestemobile1(mob));
//            }
//        });

//        customspinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
//                customers1=item.toString();
//                customercode=customercodelist.get(position).toString();
//            }
//        });

        //  Getpermissionforsms();

    }

    private void CheckInternetforpass() {
        NetworkConnection network = new NetworkConnection(SalesOrderExistingActivity.this);
        if (network.CheckInternet()) {
            PasswordChangeAlert alert = new PasswordChangeAlert(SalesOrderExistingActivity.this);// new saleslogin().execute();
            alert.showLoginbox();
        } else {
            Alertbox alert = new Alertbox(SalesOrderExistingActivity.this);
            alert.showAlertbox("Kindly check your Internet Connection");
        }

    }



    private void GetPartnumber(String t) {

        partlistauto1=db1.adhereDao().GetPartnolist(t);
        packag.setAdapter(new ArrayAdapter<String>(SalesOrderExistingActivity.this, android.R.layout.simple_dropdown_item_1line,partlistauto1));
        packag.setThreshold(1);
    }

    private void GetCItystrings(List<Citylist> citylists) {
        List<Object>cities=(List<Object>) CollectionUtils.collect(citylists, TransformerUtils.invokerTransformer("getTown1"));
        cities.add(0,"Select City");
        city_edit.setItems(cities);
    }

    private void GetStatelist(List<String> strings) {
        strings.add(0,"Select State");
        state_spinner.setItems(strings);
    }


    private void GetMobileNumber(List<String> s) {
        mobileedit.setAdapter(new ArrayAdapter<String>(SalesOrderExistingActivity.this, android.R.layout.simple_dropdown_item_1line,s));
        mobileedit.setThreshold(1);

    }

    private void GetCustomervalues(List<deal> deals) {
        if (customerspinner.getText().toString().equals("Retailer")) {
            customername = (List<Object>) CollectionUtils.collect(deals, TransformerUtils.invokerTransformer("getContactPersonname"));
            typeofaccount = (List<Object>) CollectionUtils.collect(deals, TransformerUtils.invokerTransformer("getUserType"));
            region = (List<Object>) CollectionUtils.collect(deals, TransformerUtils.invokerTransformer("getDaelerRegion"));
             state=(List<Object>) CollectionUtils.collect(deals, TransformerUtils.invokerTransformer("getDealerState"));
            city=(List<Object>) CollectionUtils.collect(deals, TransformerUtils.invokerTransformer("getDealerCity"));
            mobileno = (List<Object>) CollectionUtils.collect(deals, TransformerUtils.invokerTransformer("getDealerMobile"));
            address = (List<Object>) CollectionUtils.collect(deals, TransformerUtils.invokerTransformer("getDealerAddress"));
            List<Object> shopNameLists = (List<Object>) CollectionUtils.collect(deals, TransformerUtils.invokerTransformer("getShopName"));
//        mobilelistlist= new ArrayList<String>();
            for (Object object : customername) {
                if (object != null)
                    cuslist.add(Objects.toString(object, null));
            }
            for (Object object : shopNameLists) {
                if (object != null)
                    shopNameList.add(Objects.toString(object, null));
            }
            for (Object object : typeofaccount) {
                if (object != null)
                    typeoffacclist.add(Objects.toString(object, null));
            }
            for (Object object : region) {
                if (object != null)
                    regionliststring.add(Objects.toString(object, null));
            }
            for (Object object : state) {
                if (object!=null)
                    stateliststring.add(Objects.toString(object, null));
            }
            for (Object object : city) {
                if (object!=null)
                    cityliststring.add(Objects.toString(object, null));
            }
            for (Object object : mobileno) {
                if (object != null)
                    mobilenolist.add(Objects.toString(object, null));
            }
            for (Object object : address) {
                if (object != null)
                    addressllist.add(Objects.toString(object, null));
            }

           if(cuslist.get(0).equals("")) {
               shopName.setText("Empty");
               customspinner.setText( "Empty");
            }else {
               customspinner.setText(cuslist.get(0));
               shopName.setText(shopNameList.get(0));
           }


        }else {
            customername = (List<Object>) CollectionUtils.collect(deals, TransformerUtils.invokerTransformer("getShopName"));
            typeofaccount = (List<Object>) CollectionUtils.collect(deals, TransformerUtils.invokerTransformer("getUserType"));
            region = (List<Object>) CollectionUtils.collect(deals, TransformerUtils.invokerTransformer("getDaelerRegion"));
            // state=(List<Object>) CollectionUtils.collect(deals, TransformerUtils.invokerTransformer("getState"));
            //  city=(List<Object>) CollectionUtils.collect(deals, TransformerUtils.invokerTransformer("getCity"));
            List<Object> shopNameLists = (List<Object>) CollectionUtils.collect(deals, TransformerUtils.invokerTransformer("getShopName"));
            mobileno = (List<Object>) CollectionUtils.collect(deals, TransformerUtils.invokerTransformer("getDealerMobile"));
            address = (List<Object>) CollectionUtils.collect(deals, TransformerUtils.invokerTransformer("getDealerAddress"));
//        mobilelistlist= new ArrayList<String>();
            for (Object object : customername) {
                if (object != null)
                    cuslist.add(Objects.toString(object, null));
            }
            for (Object object : shopNameLists) {
                if (object != null)
                    shopNameList.add(Objects.toString(object, null));
            }
            for (Object object : typeofaccount) {
                if (object != null)
                    typeoffacclist.add(Objects.toString(object, null));
            }
            for (Object object : region) {
                if (object != null)
                    regionliststring.add(Objects.toString(object, null));
            }
//            for (Object object : state) {
//                if (object!=null)
//                    stateliststring.add(Objects.toString(object, null));
//            }
//            for (Object object : city) {
//                if (object!=null)
//                    cityliststring.add(Objects.toString(object, null));
//            }
            for (Object object : mobileno) {
                if (object != null)
                    mobilenolist.add(Objects.toString(object, null));
            }
            for (Object object : address) {
                if (object != null)
                    addressllist.add(Objects.toString(object, null));
            }

            if(cuslist.get(0).equals("")) {
                customspinner.setText("Empty");
            }else {
                customspinner.setText(cuslist.get(0));
                shopName.setText(shopNameList.get(0));
            }


        }
    }


    private void getcreatecustomer() {
        customerslist=new ArrayList<>();
        customerslist.add(0,getIntent().getStringExtra("ordertype"));
        customerspinner.setItems(customerslist);
    }

    private void getcustomertype() {
        customerslist=new ArrayList<>();
        customerslist.add(0,"Select Customer Type");
        customerslist.add("Retailer");
        customerslist.add("Mechanic");
        customerslist.add("Electrician");
        customerslist.add("End Customer");
        customerslist.add("Others");
        customerspinner.setItems(customerslist);
    }

    private void createdcustomer() {
        Namelist.add(0,"Secondary Order");
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(SalesOrderExistingActivity.this,R.layout.simple_spinner_item,Namelist);
        spinnerAdapter.setNotifyOnChange(true);
        spinnerAdapter.setDropDownViewResource(R.layout.simple_spinner_item);
        dealerspinner.setAdapter(spinnerAdapter);
    }


    private void GetCustname(List<createlist> createlists) {
        if(customerspinner.getText().toString().equals("Retailer")){
            customername=(List<Object>) CollectionUtils.collect(createlists, TransformerUtils.invokerTransformer("getShopname"));
            typeofaccount=(List<Object>) CollectionUtils.collect(createlists, TransformerUtils.invokerTransformer("getTypeofacc"));
            region=(List<Object>) CollectionUtils.collect(createlists, TransformerUtils.invokerTransformer("getRegion"));
            state=(List<Object>) CollectionUtils.collect(createlists, TransformerUtils.invokerTransformer("getState"));
            city=(List<Object>) CollectionUtils.collect(createlists, TransformerUtils.invokerTransformer("getCity"));
            mobileno=(List<Object>) CollectionUtils.collect(createlists, TransformerUtils.invokerTransformer("getMobilenumber"));
            address=(List<Object>) CollectionUtils.collect(createlists, TransformerUtils.invokerTransformer("getAddress"));
//        mobilelistlist= new ArrayList<String>();
            for (Object object : customername) {
                if (object!=null)
                    cuslist.add(Objects.toString(object, null));
            }
            for (Object object : typeofaccount) {
                if (object!=null)
                    typeoffacclist.add(Objects.toString(object, null));
            }
            for (Object object : region) {
                if (object!=null)
                    regionliststring.add(Objects.toString(object, null));
            }
            for (Object object : state) {
                if (object!=null)
                    stateliststring.add(Objects.toString(object, null));
            }
            for (Object object : city) {
                if (object!=null)
                    cityliststring.add(Objects.toString(object, null));
            }
            for (Object object : mobileno) {
                if (object!=null)
                    mobilenolist.add(Objects.toString(object, null));
            }
            for (Object object : address) {
                if (object!=null)
                    addressllist.add(Objects.toString(object, null));
            }

            customspinner.setText(cuslist.get(0));

        }else {
            customername=(List<Object>) CollectionUtils.collect(createlists, TransformerUtils.invokerTransformer("getName"));
            typeofaccount=(List<Object>) CollectionUtils.collect(createlists, TransformerUtils.invokerTransformer("getTypeofacc"));
            region=(List<Object>) CollectionUtils.collect(createlists, TransformerUtils.invokerTransformer("getRegion"));
            state=(List<Object>) CollectionUtils.collect(createlists, TransformerUtils.invokerTransformer("getState"));
            city=(List<Object>) CollectionUtils.collect(createlists, TransformerUtils.invokerTransformer("getCity"));
            mobileno=(List<Object>) CollectionUtils.collect(createlists, TransformerUtils.invokerTransformer("getMobilenumber"));
            address=(List<Object>) CollectionUtils.collect(createlists, TransformerUtils.invokerTransformer("getAddress"));
            for (Object object : customername) {
                if (object!=null)
                    cuslist.add(Objects.toString(object, null));
            }
            for (Object object : typeofaccount) {
                if (object!=null)
                    typeoffacclist.add(Objects.toString(object, null));
            }
            for (Object object : region) {
                if (object!=null)
                    regionliststring.add(Objects.toString(object, null));
            }
            for (Object object : state) {
                if (object!=null)
                    stateliststring.add(Objects.toString(object, null));
            }
            for (Object object : city) {
                if (object!=null)
                    cityliststring.add(Objects.toString(object, null));
            }
            for (Object object : mobileno) {
                if (object!=null)
                    mobilenolist.add(Objects.toString(object, null));
            }
            for (Object object : address) {
                if (object!=null)
                    addressllist.add(Objects.toString(object, null));
            }
//        mobilelistlist= new ArrayList<String>();
            for (Object object : customername) {
                if (object!=null)
                    cuslist.add(Objects.toString(object, null));
            }
            customspinner.setText(cuslist.get(0));

        }

    }

    private void GetValues(List<deal> createlists) {
        List<Object>mobilenumber=(List<Object>) CollectionUtils.collect(createlists, TransformerUtils.invokerTransformer("getDealerMobile"));
//        mobilelistlist= new ArrayList<String>();
        for (Object object : mobilenumber) {
            if (object!=null)
                mobilelistlist.add(Objects.toString(object, null));
        }
        mobilenumberstatus.setAdapter(new ArrayAdapter<String>(SalesOrderExistingActivity.this, android.R.layout.simple_dropdown_item_1line,mobilelistlist));
        mobilenumberstatus.setThreshold(1);
//        mobilenumberstatus.setAdapter(new ArrayAdapter<String>(SalesOrderExistingActivity.this, android.R.layout.simple_dropdown_item_1line,mobilelistlist));
//        mobilenumberstatus.setThreshold(1);
    }



    private void GetCustomerData(List<Customer_list> customer_lists) {
        customerobject=(List<Object>) CollectionUtils.collect(customer_lists, TransformerUtils.invokerTransformer("getName"));
        customercodeobject=(List<Object>) CollectionUtils.collect(customer_lists, TransformerUtils.invokerTransformer("getCustomerCode"));
        customerlist= new ArrayList<String>(customerobject.size());
        for (Object object : customerobject) {
            customerlist.add(Objects.toString(object, null));
        }

        customercodelist =new ArrayList<String>(customercodeobject.size());
        for (Object object : customercodeobject) {
            customercodelist.add(Objects.toString(object, null));
        }
        customerlist.add(0,"Select Customer Name");
        customercodelist.add(0,"Select CustomerCode");
//        customspinner.setItems(customerlist);
    }

    private void GetProductsegcat(List<Productlist> getprducts) {
//        segmentobjectlist=(List<Object>) CollectionUtils.collect(getprducts, TransformerUtils.invokerTransformer("getSegments"));
//        catagoryobjectlist=(List<Object>) CollectionUtils.collect(getprducts, TransformerUtils.invokerTransformer("getCategory"));
//        mrptcat=(List<Object>) CollectionUtils.collect(getprducts, TransformerUtils.invokerTransformer("getMrp"));
        segmentstring=getprducts.get(0).getSegments();
        catagorystring=getprducts.get(0).getCategory();
        segments.setText(segmentstring+"/"+catagorystring);
        //catagory.setText(catagorystring);
        mrp1.setText((CharSequence) getprducts.get(0).getMrp());
    }

//    private void GetPartnumber() {
//        partlistauto=db1.adhereDao().GetPartnolist();
//        packag.setAdapter(new ArrayAdapter<String>(SalesOrderExistingActivity.this, android.R.layout.simple_dropdown_item_1line,partlistauto));
//        packag.setThreshold(1);
//    }

    private void getDealerNamesForSpinner() {

        if(!isTableExists1()){
            GetDelerList(db1.adhereDao().GetDeallist());
        }else {
            String query = "select * from " + SQLITE.TABLE_Create_Profile +"" ;
            //  String query = "select distinct " + SQLITE.COLUMN_Create_retname + " from " + SQLITE.TABLE_Create_Profile + " order by " + SQLITE.COLUMN_Create_retname + " asc";
            Log.v("Name from createcus", query);
            DBHelper dbHelp = new DBHelper(SalesOrderExistingActivity.this);
            db = dbHelp.getWritableDatabase();
            Cursor cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    dealerNameList.add(cursor.getString(cursor.getColumnIndex(SQLITE.COLUMN_Create_retname)));
                    dealermobilelist.add(cursor.getString(cursor.getColumnIndex(SQLITE.COLUMN_Create_retmobile)));
                    statelist.add(cursor.getString(cursor.getColumnIndex(SQLITE.COLUMN_Create_retstate)));
                    citylist.add(cursor.getString(cursor.getColumnIndex(SQLITE.COLUMN_Create_retcity)));
                    regionlist.add(cursor.getString(cursor.getColumnIndex(SQLITE.COLUMN_Create_retregion)));
                    addresslist.add(cursor.getString(cursor.getColumnIndex(SQLITE.COLUMN_Create_retaddress)));
                    //emaillist.add(cursor.getString(cursor.getColumnIndex(SQLITE.COLUMN_Create_retemail)));

                } while (cursor.moveToNext());
                cursor.close();
            }
        }

    }

    private void GetDelerList(List<deal> deals) {
        dealerObjectNameList=(List<Object>) CollectionUtils.collect(deals, TransformerUtils.invokerTransformer("getDealerName"));
        dealerObjectmobilelist=(List<Object>) CollectionUtils.collect(deals, TransformerUtils.invokerTransformer("getDealerMobile"));
        stateObjectlist=(List<Object>) CollectionUtils.collect(deals, TransformerUtils.invokerTransformer("getDealerState"));
        cityObjectlist=(List<Object>) CollectionUtils.collect(deals, TransformerUtils.invokerTransformer("getDealerCity"));
        regionObjectlist=(List<Object>) CollectionUtils.collect(deals, TransformerUtils.invokerTransformer("getDaelerRegion"));
        addressObjectlist=(List<Object>) CollectionUtils.collect(deals, TransformerUtils.invokerTransformer("getDealerAddress"));
        emailObjectlist=(List<Object>) CollectionUtils.collect(deals, TransformerUtils.invokerTransformer("getDealerEmail"));
        //  distiid=(List<String>) CollectionUtils.collect(distributor_lists, TransformerUtils.invokerTransformer("getP_Id"));

        dealerNameList= new ArrayList<String>(dealerObjectNameList.size());
        for (Object object : dealerObjectNameList) {
            dealerNameList.add(Objects.toString(object, null));
        }
        dealermobilelist= new ArrayList<String>(dealerObjectmobilelist.size());
        for (Object object : dealerObjectmobilelist) {
            dealermobilelist.add(Objects.toString(object, null));
        }
        statelist= new ArrayList<String>(stateObjectlist.size());
        for (Object object : stateObjectlist) {
            statelist.add(Objects.toString(object, null));
        }
        citylist= new ArrayList<String>(cityObjectlist.size());
        for (Object object : cityObjectlist) {
            citylist.add(Objects.toString(object, null));
        }
        regionlist= new ArrayList<String>(regionObjectlist.size());
        for (Object object : regionObjectlist) {
            regionlist.add(Objects.toString(object, null));
        }
        addresslist= new ArrayList<String>(addressObjectlist.size());
        for (Object object : addressObjectlist) {

            addresslist.add(Objects.toString(object, null));
        }
        emaillist= new ArrayList<String>(emailObjectlist.size());
        for (Object object : emailObjectlist) {
            emaillist.add(Objects.toString(object, null));
        }
        dealerNameList.add(0,"Select Dealer");
        dealermobilelist.add(0,"Select Mobile");
        statelist.add(0,"Select Mobile");
        citylist.add(0,"Select Mobile");
        regionlist.add(0,"Select Mobile");
        addresslist.add(0,"Select Mobile");
        emaillist.add(0,"Select Mobile");
        dealernametext.setItems(dealerNameList);


//        for (String str : distributstringlist)
//                for (String s : str.split("-"))
//                    list.add(s);

        //distributname.setItems(distributeliststring);


    }

    private void GetDitribut(List<Distributor_list> distributor_lists) {
        distributor=(List<Object>) CollectionUtils.collect(distributor_lists, TransformerUtils.invokerTransformer("getDisname"));
        //  distiid=(List<String>) CollectionUtils.collect(distributor_lists, TransformerUtils.invokerTransformer("getP_Id"));
        distributor.add(0,"Select Distributor-0");
        List<String> distributstringlist= new ArrayList<String>(distributor.size());
        for (Object object : distributor) {
            distributstringlist.add(Objects.toString(object, null));
        }
        for(int i=0;i<distributstringlist.size();i++){
            String [] distrilist=distributstringlist.get(i).split("-");
            distributeliststring.add(distrilist[0]);
            distributidlist.add(distrilist[1]);
        }


//        for (String str : distributstringlist)
//                for (String s : str.split("-"))
//                    list.add(s);

        distributname.setItems(distributeliststring);
    }

    private void GetOrder() {
        Namelist.add(0,"Select OrderType");
        if (myshare.getBoolean("primaryorder",true)){
            selectordertype.setVisibility(View.VISIBLE);
            secondaryorder.setVisibility(View.GONE);
            Namelist.add("Primary Order");
            Namelist.add("Secondary Order");
        }
        else {
            selectordertype.setVisibility(View.GONE);
            secondaryorder.setVisibility(View.VISIBLE);
            Namelist.add("Secondary Order");
            distributrow.setVisibility(View.GONE);
            dealerrowtext.setVisibility(View.GONE);
            cusrow.setVisibility(View.VISIBLE);
            customertype.setVisibility(View.VISIBLE);
            mobilenumberrow.setVisibility(View.VISIBLE);
            companyname.setVisibility(View.VISIBLE);
            addretailerbol=true;
            editor.putBoolean("addretailerbol",addretailerbol);
            editor.commit();
        }
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(SalesOrderExistingActivity.this,R.layout.simple_spinner_item,Namelist);
        spinnerAdapter.setNotifyOnChange(true);
        spinnerAdapter.setDropDownViewResource(R.layout.simple_spinner_item);
        dealerspinner.setAdapter(spinnerAdapter);
    }


    private void productcat() {
        productcatlist=db1.adhereDao().GetProductcat(segmentstring);
        //List<Object>productcat=(List<Object>) CollectionUtils.collect(productcatlist, TransformerUtils.invokerTransformer("getCategory"));
//        catagory.setAdapter(new ArrayAdapter<String>(SalesOrderExistingActivity.this, android.R.layout.simple_dropdown_item_1line,productcatlist));
//        catagory.setThreshold(1);

    }

    private void segment1() {
        segmentlist=db1.adhereDao().GetProdutlist();
//        segments.setAdapter(new ArrayAdapter<String>(SalesOrderExistingActivity.this, android.R.layout.simple_dropdown_item_1line,segmentlist));
//        segments.setThreshold(1);
    }

//    private void Getpermissionforsms() {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
//                if (shouldShowRequestPermissionRationale(android.Manifest.permission.CALL_PHONE)) {
//                    Toast.makeText(getApplicationContext(),"You need to give permission",Toast.LENGTH_SHORT).show();
//                } else {
//                    requestPermissions(new String[]{android.Manifest.permission.CALL_PHONE}, PERMISSION_REQUEST);
//                }
//            } else {
//                GetDealerNamesForSpinner1();
//            }
//        } else {
//            GetDealerNamesForSpinner1();
//
//        }
//
//
//        dealerspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//
//                customers = parent.getItemAtPosition(position).toString();
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
//    }

//    private void GetDealerNamesForSpinner1() {
//
//        Namelist =  db1.adhereDao().GetCustomer_name_list();
//        Namelist.add(0,"Select Customer");
//        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(SalesOrderExistingActivity.this,R.layout.simple_spinner_item,Namelist);
//        spinnerAdapter.setNotifyOnChange(true);
//        spinnerAdapter.setDropDownViewResource(R.layout.simple_spinner_item);
//        dealerspinner.setAdapter(spinnerAdapter);
//    }

    private boolean isTableExists() {
        SQLiteDatabase mDatabase;
        DBHelpersync dbHelp = new DBHelpersync(SalesOrderExistingActivity.this);
        mDatabase = dbHelp.getWritableDatabase();
        String query = "select DISTINCT tbl_name from sqlite_master where tbl_name = '"+ TABLE_Additems+"'";
        //String query ="select * from "+ SQLITE.TABLE_NewPartsOrder+" order by date asc";
        Cursor cursor = mDatabase.rawQuery(query, null);
        Log.v("preview",query);
        if(cursor!=null) {
            if(cursor.getCount()>0) {
                cursor.close();
                mDatabase.close();
                return true;
            }
            cursor.close();
            mDatabase.close();
        }
        return false;

    }

    private boolean isTableExists1() {
        SQLiteDatabase mDatabase;
        DBHelpersync dbHelp = new DBHelpersync(SalesOrderExistingActivity.this);
        mDatabase = dbHelp.getWritableDatabase();
        String query = "select DISTINCT tbl_name from sqlite_master where tbl_name = '"+ SQLITE.TABLE_Additems+"'";
        //String query ="select * from "+ SQLITE.TABLE_NewPartsOrder+" order by date asc";
        Cursor cursor = mDatabase.rawQuery(query, null);
        Log.v("preview",query);
        if(cursor!=null) {
            if(cursor.getCount()>0) {
                cursor.close();
                mDatabase.close();
                return true;
            }
            cursor.close();
            mDatabase.close();
        }
        return false;

    }

    private class segment extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute () {
            super.onPreExecute();
            productsegment=new ArrayList<>();


        }

        @SuppressLint("LongLogTag")
        @Override
        protected String doInBackground (String...strings){
            try {
                dbHelper = new DBHelper(SalesOrderExistingActivity.this);
                db = dbHelper.readDataBase();

                String selectQuery = "Select distinct segments from Product ";

                c = db.rawQuery(selectQuery, null);
                Log.e(TAG, "doInBackground: top");
                if (c.moveToFirst()) {
                    do {


                        productsegment.add(c.getString(c.getColumnIndex("segments")));


                    }
                    while (c.moveToNext());
                }
                c.close();
            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, "doInBackground: \"error\"");
                return "Error";
            }
            return "sucess";

        }

        @Override
        protected void onPostExecute (String s){
            super.onPostExecute(s);

            if (s.equals("sucess")) {
                productsegment.add(0,"Select Segment");
                //segments.setItems(productsegment);
            }


        }
    }

//    private class productCatagory extends AsyncTask<String, Void, String> {
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            productcatagorylist = new ArrayList<>();
//
//
//        }
//
//        @SuppressLint("LongLogTag")
//        @Override
//        protected String doInBackground(String... strings) {
//            try {
//                dbHelper = new DBHelper(SalesOrderExistingActivity.this);
//                db = dbHelper.readDataBase();
//
//                String selectQuery = "Select distinct segments,category from Product where  segments ='"+segmentstring+"' ";
//
//                c = db.rawQuery(selectQuery, null);
//                Log.e(TAG, "doInBackground: top");
//                if (c.moveToFirst()) {
//                    do {
//
//
//                        productcatagorylist.add(c.getString(c.getColumnIndex("category")));
//
//
//                    }
//                    while (c.moveToNext());
//                }
//                c.close();
//            } catch (Exception e) {
//                e.printStackTrace();
//                Log.e(TAG, "doInBackground: \"error\"");
//                return "Error";
//            }
//            return "sucess";
//
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//            super.onPostExecute(s);
//
//            if (s.equals("sucess")) {
//                catagory.setAdapter(new ArrayAdapter<String>(SalesOrderExistingActivity.this, android.R.layout.simple_dropdown_item_1line,productcatagorylist));
//                catagory.setThreshold(1);
//
//            }
//
//
//        }
//    }

    private class Partnumber extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            partlist = new ArrayList<>();


        }

        @SuppressLint("LongLogTag")
        @Override
        protected String doInBackground(String... strings) {
            try {
                dbHelper = new DBHelper(SalesOrderExistingActivity.this);
                db = dbHelper.readDataBase();

                String selectQuery = "Select distinct segments,category,oempartno from Product where  segments ='"+segmentstring+"' and category = '"+catagory.getText().toString()+"'";

                c = db.rawQuery(selectQuery, null);
                Log.e(TAG, "doInBackground: top");
                if (c.moveToFirst()) {
                    do {


                        partlist.add(c.getString(c.getColumnIndex("oempartno")));


                    }
                    while (c.moveToNext());
                }
                c.close();
            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, "doInBackground: \"error\"");
                return "Error";
            }
            return "sucess";

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (s.equals("sucess")) {
                productname.setAdapter(new ArrayAdapter<String>(SalesOrderExistingActivity.this, android.R.layout.simple_dropdown_item_1line, partlist));
                productname.setThreshold(1);

            }


        }
    }

    private class Additems1 extends AsyncTask<String, Void, String> {
        ContentValues values = new ContentValues();
        SQLiteDatabase dbs;
        AppDatabase customerDatabase;
        private double success;
        private int a;
        private String orderNo;
        public String quantity, discount1, remarksstring, unpckage,shopnamestring,conatctnamestring,addressstring,emailidstring,gststring,mobilenumber,Statelsit,citylist;

        @SuppressLint("InflateParams")
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // instantiate new progress dialog
            progressDialog = new ProgressDialog(SalesOrderExistingActivity.this);
            customerDatabase = getAppDatabase(SalesOrderExistingActivity.this);
            // spinner (wheel) style dialog
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Adding data...");
            progressDialog.show();
            quantity = qty.getText().toString();
            discount1 = discount.getText().toString();
            remarksstring = remarks.getText().toString();
            shopnamestring =  shopname.getText().toString();
            conatctnamestring=contactperson.getText().toString();
            addressstring=addressedit.getText().toString();
            emailidstring=emailid.getText().toString();
            gststring=gst.getText().toString();
            mobilenumber=mobileedit.getText().toString();
            Statelsit=state_spinner.getText().toString();
            citylist=city_edit.getText().toString();
            //   Numbers=numbers.getText().toString();
            //  unpckage=packageedit.getText().toString();


        }

        @Override
        protected String doInBackground(String... args0) {
            try {
//                if(customers.equals("Primary Order")){
//
//                    values.put(COLUMN_Addexecutive, salesname);
//                    values.put(COLUMN_AddcustomerType, customerstring);
//                    values.put(COLUMN_AddNames, distiname);
//                    values.put(COLUMN_Addproductname, segmentstring);
//                    values.put(COLUMN_Productcat, catagorystring);
//                    values.put(COLUMN_Addpackage, partnostring);
//                    values.put(COLUMN_Addquantity, quantity);
//                    values.put(COLUMN_Adddiscount, discount1);
////                    values.put(COLUMN_AddOrderType, ordertypestring);
//                    values.put(COLUMN_AddOrderType, customers);
//                    values.put(COLUMN_Addcurrentdate, formattedDate);
//                    values.put(COLUMN_AddRemarks, remarks.getText().toString());
//                    values.put(COLUMN_Addcurrentdate, formattedDate);
//                    values.put(COLUMN_Unpackage, totvalues);
//                    values.put(COLUMN_Addstring_mail, executeid);
//                    values.put(COLUMN_AddMobileNO, distiid);
//                }else {
                    if(customertypestring1.equals("Retailer")||customertypestring1.equals("Mechanic")|| customertypestring1.equals("Electrician")){
                        values.put(COLUMN_Addexecutive, salesname);
                        values.put(COLUMN_AddcustomerType, customertypestring1);
                        values.put(COLUMN_Addproductname, segmentstring);
                        values.put(COLUMN_Productcat, catagorystring);
                        values.put(COLUMN_Addpackage, partnostring);
                        values.put(COLUMN_Addquantity, quantity);
                        values.put(COLUMN_Adddiscount, discount1);
                        values.put(COLUMN_AddOrderType, "Secondary Order");
                        values.put(COLUMN_Addcurrentdate, formattedDate);
                        values.put(COLUMN_AddRemarks, remarksstring);
                        values.put(COLUMN_Addcurrentdate, formattedDate);
                        values.put(COLUMN_Unpackage, totvalues);
//                        values.put(COLUMN_Addstring_ordercolors,getIntent().getStringExtra("shopname"));
                        values.put(COLUMN_Addstring_mail, executeid);
                        values.put(COLUMN_AddMobileNO,mobilenumber);
                        values.put(COLUMN_RetState, Statelsit);

                        if(citylist.equals("Others")){
                            values.put(COLUMN_RetCity,citylist);
                        }else {
                            values.put(COLUMN_RetCity,othecityedit.getText().toString());
                        }

                        values.put(COLUMN_RetAddress, addressstring);
                        values.put(COLUMN_RetEmail,emailidstring);
                        values.put(COLUMN_RetRegion, regionstr);
                        values.put(COLUMN_CUSTOMERNAME, Companynamestring);
                        //values.put(COLUMN_Addcustomercode, gsttext.getText().toString());
                        values.put(COLUMN_Addstring_ordercolors,conatctnamestring);
                        values.put(COLUMN_Addcustomercode, shopnamestring);
                        values.put(COLUMN_Addstring_ordernumber, gststring);
//                        //new
//                        values.put(COLUMN_Addexecutive, salesname);
//                        values.put(COLUMN_AddcustomerType, customertypestring1);
//                        values.put(COLUMN_Addproductname, segmentstring);
//                        values.put(COLUMN_Productcat, catagorystring);
//                        values.put(COLUMN_Addpackage, partnostring);
//                        values.put(COLUMN_Addquantity, quantity);
//                        values.put(COLUMN_Adddiscount, discount1);
//                        values.put(COLUMN_AddOrderType,"Secondary Order");
//                        values.put(COLUMN_Addcurrentdate, formattedDate);
//                        values.put(COLUMN_AddRemarks, remarks.getText().toString());
//                        values.put(COLUMN_Addcurrentdate, formattedDate);
//                        values.put(COLUMN_Unpackage, totvalues);
////                        values.put(COLUMN_Addstring_ordercolors,retailername1);
//                        values.put(COLUMN_Addstring_ordercolors,contactperson.getText().toString());
//                        values.put(COLUMN_Addstring_mail, executeid);
//                        values.put(COLUMN_AddMobileNO, mobileedit.getText().toString());
//                        values.put(COLUMN_RetAddress, addressedit.getText().toString());
//                        values.put(COLUMN_RetRegion,regionstr);
//                        values.put(COLUMN_Addcustomercode,shopname.getText().toString());
//                        values.put(COLUMN_RetEmail, emailid.getText().toString());
//                        values.put(COLUMN_Addstring_ordernumber, gst.getText().toString());
//                        values.put(COLUMN_RetState,state_spinner.getText().toString());
//                        values.put(COLUMN_RetCity, city_edit.getText().toString());
//                        //new

//                        if(stateliststring.size()!=0) {
//                            values.put(COLUMN_AddcustomerType, customertypestring1);
//                            values.put(COLUMN_RetState, stateliststring.get(0));
//                            values.put(COLUMN_RetCity, cityliststring.get(0));
//                            values.put(COLUMN_RetAddress, addressllist.get(0));
//                            values.put(COLUMN_RetRegion, regionliststring.get(0));
//                        }
//                        else {
//                            values.put(COLUMN_AddcustomerType, detail.get(0).getCustomerType());
//                            values.put(COLUMN_RetState, detail.get(0).getState());
//                            values.put(COLUMN_RetCity,detail.get(0).getCity());
//                            values.put(COLUMN_RetAddress, detail.get(0).getAddress());
//                            values.put(COLUMN_RetRegion, detail.get(0).getRegion());
//                        }
//                        values.put(COLUMN_RetEmail, retemail);
//                        values.put(COLUMN_CUSTOMERNAME, customers1);
//                        values.put(COLUMN_Addcustomercode, shopName.getText().toString());
//                        values.put(COLUMN_Addcustomercode, customercode);
                    }else {
                        values.put(COLUMN_Addexecutive, salesname);
                        values.put(COLUMN_AddcustomerType, customertypestring1);
                        values.put(COLUMN_Addproductname, segmentstring);
                        values.put(COLUMN_Productcat, catagorystring);
                        values.put(COLUMN_Addpackage, partnostring);
                        values.put(COLUMN_Addquantity, quantity);
                        values.put(COLUMN_Adddiscount, discount1);
                        values.put(COLUMN_AddOrderType, "Secondary Order");
                        values.put(COLUMN_Addcurrentdate, formattedDate);
                        values.put(COLUMN_AddRemarks, remarksstring);
                        values.put(COLUMN_Addcurrentdate, formattedDate);
                        values.put(COLUMN_Unpackage, totvalues);
//                        values.put(COLUMN_Addstring_ordercolors,getIntent().getStringExtra("shopname"));
                        values.put(COLUMN_Addstring_mail, executeid);
                        values.put(COLUMN_AddMobileNO, mobilenumber);
                        values.put(COLUMN_RetState, Statelsit);
                        if(citylist.equals("Others")){
                            values.put(COLUMN_RetCity,citylist);
                        }else {
                            values.put(COLUMN_RetCity,othecityedit.getText().toString());
                        }
                        values.put(COLUMN_RetAddress,addressstring);
                        values.put(COLUMN_RetEmail, emailidstring);
                        values.put(COLUMN_RetRegion, regionstr);
                        values.put(COLUMN_CUSTOMERNAME, Companynamestring);
                        //values.put(COLUMN_Addcustomercode, gsttext.getText().toString());
                        values.put(COLUMN_Addstring_ordercolors,conatctnamestring);
                       // values.put(COLUMN_Addcustomercode, getIntent().getStringExtra("shopname"));
                        values.put(COLUMN_Addstring_ordernumber, gststring);
//                        //new
//                        values.put(COLUMN_Addexecutive, salesname);
//                        values.put(COLUMN_AddcustomerType, customertypestring1);
//                        values.put(COLUMN_Addproductname, segmentstring);
//                        values.put(COLUMN_Productcat, catagorystring);
//                        values.put(COLUMN_Addpackage, partnostring);
//                        values.put(COLUMN_Addquantity, quantity);
//                        values.put(COLUMN_Adddiscount, discount1);
//                        values.put(COLUMN_AddOrderType,"Secondary Order");
//                        values.put(COLUMN_Addcurrentdate, formattedDate);
//                        values.put(COLUMN_AddRemarks, remarks.getText().toString());
//                        values.put(COLUMN_Addcurrentdate, formattedDate);
//                        values.put(COLUMN_Unpackage, totvalues);
//                        values.put(COLUMN_RetRegion,regionstr);
////                        values.put(COLUMN_Addstring_ordercolors,retailername1);
//                        values.put(COLUMN_Addstring_ordercolors,contactperson.getText().toString());
//                        values.put(COLUMN_Addstring_mail, executeid);
//                        values.put(COLUMN_AddMobileNO, mobileedit.getText().toString());
//                        values.put(COLUMN_RetAddress, addressedit.getText().toString());
//                     //   values.put(COLUMN_Addcustomercode,shopname.getText().toString());
//                        values.put(COLUMN_RetEmail, emailid.getText().toString());
//                        values.put(COLUMN_Addstring_ordernumber, gst.getText().toString());
//                        values.put(COLUMN_RetState,state_spinner.getText().toString());
//                        values.put(COLUMN_RetCity, city_edit.getText().toString());
//                        //new
//                        values.put(COLUMN_Addexecutive, salesname);
//                        values.put(COLUMN_AddOrderType, customers);
//                        values.put(COLUMN_Addproductname, segmentstring);
//                        values.put(COLUMN_Productcat, catagorystring);
//                        values.put(COLUMN_Addpackage, partnostring);
//                        values.put(COLUMN_Addquantity, quantity);
//                        values.put(COLUMN_Adddiscount, discount1);
//                        values.put(COLUMN_Addcurrentdate, formattedDate);
//                        values.put(COLUMN_AddRemarks, remarks.getText().toString());
//                        values.put(COLUMN_Addcurrentdate, formattedDate);
//                        values.put(COLUMN_Unpackage, totvalues);
//                        //customer name
//                        values.put(COLUMN_Addstring_ordercolors,customspinner.getText().toString());
//                        values.put(COLUMN_Addstring_mail, executeid);
//                        if(cuslist.size()!=0) {
//                            values.put(COLUMN_AddcustomerType, customerstring);
//                            values.put(COLUMN_Addstring_ordercolors, cuslist.get(0));
//                            values.put(COLUMN_AddMobileNO, mobilenolist.get(0));
//                            values.put(COLUMN_RetState, stateliststring.get(0));
//                            values.put(COLUMN_RetCity, cityliststring.get(0));
//                            values.put(COLUMN_RetAddress, addressllist.get(0));
//                            values.put(COLUMN_RetRegion, regionliststring.get(0));
//                        }
//                        else
//                        {
//                            values.put(COLUMN_AddcustomerType, detail.get(0).getCustomerType());
//                            values.put(COLUMN_Addstring_ordercolors, detail.get(0).getCustomerName());
//                            values.put(COLUMN_AddMobileNO,detail.get(0).getMobileNo());
//                            values.put(COLUMN_RetState, detail.get(0).getState());
//                            values.put(COLUMN_RetCity, detail.get(0).getCity());
//                            values.put(COLUMN_RetAddress,detail.get(0).getAddress());
//                            values.put(COLUMN_RetRegion, detail.get(0).getRegion());
//                        }
//                        values.put(COLUMN_RetEmail, retemail);
//                        values.put(COLUMN_CUSTOMERNAME, companynamestring);
////                        values.put(COLUMN_Addcustomercode, customercode);
//                        values.put(COLUMN_Addcustomercode, shopName.getText().toString());
                    }
//
//                if(customerspinner.getText().toString().equals("Retailer")){
//                    ExistingCustomerList details=new ExistingCustomerList();
//                    if(detail.size()==0) {
//                        details.setCustomerName(cuslist.get(0));
//                        details.setCustomerType(customerstring);
//                        details.setMobileNo(mob);
//                        details.setAddress(addressllist.get(0));
//                        details.setState(stateliststring.get(0));
//                        details.setCity(cityliststring.get(0));
//                        details.setRegion(regionliststring.get(0));
//                        details.setOrderType("Secondary Order");
//                        db1.adhereDao().insertExistingCustomer(details);
//                    }
//                }else {
//                    if(detail.size()==0) {
//                        ExistingCustomerList details = new ExistingCustomerList();
//                        details.setCustomerName(cuslist.get(0));
//                        details.setCustomerType(customerstring);
//                        details.setMobileNo(mob);
//                        details.setAddress(addressllist.get(0));
//                        details.setState(stateliststring.get(0));
//                        details.setCity(cityliststring.get(0));
//                        details.setRegion(regionliststring.get(0));
//                        details.setOrderType("Secondary Order");
//                        db1.adhereDao().insertExistingCustomer(details);
//                    }
//                }
                if(customertypestring1.equals("Retailer") ||customertypestring1.equals("Mechanic")|| customertypestring1.equals("Electrician")){
                    SecondaryOrderDetails details=new SecondaryOrderDetails();
                    details.setCustomerName(shopname.getText().toString());
                    details.setCustomerType(customertypestring1);
                    details.setMobileNo(mobileedit.getText().toString());
                    details.setOrderType("Secondary Order");
                    try {
                        if (customerDatabase.isOpen())
                            customerDatabase.adhereDao().insertCustomerDetails(details);
                        else {
                            Log.d(TAG, "doInBackground: customerDatabase not open");
                            customerDatabase.beginTransaction();
                            customerDatabase.adhereDao().insertCustomerDetails(details);
                        }
                    }
                    catch(Exception e)
                    {
                        e.printStackTrace();
                    }

                }else {
                    SecondaryOrderDetails details=new SecondaryOrderDetails();
                    details.setCustomerName(contactperson.getText().toString());
                    details.setCustomerType(customertypestring1);
                    details.setMobileNo(mobileedit.getText().toString());
                    details.setOrderType("Secondary Order");
                    try {
                        if (customerDatabase.isOpen())
                            customerDatabase.adhereDao().insertCustomerDetails(details);
                        else {
                            Log.d(TAG, "doInBackground: customerDatabase not open");
                            customerDatabase.beginTransaction();
                            customerDatabase.adhereDao().insertCustomerDetails(details);
                        }
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
                DBHelpersync dbHelper = new DBHelpersync(SalesOrderExistingActivity.this);
                dbs = dbHelper.getWritableDatabase();
                // dbs.execSQL("DROP TABLE  IF EXISTS "+ SQLITE.TABLE_Additems);
                dbHelper.CreateAddorderTable(dbs);
                if(customertypestring.equals("")){
                    success = dbs.insert(TABLE_Additems, null, values);
                    editor.putString("customertypestring","Secondary Order");
                    editor.putString("orders",customerstring);
                    editor.commit();
                    return "received";
                }else if(customertypestring.equals("Secondary Order")){
                    success = dbs.insert(TABLE_Additems, null, values);
                    editor.putString("customertypestring","Secondary Order");
                    editor.putString("orders",customerstring);
                    editor.commit();
                    return "received";
                }else {
                    return "Not Received";
                }


            } catch (Exception e) {

                Log.v("Error in Add local db ", "catch is working");
                Log.v("Error in Add local db", e.getMessage());
//                dbs.close();
//                customerDatabase.close();
                e.printStackTrace();
                return "Not Received";
            }

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            destroyInstance();
            if (result.equals("received")) {
                Log.v("success", Double.toString(success));
                /*startActivity(new Intent(Sales_Order_Activity.this,Sales_Order_Activity.class));
                finish();*/
                final AlertDialog alertDialog = new AlertDialog.Builder(SalesOrderExistingActivity.this).create();
                LayoutInflater inflater = getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.loginalert, null);
                alertDialog.setView(dialogView);
                TextView login =(TextView)dialogView.findViewById(R.id.login);
                TextView submit =(Button)dialogView.findViewById(R.id.submit);
                login.setText("Item Added to Cart!");
                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                         alertDialog.dismiss();
                    }
                });
                alertDialog.show();
              //  box.showAlertbox("Item Added to Cart");
//                String customerType=customerspinner.getText().toString();
////                customerspinner.getAd(customerType);
//
//                ArrayList<String> customerTypeList=new ArrayList<>();
//                customerTypeList.add(customerType);
//                customerspinner.setItems(customerTypeList);
////                createdcustomer1();
             //   mobilenumberstatus.setText("");
              //  customspinner.setText("");
                packag.setText("");
                qty.setText("");
                view.setVisibility(View.VISIBLE);
               // comname.setText("Select Company");
                //  Discountedittext.setText("");
                remarks.setText("");
                segments.setText("");
                catagory.setText("");
                packag.setEnabled(true);
                mobileedit.setEnabled(false);
                shopname.setEnabled(false);
                contactperson.setEnabled(false);
                state_spinner.setEnabled(false);
                city_edit.setEnabled(false);
                emailid.setEnabled(false);
                addressedit.setEnabled(false);
//                segments.setAdapter(new ArrayAdapter<String>(SalesOrderExistingActivity.this, android.R.layout.simple_dropdown_item_1line,segmentlist));
//                segments.setThreshold(1);Toast.makeText(getApplicationContext(), "Item added to Cart", Toast.LENGTH_LONG).show();
            } else if(result.equals("Not Received")){
                // box.showAlertbox("You have" + customertypestring +" type in the Cart. Please Place the Order first. And then add this" +customers+" type in the cart");
                final AlertDialog alertDialog = new AlertDialog.Builder(SalesOrderExistingActivity.this).create();
                LayoutInflater inflater = getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.loginalert, null);
                alertDialog.setView(dialogView);
                TextView login =(TextView)dialogView.findViewById(R.id.login);
                TextView submit =(Button)dialogView.findViewById(R.id.submit);
                login.setText("Cannot Add this Item to Cart. Please contact Admin");
                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });
                alertDialog.show();
            } else {
                final AlertDialog alertDialog = new AlertDialog.Builder(SalesOrderExistingActivity.this).create();
                LayoutInflater inflater = getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.loginalert, null);
                alertDialog.setView(dialogView);
                TextView login =(TextView)dialogView.findViewById(R.id.login);
                TextView submit =(Button)dialogView.findViewById(R.id.submit);
                login.setText("Error in adding!");
                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });
                alertDialog.show();
                //box.showAlertbox("Error in adding ");
            }
        }
    }

    private void createdcustomer1() {

    }

    private class GetDealerNamesForSpinner extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            try {
                getDealerNamesForSpinner();
                Log.v("OFF Line", "SQLITE Table");
                return "received";

            } catch (Exception e) {
                Log.v("Error in get date", "catch is working");
                Log.v("Error in Incentive", e.getMessage());
                e.printStackTrace();
                return "notsuccess";
            }

        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if(!isTableExists1()){
            }else {
//                dealerNameList.add(0,"Select Dealer");
//                dealermobilelist.add(0,"Select Mobile");
//                statelist.add(0,"Select Mobile");
//                citylist.add(0,"Select Mobile");
//                regionlist.add(0,"Select Mobile");
//                addresslist.add(0,"Select Mobile");
//                emaillist.add(0,"Select Mobile");
//                dealernametext.setItems(dealerNameList);
            }

        }
    }

//    private class GetMobilenumberspinner extends AsyncTask<String, Void, String> {
//
//        @Override
//        protected String doInBackground(String... params) {
//            // TODO Auto-generated method stub
//            try {
//                getDealerMobileForSpinner();
//                Log.v("OFF Line", "SQLITE Table");
//                return "received";
//
//            } catch (Exception e) {
//                Log.v("Error in get date", "catch is working");
//                Log.v("Error in Incentive", e.getMessage());
//                return "notsuccess";
//            }
//
//        }
//
//        @Override
//        protected void onPostExecute(String result) {
//            // TODO Auto-generated method stub
//            super.onPostExecute(result);
//            dealermobilelist.add(0, "Select MobileNumber");
//           // dealernametext.setItems(dealermobilelist);
//        }
//    }

    private void getDealerMobileForSpinner() {
        String query = "select distinct " + SQLITE.COLUMN_Create_retmobile + " from " + SQLITE.TABLE_Create_Profile + " order by " + SQLITE.COLUMN_Create_retmobile + " asc";
        Log.v("Name from createcus", query);
        DBHelper dbHelp = new DBHelper(SalesOrderExistingActivity.this);
        db = dbHelp.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                dealermobilelist.add(cursor.getString(cursor.getColumnIndex(SQLITE.COLUMN_Create_retmobile)));
            } while (cursor.moveToNext());
            cursor.close();
        }

    }
}
