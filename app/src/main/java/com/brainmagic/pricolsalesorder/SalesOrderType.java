package com.brainmagic.pricolsalesorder;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.AsyncTask;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import Home.homevalues;
import Logout.logout;
import SQL_Fields.SQLITE;
import alertbox.Alertbox;
import alertbox.PasswordChangeAlert;
import alertbox.PrimaryOrder;
import database.DBHelpersync;
import network.NetworkConnection;
import roomdb.database.AppDatabase;

import static roomdb.database.AppDatabase.getAppDatabase;

public class SalesOrderType extends AppCompatActivity implements BottomSheetNavigation.BottomSheetListener {
    private RelativeLayout order,status;
    private ImageView home,logout,setting;
    private TextView title;
    private List<String>companylist,customerslist,ordertypelist;
    private MaterialSpinner companyspinner,customertype,ordertypespin;
    private TextView ordertype;
    private TableRow ordertypetablerow;
    private Button submit;
    private String companystring,customerstring,ordertypestring;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private BottomSheetNavigation bottomSheetNavigation;
    private AppDatabase db;
    private SearchableSpinner Salesspinner;
    static Button fromdateText,todatetext;
    private List<String>customertypelist;
    private String salesname1,ordertypetopost="Secondary Order";
    private Boolean OnClick =false;
    static String fromdatestring,todatestring,ordertypeincart;
    public static final String COLUMN_AddOrderType="OrderType";
    private SQLiteDatabase sqLiteDatabase;
    private ProgressDialog progressDialog;
   private ArrayList<String> orders;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myshare = getSharedPreferences("pricolsalesorder", MODE_PRIVATE);
        editor = myshare.edit();
        setContentView(R.layout.activity_sales_order_type);
        order=(RelativeLayout)findViewById(R.id.order);
        status=(RelativeLayout)findViewById(R.id.status);
        home = (ImageView) findViewById(R.id.home);
        logout = (ImageView) findViewById(R.id.logout);
        title=(TextView)findViewById(R.id.title);
        setting=(ImageView) findViewById(R.id.setting);
        db=getAppDatabase(SalesOrderType.this);
        progressDialog = new ProgressDialog(SalesOrderType.this);
        title.setText("Sales");
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PopupMenu popupMenu = new PopupMenu(SalesOrderType.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu pop) {
                        pop.dismiss();
                    }
                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.primary:
                                PrimaryOrder alert = new PrimaryOrder(SalesOrderType.this);// new saleslogin().execute();
                                alert.showLoginboxtoggle();
                                return true;
                            case R.id.ch:
                                CheckInternetforpass();
                                return true;
                            case R.id.logout:
                                new logout(SalesOrderType.this).log_out();
                                return true;
                        }
                        return false;
                    }
                });
                popupMenu.inflate(R.menu.pricolcms);
                Menu pop = popupMenu.getMenu();
                popupMenu.show();

            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new homevalues(SalesOrderType.this).home_io();
            }
        });

        status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetNavigation = new BottomSheetNavigation();
                bottomSheetNavigation.show(getSupportFragmentManager(), "exampleBottomSheet-");



//                final android.support.v7.app.AlertDialog alertDialog = new android.support.v7.app.AlertDialog.Builder(SalesOrderType.this).create();
//
//                LayoutInflater inflater = getLayoutInflater();
//                View dialogView = inflater.inflate(R.layout.order_type, null);
//                alertDialog.setView(dialogView);
//
//                TextView primaryorder = (TextView) dialogView.findViewById(R.id.text2);
//                TextView secondaryorder = (TextView) dialogView.findViewById(R.id.text3);
//
//                primaryorder.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View arg0) {
//                        // TODO Auto-generated method stub
//                        alertDialog.dismiss();
//
//
//
//                        alertDialog.dismiss();
//                    }
//                });
//
//                secondaryorder.setOnClickListener(new View.OnClickListener() {
//
//                    @Override
//                    public void onClick(View arg0) {
//                        // TODO Auto-generated method stub
//
//                        Intent i =new Intent(SalesOrderType.this,ViewOrderStausSecondary.class).putExtra("type","Secondary Order");
//                        startActivity(i);
//
//                        // /startActivity(new Intent(Sales_segments_Activity.this,Sales_OrderNew_Activity.class));
//                        alertDialog.dismiss();
//                    }
//                });
//
//                alertDialog.show();
//                //startActivity(new Intent(SalesOrderExistingActivity.this, SelectDistributorActivity.class));
//
//            }
            }
        });
//        logout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                new logout(SalesOrderType.this).log_out();
//            }
//        });
        order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog alertDialog = new AlertDialog.Builder(SalesOrderType.this).create();
                LayoutInflater inflater = getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.order_type, null);
                alertDialog.setView(dialogView);
                companyspinner=(MaterialSpinner)dialogView.findViewById(R.id.companyspinner);
                customertype=(MaterialSpinner)dialogView.findViewById(R.id.customertype);
                ordertypespin=(MaterialSpinner)dialogView.findViewById(R.id.ordertypespin);
                ordertype=(TextView) dialogView.findViewById(R.id.ordertype);
                ordertypetablerow=(TableRow) dialogView.findViewById(R.id.ordertypetablerow);
                submit=(Button)dialogView.findViewById(R.id.submit);
                //companylist
                companylist=new ArrayList<>();
                companylist.add(0,"Company Name");
                companylist.add("Pricol");
                companylist.add("PEIL");
                companylist.add("Xenos");
                companyspinner.setItems(companylist);

                //customerlist
                customerslist=new ArrayList<>();
                customerslist.add(0,"Customer Type");
                customerslist.add("Retailer");
                customerslist.add("Mechanic");
                customerslist.add("Electrician");
                customerslist.add("End Customer");
                customerslist.add("Others");
                customertype.setItems(customerslist);

             //ordertypelist
                ordertypelist=new ArrayList<>();
                ordertypelist.add(0,"Order Type");
                ordertypelist.add("Primary Order");
                ordertypelist.add("Secondary Order");
                ordertypespin.setItems(ordertypelist);

                ordertypespin.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                        ordertypestring=item.toString();
                    }
                });

                companyspinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                        new  getexistordertype().execute();
                        companystring=item.toString();
                    }
                });
                customertype.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                        customerstring=item.toString();
                    }
                });

                if(myshare.getBoolean("primaryorder",true)){
                    ordertypetablerow.setVisibility(View.VISIBLE);
                }else {
                    ordertype.setText("Secondary Order");
                    ordertypetablerow.setVisibility(View.GONE);
                    //Action
                }


                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(myshare.getBoolean("primaryorder",true)){
                            if(companyspinner.getText().toString().equals("Company Name")){
                                companyspinner.setError("Select Company Name");
//                                StyleableToast st = new StyleableToast(SalesOrderType.this, "Select Company Name", Toast.LENGTH_SHORT);
//                                st.setBackgroundColor(SalesOrderType.this.getResources().getColor(R.color.colorPrimary));
//                                st.setTextColor(Color.WHITE);
//                                st.setMaxAlpha();
//                                st.show();

                            }else if(customertype.getText().toString().equals("Customer Type")) {
                                customertype.setError("Select Customer Type");
//                                StyleableToast st = new StyleableToast(SalesOrderType.this, "Select Company Type", Toast.LENGTH_SHORT);
//                                st.setBackgroundColor(SalesOrderType.this.getResources().getColor(R.color.colorPrimary));
//                                st.setTextColor(Color.WHITE);
//                                st.setMaxAlpha();
//                                st.show();
                            }else if(ordertypespin.getText().toString().equals("Order Type")){
                                ordertypespin.setError("Select Order Type");
//                                StyleableToast st = new StyleableToast(SalesOrderType.this, "Select Order Type", Toast.LENGTH_SHORT);
//                                st.setBackgroundColor(SalesOrderType.this.getResources().getColor(R.color.colorPrimary));
//                                st.setTextColor(Color.WHITE);
//                                st.setMaxAlpha();
//                                st.show();

                            } else {
                                Intent i =new Intent(SalesOrderType.this,SalesOrderExistingActivity.class).putExtra("companynamae",companystring).putExtra("customertype",customerstring).putExtra("ordertypes",ordertypestring);
                                startActivity(i);
                                alertDialog.dismiss();
                            }


                        }else {
                            if(companyspinner.getText().toString().equals("Company Name")){
                                companyspinner.setError("Select Company Name");
//                                StyleableToast st = new StyleableToast(SalesOrderType.this, "Select Company Name", Toast.LENGTH_SHORT);
//                                st.setBackgroundColor(SalesOrderType.this.getResources().getColor(R.color.colorPrimary));
//                                st.setTextColor(Color.WHITE);
//                                st.setMaxAlpha();
//                                st.show();

                            }else if(customertype.getText().toString().equals("Customer Type")) {
                                customertype.setError("Select Customer Type");
//                                StyleableToast st = new StyleableToast(SalesOrderType.this, "Select Company Type", Toast.LENGTH_SHORT);
//                                st.setBackgroundColor(SalesOrderType.this.getResources().getColor(R.color.colorPrimary));
//                                st.setTextColor(Color.WHITE);
//                                st.setMaxAlpha();
//                                st.show();
                            }else if(ordertype.getText().toString().equals("")){
                                ordertypespin.setError("Select Order Type");
//                                StyleableToast st = new StyleableToast(SalesOrderType.this, "Order Type", Toast.LENGTH_SHORT);
//                                st.setBackgroundColor(SalesOrderType.this.getResources().getColor(R.color.colorPrimary));
//                                st.setTextColor(Color.WHITE);
//                                st.setMaxAlpha();
//                                st.show();

                            } else if (companystring.equals(ordertypeincart)||ordertypeincart.equals("noitemsincart")){
                                Intent i =new Intent(SalesOrderType.this,SalesOrderExistingActivity.class).putExtra("companynamae",companystring).putExtra("customertype",customerstring).putExtra("ordertypes",ordertype.getText().toString());
                                startActivity(i);
                                alertDialog.dismiss();
                            }
                            else {
                                StyleableToast st = new StyleableToast(SalesOrderType.this, "You Have Another Company Items In Cart Please Place Order Then Order For" +" "+companystring+ ""+"Product", Toast.LENGTH_LONG);
                                st.setBackgroundColor(SalesOrderType.this.getResources().getColor(R.color.colorPrimary));
                                st.setTextColor(Color.WHITE);
                                st.setMaxAlpha();
                                st.show();
                            }
                        }
                    }
                });

                alertDialog.show();
            }
        });


    }

    private void CheckInternetforpass() {
        NetworkConnection network = new NetworkConnection(SalesOrderType.this);
        if (network.CheckInternet()) {
            PasswordChangeAlert alert = new PasswordChangeAlert(SalesOrderType.this);// new saleslogin().execute();
            alert.showLoginbox();
        } else {
            Alertbox alert = new Alertbox(SalesOrderType.this);
            alert.showAlertbox("Kindly check your Internet Connection");
        }
    }

    private void getnewoptions() {
        final AlertDialog alertDialog = new AlertDialog.Builder(SalesOrderType.this).create();

        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.new_order_alertbox, null);
        alertDialog.setView(dialogView);

        TextView existing = (TextView) dialogView.findViewById(R.id.text2);
        TextView newcustomer = (TextView) dialogView.findViewById(R.id.text3);

        existing.setText("Create Profile");
        newcustomer.setText("Order");
        existing.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                alertDialog.dismiss();
                startActivity(new Intent(SalesOrderType.this,CreateNewActivity.class).putExtra("Order",true));
            }
        });

        newcustomer.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                startActivity(new Intent(SalesOrderType.this,Sales_OrderNew_Activity.class));
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }

    @Override
    public void bottomSheetListener(String orderhistoryfilter) {
        if (orderhistoryfilter.equals("currentmonthwise")) {
            Intent i = new Intent(SalesOrderType.this, ViewOrderStaus.class).putExtra("type", "Secondary Order");
            startActivity(i);
            bottomSheetNavigation.dismiss();

        } else {
            final AlertDialog alertDialog = new AlertDialog.Builder(SalesOrderType.this).create();
            LayoutInflater inflater = getLayoutInflater();

            View dialogView = inflater.inflate(R.layout.viewdatewisehistory, null);
            alertDialog.setView(dialogView);
            Salesspinner = (SearchableSpinner) dialogView.findViewById(R.id.Salesspinner);
            fromdateText = (Button) dialogView.findViewById(R.id.fromdateText);
            todatetext = (Button) dialogView.findViewById(R.id.todatetext);
            Button Search = (Button) dialogView.findViewById(R.id.Search);
            customertypelist=new ArrayList<>();
            customertypelist.add("All");
            customertypelist.add("Retailer");
            customertypelist.add("Mechanic");
            customertypelist.add("Electrician");
            customertypelist.add("End Customer");
            customertypelist.add("Others");
            ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(SalesOrderType.this,R.layout.simple_spinner_item,customertypelist);
            spinnerAdapter.setNotifyOnChange(true);
            spinnerAdapter.setDropDownViewResource(R.layout.simple_spinner_item);
            Salesspinner.setAdapter(spinnerAdapter);

            Salesspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if(salesname1 == null){
                        salesname1=Salesspinner.getSelectedItem().toString();
                    }else {
                        salesname1=Salesspinner.getSelectedItem().toString();
                        OnClick =true;
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
            Search.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(fromdateText.getText().toString().equals("")){
                        //fromdateText.setError("Select From Date");
                            StyleableToast st = new StyleableToast(SalesOrderType.this, "Select From Date", Toast.LENGTH_SHORT);
                            st.setBackgroundColor(SalesOrderType.this.getResources().getColor(R.color.colorPrimary));
                            st.setTextColor(Color.WHITE);
                            st.setMaxAlpha();
                            st.show();

                    }else if(todatetext.getText().toString().equals("")){
                      //  todatetext.setError("Select To Date");
                            StyleableToast st = new StyleableToast(SalesOrderType.this, "Select To Date", Toast.LENGTH_SHORT);
                            st.setBackgroundColor(SalesOrderType.this.getResources().getColor(R.color.colorPrimary));
                            st.setTextColor(Color.WHITE);
                            st.setMaxAlpha();
                            st.show();
                    }else {
                        if(OnClick == true){
                            if(salesname1.equals("All")){
                                Intent i =new Intent(SalesOrderType.this,ViewOrderAllUsertype.class).putExtra("fromdate",fromdatestring).putExtra("todate",todatestring).putExtra("usertypecus",Salesspinner.getSelectedItem().toString());
                                startActivity(i);
                                alertDialog.dismiss();
                            }else {

                                Intent i =new Intent(SalesOrderType.this,ViewOrderAllUsertype.class).putExtra("fromdate",fromdatestring).putExtra("todate",todatestring).putExtra("usertypecus",Salesspinner.getSelectedItem().toString());
                                startActivity(i);
                                alertDialog.dismiss();
                            }

                        }else {
                            if(salesname1.equals("All")){
                                Intent i =new Intent(SalesOrderType.this,ViewOrderAllUsertype.class).putExtra("fromdate",fromdatestring).putExtra("todate",todatestring).putExtra("usertypecus",Salesspinner.getSelectedItem().toString());
                                startActivity(i);
                                alertDialog.dismiss();
                            }
                        }



                    }

                }
            });

            fromdateText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DialogFragment newFragment = new FromDatePickerFragment();
                    newFragment.show(getSupportFragmentManager(),"datePicker");
                }
            });

            Salesspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int postion, long id) {
                    if(salesname1 == null){
                        salesname1=Salesspinner.getSelectedItem().toString();

                    }else {
                        salesname1=Salesspinner.getSelectedItem().toString();
                        OnClick =true;
                    }





                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            todatetext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DialogFragment newFragment = new FromDatePickerFragment1();
                    newFragment.show(getSupportFragmentManager(),"datePicker");
                }
            });

            alertDialog.show();


        }

//            final AlertDialog alertDialog = new AlertDialog.Builder(SalesOrderType.this).create();
//            LayoutInflater inflater = getLayoutInflater();
//            View dialogView = inflater.inflate(R.layout.loginalert, null);
//            alertDialog.setView(dialogView);
//            TextView login =(TextView)dialogView.findViewById(R.id.login);
//            login.setText("This page is Under Construction! Sorry for inconvenience.");
//            Button submit =(Button) dialogView.findViewById(R.id.submit);
//            submit.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    alertDialog.dismiss();
//
//                }
//
//            });
//            alertDialog.show();


//            final AlertDialog alertDialog = new AlertDialog.Builder(SalesOrderType.this).create();
//            LayoutInflater inflater = getLayoutInflater();
//            View dialogView = inflater.inflate(R.layout.order_type, null);
//            alertDialog.setView(dialogView);
//            companyspinner = (MaterialSpinner) dialogView.findViewById(R.id.companyspinner);
//            customertype = (MaterialSpinner) dialogView.findViewById(R.id.customertype);
//            ordertypespin = (MaterialSpinner) dialogView.findViewById(R.id.ordertypespin);
//            ordertype = (TextView) dialogView.findViewById(R.id.ordertype);
//            ordertypetablerow = (TableRow) dialogView.findViewById(R.id.ordertypetablerow);
//            submit = (Button) dialogView.findViewById(R.id.submit);
//            //companylist
//            companylist = new ArrayList<>();
//            companylist.add(0, "Company Name");
//            companylist.add("Pricol");
//            companylist.add("PEIL");
//            companylist.add("Xenos");
//            companyspinner.setItems(companylist);
//
//            //customerlist
//            customerslist = new ArrayList<>();
//            customerslist.add(0, "Customer Type");
//            customerslist.add("Retailer");
//            customerslist.add("Mechanic");
//            customerslist.add("Electrician");
//            customerslist.add("End Customer");
//            customerslist.add("Others");
//            customertype.setItems(customerslist);
//
//            //ordertypelist
//            ordertypelist = new ArrayList<>();
//            ordertypelist.add(0, "Order Type");
//            ordertypelist.add("Primary Order");
//            ordertypelist.add("Secondary Order");
//            ordertypespin.setItems(ordertypelist);
//
//            ordertypespin.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
//                @Override
//                public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
//                    ordertypestring = item.toString();
//                }
//            });
//
//
//            companyspinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
//                @Override
//                public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
//                    companystring = item.toString();
//                }
//            });
//            customertype.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
//                @Override
//                public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
//                    customerstring = item.toString();
//                }
//            });
//            if (myshare.getBoolean("primaryorder", true)) {
//                ordertypetablerow.setVisibility(View.VISIBLE);
//            } else {
//                ordertype.setText("Secondary Order");
//                ordertypetablerow.setVisibility(View.GONE);
//                //Action
//            }
//
//
//            submit.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    if (myshare.getBoolean("primaryorder", true)) {
//                        if (companyspinner.getText().toString().equals("Company Name")) {
//                            companyspinner.setError("Select Company Name");
////                                StyleableToast st = new StyleableToast(SalesOrderType.this, "Select Company Name", Toast.LENGTH_SHORT);
////                                st.setBackgroundColor(SalesOrderType.this.getResources().getColor(R.color.colorPrimary));
////                                st.setTextColor(Color.WHITE);
////                                st.setMaxAlpha();
////                                st.show();
//
//                        } else if (customertype.getText().toString().equals("Customer Type")) {
//                            customertype.setError("Select Customer Type");
////                                StyleableToast st = new StyleableToast(SalesOrderType.this, "Select Company Type", Toast.LENGTH_SHORT);
////                                st.setBackgroundColor(SalesOrderType.this.getResources().getColor(R.color.colorPrimary));
////                                st.setTextColor(Color.WHITE);
////                                st.setMaxAlpha();
////                                st.show();
//                        } else if (ordertypespin.getText().toString().equals("Order Type")) {
//                            ordertypespin.setError("Select Order Type");
////                                StyleableToast st = new StyleableToast(SalesOrderType.this, "Select Order Type", Toast.LENGTH_SHORT);
////                                st.setBackgroundColor(SalesOrderType.this.getResources().getColor(R.color.colorPrimary));
////                                st.setTextColor(Color.WHITE);
////                                st.setMaxAlpha();
////                                st.show();
//
//                        } else {
//                            Intent i = new Intent(SalesOrderType.this, SalesOrderExistingActivity.class).putExtra("companynamae", companystring).putExtra("customertype", customerstring).putExtra("ordertypes", ordertypestring);
//                            startActivity(i);
//                            alertDialog.dismiss();
//                        }
//
//
//                    } else {
//                        if (companyspinner.getText().toString().equals("Company Name")) {
//                            companyspinner.setError("Select Company Name");
////                                StyleableToast st = new StyleableToast(SalesOrderType.this, "Select Company Name", Toast.LENGTH_SHORT);
////                                st.setBackgroundColor(SalesOrderType.this.getResources().getColor(R.color.colorPrimary));
////                                st.setTextColor(Color.WHITE);
////                                st.setMaxAlpha();
////                                st.show();
//
//                        } else if (customertype.getText().toString().equals("Customer Type")) {
//                            customertype.setError("Select Customer Type");
////                                StyleableToast st = new StyleableToast(SalesOrderType.this, "Select Company Type", Toast.LENGTH_SHORT);
////                                st.setBackgroundColor(SalesOrderType.this.getResources().getColor(R.color.colorPrimary));
////                                st.setTextColor(Color.WHITE);
////                                st.setMaxAlpha();
////                                st.show();
//                        } else if (ordertype.getText().toString().equals("")) {
//                            ordertypespin.setError("Select Order Type");
////                                StyleableToast st = new StyleableToast(SalesOrderType.this, "Order Type", Toast.LENGTH_SHORT);
////                                st.setBackgroundColor(SalesOrderType.this.getResources().getColor(R.color.colorPrimary));
////                                st.setTextColor(Color.WHITE);
////                                st.setMaxAlpha();
////                                st.show();
//
//                        } else {
//                            Intent i = new Intent(SalesOrderType.this, SalesOrderExistingActivity.class).putExtra("companynamae", companystring).putExtra("customertype", customerstring).putExtra("ordertypes", ordertype.getText().toString());
//                            startActivity(i);
//                            alertDialog.dismiss();
//                        }
//
//                    }
//
//
//                }
//            });
//
//            alertDialog.show();
//
//            Intent i = new Intent(SalesOrderType.this, ViewOrderStaus.class).putExtra("type", "Secondary Order");
//            startActivity(i);
            bottomSheetNavigation.dismiss();

        }

    public static class FromDatePickerFragment extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);
            dialog.getDatePicker().setMaxDate(new Date().getTime());
            return dialog;
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            //FromDate.setText(day + "/" + (month + 1) + "/" + year);
            fromdateText.setText(day + "-" + (month + 1) + "-" + year);
            fromdatestring=year + "-" + (month + 1) + "-" + day;
            fromdateText.clearFocus();
        }
    }

    public static class FromDatePickerFragment1 extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);
            dialog.getDatePicker().setMaxDate(new Date().getTime());
            return dialog;
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            //FromDate.setText(day + "/" + (month + 1) + "/" + year);
            todatetext.setText(day + "-" + (month + 1) + "-" + year);
            todatestring=year + "-" + (month + 1) + "-" + day;
            todatetext.clearFocus();
        }
    }


    private class getexistordertype extends AsyncTask<String,Void,String > {

        DBHelpersync dbhelper = new DBHelpersync(SalesOrderType.this);
        Cursor cursor;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            orders=new ArrayList<>();
            sqLiteDatabase = dbhelper.getWritableDatabase();
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Loading...");
            progressDialog.show();
        }



        @SuppressLint("Range")
        @Override
        protected String doInBackground(String... strings) {
            Log.v("OFF Line", "SQLITE Table");
            try {
                //String query ="select * from Orderpreview order by formattedDate asc";
                String query = "select * from Orderpreview where OrderType ='" + ordertypetopost + "'";
//                String query = "select * from " + SQLITE.TABLE_Additems + " order by formattedDate asc";
                Log.v("Dealer Name", query);
                cursor = sqLiteDatabase.rawQuery(query, null);
                if (cursor.moveToFirst()) {
                    do {
                         ordertypeincart=(cursor.getString(cursor.getColumnIndex(SQLITE.COLUMN_CUSTOMERNAME)));
                    } while (cursor.moveToNext());
                    cursor.close();
//                    db.close();
                    return "received";
                } else {
                    cursor.close();
//                    db.close();
                    return "nodata";
                }
            } catch (Exception e) {
                if (cursor != null)
                    cursor.close();
//                db.close();
                Log.v("Error in get date", "catch is working");
                Log.v("Error in Incentive", e.getMessage());
                return "nodata";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            switch (s){
                case "received":
                    break;
                case "nodata":
                    ordertypeincart="noitemsincart";
                    break;
            }
        }
    }
}


