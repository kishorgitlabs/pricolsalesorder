package com.brainmagic.pricolsalesorder;

import android.app.ProgressDialog;
import android.content.SharedPreferences;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

import Home.homevalues;
import Logout.logout;
import adapter.ViewAllItemAdapter;
import alertbox.Alertbox;
import alertbox.PrimaryOrder;
import api.models.AttendanceAllTimeData.AttendanceAll;
import api.models.AttendanceAllTimeData.AttendanceAllData;
import api.retrofit.APIService;
import api.retrofit.RetrofitClient;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VIewMultipleExecutiveActivity extends AppCompatActivity {
    private ListView listattendancesearch;
    private Alertbox box = new Alertbox(this);
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private String date;
    private int id;
    private List<AttendanceAll> data;
    private ImageView menu, back,home,logout,setting;
    private TextView title;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_multiple_executive);
        myshare = getSharedPreferences("pricolsalesorder", MODE_PRIVATE);
        editor = myshare.edit();
        listattendancesearch=(ListView)findViewById(R.id.listattendancesearch);
        home = (ImageView) findViewById(R.id.home);
        logout = (ImageView) findViewById(R.id.logout);
        back = (ImageView) findViewById(R.id.back);
        setting=(ImageView) findViewById(R.id.setting);
        title=(TextView) findViewById(R.id.title);
        title.setText("View Attendance");
        date=getIntent().getStringExtra("Datereg");
        id=getIntent().getIntExtra("empid",0);

        GetDateView();
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new homevalues(VIewMultipleExecutiveActivity.this).home_io();
            }
        });

        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PopupMenu popupMenu = new PopupMenu(VIewMultipleExecutiveActivity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu pop) {
                        pop.dismiss();
                    }
                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.primary:
                                PrimaryOrder alert = new PrimaryOrder(VIewMultipleExecutiveActivity.this);// new saleslogin().execute();
                                alert.showLoginboxtoggle();
                                return true;
                            case R.id.ch:
                                CheckInternetforpass();
                                return true;
                            case R.id.logout:
                                new logout(VIewMultipleExecutiveActivity.this).log_outReg();
                                return true;
                        }
                        return false;
                    }
                });
                popupMenu.inflate(R.menu.pricolcms);
                Menu pop = popupMenu.getMenu();
                popupMenu.show();

            }
        });


    }

    private void CheckInternetforpass() {
        NetworkConnection network = new NetworkConnection(VIewMultipleExecutiveActivity.this);
        if (network.CheckInternet()) {
            PasswordChangeAlertRegion alert = new PasswordChangeAlertRegion(VIewMultipleExecutiveActivity.this);// new saleslogin().execute();
            alert.showLoginbox();
        } else {
            final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(VIewMultipleExecutiveActivity.this).create();
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.loginalert, null);
            alertDialog.setView(dialogView);
            TextView login =(TextView)dialogView.findViewById(R.id.login);
            login.setText("Please Check Your internet Connection !");
            Button submit =(Button) dialogView.findViewById(R.id.submit);
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();

                }

            });
            alertDialog.show();
        }
    }

    private void GetDateView() {
        try {

            final ProgressDialog loading = ProgressDialog.show(VIewMultipleExecutiveActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);
            APIService service = RetrofitClient.getApiService();
            Call<AttendanceAllData> call = service.GetAttendanceAllData(date, id);
            call.enqueue(new Callback<AttendanceAllData>() {
                @Override

                public void onResponse(Call<AttendanceAllData> call, Response<AttendanceAllData> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success")) {
                            data=response.body().getData();
                            ViewAllItemAdapter viewAllItemAdapter=new ViewAllItemAdapter(VIewMultipleExecutiveActivity.this,data);
                            listattendancesearch.setAdapter(viewAllItemAdapter);

                        } else if(response.body().getResult().equals("InvalidUser")) {
                            final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(VIewMultipleExecutiveActivity.this).create();
                            LayoutInflater inflater = getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.loginalert, null);
                            alertDialog.setView(dialogView);
                            TextView login =(TextView)dialogView.findViewById(R.id.login);
                            login.setText("Invalid User !");
                            Button submit =(Button) dialogView.findViewById(R.id.submit);
                            submit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    alertDialog.dismiss();

                                }

                            });
                            alertDialog.show();
                        }else
                        {
                            final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(VIewMultipleExecutiveActivity.this).create();
                            LayoutInflater inflater = getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.loginalert, null);
                            alertDialog.setView(dialogView);
                            TextView login =(TextView)dialogView.findViewById(R.id.login);
                            login.setText("Something went wrong .Please try again later .");
                            Button submit =(Button) dialogView.findViewById(R.id.submit);
                            submit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    alertDialog.dismiss();

                                }

                            });
                            alertDialog.show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(VIewMultipleExecutiveActivity.this).create();
                        LayoutInflater inflater = getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.loginalert, null);
                        alertDialog.setView(dialogView);
                        TextView login =(TextView)dialogView.findViewById(R.id.login);
                        login.setText("Something went wrong .Please try again later .");
                        Button submit =(Button) dialogView.findViewById(R.id.submit);
                        submit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                alertDialog.dismiss();

                            }

                        });
                        alertDialog.show();

                    }
                }

                @Override
                public void onFailure(Call<AttendanceAllData> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(VIewMultipleExecutiveActivity.this).create();
                    LayoutInflater inflater = getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.loginalert, null);
                    alertDialog.setView(dialogView);
                    TextView login =(TextView)dialogView.findViewById(R.id.login);
                    login.setText("Something went wrong .Please try again later .");
                    Button submit =(Button) dialogView.findViewById(R.id.submit);
                    submit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();

                        }

                    });
                    alertDialog.show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
    }

