package com.brainmagic.pricolsalesorder;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

public class Splash_Activity extends AppCompatActivity {
    private static final long SPLASH_DISPLAY_LENGTH = 1500;
    Boolean islogin,isLoginreg;
    SharedPreferences myshare;
    SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_);
        myshare = getSharedPreferences("pricolsalesorder", MODE_PRIVATE);
        editor = myshare.edit();
        islogin = myshare.getBoolean("isLogin",false);
        isLoginreg = myshare.getBoolean("isLoginreg",false);

        //* and close this Splash-Screen after some seconds.*/
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
            if(islogin){
                Intent i =new Intent(Splash_Activity.this,HomeActivty.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);

            }else {
                if(isLoginreg){
                    Intent i =new Intent(Splash_Activity.this,RegionHomeActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                }else {
                    Intent i =new Intent(Splash_Activity.this,LoginActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                }

            }



            }
        }, SPLASH_DISPLAY_LENGTH);
    }


}
