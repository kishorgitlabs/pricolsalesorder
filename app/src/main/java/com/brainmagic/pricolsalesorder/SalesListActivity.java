package com.brainmagic.pricolsalesorder;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

import adapter.CreateListAdapter;
import api.models.master.createlist;
import roomdb.database.AppDatabase;

import static roomdb.database.AppDatabase.getAppDatabase;

public class SalesListActivity extends AppCompatActivity {
    private ListView listselect;
    private AppDatabase db;
    private CreateListAdapter createListAdapter;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private int Salesid;
    private TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_list);
        listselect=(ListView)findViewById(R.id.listvieworder);
        title=(TextView) findViewById(R.id.title);
        title.setText("Created Customer List");
        db = getAppDatabase(SalesListActivity.this);
        myshare = getSharedPreferences("pricolsalesorder", MODE_PRIVATE);
        editor = myshare.edit();
        Salesid=myshare.getInt("idlist",0);
        if(db.adhereDao().GetCustomer_field().size()!=0)
            GetValues(db.adhereDao().GetCustomer_field());
        else {
            final android.app.AlertDialog dialog = new android.app.AlertDialog.Builder(SalesListActivity.this).create();
            dialog.setTitle(R.string.app_name);
            dialog.setMessage("Customer List is Empty. Please add new Customer");
            dialog.setButton("Okay", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {

                    Intent i =new Intent(SalesListActivity.this,CreateNewActivity.class);
                    startActivity(i);
                }
            });
            dialog.show();
        }
    }

    private void GetValues(List<createlist> createlists) {
        createListAdapter=new CreateListAdapter(SalesListActivity.this,createlists,Salesid);
        listselect.setAdapter(createListAdapter);

    }
}
