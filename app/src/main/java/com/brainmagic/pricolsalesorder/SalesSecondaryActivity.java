package com.brainmagic.pricolsalesorder;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.jaredrummler.materialspinner.MaterialSpinner;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.TransformerUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import Home.homevalues;
import Logout.logout;
import SQL_Fields.SQLITE;
import alertbox.Alertbox;
import alertbox.PasswordChangeAlert;
import alertbox.PrimaryOrder;
import api.models.deal;
import api.models.master.Customer_list;
import api.models.master.Distributor_list;
import api.models.master.Productlist;
import api.models.master.createlist;
import database.DBHelper;
import database.DBHelpersync;
import network.NetworkConnection;
import roomdb.database.AppDatabase;
import roomdb.entity.SecondaryOrderDetails;

import static roomdb.database.AppDatabase.getAppDatabase;

public class SalesSecondaryActivity extends AppCompatActivity {
    //private SearchableSpinner dealerspinner;
    private Spinner dealerspinner;
    private AutoCompleteTextView productcatagory,productname,packag,mobilenumberstatus;
    private TextView catagory,segments,customspinner;
    private EditText qty,remarks,discount;
    private MaterialSpinner ordertype,distributname,dealername,dealernametext,customerspinner,comname;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private Button add,view;
    private TextView mrp1;
    SQLiteDatabase db;
    DBHelper dbHelper;
    Cursor c;
    private static final String TAG = "SalesSecondaryActivity";
    private List<String> productsegment;
    private List<String> productcatagorylist;
    private List<String> partlist;
    private List<String> ordertypelist;
    private List<String> dealerNameList,dealermobilelist,statelist,citylist,regionlist,addresslist,emaillist,distributeliststring,distributidlist,customerslist;
    private String segmentstring,ordertypestring,catagorystring,partnostring;
    private ImageView menu;
    public ProgressDialog progressDialog;
    private Alertbox box = new Alertbox(SalesSecondaryActivity.this);
    // Table Additems
    private static final String TABLE_Additems = "Orderpreview";
    public static final String COLUMN_Addexecutive = "SalesName";
    public static final String COLUMN_AddcustomerType = "CustomerType";
    public static final String COLUMN_Addproductname = "ProductName";
    public static final String COLUMN_Addpackage = "Package";
    public static final String COLUMN_Addquantity = "quantity";
    public static final String COLUMN_Adddiscount = "discount";
    public static final String COLUMN_AddOrderType="OrderType";
    public static final String COLUMN_Addcustomercode="Customercode";
    public static final String COLUMN_Addcurrentdate="formattedDate";
    public static final String COLUMN_Productcat="ProductCatagory";
    public static final String COLUMN_AddMobileNO="MobileNO";
    public static final String COLUMN_Addstring_mail="string_mail";
    public static final String COLUMN_Addstring_ordermode="order_mode";
    private static final String COLUMN_AddNames="AddNames";
    private static final String COLUMN_AddRemarks="Remarks";
    private static final String COLUMN_Addstring_ordernumber="numbers";
    private static final String COLUMN_Addstring_ordercolors="colours";
    private static final String COLUMN_Unpackage="Unpackage";
    private static final String COLUMN_RetState="retailerstate";
    private static final String COLUMN_RetCity="retailercity";
    private static final String COLUMN_RetAddress="Retaddress";
    private static final String COLUMN_RetEmail="Retemail";
    private static final String COLUMN_RetRegion="Retregion";
    public static final String COLUMN_CUSTOMERNAME="CustomerNameDealer";
    //    public static final String COLUMN_Execute_id="salesExecuteId";
//    public static final String COLUMN_Distirbute_id="distributeId";
    private String formattedDate;
    private static final int PERMISSION_REQUEST = 100;
    private List<String>Namelist;
    private List<String>productcatlist;
    private List<Productlist>partnolist;
    private String customers;
    private String salesname,quantity,totalvalues;
    private int totqty,executeid;
    private int totvalues;
    List<Object>mrptcat;
    private TableRow distributrow,dealerrow,dealerrowtext,cusrow,customertype,mobilenumberrow,companyname,shopNameView;
    private String distiname,retailername1,distid;
    private ImageView addretailer,addretailer1;
    private String retailename = "";
    private Boolean addretailerbol=false;
    private TextView shopName;
    List<Object>distributor,dealerObjectNameList,dealerObjectmobilelist,stateObjectlist,cityObjectlist,regionObjectlist,addressObjectlist,emailObjectlist,segmentobjectlist,catagoryobjectlist,customerobject,customercodeobject;
    List<String> list = new ArrayList<String>();
    private String retmobilenumber,retstate,retcity,retaddress,retemail,retregion,distiid,customers1,customercode,customerstring;
    private ArrayList<String>Distilistname=new ArrayList<String>();
    private List<String>partlistauto,segmentlist,catagorylist,customerlist,customercodelist=new ArrayList<String>();
    private List<String> mobilelistlist=new ArrayList<String>();
    private List<String> cuslist=new ArrayList<String>();
    private List<String> typeoffacclist=new ArrayList<String>();
    private List<String> regionliststring=new ArrayList<String>();
    private List<String> stateliststring=new ArrayList<String>();
    private List<String> cityliststring=new ArrayList<String>();
    private List<String> mobilenolist=new ArrayList<String>();
    private List<String> addressllist=new ArrayList<String>();
    private List<String>companylist=new ArrayList<>();
    private List<String> shopNameList=new ArrayList<String>();
    private TableRow selectordertype,secondaryorder;
    private String mob,companynamestring;
    private String customertypestring = "",choosen,ordertypestring1;
    List<Object>customername,typeofaccount,region,state,city,mobileno,address;
    private  AppDatabase db1;

    //new
    //new
    private TextView mobiletext,shopnametext,contactpersontext,regiontext,statetext,citytext,emailidtext,addresstext,gsttext;
    private TableRow shplinear;
    private String Companynamestring,customertypestring1,ordertypeString;

    private ImageView home,logout,setting;
    private TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_secondary);
        shopName=findViewById(R.id.shop_names);
        shopNameView=findViewById(R.id.shop_name_row);
        myshare = getSharedPreferences("pricolsalesorder", MODE_PRIVATE);
        editor = myshare.edit();
        choosen=getIntent().getStringExtra("chooseactivity");
        db1=getAppDatabase(SalesSecondaryActivity.this);
        addretailerbol=myshare.getBoolean("addretailerbol",addretailerbol);
        customertypestring=myshare.getString("customertypestring","");
        salesname=myshare.getString("name","");
        executeid=myshare.getInt("idlist",0);
        home = (ImageView) findViewById(R.id.home);
        logout = (ImageView) findViewById(R.id.logout);
        //new
        mobiletext=findViewById(R.id.mobiletext);
        shopnametext=findViewById(R.id.shopnametext);
        contactpersontext=findViewById(R.id.contactpersontext);
        regiontext=findViewById(R.id.region);
        statetext=findViewById(R.id.statetext);
        citytext=findViewById(R.id.citytext);
        emailidtext=findViewById(R.id.emailidtext);
        addresstext=findViewById(R.id.addresstext);
        gsttext=findViewById(R.id.gsttext);
        shplinear=findViewById(R.id.shplinear);
        Companynamestring=getIntent().getStringExtra("companynamae");
        customertypestring1=getIntent().getStringExtra("customertype");
        ordertypeString=getIntent().getStringExtra("ordertypes");
        home = (ImageView) findViewById(R.id.home);
        title=(TextView)findViewById(R.id.title);
        setting=(ImageView) findViewById(R.id.setting);
        if(customertypestring1.equals("Retailer")||customertypestring1.equals("Mechanic")|| customertypestring1.equals("Electrician") ){
            shplinear.setVisibility(View.VISIBLE);
        }else {
            shplinear.setVisibility(View.GONE);
        }

//        db1 = getAppDatabase(SalesSecondaryActivity.this);
       // dealerspinner=(Spinner) findViewById(R.id.dealerspinner);
        packag=(AutoCompleteTextView) findViewById(R.id.packagespinner);
        qty=(EditText)findViewById(R.id.quantityedittext);
        discount=(EditText) findViewById(R.id.discountedittext);
      //  comname=(MaterialSpinner) findViewById(R.id.comname);
        mrp1=(TextView) findViewById(R.id.mrp1);
      //  dealernametext=(MaterialSpinner) findViewById(R.id.dealernametext);
        remarks=(EditText)findViewById(R.id.remark);
      //  mobilenumberstatus=(AutoCompleteTextView) findViewById(R.id.mobilenumberstatus);
      //  ordertype=(MaterialSpinner)findViewById(R.id.ordertypespinner);
        segments=(TextView) findViewById(R.id.segments);
        catagory=(TextView) findViewById(R.id.catagory);
      //  distributname=(MaterialSpinner) findViewById(R.id.distribute);
      //  dealername=(MaterialSpinner) findViewById(R.id.dealername);
       // customspinner=(TextView) findViewById(R.id.customspinner);
       // customerspinner=(MaterialSpinner) findViewById(R.id.customerspinner);
      //  distributrow=(TableRow)findViewById(R.id.distributrow);
       // dealerrow=(TableRow)findViewById(R.id.dealerrow) ;
      //  cusrow=(TableRow)findViewById(R.id.cusrow) ;
      //  customertype=(TableRow)findViewById(R.id.customertype) ;
     //   dealerrowtext=(TableRow)findViewById(R.id.dealerrowtext) ;
      //  mobilenumberrow=(TableRow)findViewById(R.id.mobilenumberrow) ;
      //  companyname=(TableRow)findViewById(R.id.companyname) ;
      //  addretailer=(ImageView) findViewById(R.id.retailer) ;
      //  addretailer1=(ImageView) findViewById(R.id.retailer1) ;

        selectordertype=findViewById(R.id.selectordertype);
        secondaryorder=findViewById(R.id.secondaryorder);
        ordertypestring1=getIntent().getStringExtra("ordertype");
        mobiletext.setText(getIntent().getStringExtra("mobilestring"));
        shopnametext.setText(getIntent().getStringExtra("shopname"));
        contactpersontext.setText(getIntent().getStringExtra("newShops"));
        regiontext.setText(getIntent().getStringExtra("regionstring"));
        statetext.setText(getIntent().getStringExtra("states"));
        citytext.setText(getIntent().getStringExtra("citys"));
        emailidtext.setText(getIntent().getStringExtra("emailstring"));
        addresstext.setText(getIntent().getStringExtra("address"));
        gsttext.setText(getIntent().getStringExtra("gst"));
        List<SecondaryOrderDetails>details=db1.adhereDao().customerDetails();
        title.setText("Sales Order");
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new homevalues(SalesSecondaryActivity.this).home_io();

            }
        });
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PopupMenu popupMenu = new PopupMenu(SalesSecondaryActivity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu pop) {
                        pop.dismiss();
                    }
                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.primary:
                                PrimaryOrder alert = new PrimaryOrder(SalesSecondaryActivity.this);// new saleslogin().execute();
                                alert.showLoginboxtoggle();
                                return true;
                            case R.id.ch:
                                CheckInternetforpass();
                                return true;
                            case R.id.logout:
                                new logout(SalesSecondaryActivity.this).log_out();
                                return true;
                        }
                        return false;
                    }
                });
                popupMenu.inflate(R.menu.pricolcms);
                Menu pop = popupMenu.getMenu();
                popupMenu.show();

            }
        });
        if(details.size()!=0)
        {
            if(details.get(0).getMobileNo().contains(getIntent().getStringExtra("mobilestring"))){
                // no action


            }else {
                final android.app.AlertDialog dialog = new android.app.AlertDialog.Builder(SalesSecondaryActivity.this).create();
                dialog.setTitle(R.string.app_name);
                dialog.setMessage("Product have been added for this customer "+details.get(0).getCustomerName()+". Please place order for this customer or move to local and then add Products for another customer");
                dialog.setButton("Okay", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        Intent i =new Intent(SalesSecondaryActivity.this,SalesConfirmDealerActivity.class).putExtra("ordertypevalues", myshare.getString("customertypestring","")).putExtra("salesType","New");
                        startActivity(i);
                       // finish();
                    }
                });
                dialog.show();
            }

        }
        else {
            // no action

        }


        add=(Button)findViewById(R.id.add);
        view=(Button)findViewById(R.id.order);
        Date d = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(d);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formattedDate = df.format(c.getTime());
        dealerNameList=new ArrayList<>();
        dealermobilelist=new ArrayList<>();
        statelist=new ArrayList<>();
        citylist=new ArrayList<>();
        regionlist=new ArrayList<>();
        addresslist=new ArrayList<>();
        emaillist=new ArrayList<>();
        distributeliststring=new ArrayList<>();
        distributidlist=new ArrayList<>();

        if(Companynamestring.equals("Pricol")){
            String type="Spares";
            GetPartnumber(type);
        }else if(Companynamestring.equals("PEIL")){
            String type="PEIL";
            GetPartnumber(type);

        }else if(Companynamestring.equals("Xenos")){
            String type="Accessories";
            GetPartnumber(type);

        }
//        companylist.add(0,"Select Company Name");
//        companylist.add("Pricol");
//        companylist.add("PEIL");
//        comname.setItems(companylist);

//        comname.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
//                companynamestring=item.toString();
//            }
//        });

//        customerspinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
//                customerstring =item.toString();
//                if(customerstring.equals("Retailer")||customerstring.equals("Mechanic"))
//                {
//                    shopNameView.setVisibility(View.VISIBLE);
//                }
//                else {
//                    shopNameView.setVisibility(View.GONE);
//                }
//                GetValues(db1.adhereDao().GetCrestestring(customerstring));
//            }
//        });


       // new SalesSecondaryActivity.GetDealerNamesForSpinner().execute();
        //  new GetMobilenumberspinner().execute();
//        if(addretailerbol){
//            dealerrowtext.setVisibility(View.GONE);
//        }else {
//            dealerrowtext.setVisibility(View.VISIBLE);
//        }
//        ordertype.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
//                ordertypestring=item.toString();
//
//            }
//        });
//        home.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                new homevalues(SalesSecondaryActivity.this).home_io();
//            }
//        });
//        logout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                new logout(SalesSecondaryActivity.this).log_out();
//            }
//        });

//        back.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                onBackPressed();
//            }
//        });
//        dealernametext.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
//                retailername1=item.toString();
//                retmobilenumber=dealermobilelist.get(position).toString();
//                retstate=statelist.get(position).toString();
//                retcity=citylist.get(position).toString();
//                retaddress=addresslist.get(position).toString();
//                retemail=emaillist.get(position).toString();
//                retregion=regionlist.get(position).toString();
//            }
//        });

//        addretailer1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent i =new Intent(SalesSecondaryActivity.this,CreateRegionActivity.class);
//                startActivity(i);
//            }
//        });

//        distributname.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
//                distiname=item.toString();
//                distiid=distributidlist.get(position).toString();
//
//
//
//                //distid=
//            }
//        });
//        dealerspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                customers = parent.getItemAtPosition(position).toString();
//
//                if(customers.equals("Primary Order")){
//                    distributrow.setVisibility(View.VISIBLE);
//                    dealerrowtext.setVisibility(View.GONE);
//                    cusrow.setVisibility(View.GONE);
//                }else if(customers.equals("Secondary Order")){
//                    distributrow.setVisibility(View.GONE);
//                    dealerrowtext.setVisibility(View.GONE);
//                    cusrow.setVisibility(View.VISIBLE);
//                    customertype.setVisibility(View.VISIBLE);
//                    mobilenumberrow.setVisibility(View.VISIBLE);
//                    companyname.setVisibility(View.VISIBLE);
//                    addretailerbol=true;
//                    editor.putBoolean("addretailerbol",addretailerbol);
//                    editor.commit();
//                }else {
//
////                    distributrow.setVisibility(View.GONE);
////                    dealerrowtext.setVisibility(View.GONE);
////                    addretailerbol=false;
////                    editor.putBoolean("addretailerbol",addretailerbol);
////                    editor.commit();
//                }
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });

        qty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.length()>0){
                    quantity=editable.toString();

                    totqty=Integer.parseInt(quantity);
                    String gettot=mrp1.getText().toString();
                    totvalues= Integer.parseInt(gettot);
                    totalvalues= String.valueOf((totqty *totvalues));
                    discount.setText(totalvalues);
                }else{
                    discount.setText("");
                }

            }
        });

//        segments.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                segmentstring=adapterView.getItemAtPosition(i).toString();
//                 productcat();
//               // new productCatagory().execute();
//            }
//        });
//        catagory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                catagorystring=adapterView.getItemAtPosition(i).toString();
//                partnolist=db1.adhereDao().Getpartno(segmentstring,catagorystring);
//                List<Object>productcat=(List<Object>) CollectionUtils.collect(partnolist, TransformerUtils.invokerTransformer("getPartno"));
//                mrptcat=(List<Object>) CollectionUtils.collect(partnolist, TransformerUtils.invokerTransformer("getMrp"));
//                packag.setText((CharSequence) productcat.get(0));
//                mrp1.setText((CharSequence) mrptcat.get(0));
//            }
//        });
//        Namelist = new ArrayList<String>();
//        segmentlist = new ArrayList<String>();
//        partnolist = new ArrayList<Productlist>();
//        ordertypelist=new ArrayList<>();
//        ordertypelist.add(0,"Select Order Type");
//        ordertypelist.add("Credit Bill");
//        ordertypelist.add("CashBill");
//        ordertypelist.add("Sample");
//        ordertype.setItems(ordertypelist);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isTableExists1()){
                    Intent i = new Intent(SalesSecondaryActivity.this, SalesConfirmDealerActivity.class).putExtra("ordertypevalues", myshare.getString("customertypestring","")).putExtra("salesType","New");
                    i.putExtra("ordertype", "secondary");
                    startActivity(i);
                }else {
                    final AlertDialog alertDialog = new AlertDialog.Builder(SalesSecondaryActivity.this).create();
                    LayoutInflater inflater = getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.loginalert, null);
                    alertDialog.setView(dialogView);
                    TextView login =(TextView)dialogView.findViewById(R.id.login);
                    login.setText("No Order Found");
                    Button submit =(Button) dialogView.findViewById(R.id.submit);
                    submit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();

                        }

                    });
                    alertDialog.show();
                }


            }
        });


        //startActivity(new Intent(SalesSecondaryActivity.this, SelectDistributorActivity.class));

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(customertypestring1.equals("Retailer")||customertypestring1.equals("Mechanic")|| customertypestring1.equals("Electrician") ){
                     if(packag.getText().toString().equals("")){
                         packag.setError("Enter Part No");
//                        StyleableToast st = new StyleableToast(SalesSecondaryActivity.this, "Enter Part No", Toast.LENGTH_SHORT);
//                        st.setBackgroundColor(SalesSecondaryActivity.this.getResources().getColor(R.color.colorPrimary));
//                        st.setTextColor(Color.WHITE);
//                        st.setMaxAlpha();
//                        st.show();

                    }else if(segments.getText().toString().equals("")){
                         segments.setError("Enter Segments");
//                        StyleableToast st = new StyleableToast(SalesSecondaryActivity.this, "Enter Segments", Toast.LENGTH_SHORT);
//                        st.setBackgroundColor(SalesSecondaryActivity.this.getResources().getColor(R.color.colorPrimary));
//                        st.setTextColor(Color.WHITE);
//                        st.setMaxAlpha();
//                        st.show();

                    }else if(qty.getText().toString().equals("")){
                         qty.setError("Enter Qty");
//                        StyleableToast st = new StyleableToast(SalesSecondaryActivity.this, "Enter Qty", Toast.LENGTH_SHORT);
//                        st.setBackgroundColor(SalesSecondaryActivity.this.getResources().getColor(R.color.colorPrimary));
//                        st.setTextColor(Color.WHITE);
//                        st.setMaxAlpha();
//                        st.show();

                    }else if(discount.getText().toString().equals("")){
                         discount.setError("Enter Mrp");
//                        StyleableToast st = new StyleableToast(SalesSecondaryActivity.this, "Enter Mrp", Toast.LENGTH_SHORT);
//                        st.setBackgroundColor(SalesSecondaryActivity.this.getResources().getColor(R.color.colorPrimary));
//                        st.setTextColor(Color.WHITE);
//                        st.setMaxAlpha();
//                        st.show();

                    }else{
                        new Additems1().execute();
                    }


                }else {
                    if(packag.getText().toString().equals("")){
                        packag.setError("Enter Part No");
//                        StyleableToast st = new StyleableToast(SalesSecondaryActivity.this, "Enter  PartNumber", Toast.LENGTH_SHORT);
//                        st.setBackgroundColor(SalesSecondaryActivity.this.getResources().getColor(R.color.colorPrimary));
//                        st.setTextColor(Color.WHITE);
//                        st.setMaxAlpha();
//                        st.show();

                    }else if(segments.getText().toString().equals("")){
                        segments.setError("Enter Segments");
//                        StyleableToast st = new StyleableToast(SalesSecondaryActivity.this, "Enter Segments", Toast.LENGTH_SHORT);
//                        st.setBackgroundColor(SalesSecondaryActivity.this.getResources().getColor(R.color.colorPrimary));
//                        st.setTextColor(Color.WHITE);
//                        st.setMaxAlpha();
//                        st.show();

                    }else if(qty.getText().toString().equals("")){
                        qty.setError("Enter Qty");
//                        StyleableToast st = new StyleableToast(SalesSecondaryActivity.this, "Enter qty", Toast.LENGTH_SHORT);
//                        st.setBackgroundColor(SalesSecondaryActivity.this.getResources().getColor(R.color.colorPrimary));
//                        st.setTextColor(Color.WHITE);
//                        st.setMaxAlpha();
//                        st.show();

                    }else if(discount.getText().toString().equals("")){
                        discount.setError("Enter Mrp");
//                        StyleableToast st = new StyleableToast(SalesSecondaryActivity.this, "Enter mrp", Toast.LENGTH_SHORT);
//                        st.setBackgroundColor(SalesSecondaryActivity.this.getResources().getColor(R.color.colorPrimary));
//                        st.setTextColor(Color.WHITE);
//                        st.setMaxAlpha();
//                        st.show();

                    }else{
                        new Additems1().execute();
                    }


                }
//                if(customers.equals("Select Order Type")) {
//                    StyleableToast st = new StyleableToast(SalesSecondaryActivity.this, "Select Order", Toast.LENGTH_SHORT);
//                    st.setBackgroundColor(SalesSecondaryActivity.this.getResources().getColor(R.color.colorPrimary));
//                    st.setTextColor(Color.WHITE);
//                    st.setMaxAlpha();
//                    st.show();
//
//                }else if(customers.equals("Primary Order")){
//                    if(packag.getText().toString().equals("")){
//                        StyleableToast st = new StyleableToast(SalesSecondaryActivity.this, "Select PartNumber", Toast.LENGTH_SHORT);
//                        st.setBackgroundColor(SalesSecondaryActivity.this.getResources().getColor(R.color.colorPrimary));
//                        st.setTextColor(Color.WHITE);
//                        st.setMaxAlpha();
//                        st.show();
//                    }else if(qty.getText().toString().equals("")){
//                        StyleableToast st = new StyleableToast(SalesSecondaryActivity.this, "Select Quantity", Toast.LENGTH_SHORT);
//                        st.setBackgroundColor(SalesSecondaryActivity.this.getResources().getColor(R.color.colorPrimary));
//                        st.setTextColor(Color.WHITE);
//                        st.setMaxAlpha();
//                        st.show();
//                    }else {
//                        new SalesSecondaryActivity.Additems1().execute();
//                    }
//
//
//                }else if(customers.equals("Secondary Order")){
//                    if(packag.getText().toString().equals("")){
//                        StyleableToast st = new StyleableToast(SalesSecondaryActivity.this, "Select PartNumber", Toast.LENGTH_SHORT);
//                        st.setBackgroundColor(SalesSecondaryActivity.this.getResources().getColor(R.color.colorPrimary));
//                        st.setTextColor(Color.WHITE);
//                        st.setMaxAlpha();
//                        st.show();
//                    }else if(qty.getText().toString().equals("")){
//                        StyleableToast st = new StyleableToast(SalesSecondaryActivity.this, "Select Quantity", Toast.LENGTH_SHORT);
//                        st.setBackgroundColor(SalesSecondaryActivity.this.getResources().getColor(R.color.colorPrimary));
//                        st.setTextColor(Color.WHITE);
//                        st.setMaxAlpha();
//                        st.show();
//                    }else {
//                        new SalesSecondaryActivity.Additems1().execute();
//                    }
//
//
//                }else{
//                    new SalesSecondaryActivity.Additems1().execute();
//                }
         }
     });

        title=(TextView) findViewById(R.id.title);
        title.setText("Sales");
        catagory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SalesSecondaryActivity.Partnumber().execute();
            }
        });
        // new segment().execute();
        // segment1();
      // GetPartnumber();
//        GetCustomerData(db1.adhereDao().GetCustomer_list());
//        if (choosen.equals("fromchoosed")) {
//            customers="Secondary Order";
//            createdcustomer();
//            getcreatecustomer();
//            mobilenumberstatus.setText(getIntent().getStringExtra("mobilestring"));
//            String customertye=getIntent().getStringExtra("ordertype");
//            if(customertye.equals("Retailer")){
//                customspinner.setText(getIntent().getStringExtra("shopname"));
//            }else {
//                customspinner.setText(getIntent().getStringExtra("name"));
//            }
//
//        }
//        else {
//            GetOrder();
//            getcustomertype();
//        }

//        GetDitribut(db1.adhereDao().GetDistilist());
        packag.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                partnostring=adapterView.getItemAtPosition(i).toString();
                GetProductsegcat(db1.adhereDao().Getprducts(partnostring));
            }
        });

//        mobilenumberstatus.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                mob=adapterView.getItemAtPosition(i).toString();
//                GetCustname(db1.adhereDao().GetCrestemobile(mob));
//            }
//        });

//        customspinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
//                customers1=item.toString();
//                customercode=customercodelist.get(position).toString();
//            }
//        });

        //  Getpermissionforsms();

    }

    private void CheckInternetforpass() {
        NetworkConnection network = new NetworkConnection(SalesSecondaryActivity.this);
        if (network.CheckInternet()) {
            PasswordChangeAlert alert = new PasswordChangeAlert(SalesSecondaryActivity.this);// new saleslogin().execute();
            alert.showLoginbox();
        } else {
            Alertbox alert = new Alertbox(SalesSecondaryActivity.this);
            alert.showAlertbox("Kindly check your Internet Connection");
        }

    }


    private void getcreatecustomer() {
        customerslist=new ArrayList<>();
        customerslist.add(0,getIntent().getStringExtra("ordertype"));
        customerspinner.setItems(customerslist);
    }

    private void getcustomertype() {
        customerslist=new ArrayList<>();
        customerslist.add(0,"Select Customer Type");
        customerslist.add("Retailer");
        customerslist.add("Mechanic");
        customerslist.add("Electrician");
        customerslist.add("End Of Customer");
        customerslist.add("Others");
        customerspinner.setItems(customerslist);
    }

    private void createdcustomer() {
        Namelist.add(0,"Secondary Order");
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(SalesSecondaryActivity.this,R.layout.simple_spinner_item,Namelist);
        spinnerAdapter.setNotifyOnChange(true);
        spinnerAdapter.setDropDownViewResource(R.layout.simple_spinner_item);
        dealerspinner.setAdapter(spinnerAdapter);
    }


    private void GetCustname(List<createlist> createlists) {
        if(customerspinner.getText().toString().equals("Retailer")){
            customername=(List<Object>) CollectionUtils.collect(createlists, TransformerUtils.invokerTransformer("getShopname"));
            typeofaccount=(List<Object>) CollectionUtils.collect(createlists, TransformerUtils.invokerTransformer("getTypeofacc"));
            region=(List<Object>) CollectionUtils.collect(createlists, TransformerUtils.invokerTransformer("getRegion"));
            state=(List<Object>) CollectionUtils.collect(createlists, TransformerUtils.invokerTransformer("getState"));
            city=(List<Object>) CollectionUtils.collect(createlists, TransformerUtils.invokerTransformer("getCity"));
            mobileno=(List<Object>) CollectionUtils.collect(createlists, TransformerUtils.invokerTransformer("getMobilenumber"));
            List<Object> shopNameLists = (List<Object>) CollectionUtils.collect(createlists, TransformerUtils.invokerTransformer("getShopName"));
            address=(List<Object>) CollectionUtils.collect(createlists, TransformerUtils.invokerTransformer("getAddress"));
//        mobilelistlist= new ArrayList<String>();
            for (Object object : customername) {
                if (object!=null)
                    cuslist.add(Objects.toString(object, null));
            }
            for (Object object : shopNameLists) {
                if (object!=null)
                    shopNameList.add(Objects.toString(object, null));
            }
            for (Object object : typeofaccount) {
                if (object!=null)
                    typeoffacclist.add(Objects.toString(object, null));
            }
            for (Object object : region) {
                if (object!=null)
                    regionliststring.add(Objects.toString(object, null));
            }
            for (Object object : state) {
                if (object!=null)
                    stateliststring.add(Objects.toString(object, null));
            }
            for (Object object : city) {
                if (object!=null)
                    cityliststring.add(Objects.toString(object, null));
            }
            for (Object object : mobileno) {
                if (object!=null)
                    mobilenolist.add(Objects.toString(object, null));
            }
            for (Object object : address) {
                if (object!=null)
                    addressllist.add(Objects.toString(object, null));
            }

            customspinner.setText(cuslist.get(0));
            shopName.setText(shopNameList.get(0));

        }else {
            customername=(List<Object>) CollectionUtils.collect(createlists, TransformerUtils.invokerTransformer("getName"));
            typeofaccount=(List<Object>) CollectionUtils.collect(createlists, TransformerUtils.invokerTransformer("getTypeofacc"));
            region=(List<Object>) CollectionUtils.collect(createlists, TransformerUtils.invokerTransformer("getRegion"));
            state=(List<Object>) CollectionUtils.collect(createlists, TransformerUtils.invokerTransformer("getState"));
            city=(List<Object>) CollectionUtils.collect(createlists, TransformerUtils.invokerTransformer("getCity"));
            mobileno=(List<Object>) CollectionUtils.collect(createlists, TransformerUtils.invokerTransformer("getMobilenumber"));
            List<Object> shopNameLists = (List<Object>) CollectionUtils.collect(createlists, TransformerUtils.invokerTransformer("getShopName"));
            address=(List<Object>) CollectionUtils.collect(createlists, TransformerUtils.invokerTransformer("getAddress"));
            for (Object object : customername) {
                if (object!=null)
                    cuslist.add(Objects.toString(object, null));
            }
            for (Object object : shopNameLists) {
                if (object!=null)
                    shopNameList.add(Objects.toString(object, null));
            }
            for (Object object : typeofaccount) {
                if (object!=null)
                    typeoffacclist.add(Objects.toString(object, null));
            }
            for (Object object : region) {
                if (object!=null)
                    regionliststring.add(Objects.toString(object, null));
            }
            for (Object object : state) {
                if (object!=null)
                    stateliststring.add(Objects.toString(object, null));
            }
            for (Object object : city) {
                if (object!=null)
                    cityliststring.add(Objects.toString(object, null));
            }
            for (Object object : mobileno) {
                if (object!=null)
                    mobilenolist.add(Objects.toString(object, null));
            }
            for (Object object : address) {
                if (object!=null)
                    addressllist.add(Objects.toString(object, null));
            }
//        mobilelistlist= new ArrayList<String>();
            for (Object object : customername) {
                if (object!=null)
                    cuslist.add(Objects.toString(object, null));
            }
            customspinner.setText(cuslist.get(0));
            shopName.setText(shopNameList.get(0));
        }

    }

    private void GetValues(List<createlist> createlists) {
        List<Object>mobilenumber=(List<Object>) CollectionUtils.collect(createlists, TransformerUtils.invokerTransformer("getMobilenumber"));
//        mobilelistlist= new ArrayList<String>();
        for (Object object : mobilenumber) {
            if (object!=null)
                mobilelistlist.add(Objects.toString(object, null));
        }
        mobilenumberstatus.setAdapter(new ArrayAdapter<String>(SalesSecondaryActivity.this, android.R.layout.simple_dropdown_item_1line,mobilelistlist));
        mobilenumberstatus.setThreshold(1);
//        mobilenumberstatus.setAdapter(new ArrayAdapter<String>(SalesSecondaryActivity.this, android.R.layout.simple_dropdown_item_1line,mobilelistlist));
//        mobilenumberstatus.setThreshold(1);
    }



    private void GetCustomerData(List<Customer_list> customer_lists) {
        customerobject=(List<Object>) CollectionUtils.collect(customer_lists, TransformerUtils.invokerTransformer("getName"));
        customercodeobject=(List<Object>) CollectionUtils.collect(customer_lists, TransformerUtils.invokerTransformer("getCustomerCode"));
        customerlist= new ArrayList<String>(customerobject.size());
        for (Object object : customerobject) {
            customerlist.add(Objects.toString(object, null));
        }

        customercodelist =new ArrayList<String>(customercodeobject.size());
        for (Object object : customercodeobject) {
            customercodelist.add(Objects.toString(object, null));
        }
        customerlist.add(0,"Select Customer Name");
        customercodelist.add(0,"Select CustomerCode");
//        customspinner.setItems(customerlist);
    }

    private void GetProductsegcat(List<Productlist> getprducts) {
//        segmentobjectlist=(List<Object>) CollectionUtils.collect(getprducts, TransformerUtils.invokerTransformer("getSegments"));
//        catagoryobjectlist=(List<Object>) CollectionUtils.collect(getprducts, TransformerUtils.invokerTransformer("getCategory"));
//        mrptcat=(List<Object>) CollectionUtils.collect(getprducts, TransformerUtils.invokerTransformer("getMrp"));
        segmentstring=getprducts.get(0).getSegments();
        catagorystring=getprducts.get(0).getCategory();
        segments.setText(segmentstring+"/"+catagorystring);
        catagory.setText(catagorystring);
        mrp1.setText((CharSequence) getprducts.get(0).getMrp());
    }

    private void GetPartnumber(String t) {
        partlistauto=db1.adhereDao().GetPartnolist(t);
        packag.setAdapter(new ArrayAdapter<String>(SalesSecondaryActivity.this, android.R.layout.simple_dropdown_item_1line,partlistauto));
        packag.setThreshold(1);
    }

    private void getDealerNamesForSpinner() {

        if(!isTableExists1()){
            GetDelerList(db1.adhereDao().GetDeallist());
        }else {
            String query = "select * from " + SQLITE.TABLE_Create_Profile +"" ;
            //  String query = "select distinct " + SQLITE.COLUMN_Create_retname + " from " + SQLITE.TABLE_Create_Profile + " order by " + SQLITE.COLUMN_Create_retname + " asc";
            Log.v("Name from createcus", query);
            DBHelper dbHelp = new DBHelper(SalesSecondaryActivity.this);
            db = dbHelp.getWritableDatabase();
            Cursor cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    dealerNameList.add(cursor.getString(cursor.getColumnIndex(SQLITE.COLUMN_Create_retname)));
                    dealermobilelist.add(cursor.getString(cursor.getColumnIndex(SQLITE.COLUMN_Create_retmobile)));
                    statelist.add(cursor.getString(cursor.getColumnIndex(SQLITE.COLUMN_Create_retstate)));
                    citylist.add(cursor.getString(cursor.getColumnIndex(SQLITE.COLUMN_Create_retcity)));
                    regionlist.add(cursor.getString(cursor.getColumnIndex(SQLITE.COLUMN_Create_retregion)));
                    addresslist.add(cursor.getString(cursor.getColumnIndex(SQLITE.COLUMN_Create_retaddress)));
                    //emaillist.add(cursor.getString(cursor.getColumnIndex(SQLITE.COLUMN_Create_retemail)));

                } while (cursor.moveToNext());
                cursor.close();
            }
        }

    }

    private void GetDelerList(List<deal> deals) {
        dealerObjectNameList=(List<Object>) CollectionUtils.collect(deals, TransformerUtils.invokerTransformer("getDealerName"));
        dealerObjectmobilelist=(List<Object>) CollectionUtils.collect(deals, TransformerUtils.invokerTransformer("getDealerMobile"));
        stateObjectlist=(List<Object>) CollectionUtils.collect(deals, TransformerUtils.invokerTransformer("getDealerState"));
        cityObjectlist=(List<Object>) CollectionUtils.collect(deals, TransformerUtils.invokerTransformer("getDealerCity"));
        regionObjectlist=(List<Object>) CollectionUtils.collect(deals, TransformerUtils.invokerTransformer("getDaelerRegion"));
        addressObjectlist=(List<Object>) CollectionUtils.collect(deals, TransformerUtils.invokerTransformer("getDealerAddress"));
        emailObjectlist=(List<Object>) CollectionUtils.collect(deals, TransformerUtils.invokerTransformer("getDealerEmail"));
        //  distiid=(List<String>) CollectionUtils.collect(distributor_lists, TransformerUtils.invokerTransformer("getP_Id"));

        dealerNameList= new ArrayList<String>(dealerObjectNameList.size());
        for (Object object : dealerObjectNameList) {
            dealerNameList.add(Objects.toString(object, null));
        }
        dealermobilelist= new ArrayList<String>(dealerObjectmobilelist.size());
        for (Object object : dealerObjectmobilelist) {
            dealermobilelist.add(Objects.toString(object, null));
        }
        statelist= new ArrayList<String>(stateObjectlist.size());
        for (Object object : stateObjectlist) {
            statelist.add(Objects.toString(object, null));
        }
        citylist= new ArrayList<String>(cityObjectlist.size());
        for (Object object : cityObjectlist) {
            citylist.add(Objects.toString(object, null));
        }
        regionlist= new ArrayList<String>(regionObjectlist.size());
        for (Object object : regionObjectlist) {
            regionlist.add(Objects.toString(object, null));
        }
        addresslist= new ArrayList<String>(addressObjectlist.size());
        for (Object object : addressObjectlist) {

            addresslist.add(Objects.toString(object, null));
        }
        emaillist= new ArrayList<String>(emailObjectlist.size());
        for (Object object : emailObjectlist) {
            emaillist.add(Objects.toString(object, null));
        }
        dealerNameList.add(0,"Select Dealer");
        dealermobilelist.add(0,"Select Mobile");
        statelist.add(0,"Select Mobile");
        citylist.add(0,"Select Mobile");
        regionlist.add(0,"Select Mobile");
        addresslist.add(0,"Select Mobile");
        emaillist.add(0,"Select Mobile");
        dealernametext.setItems(dealerNameList);


//        for (String str : distributstringlist)
//                for (String s : str.split("-"))
//                    list.add(s);

        //distributname.setItems(distributeliststring);


    }

    private void GetDitribut(List<Distributor_list> distributor_lists) {
        distributor=(List<Object>) CollectionUtils.collect(distributor_lists, TransformerUtils.invokerTransformer("getDisname"));
        //  distiid=(List<String>) CollectionUtils.collect(distributor_lists, TransformerUtils.invokerTransformer("getP_Id"));
        distributor.add(0,"Select Distributor-0");
        List<String> distributstringlist= new ArrayList<String>(distributor.size());
        for (Object object : distributor) {
            distributstringlist.add(Objects.toString(object, null));
        }
        for(int i=0;i<distributstringlist.size();i++){
            String [] distrilist=distributstringlist.get(i).split("-");
            distributeliststring.add(distrilist[0]);
            distributidlist.add(distrilist[1]);
        }


//        for (String str : distributstringlist)
//                for (String s : str.split("-"))
//                    list.add(s);

        distributname.setItems(distributeliststring);
    }

    private void GetOrder() {
        Namelist.add(0,"Select OrderType");
        if (myshare.getBoolean("primaryorder",true)){
            selectordertype.setVisibility(View.VISIBLE);
            secondaryorder.setVisibility(View.GONE);
            Namelist.add("Primary Order");
            Namelist.add("Secondary Order");
        }
        else {
            selectordertype.setVisibility(View.GONE);
            secondaryorder.setVisibility(View.VISIBLE);
            Namelist.add("Secondary Order");
            distributrow.setVisibility(View.GONE);
            dealerrowtext.setVisibility(View.GONE);
            cusrow.setVisibility(View.VISIBLE);
            customertype.setVisibility(View.VISIBLE);
            mobilenumberrow.setVisibility(View.VISIBLE);
            companyname.setVisibility(View.VISIBLE);
            addretailerbol=true;
            editor.putBoolean("addretailerbol",addretailerbol);
            editor.commit();
        }
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(SalesSecondaryActivity.this,R.layout.simple_spinner_item,Namelist);
        spinnerAdapter.setNotifyOnChange(true);
        spinnerAdapter.setDropDownViewResource(R.layout.simple_spinner_item);
        dealerspinner.setAdapter(spinnerAdapter);
    }


    private void productcat() {
        productcatlist=db1.adhereDao().GetProductcat(segmentstring);
        //List<Object>productcat=(List<Object>) CollectionUtils.collect(productcatlist, TransformerUtils.invokerTransformer("getCategory"));
//        catagory.setAdapter(new ArrayAdapter<String>(SalesSecondaryActivity.this, android.R.layout.simple_dropdown_item_1line,productcatlist));
//        catagory.setThreshold(1);

    }

    private void segment1() {
        segmentlist=db1.adhereDao().GetProdutlist();
//        segments.setAdapter(new ArrayAdapter<String>(SalesSecondaryActivity.this, android.R.layout.simple_dropdown_item_1line,segmentlist));
//        segments.setThreshold(1);
    }

//    private void Getpermissionforsms() {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
//                if (shouldShowRequestPermissionRationale(android.Manifest.permission.CALL_PHONE)) {
//                    Toast.makeText(getApplicationContext(),"You need to give permission",Toast.LENGTH_SHORT).show();
//                } else {
//                    requestPermissions(new String[]{android.Manifest.permission.CALL_PHONE}, PERMISSION_REQUEST);
//                }
//            } else {
//                GetDealerNamesForSpinner1();
//            }
//        } else {
//            GetDealerNamesForSpinner1();
//
//        }
//
//
//        dealerspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//
//                customers = parent.getItemAtPosition(position).toString();
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
//    }

//    private void GetDealerNamesForSpinner1() {
//
//        Namelist =  db1.adhereDao().GetCustomer_name_list();
//        Namelist.add(0,"Select Customer");
//        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(SalesSecondaryActivity.this,R.layout.simple_spinner_item,Namelist);
//        spinnerAdapter.setNotifyOnChange(true);
//        spinnerAdapter.setDropDownViewResource(R.layout.simple_spinner_item);
//        dealerspinner.setAdapter(spinnerAdapter);
//    }

    private boolean isTableExists() {
        SQLiteDatabase mDatabase;
        DBHelpersync dbHelp = new DBHelpersync(SalesSecondaryActivity.this);
        mDatabase = dbHelp.getWritableDatabase();
        String query = "select DISTINCT tbl_name from sqlite_master where tbl_name = '"+ SQLITE.TABLE_Additems+"'";
        //String query ="select * from "+ SQLITE.TABLE_NewPartsOrder+" order by date asc";
        Cursor cursor = mDatabase.rawQuery(query, null);
        Log.v("preview",query);
        if(cursor!=null) {
            if(cursor.getCount()>0) {
                cursor.close();
                mDatabase.close();
                return true;
            }
            cursor.close();
            mDatabase.close();
        }
        return false;

    }

    private boolean isTableExists1() {
        SQLiteDatabase mDatabase;
        DBHelpersync dbHelp = new DBHelpersync(SalesSecondaryActivity.this);
        mDatabase = dbHelp.getWritableDatabase();
        String query = "select DISTINCT tbl_name from sqlite_master where tbl_name = '"+ SQLITE.TABLE_Additems+"'";
        //String query ="select * from "+ SQLITE.TABLE_NewPartsOrder+" order by date asc";
        Cursor cursor = mDatabase.rawQuery(query, null);
        Log.v("preview",query);
        if(cursor!=null) {
            if(cursor.getCount()>0) {
                cursor.close();
                mDatabase.close();
                return true;
            }
            cursor.close();
            mDatabase.close();
        }
        return false;

    }

    private class segment extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute () {
            super.onPreExecute();
            productsegment=new ArrayList<>();


        }

        @SuppressLint("LongLogTag")
        @Override
        protected String doInBackground (String...strings){
            try {
                dbHelper = new DBHelper(SalesSecondaryActivity.this);
                db = dbHelper.readDataBase();

                String selectQuery = "Select distinct segments from Product ";

                c = db.rawQuery(selectQuery, null);
                Log.e(TAG, "doInBackground: top");
                if (c.moveToFirst()) {
                    do {


                        productsegment.add(c.getString(c.getColumnIndex("segments")));


                    }
                    while (c.moveToNext());
                }
                c.close();
            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, "doInBackground: \"error\"");
                return "Error";
            }
            return "sucess";

        }

        @Override
        protected void onPostExecute (String s){
            super.onPostExecute(s);

            if (s.equals("sucess")) {
                productsegment.add(0,"Select Segment");
                //segments.setItems(productsegment);
            }


        }
    }

//    private class productCatagory extends AsyncTask<String, Void, String> {
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            productcatagorylist = new ArrayList<>();
//
//
//        }
//
//        @SuppressLint("LongLogTag")
//        @Override
//        protected String doInBackground(String... strings) {
//            try {
//                dbHelper = new DBHelper(SalesSecondaryActivity.this);
//                db = dbHelper.readDataBase();
//
//                String selectQuery = "Select distinct segments,category from Product where  segments ='"+segmentstring+"' ";
//
//                c = db.rawQuery(selectQuery, null);
//                Log.e(TAG, "doInBackground: top");
//                if (c.moveToFirst()) {
//                    do {
//
//
//                        productcatagorylist.add(c.getString(c.getColumnIndex("category")));
//
//
//                    }
//                    while (c.moveToNext());
//                }
//                c.close();
//            } catch (Exception e) {
//                e.printStackTrace();
//                Log.e(TAG, "doInBackground: \"error\"");
//                return "Error";
//            }
//            return "sucess";
//
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//            super.onPostExecute(s);
//
//            if (s.equals("sucess")) {
//                catagory.setAdapter(new ArrayAdapter<String>(SalesSecondaryActivity.this, android.R.layout.simple_dropdown_item_1line,productcatagorylist));
//                catagory.setThreshold(1);
//
//            }
//
//
//        }
//    }

    private class Partnumber extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            partlist = new ArrayList<>();


        }

        @SuppressLint("LongLogTag")
        @Override
        protected String doInBackground(String... strings) {
            try {
                dbHelper = new DBHelper(SalesSecondaryActivity.this);
                db = dbHelper.readDataBase();

                String selectQuery = "Select distinct segments,category,oempartno from Product where  segments ='"+segmentstring+"' and category = '"+catagory.getText().toString()+"'";

                c = db.rawQuery(selectQuery, null);
                Log.e(TAG, "doInBackground: top");
                if (c.moveToFirst()) {
                    do {


                        partlist.add(c.getString(c.getColumnIndex("oempartno")));


                    }
                    while (c.moveToNext());
                }
                c.close();
            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, "doInBackground: \"error\"");
                return "Error";
            }
            return "sucess";

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (s.equals("sucess")) {
                productname.setAdapter(new ArrayAdapter<String>(SalesSecondaryActivity.this, android.R.layout.simple_dropdown_item_1line, partlist));
                productname.setThreshold(1);

            }


        }
    }

    private class Additems1 extends AsyncTask<String, Void, String> {
        ContentValues values = new ContentValues();
        SQLiteDatabase db;
        private double success;
        private int a;
        private String orderNo;
        public String quantity, discount1, remarksstring, unpckage;

        @SuppressLint("InflateParams")
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // instantiate new progress dialog
            progressDialog = new ProgressDialog(SalesSecondaryActivity.this);
            // spinner (wheel) style dialog
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Adding data...");
            progressDialog.show();
            quantity = qty.getText().toString();
            discount1 = discount.getText().toString();
            remarksstring = remarks.getText().toString();
            //   Numbers=numbers.getText().toString();
            //  unpckage=packageedit.getText().toString();


        }

        @Override
        protected String doInBackground(String... args0) {
            try {
//                if(customers.equals("Primary Order")){
//                    values.put(COLUMN_Addexecutive, salesname);
//                    values.put(COLUMN_AddcustomerType, customers);
//                    values.put(COLUMN_AddNames, distiname);
//                    values.put(COLUMN_Addproductname, segmentstring);
//                    values.put(COLUMN_Productcat, catagorystring);
//                    values.put(COLUMN_Addpackage, partnostring);
//                    values.put(COLUMN_Addquantity, quantity);
//                    values.put(COLUMN_Adddiscount, discount1);
//                    values.put(COLUMN_AddOrderType, ordertypestring);
//                    values.put(COLUMN_Addcurrentdate, formattedDate);
//                    values.put(COLUMN_AddRemarks, remarks.getText().toString());
//                    values.put(COLUMN_Addcurrentdate, formattedDate);
//                    values.put(COLUMN_Unpackage, totvalues);
//                    values.put(COLUMN_Addstring_mail, executeid);
//                    values.put(COLUMN_AddMobileNO, distiid);
//                }else {
                    if(customertypestring1.equals("Retailer") ||customertypestring1.equals("Mechanic")|| customertypestring1.equals("Electrician") ){
                        values.put(COLUMN_Addexecutive, salesname);
                        values.put(COLUMN_AddcustomerType, getIntent().getStringExtra("ordertype"));
                        values.put(COLUMN_Addproductname, segmentstring);
                        values.put(COLUMN_Productcat, catagorystring);
                        values.put(COLUMN_Addpackage, partnostring);
                        values.put(COLUMN_Addquantity, quantity);
                        values.put(COLUMN_Adddiscount, discount1);
                        values.put(COLUMN_AddOrderType, getIntent().getStringExtra("ordertypes"));
                        values.put(COLUMN_Addcurrentdate, formattedDate);
                        values.put(COLUMN_AddRemarks, remarks.getText().toString());
                        values.put(COLUMN_Addcurrentdate, formattedDate);
                        values.put(COLUMN_Unpackage, totvalues);
//                        values.put(COLUMN_Addstring_ordercolors,getIntent().getStringExtra("shopname"));
                        values.put(COLUMN_Addstring_mail, executeid);
                        values.put(COLUMN_AddMobileNO, getIntent().getStringExtra("mobilestring"));
                        values.put(COLUMN_RetState, getIntent().getStringExtra("states"));
                        values.put(COLUMN_RetCity, getIntent().getStringExtra("citys"));
                        values.put(COLUMN_RetAddress, getIntent().getStringExtra("address"));
                        values.put(COLUMN_RetEmail, getIntent().getStringExtra("emailstring"));
                        values.put(COLUMN_RetRegion, getIntent().getStringExtra("regionstring"));
                        values.put(COLUMN_CUSTOMERNAME, Companynamestring);
                       //values.put(COLUMN_Addcustomercode, gsttext.getText().toString());
                        values.put(COLUMN_Addstring_ordercolors,getIntent().getStringExtra("newShops"));
                        values.put(COLUMN_Addcustomercode, getIntent().getStringExtra("shopname"));
                        values.put(COLUMN_Addstring_ordernumber, gsttext.getText().toString());
                    }else {
                        values.put(COLUMN_Addexecutive, salesname);
                        values.put(COLUMN_AddcustomerType, getIntent().getStringExtra("ordertype"));
                        values.put(COLUMN_Addproductname, segmentstring);
                        values.put(COLUMN_Productcat, catagorystring);
                        values.put(COLUMN_Addpackage, partnostring);
                        values.put(COLUMN_Addquantity, quantity);
                        values.put(COLUMN_Adddiscount, discount1);
                        values.put(COLUMN_AddOrderType, getIntent().getStringExtra("ordertypes"));
                        values.put(COLUMN_Addcurrentdate, formattedDate);
                        values.put(COLUMN_AddRemarks, remarks.getText().toString());
                        values.put(COLUMN_Addcurrentdate, formattedDate);
                        values.put(COLUMN_Unpackage, totvalues);
//                        values.put(COLUMN_Addstring_ordercolors,getIntent().getStringExtra("name"));
                        values.put(COLUMN_Addstring_mail, executeid);
                        values.put(COLUMN_AddMobileNO, getIntent().getStringExtra("mobilestring"));
                        values.put(COLUMN_RetState, getIntent().getStringExtra("states"));
                        values.put(COLUMN_RetCity, getIntent().getStringExtra("citys"));
                        values.put(COLUMN_RetAddress, getIntent().getStringExtra("address"));
                        values.put(COLUMN_RetEmail, getIntent().getStringExtra("emailstring"));
                        values.put(COLUMN_RetRegion, getIntent().getStringExtra("regionstring"));
                        values.put(COLUMN_CUSTOMERNAME, Companynamestring);
                        values.put(COLUMN_Addcustomercode, customercode);
                        values.put(COLUMN_Addstring_ordercolors,getIntent().getStringExtra("newShops"));
                        values.put(COLUMN_Addstring_ordernumber, gsttext.getText().toString());
                       // values.put(COLUMN_Addcustomercode, getIntent().getStringExtra("shopname"));
                    }
                    if(customertypestring1.equals("Retailer") ||customertypestring1.equals("Mechanic")|| customertypestring1.equals("Electrician")){
                        SecondaryOrderDetails details=new SecondaryOrderDetails();
                        details.setCustomerName(getIntent().getStringExtra("shopname"));
                        details.setCustomerType(getIntent().getStringExtra("ordertype"));
                        details.setMobileNo(getIntent().getStringExtra("mobilestring"));
                        details.setOrderType(getIntent().getStringExtra("ordertypes"));
                        db1.adhereDao().insertCustomerDetails(details);
                    }else {
                        SecondaryOrderDetails details=new SecondaryOrderDetails();
                        details.setCustomerName(getIntent().getStringExtra("name"));
                        details.setCustomerType(getIntent().getStringExtra("ordertype"));
                        details.setMobileNo(getIntent().getStringExtra("mobilestring"));
                        details.setOrderType(getIntent().getStringExtra("ordertypes"));
                        db1.adhereDao().insertCustomerDetails(details);
                    }




                DBHelpersync dbHelper = new DBHelpersync(SalesSecondaryActivity.this);
                db = dbHelper.getWritableDatabase();
                // db.execSQL("DROP TABLE  IF EXISTS "+ SQLITE.TABLE_Additems);
                dbHelper.CreateAddorderTable(db);
                if(customertypestring.equals("")){
                    success = db.insert(TABLE_Additems, null, values);
                    editor.putString("customertypestring",getIntent().getStringExtra("ordertypes"));
                    editor.commit();
                    return "received";
                }else if(customertypestring.equals(getIntent().getStringExtra("ordertypes"))){
                    success = db.insert(TABLE_Additems, null, values);
                    return "received";
                }else {

                    return "Not Received";
                }


            } catch (Exception e) {
                Log.v("Error in Add local db ", "catch is working");
                Log.v("Error in Add local db", e.getMessage());
                return "Error";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            if (result.equals("received")) {
                Log.v("success", Double.toString(success));
                /*startActivity(new Intent(Sales_Order_Activity.this,Sales_Order_Activity.class));
                finish();*/
                //box.showAlertbox("Item Added to Cart");

                packag.setText("");
                qty.setText("");
                segments.setText("");
                view.setVisibility(View.VISIBLE);
                //  Discountedittext.setText("");
                remarks.setText("");
                final AlertDialog alertDialog = new AlertDialog.Builder(SalesSecondaryActivity.this).create();
                LayoutInflater inflater = getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.loginalert, null);
                alertDialog.setView(dialogView);
                TextView login =(TextView)dialogView.findViewById(R.id.login);
                TextView submit =(Button)dialogView.findViewById(R.id.submit);
                login.setText("Item Added to Cart");
                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });
                alertDialog.show();
              //  box.showAlertbox("Items Added to cart");

//                segments.setAdapter(new ArrayAdapter<String>(SalesSecondaryActivity.this, android.R.layout.simple_dropdown_item_1line,segmentlist));
//                segments.setThreshold(1);Toast.makeText(getApplicationContext(), "Item added to Cart", Toast.LENGTH_LONG).show();
            } else if(result.equals("Not Received")){
                box.showAlertbox("You have" + customertypestring +" type in the Cart. Please Place the Order first. And then add this" +customers+" type in the cart");
            } else {
                final AlertDialog alertDialog = new AlertDialog.Builder(SalesSecondaryActivity.this).create();
                LayoutInflater inflater = getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.loginalert, null);
                alertDialog.setView(dialogView);
                TextView login =(TextView)dialogView.findViewById(R.id.login);
                TextView submit =(Button)dialogView.findViewById(R.id.submit);
                login.setText("Error in Adding");
                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });
                alertDialog.show();
            }
        }
    }

    private class GetDealerNamesForSpinner extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            try {
                getDealerNamesForSpinner();
                Log.v("OFF Line", "SQLITE Table");
                return "received";

            } catch (Exception e) {
                Log.v("Error in get date", "catch is working");
                Log.v("Error in Incentive", e.getMessage());
                return "notsuccess";
            }

        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if(!isTableExists1()){

            }else {
                dealerNameList.add(0,"Select Dealer");
                dealermobilelist.add(0,"Select Mobile");
                statelist.add(0,"Select Mobile");
                citylist.add(0,"Select Mobile");
                regionlist.add(0,"Select Mobile");
                addresslist.add(0,"Select Mobile");
                emaillist.add(0,"Select Mobile");
                dealernametext.setItems(dealerNameList);
            }

        }
    }

//    private class GetMobilenumberspinner extends AsyncTask<String, Void, String> {
//
//        @Override
//        protected String doInBackground(String... params) {
//            // TODO Auto-generated method stub
//            try {
//                getDealerMobileForSpinner();
//                Log.v("OFF Line", "SQLITE Table");
//                return "received";
//
//            } catch (Exception e) {
//                Log.v("Error in get date", "catch is working");
//                Log.v("Error in Incentive", e.getMessage());
//                return "notsuccess";
//            }
//
//        }
//
//        @Override
//        protected void onPostExecute(String result) {
//            // TODO Auto-generated method stub
//            super.onPostExecute(result);
//            dealermobilelist.add(0, "Select MobileNumber");
//           // dealernametext.setItems(dealermobilelist);
//        }
//    }

    private void getDealerMobileForSpinner() {
        String query = "select distinct " + SQLITE.COLUMN_Create_retmobile + " from " + SQLITE.TABLE_Create_Profile + " order by " + SQLITE.COLUMN_Create_retmobile + " asc";
        Log.v("Name from createcus", query);
        DBHelper dbHelp = new DBHelper(SalesSecondaryActivity.this);
        db = dbHelp.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                dealermobilelist.add(cursor.getString(cursor.getColumnIndex(SQLITE.COLUMN_Create_retmobile)));
            } while (cursor.moveToNext());
            cursor.close();
        }

    }


}
