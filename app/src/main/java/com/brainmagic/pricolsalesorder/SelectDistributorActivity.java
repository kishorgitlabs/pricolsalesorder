package com.brainmagic.pricolsalesorder;

import android.content.Intent;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.jaredrummler.materialspinner.MaterialSpinner;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.TransformerUtils;

import java.util.ArrayList;
import java.util.List;

import adapter.DistributorAdapter;
import alertbox.Alertbox;
import api.models.master.Citylist;
import api.models.master.Distributor_list;
import api.models.master.Statelist;
import roomdb.database.AppDatabase;

public class SelectDistributorActivity extends AppCompatActivity implements DistributorAdapter.SelectValue{
    private MaterialSpinner statespinner,cityspinner;
    private ListView list;
    private Button selectdistributor;
    private List<String>statelsit,citylist;
    private AppDatabase db;
    private List<Statelist> statelist;
    List<Distributor_list> distribut;
    private String states,cities;
    private DistributorAdapter distributoradapter;
    private Alertbox alertbox=new Alertbox(SelectDistributorActivity.this);
    private String idString,nameString,mobileString,addressString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_distributor);
        statespinner=(MaterialSpinner) findViewById(R.id.statespinner);
        cityspinner=(MaterialSpinner) findViewById(R.id.cityspinner);
        list=(ListView)findViewById(R.id.distributerlist);
        selectdistributor=(Button)findViewById(R.id.selectdistributor);
        distribut=new ArrayList<>();
        db=AppDatabase.getAppDatabase(SelectDistributorActivity.this);
        selectdistributor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i =new Intent(SelectDistributorActivity.this,SalesConfirmOrder.class).putExtra("disid",idString).putExtra("distiname",nameString).putExtra("distimobile",mobileString).putExtra("disaddress",addressString);
                startActivity(i);
            }
        });
        GetStatelist(db.adhereDao().GetStatelist());
        GetCitylist(db.adhereDao().GetCitylist());
        statespinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                states=item.toString();
            }
        });

        cityspinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                cities=item.toString();
                GetDistributorlist();
            }
        });
    }

    private void GetDistributorlist() {
        distribut=db.adhereDao().GetDistributorlist(states,cities);
        if(distribut.size()== 0){
            alertbox.showAlertbox("No Distributor");
        }else {
            distributoradapter=new DistributorAdapter(SelectDistributorActivity.this,distribut);
            list.setAdapter(distributoradapter);
        }

    }

    private void GetCitylist(List<Citylist> citylists) {
        List<Object>cities=(List<Object>) CollectionUtils.collect(citylists, TransformerUtils.invokerTransformer("getTown1"));
        cities.add(0,"Select City");
        cityspinner.setItems(cities);
    }

    private void GetStatelist(List<Statelist> statelists) {
        List<Object>states=(List<Object>) CollectionUtils.collect(statelists, TransformerUtils.invokerTransformer("getState1"));
        states.add(0,"Select State");
        statespinner.setItems(states);
    }

    @Override
    public void selectid(String id) {
        idString=id;
    }

    @Override
    public void selectname(String name) {
        nameString=name;
    }

    @Override
    public void selectmobile(String mobile) {
        mobileString=mobile;
    }

    @Override
    public void selectadress(String addreess) {
        addressString=addreess;
    }
}
