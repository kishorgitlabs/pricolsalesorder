package com.brainmagic.pricolsalesorder;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import Logout.logout;
import alertbox.Alertbox;
import alertbox.PrimaryOrder;
import api.models.GetSalesExecutive.SalesExecutiveData;
import api.models.codedata.ExecutiveCodeData;
import api.models.updateversion.UpdateData;
import api.retrofit.APIService;
import api.retrofit.RetrofitClient;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import roomdb.database.AppDatabase;

public class RegionHomeActivity extends AppCompatActivity implements BottomSheetNavigation.BottomSheetListener{
    private LinearLayout attendanceregion,approvalregion,viewexecutiveregion,orderregion,viewVisitReport;
    private TextView title,aspname;
    private ImageView menu,logout;
    private String salesname;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private NetworkConnection networkConnection;
    private Alertbox box=new Alertbox(RegionHomeActivity.this);
    private AppDatabase db;
    private String name;
    private int regid;
    private List<Object>salesexecutive,salesexecutiveid;
    private SearchableSpinner Salesspinner;
    static Button fromdateText,todatetext;
    private String salesname1,salescode1;
    private List<String>data;
    private String[] salescode;
    private String Salesempcode ="All";
    static String fromdatestring,todatestring,fromdatestringorderfrom,todatetextordertp;
    private Boolean OnClick =false;
    private ImageView  setting;
    private TextView appname;
    private BottomSheetNavigation bottomSheetNavigation;
    private SearchableSpinner codespinner;
    private List<String>data1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_region_home);
        attendanceregion=(LinearLayout)findViewById(R.id.attendanceregion);
        approvalregion=(LinearLayout)findViewById(R.id.approvalregion);
        viewexecutiveregion=(LinearLayout)findViewById(R.id.viewexecutiveregion);
        orderregion=(LinearLayout)findViewById(R.id.orderregion);
        viewVisitReport=(LinearLayout)findViewById(R.id.view_visit_report);
        //changepassword=(ImageView) findViewById(R.id.changepassword1);
        title=(TextView) findViewById(R.id.title);
        aspname=(TextView)findViewById(R.id.aspname);
        appname=(TextView)findViewById(R.id.appname);
        logout=(ImageView) findViewById(R.id.logout);
        setting=(ImageView) findViewById(R.id.setting);
        myshare = getSharedPreferences("pricolsalesorder", MODE_PRIVATE);
        editor = myshare.edit();
        title.setText("Welcome");
        name=myshare.getString("namereg","");
        regid=myshare.getInt("idlistreg",0);
        aspname.setText(name);
        appname.setText(name);
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PopupMenu popupMenu = new PopupMenu(RegionHomeActivity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu pop) {
                        pop.dismiss();
                    }
                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.primary:
                                PrimaryOrder alert = new PrimaryOrder(RegionHomeActivity.this);// new saleslogin().execute();
                                alert.showLoginboxtoggle();
                                return true;
                            case R.id.ch:
                                CheckInternetforpass();
                                return true;
                            case R.id.logout:
                                new logout(RegionHomeActivity.this).log_outReg();
                                return true;
                        }
                        return false;
                    }
                });
                popupMenu.inflate(R.menu.pricolcms);
                Menu pop = popupMenu.getMenu();
                popupMenu.show();

            }
        });
//        logout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                new logout(RegionHomeActivity.this).log_outReg();
//            }
//        });
        attendanceregion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i =new Intent(RegionHomeActivity.this,AttendanceRegionActivity.class);
                startActivity(i);

            }
        });


        viewVisitReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(RegionHomeActivity.this,ViewVisitReport.class));
            }
        });

//        changepassword.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                CheckInternetforpass();
//            }
//        });
        viewexecutiveregion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog alertDialog = new AlertDialog.Builder(RegionHomeActivity.this).create();
                LayoutInflater inflater = getLayoutInflater();
                ShowSalesExecutive();
                View dialogView = inflater.inflate(R.layout.viewsalesattendance, null);
                alertDialog.setView(dialogView);
                Salesspinner = (SearchableSpinner) dialogView.findViewById(R.id.Salesspinner);
                fromdateText = (Button) dialogView.findViewById(R.id.fromdateText);
                todatetext = (Button) dialogView.findViewById(R.id.todatetext);
                Button Search = (Button) dialogView.findViewById(R.id.Search);
                 Search.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                       if(fromdateText.getText().toString().equals("")){
                       //  fromdateText.setError("Select From Date");
                            StyleableToast st = new StyleableToast(RegionHomeActivity.this, "Select From Date", Toast.LENGTH_SHORT);
                            st.setBackgroundColor(RegionHomeActivity.this.getResources().getColor(R.color.colorPrimary));
                            st.setTextColor(Color.WHITE);
                            st.setMaxAlpha();
                            st.show();

                        }else if(todatetext.getText().toString().equals("")){
                       //  todatetext.setError("Select To Date");
                            StyleableToast st = new StyleableToast(RegionHomeActivity.this, "Select To Date", Toast.LENGTH_SHORT);
                            st.setBackgroundColor(RegionHomeActivity.this.getResources().getColor(R.color.colorPrimary));
                            st.setTextColor(Color.WHITE);
                            st.setMaxAlpha();
                            st.show();
                        }else {
                            if(OnClick == true)
                            {
                                if(salesname1.equals("All"))
                                {
                                    if(salesname1.equals("All"))
                                    {
                                        Salesempcode="All";
                                    }
                                    else {
                                        Salesempcode =salescode[1];
                                    }
                                    Intent i =new Intent(RegionHomeActivity.this,ViewExecuteAllRegionActivty.class)
                                            .putExtra("salescode",Salesempcode)
                                            .putExtra("fromdate",fromdatestring)
                                            .putExtra("todate",todatestring)
                                            .putExtra("Salesname",Salesspinner.getSelectedItem().toString());
                                    startActivity(i);
                                    alertDialog.dismiss();
                                }
                                else {
                                    if(salesname1.equals("All")){
                                        Salesempcode="All";
                                    }
                                    else {
                                        Salesempcode =salescode[1];
                                    }
                                    Intent i =new Intent(RegionHomeActivity.this,ViewExecuteRegionActiivty.class).putExtra("salescode",Salesempcode).putExtra("fromdate",fromdatestring).putExtra("todate",todatestring).putExtra("Salesname",Salesspinner.getSelectedItem().toString());
                                    startActivity(i);
                                    alertDialog.dismiss();
                                }

                            }
                            else {
                                if(Salesempcode.equals("All")){
                                    if(salesname1.equals("All")){
                                        Salesempcode="All";
                                    }else {
                                        Salesempcode =salescode[1];
                                    }
                                    Intent i =new Intent(RegionHomeActivity.this,ViewExecuteAllRegionActivty.class)
                                            .putExtra("salescode",Salesempcode)
                                            .putExtra("fromdate",fromdatestring)
                                            .putExtra("todate",todatestring)
                                            .putExtra("Salesname",Salesspinner.getSelectedItem().toString());
                                    startActivity(i);
                                    alertDialog.dismiss();
                                }
                            }



                        }

                    }
                });

                fromdateText.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        DialogFragment newFragment = new FromDatePickerFragment();
                        newFragment.show(getSupportFragmentManager(),"datePicker");
                    }
                });

                Salesspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int postion, long id) {
                        if(salesname1 == null){
                            salesname1=Salesspinner.getSelectedItem().toString();
                            salescode=salesname1.split("-");
                        }else {
                            salesname1=Salesspinner.getSelectedItem().toString();
                            salescode=salesname1.split("-");
                            OnClick =true;
                        }

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                todatetext.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        DialogFragment newFragment = new FromDatePickerFragment1();
                        newFragment.show(getSupportFragmentManager(),"datePicker");
                    }
                });

                alertDialog.show();


            }
        });

        approvalregion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i =new Intent(RegionHomeActivity.this,ApprovalDealerRegionActivity.class);
                startActivity(i);
            }

        });

        orderregion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetNavigation = new BottomSheetNavigation();
                bottomSheetNavigation.show(getSupportFragmentManager(), "exampleBottomSheet-");
//                final AlertDialog alertDialog = new AlertDialog.Builder(RegionHomeActivity.this).create();
//
//                LayoutInflater inflater = getLayoutInflater();
//                View dialogView = inflater.inflate(R.layout.order_type, null);
//                alertDialog.setView(dialogView);
//
//                TextView primaryorder = (TextView) dialogView.findViewById(R.id.text2);
//                TextView secondaryorder = (TextView) dialogView.findViewById(R.id.text3);
//
//                primaryorder.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View arg0) {
//                        // TODO Auto-generated method stub
//                        Intent i =new Intent(RegionHomeActivity.this,OrderRegionActivity.class).putExtra("ordertype","Primary Order");
//                        startActivity(i);
//                        alertDialog.dismiss();
//                    }
//                });
//
//                secondaryorder.setOnClickListener(new View.OnClickListener() {
//
//                    @Override
//                    public void onClick(View arg0) {
//                        // TODO Auto-generated method stub
//                        Intent i =new Intent(RegionHomeActivity.this,OrderRegionActivity.class).putExtra("ordertype","Secondary Order");
//                        startActivity(i);
//                        alertDialog.dismiss();
//                    }
//                });
//
//                alertDialog.show();
            }
        });

        getpackageinfo();
        checkInternetversion();
    }

    private int getpackageinfo() {
        int version = -1;
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_META_DATA);
            version = pInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e(this.getClass().getSimpleName(), "Name not found", e1);
        }
        return version;
    }

    private void checkInternetversion() {
        NetworkConnection networkConnection=new NetworkConnection(RegionHomeActivity.this);
        if(networkConnection.CheckInternet()){
            getupdatestatus();
        }else {
            final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(RegionHomeActivity.this).create();
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.loginalert, null);
            alertDialog.setView(dialogView);
            TextView login =(TextView)dialogView.findViewById(R.id.login);
            login.setText("Please Check your Internet Connection!");
            Button submit =(Button) dialogView.findViewById(R.id.submit);
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();

                }

            });
            alertDialog.show();
        }
    }



    private void CheckInternetforpass() {
        NetworkConnection network = new NetworkConnection(RegionHomeActivity.this);
        if (network.CheckInternet()) {
            PasswordChangeAlertRegion alert = new PasswordChangeAlertRegion(RegionHomeActivity.this);// new saleslogin().execute();
            alert.showLoginbox();
        } else {
            final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(RegionHomeActivity.this).create();
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.loginalert, null);
            alertDialog.setView(dialogView);
            TextView login =(TextView)dialogView.findViewById(R.id.login);
            login.setText("Please Check Your internet Connection !");
            Button submit =(Button) dialogView.findViewById(R.id.submit);
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();

                }

            });
            alertDialog.show();
        }

    }

    private void getupdatestatus() {
        try {
//            final ProgressDialog loading = ProgressDialog.show(HomeActivty.this, getResources().getString(R.string.app_name), "Downloading...", false, false);
            APIService service = RetrofitClient.getApiService();
            Call<UpdateData> call = service.update(getpackageinfo());
            //  Call<Master> call = service.GetMasterData("Siva");
            call.enqueue(new Callback<UpdateData>() {
                @Override
                public void onResponse(Call<UpdateData> call, Response<UpdateData> response) {
                    try {
                        // loading.dismiss();
                        if (response.body().getData().equals("Update Available")) {
                            final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(RegionHomeActivity.this).create();
                            LayoutInflater inflater = getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.loginalertupdate, null);
                            alertDialog.setView(dialogView);
                            TextView login = (TextView) dialogView.findViewById(R.id.login);
                            login.setText("Update Available!");
                            Button update1 = (Button) dialogView.findViewById(R.id.update1);
                            Button skip = (Button) dialogView.findViewById(R.id.skip);
                            update1.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                                    try {
                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                    } catch (android.content.ActivityNotFoundException anfe) {
                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                    }


                                }

                            });

                            skip.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    alertDialog.dismiss();
                                }
                            });
                            alertDialog.show();
                        }


//                            box.showAlertbox(getResources().getString(R.string.server_error));
                    } catch (Exception e) {
                        e.printStackTrace();
                        //loading.dismiss();
                        final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(RegionHomeActivity.this).create();
                        LayoutInflater inflater = getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.loginalert, null);
                        alertDialog.setView(dialogView);
                        TextView login =(TextView)dialogView.findViewById(R.id.login);
                        login.setText(getResources().getString(R.string.server_error));
                        Button submit =(Button) dialogView.findViewById(R.id.submit);
                        submit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                alertDialog.dismiss();

                            }

                        });
                        alertDialog.show();
                    }
                }
                @Override
                public void onFailure(Call<UpdateData> call, Throwable t) {
                    // loading.dismiss();
                    t.printStackTrace();
                    final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(RegionHomeActivity.this).create();
                    LayoutInflater inflater = getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.loginalert, null);
                    alertDialog.setView(dialogView);
                    TextView login =(TextView)dialogView.findViewById(R.id.login);
                    login.setText(getResources().getString(R.string.server_error));
                    Button submit =(Button) dialogView.findViewById(R.id.submit);
                    submit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();

                        }

                    });
                    alertDialog.show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

//        if (getpackageinfo() < 2){
//            final AlertDialog alertDialog = new AlertDialog.Builder(HomeActivty.this).create();
//            LayoutInflater inflater = getLayoutInflater();
//            View dialogView = inflater.inflate(R.layout.loginalertupdate, null);
//            alertDialog.setView(dialogView);
//            TextView login =(TextView)dialogView.findViewById(R.id.login);
//            login.setText("Update Available!");
//            Button update1 =(Button) dialogView.findViewById(R.id.update1);
//            Button skip =(Button) dialogView.findViewById(R.id.skip);
//            update1.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
//                    try {
//                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
//                    } catch (android.content.ActivityNotFoundException anfe) {
//                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
//                    }
//
//
//                }
//
//            });
//
//            skip.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    alertDialog.dismiss();
//                }
//            });
//            alertDialog.show();
//            final android.support.v7.app.AlertDialog.Builder alertbox=new android.support.v7.app.AlertDialog.Builder(this);
//            alertbox.setTitle("Update Available");
//            alertbox.setCancelable(false);
//            alertbox.setPositiveButton("Update", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
//                    try {
//                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
//                    } catch (android.content.ActivityNotFoundException anfe) {
//                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
//                    }
//
//
//                }
//            });
//
//            alertbox.setNegativeButton("Skip", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//
//                    dialog.cancel();
//
//                }
//            });
//            android.support.v7.app.AlertDialog alert11 = alertbox.create();
//            alert11.show();
//
        // }
    }
    private void ShowSalesExecutive() {
        try {

            final ProgressDialog loading = ProgressDialog.show(RegionHomeActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);
            APIService service = RetrofitClient.getApiService();

            Call<SalesExecutiveData> call = service.GetSalesExecutive(regid);
            call.enqueue(new Callback<SalesExecutiveData>() {
                @Override

                public void onResponse(Call<SalesExecutiveData> call, Response<SalesExecutiveData> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success"))
                        {
                            data=response.body().getData();
                            //data.add(0,"Select Sales Executive Name");
                            data.add(0,"All");
                            ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(RegionHomeActivity.this,R.layout.simple_spinner_item,data);
                            spinnerAdapter.setNotifyOnChange(true);
                            spinnerAdapter.setDropDownViewResource(R.layout.simple_spinner_item);
                            Salesspinner.setTitle("Select Sales Executive Name");
                            Salesspinner.setAdapter(spinnerAdapter);



                        }
                        else if(response.body().getResult().equals("InvalidUser")) {
                            final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(RegionHomeActivity.this).create();
                            LayoutInflater inflater = getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.loginalert, null);
                            alertDialog.setView(dialogView);
                            TextView login =(TextView)dialogView.findViewById(R.id.login);
                            login.setText("Invalid User !");
                            Button submit =(Button) dialogView.findViewById(R.id.submit);
                            submit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    alertDialog.dismiss();

                                }

                            });
                            alertDialog.show();
                        }else
                        {
                            final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(RegionHomeActivity.this).create();
                            LayoutInflater inflater = getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.loginalert, null);
                            alertDialog.setView(dialogView);
                            TextView login =(TextView)dialogView.findViewById(R.id.login);
                            login.setText("No SalesExecutive !");
                            Button submit =(Button) dialogView.findViewById(R.id.submit);
                            submit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    alertDialog.dismiss();

                                }

                            });
                            alertDialog.show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(RegionHomeActivity.this).create();
                        LayoutInflater inflater = getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.loginalert, null);
                        alertDialog.setView(dialogView);
                        TextView login =(TextView)dialogView.findViewById(R.id.login);
                        login.setText("Something went wrong .Please try again later .");
                        Button submit =(Button) dialogView.findViewById(R.id.submit);
                        submit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                alertDialog.dismiss();

                            }

                        });
                        alertDialog.show();

                    }
                }

                @Override
                public void onFailure(Call<SalesExecutiveData> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(RegionHomeActivity.this).create();
                    LayoutInflater inflater = getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.loginalert, null);
                    alertDialog.setView(dialogView);
                    TextView login =(TextView)dialogView.findViewById(R.id.login);
                    login.setText("Something went wrong .Please try again later .");
                    Button submit =(Button) dialogView.findViewById(R.id.submit);
                    submit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();

                        }

                    });
                    alertDialog.show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void bottomSheetListener(String orderhistoryfilter) {
        if (orderhistoryfilter.equals("currentmonthwise")) {
            Intent i = new Intent(RegionHomeActivity.this, OrderRegionActivity.class).putExtra("type", "Secondary Order");
            startActivity(i);
            bottomSheetNavigation.dismiss();

        } else {
            final AlertDialog alertDialog = new AlertDialog.Builder(RegionHomeActivity.this).create();
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.viewsalesdatewisehistory, null);
            alertDialog.setView(dialogView);
            //GetCode();
            ShowSalesExecutivecode();
            fromdateText = (Button) dialogView.findViewById(R.id.fromdateText);
            todatetext = (Button) dialogView.findViewById(R.id.todatetext);
            codespinner = (SearchableSpinner) dialogView.findViewById(R.id.codespinner);
            Button Search = (Button) dialogView.findViewById(R.id.Search);

           codespinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
               @Override
               public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                   if(salescode1 == null){
                       salescode1=codespinner.getSelectedItem().toString();

                   }else {
                       salescode1=codespinner.getSelectedItem().toString();
                       OnClick =true;
                   }

               }

               @Override
               public void onNothingSelected(AdapterView<?> adapterView) {

               }
           });
            Search.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (fromdateText.getText().toString().equals("")) {
                       // fromdateText.setError("Select From Date");
                            StyleableToast st = new StyleableToast(RegionHomeActivity.this, "Select From Date", Toast.LENGTH_SHORT);
                            st.setBackgroundColor(RegionHomeActivity.this.getResources().getColor(R.color.colorPrimary));
                            st.setTextColor(Color.WHITE);
                            st.setMaxAlpha();
                            st.show();

                    } else if (todatetext.getText().toString().equals("")) {
                       // todatetext.setError("Select To Date");
                            StyleableToast st = new StyleableToast(RegionHomeActivity.this, "Select To Date", Toast.LENGTH_SHORT);
                            st.setBackgroundColor(RegionHomeActivity.this.getResources().getColor(R.color.colorPrimary));
                            st.setTextColor(Color.WHITE);
                            st.setMaxAlpha();
                            st.show();
                    } else {
                        if (OnClick == true) {
                            if (salescode1.equals("All")) {
                                Intent i = new Intent(RegionHomeActivity.this, OrderWiseDateHistoryActivity.class).putExtra("salescode", salescode1).putExtra("fromdate", fromdatestringorderfrom).putExtra("todate", todatetextordertp);
                                startActivity(i);
                                alertDialog.dismiss();
                            } else {
                                String[] salescodename=salescode1.split("-");
                                Intent i = new Intent(RegionHomeActivity.this, OrderWiseDateHistoryActivity.class).putExtra("fromdate", fromdatestringorderfrom).putExtra("todate", todatetextordertp).putExtra("salescode", salescodename[1]);
                                startActivity(i);
                                alertDialog.dismiss();
                            }
                        } else {
                            if (salescode1.equals("All")) {
                             //   String[] salescodename=salescode1.split("-");
                                Intent i = new Intent(RegionHomeActivity.this, OrderWiseDateHistoryActivity.class).putExtra("salescode", salescode1).putExtra("fromdate", fromdatestringorderfrom).putExtra("todate", todatetextordertp);
                                startActivity(i);
                                alertDialog.dismiss();
                            } else {
                                String[] salescodename=salescode1.split("-");
                                Intent i = new Intent(RegionHomeActivity.this, OrderWiseDateHistoryActivity.class).putExtra("fromdate", fromdatestringorderfrom).putExtra("todate", todatetextordertp).putExtra("salescode", salescodename[1]);
                                startActivity(i);
                                alertDialog.dismiss();
                            }
                        }
                    }
                }


            });

            fromdateText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DialogFragment newFragment = new FromDatePickerFragmenthistoryfrom();
                    newFragment.show(getSupportFragmentManager(),"datePicker");
                }
            });



            todatetext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DialogFragment newFragment = new FromDatePickerFragmenthistoryto();
                    newFragment.show(getSupportFragmentManager(),"datePicker");
                }
            });

            alertDialog.show();

            //new
//            final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(RegionHomeActivity.this).create();
//            LayoutInflater inflater = getLayoutInflater();
//            View dialogView = inflater.inflate(R.layout.loginalert, null);
//            alertDialog.setView(dialogView);
//            TextView login =(TextView)dialogView.findViewById(R.id.login);
//            login.setText("This page is Under Construction! Sorry for inconvenience.");
//            Button submit =(Button) dialogView.findViewById(R.id.submit);
//            submit.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    alertDialog.dismiss();
//
//                }
//
//            });
//            alertDialog.show();

            bottomSheetNavigation.dismiss();

        }
    }

    private void ShowSalesExecutivecode() {
        try {

            final ProgressDialog loading = ProgressDialog.show(RegionHomeActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);
            APIService service = RetrofitClient.getApiService();

            Call<SalesExecutiveData> call = service.GetSalesExecutive(regid);
            call.enqueue(new Callback<SalesExecutiveData>() {
                @Override

                public void onResponse(Call<SalesExecutiveData> call, Response<SalesExecutiveData> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success")) {
                            data=response.body().getData();
                            data.add(0,"All");
                            ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(RegionHomeActivity.this,R.layout.simple_spinner_item,data);
                            spinnerAdapter.setNotifyOnChange(true);
                            spinnerAdapter.setDropDownViewResource(R.layout.simple_spinner_item);
                            codespinner.setTitle("Select Sales Executive Code");
                            codespinner.setAdapter(spinnerAdapter);





                        } else if(response.body().getResult().equals("InvalidUser")) {
                            final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(RegionHomeActivity.this).create();
                            LayoutInflater inflater = getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.loginalert, null);
                            alertDialog.setView(dialogView);
                            TextView login =(TextView)dialogView.findViewById(R.id.login);
                            login.setText("Invalid User !");
                            Button submit =(Button) dialogView.findViewById(R.id.submit);
                            submit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    alertDialog.dismiss();

                                }

                            });
                            alertDialog.show();
                        }else
                        {
                            final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(RegionHomeActivity.this).create();
                            LayoutInflater inflater = getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.loginalert, null);
                            alertDialog.setView(dialogView);
                            TextView login =(TextView)dialogView.findViewById(R.id.login);
                            login.setText("No SalesExecutive !");
                            Button submit =(Button) dialogView.findViewById(R.id.submit);
                            submit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    alertDialog.dismiss();

                                }

                            });
                            alertDialog.show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(RegionHomeActivity.this).create();
                        LayoutInflater inflater = getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.loginalert, null);
                        alertDialog.setView(dialogView);
                        TextView login =(TextView)dialogView.findViewById(R.id.login);
                        login.setText("Something went wrong .Please try again later .");
                        Button submit =(Button) dialogView.findViewById(R.id.submit);
                        submit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                alertDialog.dismiss();

                            }

                        });
                        alertDialog.show();

                    }
                }

                @Override
                public void onFailure(Call<SalesExecutiveData> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(RegionHomeActivity.this).create();
                    LayoutInflater inflater = getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.loginalert, null);
                    alertDialog.setView(dialogView);
                    TextView login =(TextView)dialogView.findViewById(R.id.login);
                    login.setText("Something went wrong .Please try again later .");
                    Button submit =(Button) dialogView.findViewById(R.id.submit);
                    submit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();

                        }

                    });
                    alertDialog.show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void GetCode() {
        try {

            final ProgressDialog loading = ProgressDialog.show(RegionHomeActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);
            APIService service = RetrofitClient.getApiService();

            Call<ExecutiveCodeData> call = service.executivecode(regid);
            call.enqueue(new Callback<ExecutiveCodeData>() {
                @Override

                public void onResponse(Call<ExecutiveCodeData> call, Response<ExecutiveCodeData> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success")) {
                            data1=  response.body().getData();
                            data1.add(0,"All");
                            ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(RegionHomeActivity.this,R.layout.simple_spinner_item,data1);
                            spinnerAdapter.setNotifyOnChange(true);
                            spinnerAdapter.setDropDownViewResource(R.layout.simple_spinner_item);
                            codespinner.setTitle("Select Sales Executive Code");
                            codespinner.setAdapter(spinnerAdapter);



                        } else if(response.body().getResult().equals("InvalidUser")) {
                            final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(RegionHomeActivity.this).create();
                            LayoutInflater inflater = getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.loginalert, null);
                            alertDialog.setView(dialogView);
                            TextView login =(TextView)dialogView.findViewById(R.id.login);
                            login.setText("Invalid User !");
                            Button submit =(Button) dialogView.findViewById(R.id.submit);
                            submit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    alertDialog.dismiss();

                                }

                            });
                            alertDialog.show();
                        }else
                        {
                            final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(RegionHomeActivity.this).create();
                            LayoutInflater inflater = getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.loginalert, null);
                            alertDialog.setView(dialogView);
                            TextView login =(TextView)dialogView.findViewById(R.id.login);
                            login.setText("No SalesExecutive !");
                            Button submit =(Button) dialogView.findViewById(R.id.submit);
                            submit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    alertDialog.dismiss();

                                }

                            });
                            alertDialog.show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(RegionHomeActivity.this).create();
                        LayoutInflater inflater = getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.loginalert, null);
                        alertDialog.setView(dialogView);
                        TextView login =(TextView)dialogView.findViewById(R.id.login);
                        login.setText("Something went wrong .Please try again later .");
                        Button submit =(Button) dialogView.findViewById(R.id.submit);
                        submit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                alertDialog.dismiss();

                            }

                        });
                        alertDialog.show();

                    }
                }

                @Override
                public void onFailure(Call<ExecutiveCodeData> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(RegionHomeActivity.this).create();
                    LayoutInflater inflater = getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.loginalert, null);
                    alertDialog.setView(dialogView);
                    TextView login =(TextView)dialogView.findViewById(R.id.login);
                    login.setText("Something went wrong .Please try again later .");
                    Button submit =(Button) dialogView.findViewById(R.id.submit);
                    submit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();

                        }

                    });
                    alertDialog.show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    private void GetSales(List<SalesExecutive> data) {
//        salesexecutive=(List<Object>) CollectionUtils.collect(data, TransformerUtils.invokerTransformer("getName"));
//        salesexecutiveid=(List<Object>) CollectionUtils.collect(data, TransformerUtils.invokerTransformer("getId"));
//        salesexecutive.add(0,"All");
//        salesexecutiveid.add(0,"All");
//        List<String> salesstringlist= new ArrayList<String>(salesexecutive.size());
//        for (Object object : salesexecutive) {
//            salesstringlist.add(Objects.toString(object, null));
//        }
//
//        List<String> salesstringidlist= new ArrayList<String>(salesexecutiveid.size());
//        for (Object object : salesexecutiveid) {
//            salesstringidlist.add(Objects.toString(object, null));
//        }
//        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(RegionHomeActivity.this,R.layout.simple_spinner_item,salesstringlist);
//        spinnerAdapter.setNotifyOnChange(true);
//        spinnerAdapter.setDropDownViewResource(R.layout.simple_spinner_item);
//        Salesspinner.setAdapter(spinnerAdapter);
//    }

    public static class FromDatePickerFragment extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);
            dialog.getDatePicker().setMaxDate(new Date().getTime());
            return dialog;
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            //FromDate.setText(day + "/" + (month + 1) + "/" + year);
            fromdateText.setText(day + "-" + (month + 1) + "-" + year);
            fromdatestring=year + "-" + (month + 1) + "-" + day;
            fromdateText.clearFocus();
        }
    }

    public static class FromDatePickerFragment1 extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);
            dialog.getDatePicker().setMaxDate(new Date().getTime());
            return dialog;
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            //FromDate.setText(day + "/" + (month + 1) + "/" + year);
            todatetext.setText(day + "-" + (month + 1) + "-" + year);
            todatestring=year + "-" + (month + 1) + "-" + day;
            todatetext.clearFocus();
        }
    }

    public static class FromDatePickerFragmenthistoryfrom extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);
            dialog.getDatePicker().setMaxDate(new Date().getTime());
            return dialog;
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            //FromDate.setText(day + "/" + (month + 1) + "/" + year);
            fromdateText.setText(day + "-" + (month + 1) + "-" + year);
            fromdatestringorderfrom=year + "-" + (month + 1) + "-" + day;
            fromdateText.clearFocus();
        }
    }
    public static class FromDatePickerFragmenthistoryto extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);
            dialog.getDatePicker().setMaxDate(new Date().getTime());
            return dialog;
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            //FromDate.setText(day + "/" + (month + 1) + "/" + year);
            todatetext.setText(day + "-" + (month + 1) + "-" + year);
            todatetextordertp=year + "-" + (month + 1) + "-" + day;
            todatetext.clearFocus();
        }
    }


}
