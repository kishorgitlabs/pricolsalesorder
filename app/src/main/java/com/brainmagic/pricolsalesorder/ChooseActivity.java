package com.brainmagic.pricolsalesorder;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.muddzdev.styleabletoastlibrary.StyleableToast;

import java.util.ArrayList;
import java.util.List;

import Home.homevalues;
import Logout.logout;
import alertbox.PasswordChangeAlert;
import alertbox.PrimaryOrder;
import network.NetworkConnection;
import roomdb.database.AppDatabase;

import static roomdb.database.AppDatabase.getAppDatabase;

public class ChooseActivity extends AppCompatActivity {
    private TextView title,salesordertext,visitreporttext,createothertext;
    private String orderacc,salesid,states,city,shopname,mobilenumber,gst,region,name,address,emailid;
    private List<String> companylist,customerslist,ordertypelist;
    private MaterialSpinner companyspinner,ordertypespin;
    private TextView ordertype,customertype;
    private TableRow ordertypetablerow;
    private Button submit;
    private String companystring,customerstring,ordertypestring;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private ImageView setting,home;
    private List<String>alreadycreatedcustomermobile;
    private AppDatabase db1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose);
        db1=getAppDatabase(ChooseActivity.this);
        myshare = getSharedPreferences("pricolsalesorder", MODE_PRIVATE);
        editor = myshare.edit();
        title=(TextView)findViewById(R.id.title);
        salesordertext=(TextView)findViewById(R.id.salesordertext);
        visitreporttext=(TextView)findViewById(R.id.visitreporttext);
        createothertext=(TextView)findViewById(R.id.createothertext);
        title.setText("New Customer");
        orderacc=getIntent().getStringExtra("ordertype");
        salesid=getIntent().getStringExtra("executeid");
        states=getIntent().getStringExtra("states");
        city=getIntent().getStringExtra("citys");
        shopname=getIntent().getStringExtra("shopname");
        mobilenumber=getIntent().getStringExtra("mobilestring");
        gst=getIntent().getStringExtra("gst");
        region=getIntent().getStringExtra("regionstring");
        name=getIntent().getStringExtra("name");
        address=getIntent().getStringExtra("address");
        emailid=getIntent().getStringExtra("emailid");
        setting = (ImageView) findViewById(R.id.setting);
        home = (ImageView) findViewById(R.id.home);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new homevalues(ChooseActivity.this).home_io();
            }
        });

        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PopupMenu popupMenu = new PopupMenu(ChooseActivity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu pop) {
                        pop.dismiss();
                    }
                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.primary:
                                PrimaryOrder alert = new PrimaryOrder(ChooseActivity.this);// new saleslogin().execute();
                                alert.showLoginboxtoggle();
                                return true;
                            case R.id.ch:
                                CheckInternetforpass();
                                return true;
                            case R.id.logout:
                                new logout(ChooseActivity.this).log_out();
                                return true;
                        }
                        return false;
                    }
                });
                popupMenu.inflate(R.menu.pricolcms);
                Menu pop = popupMenu.getMenu();
                popupMenu.show();

            }
        });
        salesordertext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog alertDialog = new AlertDialog.Builder(ChooseActivity.this).create();

                LayoutInflater inflater = getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.order_type, null);
                alertDialog.setView(dialogView);
                companyspinner=(MaterialSpinner)dialogView.findViewById(R.id.companyspinner);
                customertype=(TextView)dialogView.findViewById(R.id.customertype);
                ordertypespin=(MaterialSpinner)dialogView.findViewById(R.id.ordertypespin);
                ordertype=(TextView) dialogView.findViewById(R.id.ordertype);
                ordertypetablerow=(TableRow) dialogView.findViewById(R.id.ordertypetablerow);
                submit=(Button)dialogView.findViewById(R.id.submit);
                customertype.setText(orderacc);
                //companylist
                companylist=new ArrayList<>();
                companylist.add(0,"Company Name");
                companylist.add("Pricol");
                companylist.add("PEIL");
                companylist.add("Xenos");
                companyspinner.setItems(companylist);

                //customerlist

                //ordertypelist
                ordertypelist=new ArrayList<>();
                ordertypelist.add(0," Order Type");
                ordertypelist.add("Primary Order");
                ordertypelist.add("Secondary Order");
                ordertypespin.setItems(ordertypelist);

                ordertypespin.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                        ordertypestring=item.toString();
                    }
                });


                companyspinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                        companystring=item.toString();
                    }
                });

                if(myshare.getBoolean("primaryorder",true)){
                    ordertypetablerow.setVisibility(View.VISIBLE);
                }else {
                    ordertype.setText("Secondary Order");
                    ordertypetablerow.setVisibility(View.GONE);
                    //Action
                }
                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(myshare.getBoolean("primaryorder",true)){
                            if(companyspinner.getText().toString().equals("Company Name")){
                                companyspinner.setError("Select Company Name");
//                                StyleableToast st = new StyleableToast(ChooseActivity.this, "Select Company Name", Toast.LENGTH_SHORT);
//                                st.setBackgroundColor(ChooseActivity.this.getResources().getColor(R.color.colorPrimary));
//                                st.setTextColor(Color.WHITE);
//                                st.setMaxAlpha();
//                                st.show();

                            }else if(customertype.getText().toString().equals("")) {
                                customertype.setError("Select Customer Type");
//                                StyleableToast st = new StyleableToast(ChooseActivity.this, "Select Company Type", Toast.LENGTH_SHORT);
//                                st.setBackgroundColor(ChooseActivity.this.getResources().getColor(R.color.colorPrimary));
//                                st.setTextColor(Color.WHITE);
//                                st.setMaxAlpha();
//                                st.show();
                            }else if(ordertypespin.getText().toString().equals("Select Order Type")){
                                ordertypespin.setError("Select Order Type");
//                                StyleableToast st = new StyleableToast(ChooseActivity.this, "Select Order Type", Toast.LENGTH_SHORT);
//                                st.setBackgroundColor(ChooseActivity.this.getResources().getColor(R.color.colorPrimary));
//                                st.setTextColor(Color.WHITE);
//                                st.setMaxAlpha();
//                                st.show();

                            } else {
                                Intent i =new Intent(ChooseActivity.this,SalesSecondaryActivity.class).putExtra("ordertype",orderacc).putExtra("executeid",salesid)
                                        .putExtra("states",states).putExtra("citys",city).putExtra("shopname",shopname).putExtra("newShops",getIntent().getStringExtra("newShops")).putExtra("mobilestring",mobilenumber)
                                        .putExtra("gst",gst).putExtra("regionstring",region).putExtra("name",name).putExtra("chooseactivity","fromchoosed").putExtra("address",address).putExtra("emailstring",emailid)
                                        .putExtra("companynamae",companystring).putExtra("customertype",customertype.getText().toString()).putExtra("ordertypes",ordertype.getText().toString());;
//                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(i);
                                alertDialog.dismiss();
                            }


                        }else {
                            if(companyspinner.getText().toString().equals("Select Company Name")){
                                companyspinner.setError("Select Company Name");
//                                StyleableToast st = new StyleableToast(ChooseActivity.this, "Select Company Name", Toast.LENGTH_SHORT);
//                                st.setBackgroundColor(ChooseActivity.this.getResources().getColor(R.color.colorPrimary));
//                                st.setTextColor(Color.WHITE);
//                                st.setMaxAlpha();
//                                st.show();

                            }else if(customertype.getText().toString().equals("")) {
                                customertype.setError("Select Customer Type");
//                                StyleableToast st = new StyleableToast(ChooseActivity.this, "Select Company Type", Toast.LENGTH_SHORT);
//                                st.setBackgroundColor(ChooseActivity.this.getResources().getColor(R.color.colorPrimary));
//                                st.setTextColor(Color.WHITE);
//                                st.setMaxAlpha();
//                                st.show();
                            }else if(ordertype.getText().toString().equals("")){
                                ordertypespin.setError("Select Order Type");
//                                StyleableToast st = new StyleableToast(ChooseActivity.this, "Select Order Type", Toast.LENGTH_SHORT);
//                                st.setBackgroundColor(ChooseActivity.this.getResources().getColor(R.color.colorPrimary));
//                                st.setTextColor(Color.WHITE);
//                                st.setMaxAlpha();
//                                st.show();

                            } else {
                                Intent i =new Intent(ChooseActivity.this,SalesSecondaryActivity.class).putExtra("ordertype",orderacc).putExtra("executeid",salesid)
                                        .putExtra("states",states).putExtra("citys",city).putExtra("shopname",shopname).putExtra("newShops",getIntent().getStringExtra("newShops")).putExtra("mobilestring",mobilenumber)
                                        .putExtra("gst",gst).putExtra("regionstring",region).putExtra("name",name).putExtra("chooseactivity","fromchoosed").putExtra("address",address).putExtra("emailstring",emailid)
                                        .putExtra("companynamae",companystring).putExtra("customertype",customertype.getText().toString()).putExtra("ordertypes",ordertype.getText().toString());;
//                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(i);
                                alertDialog.dismiss();
                            }

                        }


                    }
                });

           alertDialog.show();
            }
        });


        visitreporttext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i =new Intent(ChooseActivity.this,ExsitingvistorActivityNew.class)
                .putExtra("accounttype","New Customer")
                .putExtra("customertype",orderacc).putExtra("customername",shopname).putExtra("mobile",mobilenumber)
                        .putExtra("frmchoosen","fromchoosen");
                startActivity(i);
            }
        });

        createothertext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i =new Intent(ChooseActivity.this,CreateNewActivity.class);
//                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
            }
        });

    }

    @Override
    public void onBackPressed() {
        if(mobilenumber!=null)
        {
            alreadycreatedcustomermobile=db1.adhereDao().Getlocallycreateddealer();
            if(alreadycreatedcustomermobile.size()!=0)
            {
                if(alreadycreatedcustomermobile.contains(mobilenumber))
                {
                    final AlertDialog alertDialog = new AlertDialog.Builder(ChooseActivity.this).create();
                    LayoutInflater inflater = getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.loginalertupdate, null);
                    alertDialog.setView(dialogView);
                    TextView login =(TextView)dialogView.findViewById(R.id.login);
//                    login.setText("Without making SalesOrder or Visit Report, the newly added customer will be removed from the list.");
                    login.setText("Do you want to close. By closing, the newly added Customer will not be saved. Do you want to Continue");
                    Button submit =(Button) dialogView.findViewById(R.id.skip);
                    Button cancel =(Button) dialogView.findViewById(R.id.update1);
                    submit.setText("Continue");
                    cancel.setText("Cancel");
                    submit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();
                            db1.adhereDao().DeleteAllCreate(mobilenumber);
                            finish();


                        }
                    });
                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();
                        }
                    });
                    alertDialog.show();
                }
                else super.onBackPressed();
            }else super.onBackPressed();
        }else super.onBackPressed();
    }

    private void CheckInternetforpass() {
        NetworkConnection network = new NetworkConnection(ChooseActivity.this);
        if (network.CheckInternet()) {
            PasswordChangeAlert alert = new PasswordChangeAlert(ChooseActivity.this);// new saleslogin().execute();
            alert.showLoginbox();
        } else {
            final AlertDialog alertDialog = new AlertDialog.Builder(ChooseActivity.this).create();
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.loginalert, null);
            alertDialog.setView(dialogView);
            TextView login =(TextView)dialogView.findViewById(R.id.login);
            login.setText("Please Check your Internet Connection !!");
            Button submit =(Button) dialogView.findViewById(R.id.submit);
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();

                }

            });
            alertDialog.show();
        }
    }
}
