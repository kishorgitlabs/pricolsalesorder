package com.brainmagic.pricolsalesorder;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.jaredrummler.materialspinner.MaterialSpinner;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.TransformerUtils;

import java.util.ArrayList;
import java.util.List;

import Home.homevalues;
import Logout.logout;
import alertbox.Alertbox;
import alertbox.PasswordChangeAlert;
import alertbox.PrimaryOrder;
import api.models.master.Citylist;
import api.models.master.Statelist;
import api.models.master.createlist;
import network.NetworkConnection;
import roomdb.database.AppDatabase;
import roomdb.entity.SecondaryOrderDetails;
import static roomdb.database.AppDatabase.getAppDatabase;

public class CreateNewActivity extends AppCompatActivity {
    private MaterialSpinner ordertypespin;
    private EditText mobilenumber,address,shopname,contactperson,name,gst,emailid;
    private LinearLayout shplinear,contactline,namelin,gstlin;
    private List<String>ordertypelist;
    private String orderacc;
    private Button add;
    private MaterialSpinner state_spinner,city_edit;
    private TextView region;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private String regionstr,states,city;
    private int Salesid;
    private AppDatabase db1;
    private ProgressDialog progressDialog;
    Alertbox box = new Alertbox(CreateNewActivity.this);
    private String namestring,shopnamestring,addressstring,mobilenumberstring,gststring,emailidString;
    private createlist create;
    private Boolean Order =false;
    private TextView title;
    private List<String>alreadycreatedcustomermobile,approveddealer;
    private ImageView setting;
    private ImageView home;
    private EditText othecityedit;
    private LinearLayout othercity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_new);
        myshare = getSharedPreferences("pricolsalesorder", MODE_PRIVATE);
        editor = myshare.edit();
        db1=getAppDatabase(CreateNewActivity.this);
        ordertypespin=(MaterialSpinner)findViewById(R.id.ordertypespin);
        state_spinner=(MaterialSpinner)findViewById(R.id.state_spinner);
        city_edit=(MaterialSpinner)findViewById(R.id.city_edit);
        mobilenumber=(EditText)findViewById(R.id.mobileedit);
        address=(EditText)findViewById(R.id.addressedit);
        shopname=(EditText)findViewById(R.id.shopname);
        contactperson=(EditText)findViewById(R.id.contactperson);
        name=(EditText)findViewById(R.id.nameedit);
        gst=(EditText)findViewById(R.id.gst);
        emailid=(EditText)findViewById(R.id.emailid);
        shplinear=(LinearLayout) findViewById(R.id.shplinear);
        contactline=(LinearLayout) findViewById(R.id.contactline);
        namelin=(LinearLayout) findViewById(R.id.namelin);
        gstlin=(LinearLayout) findViewById(R.id.gstlin);
        add=(Button) findViewById(R.id.add);
        region=(TextView) findViewById(R.id.region);
        setting=(ImageView) findViewById(R.id.setting);
        home=(ImageView) findViewById(R.id.home);
        title=(TextView) findViewById(R.id.title);
        title.setText("Create New Customer");
        othercity=(LinearLayout) findViewById(R.id.othercity);
        othecityedit=(EditText) findViewById(R.id.othecityedit);
        regionstr=myshare.getString("regName","");
        Salesid=myshare.getInt("idlist",0);
        Order=getIntent().getBooleanExtra("Order",false);
        ordertypelist=new ArrayList<>();
        ordertypelist.add(0,"Select Customer Type");
        ordertypelist.add("Retailer");
        ordertypelist.add("Mechanic");
        ordertypelist.add("Electrician");
        ordertypelist.add("End Customer");
        ordertypelist.add("Others");
        ordertypespin.setItems(ordertypelist);
        region.setText(regionstr);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new homevalues(CreateNewActivity.this).home_io();

            }
        });
        ordertypespin.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                orderacc=item.toString();
                if(orderacc.equals("Retailer") || (orderacc.equals("Mechanic")) || (orderacc.equals("Electrician"))){
                    shplinear.setVisibility(View.VISIBLE);
                }else if(orderacc.equals("Select Account")) {
                    shplinear.setVisibility(View.GONE);
                }else {
                    shplinear.setVisibility(View.GONE);
                }
            }
        });

        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PopupMenu popupMenu = new PopupMenu(CreateNewActivity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu pop) {
                        pop.dismiss();
                    }
                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.primary:
                                PrimaryOrder alert = new PrimaryOrder(CreateNewActivity.this);// new saleslogin().execute();
                                alert.showLoginboxtoggle();
                                return true;
                            case R.id.ch:
                                CheckInternetforpass();
                                return true;
                            case R.id.logout:
                                new logout(CreateNewActivity.this).log_out();
                                return true;
                        }
                        return false;
                    }
                });
                popupMenu.inflate(R.menu.pricolcms);
                Menu pop = popupMenu.getMenu();
                popupMenu.show();

            }
        });
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<SecondaryOrderDetails>details=db1.adhereDao().customerDetails();
                if(ordertypespin.getText().toString().equals("Select Customer Type")){

                    ordertypespin.setError("Select Customer Type");
//                    StyleableToast st = new StyleableToast(CreateNewActivity.this, "Choose Account", Toast.LENGTH_SHORT);
//                    st.setBackgroundColor(CreateNewActivity.this.getResources().getColor(R.color.colorPrimary));
//                    st.setTextColor(Color.WHITE);
//                    st.setMaxAlpha();
//                    st.show();

                }else if(ordertypespin.getText().toString().equals("Retailer") ||ordertypespin.getText().toString().equals("Mechanic") || ordertypespin.getText().toString().equals("Electrician") ){
                    if(details.size()!=0) {
                        if (details.get(0).getMobileNo().contains(mobilenumber.getText().toString())) {
                            // no action
                            if(shopname.getText().toString().equals("")){
                                shopname.setError("Enter Shop Name");
//                                StyleableToast st = new StyleableToast(CreateNewActivity.this, "Enter Shop Name", Toast.LENGTH_SHORT);
//                                st.setBackgroundColor(CreateNewActivity.this.getResources().getColor(R.color.colorPrimary));
//                                st.setTextColor(Color.WHITE);
//                                st.setMaxAlpha();
//                                st.show();

                            }else if(contactperson.getText().toString().equals("")){
                                contactperson.setError("Enter Contact Person Name");
//                                StyleableToast st = new StyleableToast(CreateNewActivity.this, "Enter Contact Person Name", Toast.LENGTH_SHORT);
//                                st.setBackgroundColor(CreateNewActivity.this.getResources().getColor(R.color.colorPrimary));
//                                st.setTextColor(Color.WHITE);
//                                st.setMaxAlpha();
//                                st.show();


                            }else  if(state_spinner.getText().toString().equals("Select State")){
                                state_spinner.setError("Select State");
//                                StyleableToast st = new StyleableToast(CreateNewActivity.this, "Select State", Toast.LENGTH_SHORT);
//                                st.setBackgroundColor(CreateNewActivity.this.getResources().getColor(R.color.colorPrimary));
//                                st.setTextColor(Color.WHITE);
//                                st.setMaxAlpha();
//                                st.show();

                            }else if(city_edit.getText().toString().equals("Select City")){
                                city_edit.setError("Select City");
//                                StyleableToast st = new StyleableToast(CreateNewActivity.this, "Select City", Toast.LENGTH_SHORT);
//                                st.setBackgroundColor(CreateNewActivity.this.getResources().getColor(R.color.colorPrimary));
//                                st.setTextColor(Color.WHITE);
//                                st.setMaxAlpha();
//                                st.show();

                            } else if (approveddealer.contains(mobilenumber.getText().toString())){
                                final AlertDialog alertDialog = new AlertDialog.Builder(CreateNewActivity.this).create();
                                LayoutInflater inflater = getLayoutInflater();
                                View dialogView = inflater.inflate(R.layout.loginalertvaldiate, null);
                                alertDialog.setView(dialogView);
                                TextView login =(TextView)dialogView.findViewById(R.id.login);
                                login.setText("User Already Created With This Mobile Number!");
                                Button submit =(Button) dialogView.findViewById(R.id.submit);
                                submit.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        alertDialog.dismiss();


                                    }

                                });
                                alertDialog.show();
                            }
                            else if(mobilenumber.getText().toString().equals("")){
                                mobilenumber.setError("Select City");
//                                StyleableToast st = new StyleableToast(CreateNewActivity.this, "Enter Mobile Number", Toast.LENGTH_SHORT);
//                                st.setBackgroundColor(CreateNewActivity.this.getResources().getColor(R.color.colorPrimary));
//                                st.setTextColor(Color.WHITE);
//                                st.setMaxAlpha();
//                                st.show();


                            }else if(address.getText().toString().equals("")){
                                address.setError("Enter Address");
//                                StyleableToast st = new StyleableToast(CreateNewActivity.this, "Enter the Address", Toast.LENGTH_SHORT);
//                                st.setBackgroundColor(CreateNewActivity.this.getResources().getColor(R.color.colorPrimary));
//                                st.setTextColor(Color.WHITE);
//                                st.setMaxAlpha();
//                                st.show();

                            }else{
                                createcalues();
                            }

                        } else {
                            final AlertDialog alertDialog = new AlertDialog.Builder(CreateNewActivity.this).create();
                            LayoutInflater inflater = getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.loginalertvaldiate, null);
                            alertDialog.setView(dialogView);
                            TextView login =(TextView)dialogView.findViewById(R.id.login);
                            login.setText("Product have been added for this customer " + details.get(0).getCustomerName() + ". Please place order for this customer or move to local and then add Products for another customer");
                            Button submit =(Button) dialogView.findViewById(R.id.submit);
                            submit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    alertDialog.dismiss();
                                    Intent i = new Intent(CreateNewActivity.this, SalesConfirmDealerActivity.class).putExtra("ordertypevalues", myshare.getString("customertypestring", "")).putExtra("salesType", "New");
                                    startActivity(i);

                                }

                            });
                            alertDialog.show();
//                            final android.app.AlertDialog dialog = new android.app.AlertDialog.Builder(CreateNewActivity.this).create();
//                            dialog.setTitle(R.string.app_name);
//                            dialog.setMessage("Product have been added for this customer " + details.get(0).getCustomerName() + ". Please place order for this customer or move to local and then add Products for another customer");
//                            dialog.setButton("Okay", new DialogInterface.OnClickListener() {
//
//                                public void onClick(DialogInterface dialog, int which) {
//                                    Intent i = new Intent(CreateNewActivity.this, SalesConfirmDealerActivity.class).putExtra("ordertypevalues", myshare.getString("customertypestring", "")).putExtra("salesType", "New");
//                                    startActivity(i);
//                                    // finish();
//                                }
//                            });
//                            dialog.show();
                        }

                    }else {
                        if(shopname.getText().toString().equals("")){
                            shopname.setError("Enter Shop Name");
//                                StyleableToast st = new StyleableToast(CreateNewActivity.this, "Enter Shop Name", Toast.LENGTH_SHORT);
//                                st.setBackgroundColor(CreateNewActivity.this.getResources().getColor(R.color.colorPrimary));
//                                st.setTextColor(Color.WHITE);
//                                st.setMaxAlpha();
//                                st.show();

                        }else if(contactperson.getText().toString().equals("")){
                            contactperson.setError("Enter Contact Person Name");
//                                StyleableToast st = new StyleableToast(CreateNewActivity.this, "Enter Contact Person Name", Toast.LENGTH_SHORT);
//                                st.setBackgroundColor(CreateNewActivity.this.getResources().getColor(R.color.colorPrimary));
//                                st.setTextColor(Color.WHITE);
//                                st.setMaxAlpha();
//                                st.show();


                        }else  if(state_spinner.getText().toString().equals("Select State")){
                            state_spinner.setError("Select State");
//                                StyleableToast st = new StyleableToast(CreateNewActivity.this, "Select State", Toast.LENGTH_SHORT);
//                                st.setBackgroundColor(CreateNewActivity.this.getResources().getColor(R.color.colorPrimary));
//                                st.setTextColor(Color.WHITE);
//                                st.setMaxAlpha();
//                                st.show();

                        }else if(city_edit.getText().toString().equals("Select City")){
                            city_edit.setError("Select City");
//                                StyleableToast st = new StyleableToast(CreateNewActivity.this, "Select City", Toast.LENGTH_SHORT);
//                                st.setBackgroundColor(CreateNewActivity.this.getResources().getColor(R.color.colorPrimary));
//                                st.setTextColor(Color.WHITE);
//                                st.setMaxAlpha();
//                                st.show();

                        } else if (approveddealer.contains(mobilenumber.getText().toString())){
                            final AlertDialog alertDialog = new AlertDialog.Builder(CreateNewActivity.this).create();
                            LayoutInflater inflater = getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.loginalertvaldiate, null);
                            alertDialog.setView(dialogView);
                            TextView login =(TextView)dialogView.findViewById(R.id.login);
                            login.setText("User Already Created With This Mobile Number!");
                            Button submit =(Button) dialogView.findViewById(R.id.submit);
                            submit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    alertDialog.dismiss();


                                }

                            });
                            alertDialog.show();
                        }
                        else if(mobilenumber.getText().toString().equals("")){
                            mobilenumber.setError("Select City");
//                                StyleableToast st = new StyleableToast(CreateNewActivity.this, "Enter Mobile Number", Toast.LENGTH_SHORT);
//                                st.setBackgroundColor(CreateNewActivity.this.getResources().getColor(R.color.colorPrimary));
//                                st.setTextColor(Color.WHITE);
//                                st.setMaxAlpha();
//                                st.show();


                        }else if(address.getText().toString().equals("")){
                            address.setError("Enter Address");
//                                StyleableToast st = new StyleableToast(CreateNewActivity.this, "Enter the Address", Toast.LENGTH_SHORT);
//                                st.setBackgroundColor(CreateNewActivity.this.getResources().getColor(R.color.colorPrimary));
//                                st.setTextColor(Color.WHITE);
//                                st.setMaxAlpha();
//                                st.show();

                        }else{
                            createcalues();
                        }
                    }

                }else {
                    if(details.size()!=0) {
                        if (details.get(0).getMobileNo().contains(mobilenumber.getText().toString())) {
                            // no action
                            if(contactperson.getText().toString().equals("")){
                                contactperson.setError("Enter Contact Person Name");
//                                StyleableToast st = new StyleableToast(CreateNewActivity.this, "Enter Contact Person Name", Toast.LENGTH_SHORT);
//                                st.setBackgroundColor(CreateNewActivity.this.getResources().getColor(R.color.colorPrimary));
//                                st.setTextColor(Color.WHITE);
//                                st.setMaxAlpha();
//                                st.show();


                            }else  if(state_spinner.getText().toString().equals("Select State")){
                                state_spinner.setError("Select State");
//                                StyleableToast st = new StyleableToast(CreateNewActivity.this, "Select State", Toast.LENGTH_SHORT);
//                                st.setBackgroundColor(CreateNewActivity.this.getResources().getColor(R.color.colorPrimary));
//                                st.setTextColor(Color.WHITE);
//                                st.setMaxAlpha();
//                                st.show();

                            }else if(city_edit.getText().toString().equals("Select City")){
                                city_edit.setError("Select City");
//                                StyleableToast st = new StyleableToast(CreateNewActivity.this, "Select City", Toast.LENGTH_SHORT);
//                                st.setBackgroundColor(CreateNewActivity.this.getResources().getColor(R.color.colorPrimary));
//                                st.setTextColor(Color.WHITE);
//                                st.setMaxAlpha();
//                                st.show();

                            } else if (approveddealer.contains(mobilenumber.getText().toString())){
                                final AlertDialog alertDialog = new AlertDialog.Builder(CreateNewActivity.this).create();
                                LayoutInflater inflater = getLayoutInflater();
                                View dialogView = inflater.inflate(R.layout.loginalertvaldiate, null);
                                alertDialog.setView(dialogView);
                                TextView login =(TextView)dialogView.findViewById(R.id.login);
                                login.setText("User Already Created With This Mobile Number!");
                                Button submit =(Button) dialogView.findViewById(R.id.submit);
                                submit.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        alertDialog.dismiss();


                                    }

                                });
                                alertDialog.show();
                            }
                            else if(mobilenumber.getText().toString().equals("")){
                                mobilenumber.setError("Select City");
//                                StyleableToast st = new StyleableToast(CreateNewActivity.this, "Enter Mobile Number", Toast.LENGTH_SHORT);
//                                st.setBackgroundColor(CreateNewActivity.this.getResources().getColor(R.color.colorPrimary));
//                                st.setTextColor(Color.WHITE);
//                                st.setMaxAlpha();
//                                st.show();


                            }else if(address.getText().toString().equals("")){
                                address.setError("Enter Address");
//                                StyleableToast st = new StyleableToast(CreateNewActivity.this, "Enter the Address", Toast.LENGTH_SHORT);
//                                st.setBackgroundColor(CreateNewActivity.this.getResources().getColor(R.color.colorPrimary));
//                                st.setTextColor(Color.WHITE);
//                                st.setMaxAlpha();
//                                st.show();

                            }else{
                                createcalues();
                            }
                        } else {
                            final AlertDialog alertDialog = new AlertDialog.Builder(CreateNewActivity.this).create();
                            LayoutInflater inflater = getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.loginalertvaldiate, null);
                            alertDialog.setView(dialogView);
                            TextView login =(TextView)dialogView.findViewById(R.id.login);
                            login.setText("Product have been added for this customer " + contactperson.getText().toString()+ ". Please place order for this customer or move to local and then add Products for another customer");
                            Button submit =(Button) dialogView.findViewById(R.id.submit);
                            submit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    alertDialog.dismiss();
                                    Intent i = new Intent(CreateNewActivity.this, SalesConfirmDealerActivity.class).putExtra("ordertypevalues", myshare.getString("customertypestring", "")).putExtra("salesType", "New");
                                    startActivity(i);

                                }

                            });
                            alertDialog.show();
//                            final android.app.AlertDialog dialog = new android.app.AlertDialog.Builder(CreateNewActivity.this).create();
//                            dialog.setTitle(R.string.app_name);
//                            dialog.setMessage("Product have been added for this customer " + contactperson.getText().toString()+ ". Please place order for this customer or move to local and then add Products for another customer");
//                            dialog.setButton("Okay", new DialogInterface.OnClickListener() {
//
//                                public void onClick(DialogInterface dialog, int which) {
//                                    Intent i = new Intent(CreateNewActivity.this, SalesConfirmDealerActivity.class).putExtra("ordertypevalues", myshare.getString("customertypestring", "")).putExtra("salesType", "New");
//                                    startActivity(i);
//                                    // finish();
//                                }
//                            });
//                            dialog.show();
                        }

                    }else {
                        if(contactperson.getText().toString().equals("")){
                            contactperson.setError("Enter Contact Person Name");
//                                StyleableToast st = new StyleableToast(CreateNewActivity.this, "Enter Contact Person Name", Toast.LENGTH_SHORT);
//                                st.setBackgroundColor(CreateNewActivity.this.getResources().getColor(R.color.colorPrimary));
//                                st.setTextColor(Color.WHITE);
//                                st.setMaxAlpha();
//                                st.show();


                        }else  if(state_spinner.getText().toString().equals("Select State")){
                            state_spinner.setError("Select State");
//                                StyleableToast st = new StyleableToast(CreateNewActivity.this, "Select State", Toast.LENGTH_SHORT);
//                                st.setBackgroundColor(CreateNewActivity.this.getResources().getColor(R.color.colorPrimary));
//                                st.setTextColor(Color.WHITE);
//                                st.setMaxAlpha();
//                                st.show();

                        }else if(city_edit.getText().toString().equals("Select City")){
                            city_edit.setError("Select City");
//                                StyleableToast st = new StyleableToast(CreateNewActivity.this, "Select City", Toast.LENGTH_SHORT);
//                                st.setBackgroundColor(CreateNewActivity.this.getResources().getColor(R.color.colorPrimary));
//                                st.setTextColor(Color.WHITE);
//                                st.setMaxAlpha();
//                                st.show();

                        } else if (approveddealer.contains(mobilenumber.getText().toString())){

                            final AlertDialog alertDialog = new AlertDialog.Builder(CreateNewActivity.this).create();
                            LayoutInflater inflater = getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.loginalertvaldiate, null);
                            alertDialog.setView(dialogView);
                            TextView login =(TextView)dialogView.findViewById(R.id.login);
                            login.setText("User Already Created With This Mobile Number!");
                            Button submit =(Button) dialogView.findViewById(R.id.submit);
                            submit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    alertDialog.dismiss();


                                }

                            });
                            alertDialog.show();
//                            StyleableToast st = new StyleableToast(CreateNewActivity.this, "User Already Created With This Mobile Number", Toast.LENGTH_SHORT);
//                            st.setBackgroundColor(CreateNewActivity.this.getResources().getColor(R.color.colorPrimary));
//                            st.setTextColor(Color.WHITE);
//                            st.setMaxAlpha();
//                            st.show();
                        }
                        else if(mobilenumber.getText().toString().equals("")){
                            mobilenumber.setError("Select City");
//                                StyleableToast st = new StyleableToast(CreateNewActivity.this, "Enter Mobile Number", Toast.LENGTH_SHORT);
//                                st.setBackgroundColor(CreateNewActivity.this.getResources().getColor(R.color.colorPrimary));
//                                st.setTextColor(Color.WHITE);
//                                st.setMaxAlpha();
//                                st.show();


                        }else if(address.getText().toString().equals("")){
                            address.setError("Enter Address");
//                                StyleableToast st = new StyleableToast(CreateNewActivity.this, "Enter the Address", Toast.LENGTH_SHORT);
//                                st.setBackgroundColor(CreateNewActivity.this.getResources().getColor(R.color.colorPrimary));
//                                st.setTextColor(Color.WHITE);
//                                st.setMaxAlpha();
//                                st.show();

                        }else{
                            createcalues();
                        }
                    }

                }
            }
        });
        getalreadycreatedcustomermobile();
        //GetStateLsit(db1.adhereDao().GetStatelist());
        GetStatelist(db1.adhereDao().sGetRegion(regionstr));
        state_spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                states=item.toString().trim();
                GetCItystrings(db1.adhereDao().GetCitystring(states));

            }
        });
        city_edit.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                city=item.toString();
                if(city.equals("Other")){
                    othercity.setVisibility(View.VISIBLE);
                }else {
                    othercity.setVisibility(View.GONE);
                }

            }
        });

    }

    private void GetStatelist(List<String> stateslsit) {
        //List<Object>states=(List<Object>) CollectionUtils.collect(stateslsit, TransformerUtils.invokerTransformer("getState1"));
        stateslsit.add(0,"Select State");
        state_spinner.setItems(stateslsit);

//        strings.add(0,"Select State");
//        state_spinner.setItems(strings);
    }

    private void CheckInternetforpass() {
        NetworkConnection network = new NetworkConnection(CreateNewActivity.this);
        if (network.CheckInternet()) {
            PasswordChangeAlert alert = new PasswordChangeAlert(CreateNewActivity.this);// new saleslogin().execute();
            alert.showLoginbox();
        } else {
            Alertbox alert = new Alertbox(CreateNewActivity.this);
            alert.showAlertbox("Kindly check your Internet Connection");
        }

    }

    private void getalreadycreatedcustomermobile() {
        alreadycreatedcustomermobile=new ArrayList<>();

        approveddealer=new ArrayList<>();

        approveddealer=db1.adhereDao().Getalreadycusmobile();

        alreadycreatedcustomermobile=db1.adhereDao().Getlocallycreateddealer();

//        approveddealer.addAll(alreadycreatedcustomermobile);

    }


    private void createcalues() {
        namestring=name.getText().toString();
        shopnamestring=shopname.getText().toString();
        addressstring=address.getText().toString();
        mobilenumberstring=mobilenumber.getText().toString();
        gststring=gst.getText().toString();
        emailidString=emailid.getText().toString();
        create=new createlist();
        create.setTypeofacc(orderacc);
        create.setSid(Salesid);
        create.setState(states);
        if(city.equals("Others")){
            create.setCity(othecityedit.getText().toString());
        }else {
            create.setCity(city);
        }
        create.setCity(city);
        create.setShopname(shopnamestring);
        create.setAddress(addressstring);
        create.setMobilenumber(mobilenumberstring);
        create.setGst(gststring);
        create.setRegion(regionstr);
        create.setName(namestring);
        create.setEmaild(emailidString);
        db1.adhereDao().insertAllregion(create);

        Intent i =new Intent(CreateNewActivity.this,ChooseActivity.class).putExtra("ordertype",orderacc).putExtra("executeid",Salesid)
                .putExtra("states",states).putExtra("citys",city).putExtra("shopname",shopnamestring).putExtra("mobilestring",mobilenumberstring)
                .putExtra("newShops",contactperson.getText().toString()).putExtra("gst",gststring).putExtra("regionstring",regionstr).putExtra("name",namestring).putExtra("address",addressstring).putExtra("emailid",emailidString);
//        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
//

    }

    private void GetCItystrings(List<Citylist> citylists) {
        List<Object>cities=(List<Object>) CollectionUtils.collect(citylists, TransformerUtils.invokerTransformer("getTown1"));
        cities.add(0,"Select City");
        city_edit.setItems(cities);
    }

    private void GetStateLsit(List<Statelist> statelists) {
        List<Object>states=(List<Object>) CollectionUtils.collect(statelists, TransformerUtils.invokerTransformer("getState1"));
        states.add(0,"Select State");
        state_spinner.setItems(states);
    }

//    private class Create extends AsyncTask<String, Void, String> {
//
//
//        ContentValues values = new ContentValues();
//        private long success;
//        SQLiteDatabase db;
//        private int a;
//
//        @SuppressLint("InflateParams")
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            progressDialog = new ProgressDialog(CreateNewActivity.this);
//            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//            progressDialog.setCancelable(false);
//            progressDialog.setMessage("Saving local...");
//            progressDialog.show();
//            namestring=name.getText().toString();
//            shopnamestring=shopname.getText().toString();
//            addressstring=address.getText().toString();
//            mobilenumberstring=mobilenumber.getText().toString();
//            gststring=gst.getText().toString();
//
//        }
//
//        @Override
//        protected String doInBackground(String... args0) {
//
//            try {
//                DBHelper dbHelper = new DBHelper(CreateNewActivity.this);
//                db = dbHelper.getWritableDatabase();
//                // db.execSQL("DROP TABLE  IF EXISTS "+ SQLITE.TABLE_Create_Profile);
//                if(ordertypespin.getText().toString().equals("Retailer")){
//                    values.put(SQLITE.COLUMN_Create_retsalesid,Salesid);
//                    values.put(SQLITE.COLUMN_Create_retstate, states);
//                    values.put(SQLITE.COLUMN_Create_retcity, city);
//                    values.put(SQLITE.COLUMN_Create_retshopname,shopnamestring);
//                    values.put(SQLITE.COLUMN_Create_retaddress,addressstring);
//                    values.put(SQLITE.COLUMN_Create_retmobile,mobilenumberstring);
//                    values.put(SQLITE.COLUMN_Create_gst, gststring);
//                    values.put(SQLITE.COLUMN_Create_ordertpe, orderacc);
//                    values.put(SQLITE.COLUMN_Create_retregion, regionstr);
//                }else {
//                    values.put(SQLITE.COLUMN_Create_retsalesid,Salesid);
//                    values.put(SQLITE.COLUMN_Create_retname, namestring);
//                    values.put(SQLITE.COLUMN_Create_retstate, states);
//                    values.put(SQLITE.COLUMN_Create_retcity, city);
//                    values.put(SQLITE.COLUMN_Create_retaddress,addressstring);
//                    values.put(SQLITE.COLUMN_Create_retmobile,mobilenumberstring);
//                    values.put(SQLITE.COLUMN_Create_ordertpe, orderacc);
//                    values.put(SQLITE.COLUMN_Create_retregion, regionstr);
//                }
//
//
//
//                dbHelper.CreateRetailerProfile(db);
//                success = db.insert(SQLITE.TABLE_Create_Profile, null, values);
//                if(success!=-1)
//                {
//                    db.close();
//                    return "received";
//                }
//                db.close();
//                return "not Success";
//
//            } catch (Exception e) {
//                Log.v("Error in Add local db ", "catch is working");
//                Log.v("Error in Add local db", e.getMessage());
//                return "notsuccess";
//            }
//        }
//
//        @Override
//        protected void onPostExecute(String result) {
//            super.onPostExecute(result);
//            progressDialog.dismiss();
//            if (result.equals("received")) {
//                Log.v("success", Double.toString(success));
//                startActivity(new Intent(CreateNewActivity.this, HomeActivty.class));
//                 if(ordertypespin.getText().toString().equals("Retailer")){
//                     StyleableToast st = new StyleableToast(CreateNewActivity.this, "Profile Created for " + shopnamestring, Toast.LENGTH_SHORT);
//                     st.setBackgroundColor(CreateNewActivity.this.getResources().getColor(R.color.colorPrimary));
//                     st.setTextColor(Color.WHITE);
//                     st.setMaxAlpha();
//                     st.show();
//
//                 }else {
//                     StyleableToast st = new StyleableToast(CreateNewActivity.this, "Profile Created for " + namestring, Toast.LENGTH_SHORT);
//                     st.setBackgroundColor(CreateNewActivity.this.getResources().getColor(R.color.colorPrimary));
//                     st.setTextColor(Color.WHITE);
//                     st.setMaxAlpha();
//                     st.show();
//                 }
//
//
//            } else {
//                box.showAlertbox("Error in adding to local");
//            }
//        }
//    }
}
