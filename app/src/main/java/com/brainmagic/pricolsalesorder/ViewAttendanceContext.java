package com.brainmagic.pricolsalesorder;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.SharedPreferences;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

import Home.homevalues;
import Logout.logout;
import adapter.ViewAllItemAdapter;
import alertbox.Alertbox;
import alertbox.PasswordChangeAlert;
import alertbox.PrimaryOrder;
import api.models.AttendanceAllTimeData.AttendanceAll;
import api.models.AttendanceAllTimeData.AttendanceAllData;
import api.retrofit.APIService;
import api.retrofit.RetrofitClient;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewAttendanceContext extends AppCompatActivity {
    private ListView listattendancesearch;
    private Alertbox box = new Alertbox(this);
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private String date;
    private int id;
    private List<AttendanceAll> data;
    private TextView title;
    private ImageView menu, back,home,logout,setting;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_attendance_context);
        myshare = getSharedPreferences("pricolsalesorder", MODE_PRIVATE);
        editor = myshare.edit();
        listattendancesearch=(ListView)findViewById(R.id.listattendancesearch);
        home = (ImageView) findViewById(R.id.home);
        setting = (ImageView) findViewById(R.id.setting);
        title = (TextView) findViewById(R.id.title);
        title.setText("Attendance Details");
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PopupMenu popupMenu = new PopupMenu(ViewAttendanceContext.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu pop) {
                        pop.dismiss();
                    }
                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.primary:
                                PrimaryOrder alert = new PrimaryOrder(ViewAttendanceContext.this);// new saleslogin().execute();
                                alert.showLoginboxtoggle();
                                return true;
                            case R.id.ch:
                                CheckInternetforpass();
                                return true;
                            case R.id.logout:
                                new logout(ViewAttendanceContext.this).log_out();
                                return true;
                        }
                        return false;
                    }
                });
                popupMenu.inflate(R.menu.pricolcms);
                Menu pop = popupMenu.getMenu();
                popupMenu.show();

            }
        });

        //  logout = (ImageView) findViewById(R.id.logout);
      //  back = (ImageView) findViewById(R.id.back);
        date=getIntent().getStringExtra("date");
        id=myshare.getInt("idlist",0);
        checkInternet();

//        back.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                onBackPressed();
//            }
//        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new homevalues(ViewAttendanceContext.this).home_io();
            }
        });
//        logout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                new logout(ViewAttendanceContext.this).log_out();
//            }
//        });

    }

    private void CheckInternetforpass() {
        NetworkConnection network = new NetworkConnection(ViewAttendanceContext.this);
        if (network.CheckInternet()) {
            PasswordChangeAlert alert = new PasswordChangeAlert(ViewAttendanceContext.this);// new saleslogin().execute();
            alert.showLoginbox();
        } else {
            final AlertDialog alertDialog = new AlertDialog.Builder(ViewAttendanceContext.this).create();
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.loginalert, null);
            alertDialog.setView(dialogView);
            TextView login =(TextView)dialogView.findViewById(R.id.login);
            login.setText("Please Check your Internet Connection !!");
            Button submit =(Button) dialogView.findViewById(R.id.submit);
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();

                }

            });
            alertDialog.show();
        }

    }

    private void checkInternet() {
        NetworkConnection networkConnection=new NetworkConnection(ViewAttendanceContext.this);
        if(networkConnection.CheckInternet()){
            GetDateView();
        }else {
            final AlertDialog alertDialog = new AlertDialog.Builder(ViewAttendanceContext.this).create();
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.loginalert, null);
            alertDialog.setView(dialogView);
            TextView login =(TextView)dialogView.findViewById(R.id.login);
            login.setText("Please Check your Internet Connection !!");
            Button submit =(Button) dialogView.findViewById(R.id.submit);
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();

                }

            });
            alertDialog.show();
        }
    }

    private void GetDateView() {
        try {

            final ProgressDialog loading = ProgressDialog.show(ViewAttendanceContext.this, getResources().getString(R.string.app_name), "Loading...", false, false);
            APIService service = RetrofitClient.getApiService();
            Call<AttendanceAllData> call = service.GetAttendanceAllData(date, id);
            call.enqueue(new Callback<AttendanceAllData>() {
                @Override

                public void onResponse(Call<AttendanceAllData> call, Response<AttendanceAllData> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success")) {
                            data=response.body().getData();
                            ViewAllItemAdapter viewAllItemAdapter=new ViewAllItemAdapter(ViewAttendanceContext.this,data);
                            listattendancesearch.setAdapter(viewAllItemAdapter);

                        } else if(response.body().getResult().equals("InvalidUser")) {
                            final AlertDialog alertDialog = new AlertDialog.Builder(ViewAttendanceContext.this).create();
                            LayoutInflater inflater = getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.loginalert, null);
                            alertDialog.setView(dialogView);
                            TextView login =(TextView)dialogView.findViewById(R.id.login);
                            login.setText("Invalid User");
                            Button submit =(Button) dialogView.findViewById(R.id.submit);
                            submit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    alertDialog.dismiss();

                                }

                            });
                            alertDialog.show();
                        }else
                        {
                            final AlertDialog alertDialog = new AlertDialog.Builder(ViewAttendanceContext.this).create();
                            LayoutInflater inflater = getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.loginalert, null);
                            alertDialog.setView(dialogView);
                            TextView login =(TextView)dialogView.findViewById(R.id.login);
                            login.setText("ow");
                            Button submit =(Button) dialogView.findViewById(R.id.submit);
                            submit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    alertDialog.dismiss();

                                }

                            });
                            alertDialog.show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        final AlertDialog alertDialog = new AlertDialog.Builder(ViewAttendanceContext.this).create();
                        LayoutInflater inflater = getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.loginalert, null);
                        alertDialog.setView(dialogView);
                        TextView login =(TextView)dialogView.findViewById(R.id.login);
                        login.setText("Something went wrong .Please try again later .");
                        Button submit =(Button) dialogView.findViewById(R.id.submit);
                        submit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                alertDialog.dismiss();

                            }

                        });
                        alertDialog.show();

                    }
                }

                @Override
                public void onFailure(Call<AttendanceAllData> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    final AlertDialog alertDialog = new AlertDialog.Builder(ViewAttendanceContext.this).create();
                    LayoutInflater inflater = getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.loginalert, null);
                    alertDialog.setView(dialogView);
                    TextView login =(TextView)dialogView.findViewById(R.id.login);
                    login.setText("Something went wrong .Please try again later .");
                    Button submit =(Button) dialogView.findViewById(R.id.submit);
                    submit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();

                        }

                    });
                    alertDialog.show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
