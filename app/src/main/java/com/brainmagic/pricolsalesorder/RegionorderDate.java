package com.brainmagic.pricolsalesorder;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.SharedPreferences;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

import Home.homevalues;
import Logout.logout;
import adapter.OrderPreviewAdapterregdate;
import alertbox.Alertbox;
import alertbox.PasswordChangeAlert;
import alertbox.PrimaryOrder;
import api.models.rmdate.RmDate;
import api.models.rmdate.RmDateData;
import api.retrofit.APIService;
import api.retrofit.RetrofitClient;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegionorderDate extends AppCompatActivity {
    private ImageView back, home, logout;
    private ListView listpricolview;
    private Alertbox box = new Alertbox(RegionorderDate.this);
    private TextView title;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private String ordertype;
    private int executeid;
    private OrderPreviewAdapterregdate orderPreviewAdapter;
    private List<RmDate> data;
    private Integer data1;
    private ImageView setting;
    TextView Text2;
    FrameLayout footerLayout;
    private String code, orderdate;
    private int regid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regionorder_date);
        home = (ImageView) findViewById(R.id.home);
        logout = (ImageView) findViewById(R.id.logout);
        // back=(ImageView)findViewById(R.id.back) ;
        title = (TextView) findViewById(R.id.title);
        title.setText("Order Details");
        setting = (ImageView) findViewById(R.id.setting);
        listpricolview = (ListView) findViewById(R.id.listorder);
        Text2 = (TextView) findViewById(R.id.Title);


//        footerLayout = (FrameLayout) getLayoutInflater().inflate(R.layout.footerview,null);
//
//        listpricolview.addFooterView(footerLayout, null, false);

        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PopupMenu popupMenu = new PopupMenu(RegionorderDate.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu pop) {
                        pop.dismiss();
                    }
                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.primary:
                                PrimaryOrder alert = new PrimaryOrder(RegionorderDate.this);// new saleslogin().execute();
                                alert.showLoginboxtoggle();
                                return true;
                            case R.id.ch:
                                CheckInternetforpass();
                                return true;
                            case R.id.logout:
                                new logout(RegionorderDate.this).log_out();
                                return true;
                        }
                        return false;
                    }
                });
                popupMenu.inflate(R.menu.pricolcms);
                Menu pop = popupMenu.getMenu();
                popupMenu.show();

            }
        });
        myshare = getSharedPreferences("pricolsalesorder", MODE_PRIVATE);
        editor = myshare.edit();
        executeid = myshare.getInt("idlist", 0);
        regid = myshare.getInt("idlistreg", 0);
        code = getIntent().getStringExtra("executecode");
        ordertype = getIntent().getStringExtra("type");
        orderdate = getIntent().getStringExtra("orderdate");

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new homevalues(RegionorderDate.this).home_io();
            }
        });

//        logout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                new logout(RegionorderDate.this).log_out();
//            }
//        });
//
//        back.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                onBackPressed();
//            }
//        });
        checkinternet();


    }

    private void CheckInternetforpass() {
        NetworkConnection network = new NetworkConnection(RegionorderDate.this);
        if (network.CheckInternet()) {
            PasswordChangeAlert alert = new PasswordChangeAlert(RegionorderDate.this);// new saleslogin().execute();
            alert.showLoginbox();
        } else {
            Alertbox alert = new Alertbox(RegionorderDate.this);
            alert.showAlertbox("Kindly check your Internet Connection");
        }

    }

    private void checkinternet() {
        NetworkConnection networkConnection = new NetworkConnection(RegionorderDate.this);
        if (networkConnection.CheckInternet()) {
            getListviews();
        } else {
            final AlertDialog alertDialog = new AlertDialog.Builder(RegionorderDate.this).create();
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.loginalert, null);
            alertDialog.setView(dialogView);
            TextView login = (TextView) dialogView.findViewById(R.id.login);
            login.setText("Please Check Your internet Connection!!!");
            Button submit = (Button) dialogView.findViewById(R.id.submit);
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();

                }

            });
            alertDialog.show();
        }
    }

    private void getListviews() {
        try {

            final ProgressDialog loading = ProgressDialog.show(RegionorderDate.this, getResources().getString(R.string.app_name), "Loading...", false, false);
            APIService service = RetrofitClient.getApiService();
            Call<RmDateData> call = service.rmdate(regid, code,orderdate);
            call.enqueue(new Callback<RmDateData>() {
                @Override

                public void onResponse(Call<RmDateData> call, Response<RmDateData> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success")) {
                            data = response.body().getData();
                            orderPreviewAdapter = new OrderPreviewAdapterregdate(RegionorderDate.this, data);
                            listpricolview.setAdapter(orderPreviewAdapter);
                        } else if (response.body().getResult().equals("NotSuccess")) {
                            final AlertDialog alertDialog = new AlertDialog.Builder(RegionorderDate.this).create();
                            LayoutInflater inflater = getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.loginalert, null);
                            alertDialog.setView(dialogView);
                            TextView login = (TextView) dialogView.findViewById(R.id.login);
                            login.setText("No Order Found");
                            Button submit = (Button) dialogView.findViewById(R.id.submit);
                            submit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    alertDialog.dismiss();

                                }

                            });
                            alertDialog.show();
                        } else {
                            final AlertDialog alertDialog = new AlertDialog.Builder(RegionorderDate.this).create();
                            LayoutInflater inflater = getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.loginalert, null);
                            alertDialog.setView(dialogView);
                            TextView login = (TextView) dialogView.findViewById(R.id.login);
                            login.setText("Something went wrong .Please try again later .");
                            Button submit = (Button) dialogView.findViewById(R.id.submit);
                            submit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    alertDialog.dismiss();

                                }

                            });
                            alertDialog.show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        final AlertDialog alertDialog = new AlertDialog.Builder(RegionorderDate.this).create();
                        LayoutInflater inflater = getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.loginalert, null);
                        alertDialog.setView(dialogView);
                        TextView login = (TextView) dialogView.findViewById(R.id.login);
                        login.setText("Something went wrong .Please try again later .");
                        Button submit = (Button) dialogView.findViewById(R.id.submit);
                        submit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                alertDialog.dismiss();

                            }

                        });
                        alertDialog.show();

                    }
                }

                @Override
                public void onFailure(Call<RmDateData> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    final AlertDialog alertDialog = new AlertDialog.Builder(RegionorderDate.this).create();
                    LayoutInflater inflater = getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.loginalert, null);
                    alertDialog.setView(dialogView);
                    TextView login = (TextView) dialogView.findViewById(R.id.login);
                    login.setText("Some Field Missing");
                    Button submit = (Button) dialogView.findViewById(R.id.submit);
                    submit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();

                        }

                    });
                    alertDialog.show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}