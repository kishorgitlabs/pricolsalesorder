package com.brainmagic.pricolsalesorder;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.Color;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.muddzdev.styleabletoastlibrary.StyleableToast;

import java.util.List;

import Home.homevalues;
import Logout.logout;
import adapter.OrderPreviewAdapterSecondary;
import alertbox.Alertbox;
import api.models.viewOrder.ViewOrder;
import api.models.viewOrder.ViewOrderStatusData;
import api.retrofit.APIService;
import api.retrofit.RetrofitClient;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewOrderStausSecondary extends AppCompatActivity {
    private ImageView back,home,logout;
    private ListView listpricolview;
    private Alertbox box=new Alertbox(ViewOrderStausSecondary.this);
    private TextView title;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private String ordertype;
    private int executeid;
    private OrderPreviewAdapterSecondary orderPreviewAdapter;
    private List<ViewOrder> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_order_staus_secondary);
        home = (ImageView) findViewById(R.id.home);
        logout = (ImageView) findViewById(R.id.logout);
        back=(ImageView)findViewById(R.id.back) ;
        title=(TextView)findViewById(R.id.title);
        title.setText("View Order");
        myshare = getSharedPreferences("pricolsalesorder", MODE_PRIVATE);
        editor = myshare.edit();
        executeid=myshare.getInt("idlist",0);
        ordertype=getIntent().getStringExtra("type");
        listpricolview=(ListView) findViewById(R.id.listpricolview) ;

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new homevalues(ViewOrderStausSecondary.this).home_io();
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new logout(ViewOrderStausSecondary.this).log_out();
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        checkinternet();


    }

    private void checkinternet() {
        NetworkConnection networkConnection=new NetworkConnection(ViewOrderStausSecondary.this);
        if(networkConnection.CheckInternet()){
            getListviews();
        }else {
            StyleableToast st = new StyleableToast(ViewOrderStausSecondary.this, "Please Check Your Internet Connection", Toast.LENGTH_SHORT);
            st.setBackgroundColor(ViewOrderStausSecondary.this.getResources().getColor(R.color.colorPrimary));
            st.setTextColor(Color.WHITE);
            st.setMaxAlpha();
            st.show();
        }
    }

    private void getListviews() {
        try {

            final ProgressDialog loading = ProgressDialog.show(ViewOrderStausSecondary.this, getResources().getString(R.string.app_name), "Loading...", false, false);
            APIService service = RetrofitClient.getApiService();
            Call<ViewOrderStatusData> call = service.GetOrderPreview(executeid,ordertype);
            call.enqueue(new Callback<ViewOrderStatusData>() {
                @Override

                public void onResponse(Call<ViewOrderStatusData> call, Response<ViewOrderStatusData> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success")) {
                            data=response.body().getData();
                            orderPreviewAdapter=new OrderPreviewAdapterSecondary(ViewOrderStausSecondary.this,data);
                            listpricolview.setAdapter(orderPreviewAdapter);

                        } else if(response.body().getResult().equals("NotSuccess")) {
                            box.showAlertbox("No Order Found");
                        }else
                        {
                            box.showAlertbox(getResources().getString(R.string.server_error));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        box.showAlertbox(getResources().getString(R.string.server_error));

                    }
                }

                @Override
                public void onFailure(Call<ViewOrderStatusData> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    box.showAlertbox(getResources().getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    }

