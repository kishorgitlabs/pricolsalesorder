package com.brainmagic.pricolsalesorder;

import android.app.ProgressDialog;
import android.content.SharedPreferences;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

import Home.homevalues;
import Logout.logout;
import adapter.ViewExecuteAdapter;
import alertbox.Alertbox;
import alertbox.PrimaryOrder;
import api.models.GetSalesAttendanceReg.GetSalesAtt;
import api.models.GetSalesAttendanceReg.GetSalesAttRegData;
import api.retrofit.APIService;
import api.retrofit.RetrofitClient;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewExecuteRegionActiivty extends AppCompatActivity {
    private ListView listsalesattendance;
    private ImageView menu, back,home,logout,setting;
    private Alertbox box = new Alertbox(this);
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private TextView title,name;
    private int regid;
    private List<GetSalesAtt>data;
    private SearchView searchview;
    private  ViewExecuteAdapter viewExecuteAdapter;
    private String empcode,fromdate,todate,salesname;
    private TextView fromdateText,todatetext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_execute_region_actiivty);
        myshare = getSharedPreferences("pricolsalesorder", MODE_PRIVATE);
        editor = myshare.edit();
        regid=myshare.getInt("idlistreg",0);
        listsalesattendance=(ListView)findViewById(R.id.listsalesattendance);
        home = (ImageView) findViewById(R.id.home);
        logout = (ImageView) findViewById(R.id.logout);
        back=(ImageView)findViewById(R.id.back) ;
        title=(TextView)findViewById(R.id.title);
        setting=(ImageView) findViewById(R.id.setting);
        name=(TextView)findViewById(R.id.name);
        fromdateText=(TextView)findViewById(R.id.fromdateText);
        todatetext=(TextView)findViewById(R.id.todatetext);
        title.setText("Sales Executive Attendance");
        empcode=getIntent().getStringExtra("salescode");
        fromdate=getIntent().getStringExtra("fromdate");
        todate=getIntent().getStringExtra("todate");
        salesname=getIntent().getStringExtra("Salesname");
        name.setText(salesname);
        fromdateText.setText(fromdate);
        todatetext.setText(todate);
        CheckInternet();
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PopupMenu popupMenu = new PopupMenu(ViewExecuteRegionActiivty.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu pop) {
                        pop.dismiss();
                    }
                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.primary:
                                PrimaryOrder alert = new PrimaryOrder(ViewExecuteRegionActiivty.this);// new saleslogin().execute();
                                alert.showLoginboxtoggle();
                                return true;
                            case R.id.ch:
                                CheckInternetforpass();
                                return true;
                            case R.id.logout:
                                new logout(ViewExecuteRegionActiivty.this).log_outReg();
                                return true;
                        }
                        return false;
                    }
                });
                popupMenu.inflate(R.menu.pricolcms);
                Menu pop = popupMenu.getMenu();
                popupMenu.show();

            }
        });


        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new homevalues(ViewExecuteRegionActiivty.this).home_io();
            }
        });
           }

    private void CheckInternetforpass() {
        NetworkConnection network = new NetworkConnection(ViewExecuteRegionActiivty.this);
        if (network.CheckInternet()) {
            PasswordChangeAlertRegion alert = new PasswordChangeAlertRegion(ViewExecuteRegionActiivty.this);// new saleslogin().execute();
            alert.showLoginbox();
        } else {
            final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(ViewExecuteRegionActiivty.this).create();
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.loginalert, null);
            alertDialog.setView(dialogView);
            TextView login =(TextView)dialogView.findViewById(R.id.login);
            login.setText("Please Check Your internet Connection !");
            Button submit =(Button) dialogView.findViewById(R.id.submit);
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();

                }

            });
            alertDialog.show();
        }
    }

    private void CheckInternet() {
        NetworkConnection networkConnection=new NetworkConnection(ViewExecuteRegionActiivty.this);
        if(networkConnection.CheckInternet()){
            GetSalesAttendance();
        }else {
            final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(ViewExecuteRegionActiivty.this).create();
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.loginalert, null);
            alertDialog.setView(dialogView);
            TextView login =(TextView)dialogView.findViewById(R.id.login);
            login.setText("Please Check Your internet Connection !");
            Button submit =(Button) dialogView.findViewById(R.id.submit);
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                    alertDialog.dismiss();

                }

            });
            alertDialog.show();
        }
    }

    private void GetSalesAttendance() {
        try {

            final ProgressDialog loading = ProgressDialog.show(ViewExecuteRegionActiivty.this, getResources().getString(R.string.app_name), "Loading...", false, false);
            APIService service = RetrofitClient.getApiService();
            Call<GetSalesAttRegData> call = service.GetSalesatt(empcode,fromdate,todate,regid);
            call.enqueue(new Callback<GetSalesAttRegData>() {
                @Override

                public void onResponse(Call<GetSalesAttRegData> call, Response<GetSalesAttRegData> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success")) {
                            data=response.body().getData();
                            viewExecuteAdapter=new ViewExecuteAdapter(ViewExecuteRegionActiivty.this,data);
                            listsalesattendance.setAdapter(viewExecuteAdapter);

                        } else if(response.body().getResult().equals("NotSuccess")) {
                            final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(ViewExecuteRegionActiivty.this).create();
                            LayoutInflater inflater = getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.loginalert, null);
                            alertDialog.setView(dialogView);
                            TextView login =(TextView)dialogView.findViewById(R.id.login);
                            login.setText("No Attendance Found !");
                            Button submit =(Button) dialogView.findViewById(R.id.submit);
                            submit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    finish();
                                    alertDialog.dismiss();
                                }

                            });
                            alertDialog.show();
                        }else
                        {
                            final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(ViewExecuteRegionActiivty.this).create();
                            LayoutInflater inflater = getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.loginalert, null);
                            alertDialog.setView(dialogView);
                            TextView login =(TextView)dialogView.findViewById(R.id.login);
                            login.setText("Something went wrong .Please try again later .");
                            Button submit =(Button) dialogView.findViewById(R.id.submit);
                            submit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    finish();
                                    alertDialog.dismiss();
                                }

                            });
                            alertDialog.show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(ViewExecuteRegionActiivty.this).create();
                        LayoutInflater inflater = getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.loginalert, null);
                        alertDialog.setView(dialogView);
                        TextView login =(TextView)dialogView.findViewById(R.id.login);
                        login.setText("Something went wrong .Please try again later .");
                        Button submit =(Button) dialogView.findViewById(R.id.submit);
                        submit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                finish();
                                alertDialog.dismiss();

                            }

                        });

                        alertDialog.show();

                    }
                }

                @Override
                public void onFailure(Call<GetSalesAttRegData> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(ViewExecuteRegionActiivty.this).create();
                    LayoutInflater inflater = getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.loginalert, null);
                    alertDialog.setView(dialogView);
                    TextView login =(TextView)dialogView.findViewById(R.id.login);
                    login.setText("Something went wrong .Please try again later .");
                    Button submit =(Button) dialogView.findViewById(R.id.submit);
                    submit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            finish();
                            alertDialog.dismiss();

                        }

                    });
                    alertDialog.show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


}
