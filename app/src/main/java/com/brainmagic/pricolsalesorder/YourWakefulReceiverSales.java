package com.brainmagic.pricolsalesorder;

import android.content.Context;
import android.content.Intent;


import androidx.legacy.content.WakefulBroadcastReceiver;

import service.BGServiceRegion;
import service.BGServicenormal;

public class YourWakefulReceiverSales extends WakefulBroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent service = new Intent(context, BGServicenormal.class);
        startWakefulService(context, service);
    }
}

