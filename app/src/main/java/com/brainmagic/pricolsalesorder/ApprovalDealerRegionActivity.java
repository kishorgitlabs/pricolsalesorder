package com.brainmagic.pricolsalesorder;

import android.app.ProgressDialog;
import android.content.SharedPreferences;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

import Home.homevalues;
import Logout.logout;
import adapter.ApprovalbleAdapter;
import alertbox.Alertbox;
import api.models.approvedealer.ApproveDealer;
import api.models.approvedealer.ApproveDealerData;
import api.retrofit.APIService;
import api.retrofit.RetrofitClient;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApprovalDealerRegionActivity extends AppCompatActivity {
    private ListView lisdealerapproval;
    private ImageView menu, back,home,logout;
    private Alertbox box = new Alertbox(this);
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private TextView title;
    private int regid;
    private List<ApproveDealer>data;
    private SearchView searchview;
    private  ApprovalbleAdapter approvalbleAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_approval_dealer_region);
        myshare = getSharedPreferences("pricolsalesorder", MODE_PRIVATE);
        editor = myshare.edit();
        lisdealerapproval=(ListView)findViewById(R.id.lisdealerapproval);
        home = (ImageView) findViewById(R.id.home);
        logout = (ImageView) findViewById(R.id.logout);
        back=(ImageView)findViewById(R.id.back) ;
        title=(TextView)findViewById(R.id.title);
        searchview=(SearchView)findViewById(R.id.searchview);
        title.setText("Approval Dealer");
        regid=myshare.getInt("idlistreg",0);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        searchview.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if(approvalbleAdapter!=null) {
                    approvalbleAdapter.filter(s);
                    approvalbleAdapter.notifyDataSetChanged();
                }

                if (s.length()==0){
                    GetApproval();
                }
                return false;
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new logout(ApprovalDealerRegionActivity.this).log_outReg();
            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new homevalues(ApprovalDealerRegionActivity.this).home_io();
            }
        });
        checkinternet();
    }

    private void checkinternet() {
        NetworkConnection networkConnection=new NetworkConnection(ApprovalDealerRegionActivity.this);
        if(networkConnection.CheckInternet()){
            GetApproval();
        }
    }

    private void GetApproval() {
        try {

            final ProgressDialog loading = ProgressDialog.show(ApprovalDealerRegionActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);
            APIService service = RetrofitClient.getApiService();
            Call<ApproveDealerData> call = service.ApproveData(regid);
            call.enqueue(new Callback<ApproveDealerData>() {
                @Override
                public void onResponse(Call<ApproveDealerData> call, Response<ApproveDealerData> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success")) {
                            data=response.body().getData();
                            approvalbleAdapter=new ApprovalbleAdapter(ApprovalDealerRegionActivity.this,data);
                            lisdealerapproval.setAdapter(approvalbleAdapter);
                        } else if(response.body().getResult().equals("NotSuccess")) {
                            box.showAlertbox("No New Dealer Found");
                        }else {
                            box.showAlertbox(getResources().getString(R.string.server_error));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        box.showAlertbox(getResources().getString(R.string.server_error));
                    }
                }
                @Override
                public void onFailure(Call<ApproveDealerData> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    box.showAlertbox(getResources().getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
