package com.brainmagic.pricolsalesorder;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.Uri;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.muddzdev.styleabletoastlibrary.StyleableToast;

import Logout.logout;
import SQL_Fields.SQLITE;
import alertbox.Alertbox;
import alertbox.PasswordChangeAlert;
import alertbox.PrimaryOrder;
import api.models.master.MasterData;
import api.models.master.master;
import api.models.updateversion.UpdateData;
import api.retrofit.APIService;
import api.retrofit.RetrofitClient;
import database.DBHelpersync;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import roomdb.database.AppDatabase;

import static roomdb.database.AppDatabase.getAppDatabase;

public class HomeActivty extends AppCompatActivity {
    private LinearLayout sales,attendance,online,Viewcus,visitreport,downloaddata,createnewcus,viewVisitReport;
    private TextView title,aspname;
    private ImageView menu,logout,setting;
    private String salesname;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private NetworkConnection networkConnection;
    private Alertbox box=new Alertbox(HomeActivty.this);
    private AppDatabase db;
    private LinearLayout changepassword ;
    private TextView appname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_activty);

        myshare = getSharedPreferences("pricolsalesorder", MODE_PRIVATE);
        editor = myshare.edit();
        networkConnection=new NetworkConnection(HomeActivty.this);
        sales=(LinearLayout)findViewById(R.id.sales);
        attendance=(LinearLayout)findViewById(R.id.attendance);
        online=(LinearLayout)findViewById(R.id.online);
        Viewcus=(LinearLayout)findViewById(R.id.Viewcus);
        visitreport=(LinearLayout)findViewById(R.id.visitreport);
        downloaddata=(LinearLayout)findViewById(R.id.downloaddata);
        createnewcus=(LinearLayout)findViewById(R.id.createnewcus);
        viewVisitReport=(LinearLayout)findViewById(R.id.view_visit_report);
        //changepassword=(LinearLayout)findViewById(R.id.changepassword);
        title=(TextView) findViewById(R.id.title);
        //checkInternet();
        aspname=(TextView)findViewById(R.id.aspname);
        appname=(TextView)findViewById(R.id.appname);

        //logout=(ImageView) findViewById(R.id.logout);
        setting=(ImageView) findViewById(R.id.setting);
//        logout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                new logout(HomeActivty.this).log_out();
//            }
//        });
        online.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if(myshare.getBoolean("synchronize",false)){
                if(isTableExists()){
                    Intent i =new Intent(HomeActivty.this,SalesConfirmDealerActivity.class);
                    i.putExtra("ordertypevalues","Secondary Order");
                    startActivity(i);
                }else {
                    final AlertDialog alertDialog = new AlertDialog.Builder(HomeActivty.this).create();
                    LayoutInflater inflater = getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.loginalert, null);
                    alertDialog.setView(dialogView);
                    TextView login =(TextView)dialogView.findViewById(R.id.login);
                    login.setText("No Order Found");
                    Button submit =(Button) dialogView.findViewById(R.id.submit);
                    submit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();

                        }

                    });
                    alertDialog.show();
                }

//                }else {
//                    final AlertDialog alertDialog = new AlertDialog.Builder(HomeActivty.this).create();
//                    LayoutInflater inflater = getLayoutInflater();
//                    View dialogView = inflater.inflate(R.layout.loginalert, null);
//                    alertDialog.setView(dialogView);
//                    TextView login =(TextView)dialogView.findViewById(R.id.login);
//                    login.setText("Kindly download data offline orders");
//                    Button submit =(Button) dialogView.findViewById(R.id.submit);
//                    submit.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            alertDialog.dismiss();
//
//                        }
//
//                    });
//                    alertDialog.show();
//                }


            }

        });

        viewVisitReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivty.this,ViewVisitReport.class));
            }
        });

        createnewcus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(myshare.getBoolean("synchronize",false)){
                    Intent i =new Intent(HomeActivty.this,CreateNewActivity.class);
                    startActivity(i);
                }else {
                    final AlertDialog alertDialog = new AlertDialog.Builder(HomeActivty.this).create();
                    LayoutInflater inflater = getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.loginalert, null);
                    alertDialog.setView(dialogView);
                    TextView login =(TextView)dialogView.findViewById(R.id.login);
                    login.setText("Kindly download data to Create New Customer!");
                    Button submit =(Button) dialogView.findViewById(R.id.submit);
                    submit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();

                        }

                    });
                    alertDialog.show();
                }

            }
        });
        salesname=myshare.getString("name","");
        aspname.setText(salesname);
        appname.setText(salesname);

        db=getAppDatabase(HomeActivty.this);
        title.setText("Welcome");

        sales.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(myshare.getBoolean("synchronize",false)){
                    Intent i =new Intent(HomeActivty.this,SalesOrderType.class);
                    startActivity(i);

                }else {
                    final AlertDialog alertDialog = new AlertDialog.Builder(HomeActivty.this).create();
                    LayoutInflater inflater = getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.loginalert, null);
                    alertDialog.setView(dialogView);
                    TextView login =(TextView)dialogView.findViewById(R.id.login);
                    login.setText("Kindly download data  to View Sales information!");
                    Button submit =(Button) dialogView.findViewById(R.id.submit);
                    submit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();

                        }

                    });
                    alertDialog.show();
                 //   box.showAlertbox("Kindly download data the data");
                }
            }
        });
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PopupMenu popupMenu = new PopupMenu(HomeActivty.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu pop) {
                        pop.dismiss();
                    }
                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.primary:
                                PrimaryOrder alert = new PrimaryOrder(HomeActivty.this);// new saleslogin().execute();
                                alert.showLoginboxtoggle();
                                return true;
                            case R.id.ch:
                                CheckInternetforpass();
                                return true;
                            case R.id.logout:
                                new logout(HomeActivty.this).log_out();
                                return true;
                        }
                        return false;
                    }
                });
                popupMenu.inflate(R.menu.pricolcms);
                Menu pop = popupMenu.getMenu();
                popupMenu.show();

            }
        });

        visitreport.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               if(myshare.getBoolean("synchronize",false)){
                   // TODO Auto-generated method stub
                   Intent i =new Intent(HomeActivty.this,ExsitingvistorActivity.class).putExtra("custype","Existing Customer");
                   startActivity(i);


               }else {
                   final AlertDialog alertDialog = new AlertDialog.Builder(HomeActivty.this).create();
                   LayoutInflater inflater = getLayoutInflater();
                   View dialogView = inflater.inflate(R.layout.loginalert, null);
                   alertDialog.setView(dialogView);
                   TextView login =(TextView)dialogView.findViewById(R.id.login);
                   login.setText("Kindly download data to Visit Report!");
                   Button submit =(Button) dialogView.findViewById(R.id.submit);
                   submit.setOnClickListener(new View.OnClickListener() {
                       @Override
                       public void onClick(View view) {
                           alertDialog.dismiss();

                       }

                   });
                   alertDialog.show();
               }

           }
       });


        attendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i =new Intent(HomeActivty.this,AttendanceActivity.class);
                startActivity(i);
            }
        });
        Viewcus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(myshare.getBoolean("synchronize",false)){
                    Intent i =new Intent(HomeActivty.this,ViewCustomerActivity.class);
                    startActivity(i);
                }else {
                    final AlertDialog alertDialog = new AlertDialog.Builder(HomeActivty.this).create();
                    LayoutInflater inflater = getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.loginalert, null);
                    alertDialog.setView(dialogView);
                    TextView login =(TextView)dialogView.findViewById(R.id.login);
                    login.setText("Kindly download data to View Customer!");
                    Button submit =(Button) dialogView.findViewById(R.id.submit);
                    submit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();

                        }

                    });
                    alertDialog.show();
                }

            }
        });
        downloaddata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(networkConnection.CheckInternet()){
                    GetMasterData();
                }else{
                    final AlertDialog alertDialog = new AlertDialog.Builder(HomeActivty.this).create();
                    LayoutInflater inflater = getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.loginalert, null);
                    alertDialog.setView(dialogView);
                    TextView login =(TextView)dialogView.findViewById(R.id.login);
                    login.setText("Please Check Your Internet Connection!");
                    Button submit =(Button) dialogView.findViewById(R.id.submit);
                    submit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();

                        }

                    });
                    alertDialog.show();
                }
            }
        });
        getpackageinfo();
        checkInternetversion();
    }

    private void checkInternetversion() {
        NetworkConnection networkConnection=new NetworkConnection(HomeActivty.this);
        if(networkConnection.CheckInternet()){
            getupdatestatus();
        }else {
            final AlertDialog alertDialog = new AlertDialog.Builder(HomeActivty.this).create();
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.loginalert, null);
            alertDialog.setView(dialogView);
            TextView login =(TextView)dialogView.findViewById(R.id.login);
            login.setText("Please Check your Internet Connection!");
            Button submit =(Button) dialogView.findViewById(R.id.submit);
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();

                }

            });
            alertDialog.show();
        }
    }

    private void getupdatestatus() {
        try {
//            final ProgressDialog loading = ProgressDialog.show(HomeActivty.this, getResources().getString(R.string.app_name), "Downloading...", false, false);
            APIService service = RetrofitClient.getApiService();
            Call<UpdateData> call = service.update(getpackageinfo());
            //  Call<Master> call = service.GetMasterData("Siva");
            call.enqueue(new Callback<UpdateData>() {
                @Override
                public void onResponse(Call<UpdateData> call, Response<UpdateData> response) {
                    try {
                       // loading.dismiss();
                        if (response.body().getData().equals("Update Available")) {
                            final AlertDialog alertDialog = new AlertDialog.Builder(HomeActivty.this).create();
                            LayoutInflater inflater = getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.loginalertupdate, null);
                            alertDialog.setView(dialogView);
                            TextView login = (TextView) dialogView.findViewById(R.id.login);
                            login.setText("Update Available!");
                            Button update1 = (Button) dialogView.findViewById(R.id.update1);
                            Button skip = (Button) dialogView.findViewById(R.id.skip);
                            update1.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                                    try {
                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                    } catch (android.content.ActivityNotFoundException anfe) {
                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                    }


                                }

                            });

                            skip.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    alertDialog.dismiss();
                                }
                            });
                            alertDialog.show();
                        }


//                            box.showAlertbox(getResources().getString(R.string.server_error));
                    } catch (Exception e) {
                        e.printStackTrace();
                        //loading.dismiss();
                        final AlertDialog alertDialog = new AlertDialog.Builder(HomeActivty.this).create();
                        LayoutInflater inflater = getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.loginalert, null);
                        alertDialog.setView(dialogView);
                        TextView login =(TextView)dialogView.findViewById(R.id.login);
                        login.setText(getResources().getString(R.string.server_error));
                        Button submit =(Button) dialogView.findViewById(R.id.submit);
                        submit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                alertDialog.dismiss();

                            }

                        });
                        alertDialog.show();
                    }
                }
                @Override
                public void onFailure(Call<UpdateData> call, Throwable t) {
                   // loading.dismiss();
                    t.printStackTrace();
                    final AlertDialog alertDialog = new AlertDialog.Builder(HomeActivty.this).create();
                    LayoutInflater inflater = getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.loginalert, null);
                    alertDialog.setView(dialogView);
                    TextView login =(TextView)dialogView.findViewById(R.id.login);
                    login.setText(getResources().getString(R.string.server_error));
                    Button submit =(Button) dialogView.findViewById(R.id.submit);
                    submit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();

                        }

                    });
                    alertDialog.show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

//        if (getpackageinfo() < 2){
//            final AlertDialog alertDialog = new AlertDialog.Builder(HomeActivty.this).create();
//            LayoutInflater inflater = getLayoutInflater();
//            View dialogView = inflater.inflate(R.layout.loginalertupdate, null);
//            alertDialog.setView(dialogView);
//            TextView login =(TextView)dialogView.findViewById(R.id.login);
//            login.setText("Update Available!");
//            Button update1 =(Button) dialogView.findViewById(R.id.update1);
//            Button skip =(Button) dialogView.findViewById(R.id.skip);
//            update1.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
//                    try {
//                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
//                    } catch (android.content.ActivityNotFoundException anfe) {
//                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
//                    }
//
//
//                }
//
//            });
//
//            skip.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    alertDialog.dismiss();
//                }
//            });
//            alertDialog.show();
//            final android.support.v7.app.AlertDialog.Builder alertbox=new android.support.v7.app.AlertDialog.Builder(this);
//            alertbox.setTitle("Update Available");
//            alertbox.setCancelable(false);
//            alertbox.setPositiveButton("Update", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
//                    try {
//                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
//                    } catch (android.content.ActivityNotFoundException anfe) {
//                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
//                    }
//
//
//                }
//            });
//
//            alertbox.setNegativeButton("Skip", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//
//                    dialog.cancel();
//
//                }
//            });
//            android.support.v7.app.AlertDialog alert11 = alertbox.create();
//            alert11.show();
//
      // }
    }

    private int getpackageinfo() {
        int version = -1;
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_META_DATA);
            version = pInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e(this.getClass().getSimpleName(), "Name not found", e1);
        }
        return version;
    }

    private void checkInternet() {
        NetworkConnection networkConnection= new NetworkConnection(HomeActivty.this);
        if(networkConnection.CheckInternet()){
            GetMasterData();
        }else {
            StyleableToast st = new StyleableToast(HomeActivty.this, "Please Check Your Internet Connection", Toast.LENGTH_SHORT);
            st.setBackgroundColor(HomeActivty.this.getResources().getColor(R.color.colorPrimary));
            st.setTextColor(Color.WHITE);
            st.setMaxAlpha();
            st.show();
        }
    }

    private void CheckInternetforpass() {
        NetworkConnection network = new NetworkConnection(HomeActivty.this);
        if (network.CheckInternet()) {
            PasswordChangeAlert alert = new PasswordChangeAlert(HomeActivty.this);// new saleslogin().execute();
            alert.showLoginbox();
        } else {
            final AlertDialog alertDialog = new AlertDialog.Builder(HomeActivty.this).create();
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.loginalert, null);
            alertDialog.setView(dialogView);
            TextView login =(TextView)dialogView.findViewById(R.id.login);
            login.setText("Please Check your Internet Connection!");
            Button submit =(Button) dialogView.findViewById(R.id.submit);
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();

                }

            });
            alertDialog.show();
        }

    }

    private boolean isTableExists() {
        SQLiteDatabase mDatabase;
        DBHelpersync dbHelp = new DBHelpersync(HomeActivty.this);
        mDatabase = dbHelp.getWritableDatabase();
        String query = "select DISTINCT tbl_name from sqlite_master where tbl_name = '"+ SQLITE.TABLE_Additems+"'";
        //String query ="select * from "+ SQLITE.TABLE_NewPartsOrder+" order by date asc";
        Cursor cursor = mDatabase.rawQuery(query, null);
        Log.v("preview",query);
        if(cursor!=null) {
            if(cursor.getCount()>0) {
                cursor.close();
                mDatabase.close();
                return true;
            }
            cursor.close();
            mDatabase.close();
        }
        return false;

    }

    private void GetMasterData() {
        try {
            final ProgressDialog loading = ProgressDialog.show(HomeActivty.this, getResources().getString(R.string.app_name), "Downloading...", false, false);
            APIService service = RetrofitClient.getApiService();
            Call<master> call = service.GetMasterData(myshare.getInt("idlist", 0));
            //  Call<Master> call = service.GetMasterData("Siva");
            call.enqueue(new Callback<master>() {
                @Override
                public void onResponse(Call<master> call, Response<master> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success") || response.body().getResult().equals("OnlyCustomer"))
                            SaveMasterData(response.body().getData());
                        else if (response.body().getResult().equals("NoCustomer")){
                            final AlertDialog alertDialog = new AlertDialog.Builder(HomeActivty.this).create();
                            LayoutInflater inflater = getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.loginalert, null);
                            alertDialog.setView(dialogView);
                            TextView login =(TextView)dialogView.findViewById(R.id.login);
                            login.setText("Customer data not available");
                            Button submit =(Button) dialogView.findViewById(R.id.submit);
                            submit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    alertDialog.dismiss();

                                }

                            });
                            alertDialog.show();
                        }
                        else if(response.body().getResult().equals("Failure"))
                        {
                            final AlertDialog alertDialog = new AlertDialog.Builder(HomeActivty.this).create();
                            LayoutInflater inflater = getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.loginalert, null);
                            alertDialog.setView(dialogView);
                            TextView login =(TextView)dialogView.findViewById(R.id.login);
                            login.setText("Something went wrong .Please  try again later .");
                            Button submit =(Button) dialogView.findViewById(R.id.submit);
                            submit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    alertDialog.dismiss();

                                }

                            });
                            alertDialog.show();
                        }
                        else{
                            final AlertDialog alertDialog = new AlertDialog.Builder(HomeActivty.this).create();
                            LayoutInflater inflater = getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.loginalert, null);
                            alertDialog.setView(dialogView);
                            TextView login =(TextView)dialogView.findViewById(R.id.login);
                            login.setText(getResources().getString(R.string.server_error));
                            Button submit =(Button) dialogView.findViewById(R.id.submit);
                            submit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    alertDialog.dismiss();

                                }

                            });
                            alertDialog.show();
                        }
//                            box.showAlertbox(getResources().getString(R.string.server_error));
                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        final AlertDialog alertDialog = new AlertDialog.Builder(HomeActivty.this).create();
                        LayoutInflater inflater = getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.loginalert, null);
                        alertDialog.setView(dialogView);
                        TextView login =(TextView)dialogView.findViewById(R.id.login);
                        login.setText(getResources().getString(R.string.server_error));
                        Button submit =(Button) dialogView.findViewById(R.id.submit);
                        submit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                alertDialog.dismiss();

                            }

                        });
                        alertDialog.show();
                    }
                }
                @Override
                public void onFailure(Call<master> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    final AlertDialog alertDialog = new AlertDialog.Builder(HomeActivty.this).create();
                    LayoutInflater inflater = getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.loginalert, null);
                    alertDialog.setView(dialogView);
                    TextView login =(TextView)dialogView.findViewById(R.id.login);
                    login.setText(getResources().getString(R.string.server_error));
                    Button submit =(Button) dialogView.findViewById(R.id.submit);
                    submit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();

                        }

                    });
                    alertDialog.show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void SaveMasterData(MasterData data) {
        db.adhereDao().DeleteAllCustomer();
        db.adhereDao().DeleteAllCity();
        db.adhereDao().DeleteAllstate();
        db.adhereDao().DeleteAllDistreibut();
        db.adhereDao().DeleteAllProduct();
        db.adhereDao().DeleteAllDeal();
        db.adhereDao().DeleteAllSetting();
        db.adhereDao().insertAllCustomer(data.getCustomer_list());
        db.adhereDao().insertAllcity(data.getCitylist());
        db.adhereDao().insertAllstate(data.getStatelist());
        db.adhereDao().insertAllDistributor(data.getDistributor_list());
        db.adhereDao().insertAllproduct(data.getProductlist());
        db.adhereDao().insertAllDeal(data.getDeal());
        db.adhereDao().insertAllSetting(data.getSettingDetails());
        setPrimaryOrder(data.getSettingDetails().get(1).getFlag());
        // db.adhereDao().insertAllAgeing(data.getAmt_list());
        editor.putBoolean("synchronize",true);
        editor.commit();
        final AlertDialog alertDialog = new AlertDialog.Builder(HomeActivty.this).create();
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.loginalert, null);
        alertDialog.setView(dialogView);
        TextView login =(TextView)dialogView.findViewById(R.id.login);
        login.setText("Master data downloaded Successfully!");
        Button submit =(Button) dialogView.findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();

            }

        });
        alertDialog.show();
        //  alert.showAlertbox(db.adhereDao().GetCustomer_list().get(0).getName());
        //box.showAlertbox("Master data downloaded Successfully");
    }

    private void setPrimaryOrder(String primaryOrderFlag)
    {
        if(primaryOrderFlag.equals("No")) {
            editor.putBoolean("primaryorder", false);
            editor.putString("toggletext", "Disabled");
        }else {
            editor.putString("toggletext","Enabled");
            editor.putBoolean("primaryorder",true);
        }
//        editor.commit();
//        editor.apply();
    }
}
