package com.brainmagic.pricolsalesorder;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.SharedPreferences;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.jaredrummler.materialspinner.MaterialSpinner;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import Logout.logout;
import adapter.ViewVisitReportOthersAdapter;
import alertbox.Alertbox;
import alertbox.PasswordChangeAlert;
import alertbox.PrimaryOrder;
import api.models.viewvisitreport.viewvisitreportrm.ViewVisitReportRM;
import api.models.viewvisitreport.viewvisitreportse.ViewVisitReportSE;
import api.retrofit.APIService;
import api.retrofit.RetrofitClient;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewVisitReport extends AppCompatActivity {

    private ImageView home,setting;
    private TextView title;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private String salesname;
    private TextView fromDate, toDate;
    private MaterialSpinner customerTypeSpinner;
    private ListView viewVisitReport;
    String executiveName="";
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_visit_report);

        myshare = getSharedPreferences("pricolsalesorder", MODE_PRIVATE);
        editor = myshare.edit();
        title=findViewById(R.id.title);
        home=(ImageView) findViewById(R.id.home);
        setting=(ImageView) findViewById(R.id.setting);
        fromDate=findViewById(R.id.from_date);
        toDate=findViewById(R.id.to_date);
        customerTypeSpinner=findViewById(R.id.customer_type);
        viewVisitReport=findViewById(R.id.view_visit_report_list);

        salesname=myshare.getString("namereg","");

//        appname.setText(salesname);

        title.setText("View Visit Report");

        List<String> customerTypes=new ArrayList<>();
        customerTypes.add("Select Customer Type");
        customerTypes.add("Electrician");
        customerTypes.add("Retailer");
        customerTypes.add("Mechanic");
        customerTypes.add("Others");

        customerTypeSpinner.setItems(customerTypes);


        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//               onBackPressed();
                finish();
            }
        });

        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PopupMenu popupMenu = new PopupMenu(ViewVisitReport.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu pop) {
                        pop.dismiss();
                    }
                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.primary:
                                PrimaryOrder alert = new PrimaryOrder(ViewVisitReport.this);// new saleslogin().execute();
                                alert.showLoginboxtoggle();
                                return true;
                            case R.id.ch:
                                CheckInternetforpass();
                                return true;
                            case R.id.logout:
                                new logout(ViewVisitReport.this).log_out();
                                return true;
                        }
                        return false;
                    }
                });
                popupMenu.inflate(R.menu.pricolcms);
                Menu pop = popupMenu.getMenu();
                popupMenu.show();

            }
        });

        fromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Get Current Date
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(ViewVisitReport.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                fromDate.setText(year + "-" +(monthOfYear + 1) + "-" +dayOfMonth);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();

            }
        });

        toDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Get Current Date
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(ViewVisitReport.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                toDate.setText(year + "-" +(monthOfYear + 1) + "-" +dayOfMonth);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();

            }
        });
        NetworkConnection network = new NetworkConnection(ViewVisitReport.this);
        if(network.CheckInternet()) {
            if (myshare.getString("userType", "").equals("Sales")) {
                String userName=myshare.getString("username","");
                viewVisitReportDetails(userName);
            }
            else {
                getSalesManagerReportList();
            }
        }
        else {
            Alertbox alertbox=new Alertbox(this);
            alertbox.showAlertboxwithback("No Internet Connection. Please switch On your Internet Connection");
        }
    }

//    private void spinnerAdapter(List<String> customerTypeList){
////        ArrayAdapter arrayAdapter=new ArrayAdapter(ViewVisitReport.this,android.R.layout.simple_spinner_dropdown_item,customerTypeList);
////        customerTypes.setAdapter(arrayAdapter);
//        customerTypes.setItems(customerTypeList);
//    }

    private void CheckInternetforpass() {
        NetworkConnection network = new NetworkConnection(ViewVisitReport.this);
        if (network.CheckInternet()) {
            PasswordChangeAlert alert = new PasswordChangeAlert(ViewVisitReport.this);// new saleslogin().execute();
            alert.showLoginbox();
        } else {
            final AlertDialog alertDialog = new AlertDialog.Builder(ViewVisitReport.this).create();
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.loginalert, null);
            alertDialog.setView(dialogView);
            TextView login =(TextView)dialogView.findViewById(R.id.login);
            login.setText("Please Check your Internet Connection!");
            Button submit =(Button) dialogView.findViewById(R.id.submit);
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();

                }

            });
            alertDialog.show();
        }

    }

    private void viewVisitReportDetails(String userName)
    {
        final ProgressDialog loading = ProgressDialog.show(this, getResources().getString(R.string.app_name), "Loading...", false, false);
        APIService api=RetrofitClient.getApiService();


        Call<ViewVisitReportSE> call=api.viewVisitReportSE(userName);

        try {
            call.enqueue(new Callback<ViewVisitReportSE>() {
                @Override
                public void onResponse(Call<ViewVisitReportSE> call, Response<ViewVisitReportSE> response) {
                    try{
                        loading.dismiss();
                        if(response.isSuccessful())
                        {
                            if(response.body().getResult().equals("Success"))
                            {
                                ViewVisitReportOthersAdapter adapter=new ViewVisitReportOthersAdapter(ViewVisitReport.this,response.body().getData());
                                viewVisitReport.setAdapter(adapter);
                            }
                            else if(response.body().getResult().equals("NotSuccess"))
                            {
                                Alertbox alertbox=new Alertbox(ViewVisitReport.this);
                                alertbox.showAlertBoxWithBack("No Record Found");
                            }
                            else {
                                Alertbox alertbox=new Alertbox(ViewVisitReport.this);
                                alertbox.showAlertbox("Something went wrong .Please  try again later .");
                            }
                        }
                    }catch (Exception e){
                        loading.dismiss();
                        e.printStackTrace();
                        Alertbox alertbox=new Alertbox(ViewVisitReport.this);
                        alertbox.showAlertbox("Something went wrong .Please  try again later .");
                    }
                }

                @Override
                public void onFailure(Call<ViewVisitReportSE> call, Throwable t) {
                    loading.dismiss();
                    Alertbox alertbox=new Alertbox(ViewVisitReport.this);
                    alertbox.showAlertbox("Something went wrong .Please  try again later .");
                }
            });
        }catch (Exception e)
        {
            loading.dismiss();
            e.printStackTrace();
            Alertbox alertbox=new Alertbox(ViewVisitReport.this);
            alertbox.showAlertbox("Something went wrong .Please try again later .. Please contact Admin");
        }
    }

    public void search(View view)
    {
        String fromDateString=fromDate.getText().toString();
        String toDateString=toDate.getText().toString();
        String customerType=customerTypeSpinner.getText().toString();

        if(TextUtils.isEmpty(fromDateString))
        {
            Toast.makeText(this, "Please Choose From Date", Toast.LENGTH_SHORT).show();
        }
        else if(TextUtils.isEmpty(toDateString))
        {
            Toast.makeText(this, "Please Choose To Date", Toast.LENGTH_SHORT).show();
        }
        else if(customerType.equals("Select Customer Type"))
        {
            Toast.makeText(this, "Please Select Customer", Toast.LENGTH_SHORT).show();
        }
        else {
            viewVisitReportBySearch(fromDateString,toDateString,customerType);
        }

    }

    private void viewVisitReportBySearch(String fromDateString, String toDateString,String customerType)
    {
        final ProgressDialog loading = ProgressDialog.show(this, getResources().getString(R.string.app_name), "Loading...", false, false);
        APIService api=RetrofitClient.getApiService();

        String userName="";
        if (myshare.getString("userType", "").equals("Sales"))
            userName=myshare.getString("username","");
        else  userName=executiveName;
        Call<ViewVisitReportSE> call=api.viewVisitReportSEBySearch(fromDateString,toDateString,userName,customerType);

        try {
            call.enqueue(new Callback<ViewVisitReportSE>() {
                @Override
                public void onResponse(Call<ViewVisitReportSE> call, Response<ViewVisitReportSE> response) {
                    try{
                        loading.dismiss();
                        if(response.isSuccessful())
                        {
                            if(response.body().getResult().equals("Success"))
                            {
                                ViewVisitReportOthersAdapter adapter=new ViewVisitReportOthersAdapter(ViewVisitReport.this,response.body().getData());
                                viewVisitReport.setAdapter(adapter);
                            }
                            else if(response.body().getResult().equals("NotSuccess"))
                            {
                                Alertbox alertbox=new Alertbox(ViewVisitReport.this);
                                alertbox.showAlertbox("No Record Found");
                            }
                            else {
                                Alertbox alertbox=new Alertbox(ViewVisitReport.this);
                                alertbox.showAlertbox("Something went wrong .Please  try again later .");
                            }
                        }
                    }catch (Exception e){
                        loading.dismiss();
                        e.printStackTrace();
                        Alertbox alertbox=new Alertbox(ViewVisitReport.this);
                        alertbox.showAlertbox("Something went wrong .Please  try again later .");
                    }
                }

                @Override
                public void onFailure(Call<ViewVisitReportSE> call, Throwable t) {
                    loading.dismiss();
                    Alertbox alertbox=new Alertbox(ViewVisitReport.this);
                    alertbox.showAlertbox("Something went wrong .Please  try again later .");
                }
            });
        }catch (Exception e)
        {
            loading.dismiss();
            e.printStackTrace();
            Alertbox alertbox=new Alertbox(ViewVisitReport.this);
            alertbox.showAlertbox("Something went wrong .Please try again later .. Please contact Admin");
        }
    }

    private void getSalesManagerReportList(){
        final ListDialog dialog=new ListDialog(ViewVisitReport.this,"Select Sales Executives");
        final ProgressDialog loading = ProgressDialog.show(this, getResources().getString(R.string.app_name), "Loading...", false, false);
        APIService api=RetrofitClient.getApiService();

        int regid=myshare.getInt("idlistreg",0);
        Call<ViewVisitReportRM> call=api.viewVisitReportRM(regid);

        try {
            call.enqueue(new Callback<ViewVisitReportRM>() {
                @Override
                public void onResponse(Call<ViewVisitReportRM> call, Response<ViewVisitReportRM> response) {
                    try{
                        loading.dismiss();
                        if(response.isSuccessful())
                        {
                            if(response.body().getResult().equals("Success"))
                            {
                                dialog.setItems(response.body().getData());
                                dialog.showSpinerDialog();
                            }
                            else {
                                Alertbox alertbox=new Alertbox(ViewVisitReport.this);
                                alertbox.showAlertbox("Record sent is Invalid. Please Login again to view this Data");
                            }
                        }
                    }catch (Exception e){
                        loading.dismiss();
                        e.printStackTrace();
                        Alertbox alertbox=new Alertbox(ViewVisitReport.this);
                        alertbox.showAlertbox("Something went wrong .Please  try again later .");
                    }
                }

                @Override
                public void onFailure(Call<ViewVisitReportRM> call, Throwable t) {
                    loading.dismiss();
                    Alertbox alertbox=new Alertbox(ViewVisitReport.this);
                    alertbox.showAlertbox("Something went wrong .Please  try again later .");
                }
            });
        }catch (Exception e)
        {
            loading.dismiss();
            e.printStackTrace();
            Alertbox alertbox=new Alertbox(ViewVisitReport.this);
            alertbox.showAlertbox("Something went wrong .Please try again later .. Please contact Admin");
        }

        dialog.bindOnSpinerListener(new OnSpinerItemClick(){
            @Override
            public void onClick(String userName, int var2) {
                executiveName=userName;
                viewVisitReportDetails(userName);
            }
        });
    }
}
