package com.brainmagic.pricolsalesorder;

import android.content.Context;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

public class BottomSheetNavigation extends BottomSheetDialogFragment {
    private BottomSheetListener bottomSheetListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.activity_bottom_sheet_navigation, container,false);
        LinearLayout currentmonthfilter=(LinearLayout)view.findViewById(R.id.currentmonthfilter);
        LinearLayout datewisefilter=(LinearLayout)view.findViewById(R.id.datewisefilter);
        currentmonthfilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetListener.bottomSheetListener("currentmonthwise");
            }
        });
        datewisefilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetListener.bottomSheetListener("datewise");
            }
        });
        return view;


    }
    public interface BottomSheetListener
    {
        public void bottomSheetListener(String orderhistoryfilter);
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            bottomSheetListener = (BottomSheetListener) context;
        } catch (Exception e) {

        }
    }
}
