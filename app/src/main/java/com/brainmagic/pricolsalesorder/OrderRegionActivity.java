package com.brainmagic.pricolsalesorder;

import android.app.ProgressDialog;
import android.content.SharedPreferences;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

import Home.homevalues;
import Logout.logout;
import adapter.OrderRegionAdapter;
import adapter.OrderRegionDealerAdapter;
import alertbox.Alertbox;
import alertbox.PrimaryOrder;
import api.models.regionneworder.Order_list;
import api.models.regionneworder.RegionNewOrderData;
import api.retrofit.APIService;
import api.retrofit.RetrofitClient;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderRegionActivity extends AppCompatActivity {
    private ListView listorderregion;
    SharedPreferences myshare;
    SharedPreferences.Editor editor;
    private ImageView menu, back,home,logout;
    private TextView title,distitext,dealtext;
    private Alertbox box = new Alertbox(this);
    private SearchView searchview;
    private int regid;
    private String ordertype;
    private List<Order_list>data;
    private OrderRegionAdapter orderRegionAdapter;
    private OrderRegionDealerAdapter orderRegionAdapter1;
    private ImageView setting;
    TextView Text2,Amount;
    LinearLayout footerlayout;
    private Integer data1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_region);
        listorderregion=(ListView)findViewById(R.id.listorderregion);
        myshare = getSharedPreferences("pricolsalesorder", MODE_PRIVATE);
        editor = myshare.edit();
        home = (ImageView) findViewById(R.id.home);
        logout = (ImageView) findViewById(R.id.logout);
        back=(ImageView)findViewById(R.id.back) ;
        title=(TextView)findViewById(R.id.title);
        distitext=(TextView)findViewById(R.id.distitext);
        dealtext=(TextView)findViewById(R.id.dealtext);
        searchview=(SearchView)findViewById(R.id.searchview);
        Text2 = (TextView) findViewById(R.id.Title);
        Amount = (TextView) findViewById(R.id.amounttext);
        title.setText("Order History");


        ordertype=getIntent().getStringExtra("ordertype");
        setting=(ImageView) findViewById(R.id.setting);
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PopupMenu popupMenu = new PopupMenu(OrderRegionActivity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu pop) {
                        pop.dismiss();
                    }
                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.primary:
                                PrimaryOrder alert = new PrimaryOrder(OrderRegionActivity.this);// new saleslogin().execute();
                                alert.showLoginboxtoggle();
                                return true;
                            case R.id.ch:
                                CheckInternetforpass();
                                return true;
                            case R.id.logout:
                                new logout(OrderRegionActivity.this).log_outReg();
                                return true;
                        }
                        return false;
                    }
                });
                popupMenu.inflate(R.menu.pricolcms);
                Menu pop = popupMenu.getMenu();
                popupMenu.show();

            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new homevalues(OrderRegionActivity.this).home_io();
            }
        });
//        logout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                new logout(OrderRegionActivity.this).log_outReg();
//            }
//        });
        checkInternet();



    }

    private void CheckInternetforpass() {
        NetworkConnection network = new NetworkConnection(OrderRegionActivity.this);
        if (network.CheckInternet()) {
            PasswordChangeAlertRegion alert = new PasswordChangeAlertRegion(OrderRegionActivity.this);// new saleslogin().execute();
            alert.showLoginbox();
        } else {
            final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(OrderRegionActivity.this).create();
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.loginalert, null);
            alertDialog.setView(dialogView);
            TextView login =(TextView)dialogView.findViewById(R.id.login);
            login.setText("Please Check Your internet Connection !");
            Button submit =(Button) dialogView.findViewById(R.id.submit);
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();

                }

            });
            alertDialog.show();
        }
    }

    private void checkInternet() {
        NetworkConnection networkConnection=new NetworkConnection(OrderRegionActivity.this);
        if(networkConnection.CheckInternet()){
            GetOrders();
        }else {
            final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(OrderRegionActivity.this).create();
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.loginalert, null);
            alertDialog.setView(dialogView);
            TextView login =(TextView)dialogView.findViewById(R.id.login);
            login.setText("Please Check Your internet Connection !");
            Button submit =(Button) dialogView.findViewById(R.id.submit);
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();

                }

            });
            alertDialog.show();
        }
    }

    private void GetOrders() {
        try {
//            if(ordertype.equals("Primary Order")){
                final ProgressDialog loading = ProgressDialog.show(OrderRegionActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);
                APIService service = RetrofitClient.getApiService();
                Call<RegionNewOrderData> call = service.regioncurrentmonthorder(regid);
                call.enqueue(new Callback<RegionNewOrderData>() {
                    @Override

                    public void onResponse(Call<RegionNewOrderData> call, Response<RegionNewOrderData> response) {
                        try {
                            loading.dismiss();
                            if (response.body().getResult().equals("Success")) {

                                    data=response.body().getData().getOrder_list();
                                    data1=response.body().getData().getTot();
                                    orderRegionAdapter=new OrderRegionAdapter(OrderRegionActivity.this,data);
                                    listorderregion.setAdapter(orderRegionAdapter);
                                    Amount.setText("₹"+String.valueOf(data1));


                            } else if(response.body().getResult().equals("NotSuccess")) {
                                final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(OrderRegionActivity.this).create();
                                LayoutInflater inflater = getLayoutInflater();
                                View dialogView = inflater.inflate(R.layout.loginalert, null);
                                alertDialog.setView(dialogView);
                                TextView login =(TextView)dialogView.findViewById(R.id.login);
                                login.setText("No Order Found !");
                                Button submit =(Button) dialogView.findViewById(R.id.submit);
                                submit.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        alertDialog.dismiss();

                                    }

                                });
                                alertDialog.show();
                            }else
                            {
                                final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(OrderRegionActivity.this).create();
                                LayoutInflater inflater = getLayoutInflater();
                                View dialogView = inflater.inflate(R.layout.loginalert, null);
                                alertDialog.setView(dialogView);
                                TextView login =(TextView)dialogView.findViewById(R.id.login);
                                login.setText("Something went wrong .Please try again later .");
                                Button submit =(Button) dialogView.findViewById(R.id.submit);
                                submit.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        alertDialog.dismiss();

                                    }

                                });
                                alertDialog.show();
//                                box.showAlertbox(getResources().getString(R.string.server_error));
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            loading.dismiss();
                            final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(OrderRegionActivity.this).create();
                            LayoutInflater inflater = getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.loginalert, null);
                            alertDialog.setView(dialogView);
                            TextView login =(TextView)dialogView.findViewById(R.id.login);
                            login.setText("Something went wrong .Please try again later .");
                            Button submit =(Button) dialogView.findViewById(R.id.submit);
                            submit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    alertDialog.dismiss();

                                }

                            });
                            alertDialog.show();

                        }
                    }

                    @Override
                    public void onFailure(Call<RegionNewOrderData> call, Throwable t) {
                        loading.dismiss();
                        t.printStackTrace();
                        final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(OrderRegionActivity.this).create();
                        LayoutInflater inflater = getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.loginalert, null);
                        alertDialog.setView(dialogView);
                        TextView login =(TextView)dialogView.findViewById(R.id.login);
                        login.setText("Something went wrong .Please try again later .");
                        Button submit =(Button) dialogView.findViewById(R.id.submit);
                        submit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                alertDialog.dismiss();

                            }

                        });
                        alertDialog.show();
                    }
                });

        } catch (Exception e) {
            e.printStackTrace();
        }



    }
}
