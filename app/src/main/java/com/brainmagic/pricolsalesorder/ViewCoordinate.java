package com.brainmagic.pricolsalesorder;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import Home.homevalues;
import Logout.logout;
import adapter.ViewCordinateAdapter;
import alertbox.Alertbox;
import alertbox.PasswordChangeAlert;
import alertbox.PrimaryOrder;
import network.NetworkConnection;
import persistance.ServerConnection;

public class ViewCoordinate extends AppCompatActivity {
    private ListView listView;
    private ImageView menu, back,home,logout,setting;
    private Alertbox box = new Alertbox(this);
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private Connection connection;
    private Statement statement;
    private ResultSet resultSet;
    private List<String> Timelist, addresslist,distanceList;
    private Integer travelid, sid;
    private String Date,empid;
    private TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_coordinate);
        title = (TextView) findViewById(R.id.title);
        title.setText("View Coordinates");
        listView = (ListView) findViewById(R.id.travelhistory1);
        home = (ImageView) findViewById(R.id.home);
       // logout = (ImageView) findViewById(R.id.logout);
        setting = (ImageView) findViewById(R.id.setting);
        myshare = getSharedPreferences("pricolsalesorder", MODE_PRIVATE);
        editor = myshare.edit();
        Date=getIntent().getStringExtra("Date");
          empid=getIntent().getStringExtra("idCordinate");
        travelid = myshare.getInt("trackid", 0);
        sid = myshare.getInt("sid", 0);
        Timelist = new ArrayList<>();
        addresslist = new ArrayList<>();
        distanceList=new ArrayList<>();
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PopupMenu popupMenu = new PopupMenu(ViewCoordinate.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu pop) {
                        pop.dismiss();
                    }
                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.primary:
                                PrimaryOrder alert = new PrimaryOrder(ViewCoordinate.this);// new saleslogin().execute();
                                alert.showLoginboxtoggle();
                                return true;
                            case R.id.ch:
                                CheckInternetforpass();
                                return true;
                            case R.id.logout:
                                new logout(ViewCoordinate.this).log_out();
                                return true;
                        }
                        return false;
                    }
                });
                popupMenu.inflate(R.menu.pricolcms);
                Menu pop = popupMenu.getMenu();
                popupMenu.show();

            }
        });

        // back=(ImageView)findViewById(R.id.back) ;
//        back.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                onBackPressed();
//            }
//        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new homevalues(ViewCoordinate.this).home_io();
            }
        });
//        logout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                new logout(ViewCoordinate.this).log_out();
//            }
//        });
        checkInternet();

    }

    private void CheckInternetforpass() {
        NetworkConnection network = new NetworkConnection(ViewCoordinate.this);
        if (network.CheckInternet()) {
            PasswordChangeAlert alert = new PasswordChangeAlert(ViewCoordinate.this);// new saleslogin().execute();
            alert.showLoginbox();
        } else {
            final AlertDialog alertDialog = new AlertDialog.Builder(ViewCoordinate.this).create();
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.loginalert, null);
            alertDialog.setView(dialogView);
            TextView login =(TextView)dialogView.findViewById(R.id.login);
            login.setText("Please Check your Internet Connection !!");
            Button submit =(Button) dialogView.findViewById(R.id.submit);
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();

                }

            });
            alertDialog.show();
        }
    }

    private void checkInternet() {
        NetworkConnection networkConnection = new NetworkConnection(ViewCoordinate.this);
        if (networkConnection.CheckInternet()) {
            new Viewcordinate().execute();
        } else {
            NetworkConnection network = new NetworkConnection(ViewCoordinate.this);
            if (network.CheckInternet()) {
                PasswordChangeAlert alert = new PasswordChangeAlert(ViewCoordinate.this);// new saleslogin().execute();
                alert.showLoginbox();
            } else {
                final AlertDialog alertDialog = new AlertDialog.Builder(ViewCoordinate.this).create();
                LayoutInflater inflater = getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.loginalert, null);
                alertDialog.setView(dialogView);
                TextView login =(TextView)dialogView.findViewById(R.id.login);
                login.setText("Please Check your Internet Connection !!");
                Button submit =(Button) dialogView.findViewById(R.id.submit);
                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();

                    }

                });
                alertDialog.show();
            }

        }
    }

    private class Viewcordinate extends AsyncTask<String, Void, String> {
        private ProgressDialog progressDialog;
        private Integer id;
        private SQLiteDatabase db;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(ViewCoordinate.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.show();
            id = myshare.getInt("id", 0);


//            descriptiondy = (EditText) newView.findViewById(R.id.descriptiondynamic);
//            quantitydy = (EditText) newView.findViewById(R.id.quantitydynamic);
//            productdy = (MaterialSpinner) newView.findViewById(R.id.productdynname);


        }

        @Override
        protected String doInBackground(String... strings) {
            ServerConnection serverConnection = new ServerConnection();
            try {
                connection = serverConnection.getConnection();
                /*statement = connection.prepareStatement(selectquery);*/
                statement = connection.createStatement();
//                String selectquery = "select distinct * from coordinate where datetime ='" + Date + "' and S_id ='"+empid+"' order by timedate desc";
                String selectquery = "select distinct * from coordinate where datetime ='" + Date + "' and S_id ='"+empid+"'";
                Log.v("query", selectquery);
                resultSet = statement.executeQuery(selectquery);
                while (resultSet.next()) {
                    Timelist.add(resultSet.getString("timedate"));
                    addresslist.add(resultSet.getString("address"));
                    distanceList.add(resultSet.getString("distance"));
                }
                if (!Timelist.isEmpty()) {
                    statement.close();
                    connection.close();
                    resultSet.close();
                    return "success";
                } else {
                    statement.close();
                    connection.close();
                    resultSet.close();
                    return "empty";
                }

            } catch (Exception e) {
                e.printStackTrace();
                return "error_insertion";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();

            if (s.equals("success")) {
                ViewCordinateAdapter viewAttendanceAdapter = new ViewCordinateAdapter(ViewCoordinate.this, Timelist, addresslist,distanceList);
                listView.setAdapter(viewAttendanceAdapter);

            } else if (s.equals("empty")) {
                NetworkConnection network = new NetworkConnection(ViewCoordinate.this);

                    final AlertDialog alertDialog = new AlertDialog.Builder(ViewCoordinate.this).create();
                    LayoutInflater inflater = getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.loginalert, null);
                    alertDialog.setView(dialogView);
                    TextView login =(TextView)dialogView.findViewById(R.id.login);
                    login.setText("No data found");
                    Button submit =(Button) dialogView.findViewById(R.id.submit);
                    submit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();

                        }

                    });
                    alertDialog.show();


            } else {
                    final AlertDialog alertDialog = new AlertDialog.Builder(ViewCoordinate.this).create();
                    LayoutInflater inflater = getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.loginalert, null);
                    alertDialog.setView(dialogView);
                    TextView login =(TextView)dialogView.findViewById(R.id.login);
                    login.setText("No data found");
                    Button submit =(Button) dialogView.findViewById(R.id.submit);
                    submit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();

                        }

                    });
                    alertDialog.show();


            }
        }
    }
}
