package com.brainmagic.pricolsalesorder;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

import SQL_Fields.SQLITE;
import adapter.PreviewAdapter;
import alertbox.Alertbox;
import database.DBHelpersync;

public class SalesViewActivity extends AppCompatActivity {
    private ListView list;
    private Button order;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private ArrayList<String> customerlist, productseglist, productcatagorylist, datelist, quantitylist, packagelsist, ordertypelist,discountlist,unpacklist;
    private ArrayList<Integer> IDList;
    private ProgressDialog progressDialog;
    private SQLiteDatabase db;
    private Alertbox box = new Alertbox(SalesViewActivity.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_view);
        myshare = getSharedPreferences("pricolsalesorder", MODE_PRIVATE);
        editor = myshare.edit();
        list = (ListView) findViewById(R.id.listview);
        order = (Button) findViewById(R.id.order);
        progressDialog = new ProgressDialog(SalesViewActivity.this);
        customerlist = new ArrayList<>();
        productseglist = new ArrayList<>();
        productseglist = new ArrayList<>();
        productcatagorylist = new ArrayList<>();
        datelist = new ArrayList<>();
        quantitylist = new ArrayList<>();
        packagelsist = new ArrayList<>();
        ordertypelist = new ArrayList<>();
        discountlist = new ArrayList<>();
        unpacklist = new ArrayList<>();
        IDList = new ArrayList<>();
        new GetListView().execute();
        order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i =new Intent(SalesViewActivity.this,SelectDistributorActivity.class);
                startActivity(i);
            }
        });
    }

    private class GetListView extends AsyncTask<String, Void, String>

    {
        DBHelpersync dbhelper = new DBHelpersync(SalesViewActivity.this);
        Cursor cursor;

        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Loading...");
            progressDialog.show();
            db = dbhelper.getWritableDatabase();

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            Log.v("OFF Line", "SQLITE Table");
            try {
                //String query ="select * from Orderpreview order by formattedDate asc";
                String query = "select * from " + SQLITE.TABLE_Additems + " order by formattedDate asc";
                Log.v("Dealer Name", query);
                cursor = db.rawQuery(query, null);
                if (cursor.moveToFirst()) {
                    do {
                        customerlist.add(cursor.getString(cursor.getColumnIndex("CustomerName")));
                        productseglist.add(cursor.getString(cursor.getColumnIndex("ProductName")));
                        productcatagorylist.add(cursor.getString(cursor.getColumnIndex("ProductCatagory")));
                        datelist.add(cursor.getString(cursor.getColumnIndex("formattedDate")));
                        quantitylist.add(cursor.getString(cursor.getColumnIndex("quantity")));
                        packagelsist.add(cursor.getString(cursor.getColumnIndex("Package")));
                        ordertypelist.add(cursor.getString(cursor.getColumnIndex("OrderType")));
                        discountlist.add(cursor.getString(cursor.getColumnIndex("discount")));
                        IDList.add(cursor.getInt(cursor.getColumnIndex("id")));
                        unpacklist.add(cursor.getString(cursor.getColumnIndex("Unpackage")));
                    } while (cursor.moveToNext());
                    cursor.close();
                    db.close();
                    return "received";
                } else {
                    cursor.close();
                    db.close();
                    return "nodata";
                }
            } catch (Exception e) {
                cursor.close();
                db.close();
                Log.v("Error in get date", "catch is working");
                Log.v("Error in Incentive", e.getMessage());
                return "notsuccess";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();

            switch (result) {
                case "received":
//                    PreviewAdapter previewAdapter = new PreviewAdapter(SalesViewActivity.this, customerlist, productseglist, productcatagorylist, datelist, quantitylist, packagelsist, ordertypelist,IDList,discountlist,unpacklist);
//                    list.setAdapter(previewAdapter);
                    break;
                case "nodata":
                    box.showAlertbox("No Orders Found");
                    break;
                default:
                    //alert.showAlertbox("Error in Preview");
                    break;
            }

        }
    }
}
