package com.brainmagic.pricolsalesorder;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;



import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Places;
import com.jaredrummler.materialspinner.MaterialSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.net.ssl.HttpsURLConnection;

import Home.homevalues;
import alertbox.Alertbox;
import api.models.master.Customer_list;
import api.retrofit.APIService;
import api.retrofit.RetrofitClient;
import api.visitreport.Visit;
import api.visitreport.VisitData;
import api.visitreport.response.VisitReq;
import network.NetworkConnection;
import persistance.ServerConnection;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import roomdb.database.AppDatabase;

import static roomdb.database.AppDatabase.getAppDatabase;

public class ExsitingvistorActivityNew extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, android.location.LocationListener, com.google.android.gms.location.LocationListener
        ,EasyPermissions.PermissionCallbacks,ResultCallback<LocationSettingsResult> {
    private Spinner customerspinner;
    private EditText remarks, customeredit;
    private TextView mobilenumber;
    private TextView mobilenumbertext, date;
    SharedPreferences myshare;
    SharedPreferences.Editor editor;
    private static final String TAG = "ExsitingvistorActivity";
    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;
    private final static int REQUEST_CHECK_SETTINGS_GPS = 100;
    private final static int REQUEST_ID_MULTIPLE_PERMISSIONS = 0x2;
    private LocationManager locationManager;
    private Location mylocation;
    private ProgressDialog mProgressDialog;
    private boolean loc = false;
    private boolean alreadyCalled = false;
    private Location mLastLocation;
    private Double latitude, longitude;
    String address, address1, City, state, country, postalCode;
    public static final String APIKEY = "AIzaSyBRmYkOOy9QhrI53Fp3h_Tt8t7amWNa4Q0";
    private String formateddate;
    private static final int PERMISSION_REQUEST = 100;
    private List<String> Namelist;
    private AppDatabase db;
    private String customers;
    private List<Customer_list> mobiles;
    private Connection connection;
    private Statement statement;
    private ResultSet resultSet;
    private List<String> cusomerlist, mobilelist;
    private Alertbox box = new Alertbox(ExsitingvistorActivityNew.this);
    private Button add;
    private static EditText nextvisitdate;
    private LinearLayout ntdate;
    private String SalesName,customername;
    private String ordertype,cusomertype,remarkss,mob,accounttype;
    private ImageView back,home,logout;
    private TextView title,customerex,dealerspinner1,gprsaddress;
    private LinearLayout retailerrow,mechanicrow,cusrow,electricianrow;
    private EditText retaileredit,mechanicedit,electricianicedit,mobileedit11,remarksp;
    private TextView typespinner,acctypess;
    private List<String>typecuslist,remarklist,acctypes;
    private String choosen=" ";
    private String customertype;
    private  AppDatabase db1;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exsitingvistornew);
        myshare = getSharedPreferences("pricolsalesorder", MODE_PRIVATE);
        editor = myshare.edit();
        db1 = getAppDatabase(ExsitingvistorActivityNew.this);
        choosen=getIntent().getStringExtra("frmchoosen");
        dealerspinner1 = (TextView) findViewById(R.id.dealerspinner1);
        acctypess = (MaterialSpinner) findViewById(R.id.acctypess);
        mobilenumber = (TextView) findViewById(R.id.mobilenumber);
        gprsaddress = (TextView) findViewById(R.id.placeofvisit);
        remarksp = (EditText) findViewById(R.id.remarksp);
//        customeredit = (EditText) findViewById(R.id.dealereredit);
        nextvisitdate = (EditText) findViewById(R.id.nextvisitdate);
        //mobilenumbertext = (TextView) findViewById(R.id.mobilenumber);
        date = (TextView) findViewById(R.id.visitdate);
        home = (ImageView) findViewById(R.id.home);
        logout = (ImageView) findViewById(R.id.logout);
        back=(ImageView)findViewById(R.id.back) ;
        add = (Button) findViewById(R.id.add);
        title=(TextView)findViewById(R.id.title);
        customerex=(TextView)findViewById(R.id.customerex);

        ntdate=(LinearLayout) findViewById(R.id.ntdate);
        cusrow=(LinearLayout) findViewById(R.id.cusrow);
        mobileedit11=(EditText) findViewById(R.id.mobileedit11);
        typespinner=(TextView) findViewById(R.id.typespinner);
        typespinner.setText(getIntent().getStringExtra("customertype"));
        customertype=getIntent().getStringExtra("customertype");
        mobilenumber.setText(getIntent().getStringExtra("mobile"));
        dealerspinner1.setText(getIntent().getStringExtra("customername"));
//        typecuslist=new ArrayList<>();
//        typecuslist.add(0,"Select Customer Type");
//        typecuslist.add("Retailer");
//        typecuslist.add("Mechanic");
//        typecuslist.add("Electrician");
//        typecuslist.add("End Customer");
//        typecuslist.add("Others");
//        typespinner.setItems(typecuslist);
//        typespinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
//                customertype=item.toString();
//                GetMobileNumber(db1.adhereDao().GetCustomermobileexisting(customertype));
//            }
//        });

//        mobilenumber.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                if(mobilenumber.length() == 10){
//                    if(db1.adhereDao().GetShopname(mobilenumber.getText().toString()).equals("")){
//                        dealerspinner1.setText("Empty");
//                        dealerspinner1.setEnabled(false);
//                    }else {
//                        dealerspinner1.setText(db1.adhereDao().GetShopname(mobilenumber.getText().toString()));
//                        dealerspinner1.setEnabled(false);
//                    }
//
//
//                }else {
//                    dealerspinner1.setEnabled(true);
//
//                }
//
//            }
//        });
//        remark=(MaterialSpinner) findViewById(R.id.remark);

//        remarklist=new ArrayList<>();
//        remarklist.add("Select Remarks");
//        remarklist.add("Stock (0)");
//        remarklist.add("Stock (a)");
//        remarklist.add("Poor Pay");
//        remarklist.add("Product Complint");
//        remarklist.add("Price Defferent");
//        remarklist.add("Next Visit");
//        remarklist.add("Not Intrested");
//        remarksp.setItems(remarklist);

//        if (choosen!=null){
//
//            if (choosen.equals("fromchoosen")){
//                acctypes=new ArrayList<>();
//                acctypes.add("New Customer");
//                acctypess.setItems(acctypes);
//            }
//        }
//
//            else {
//                acctypes=new ArrayList<>();
//                acctypes.add(0,"Select Account Type");
//                acctypes.add("Existing Customer");
//                acctypes.add("New Customer");
//                acctypess.setItems(acctypes);
//            }
//
//            if (choosen!=null){
//                if (choosen.equals("fromchoosen")){
//                    typecuslist=new ArrayList<>();
//                    typecuslist.add(getIntent().getStringExtra("customertype"));
//                    typespinner.setItems(typecuslist);
//                    Namelist=new ArrayList<>();
//                    Namelist.add(getIntent().getStringExtra("customername"));
//                    dealerspinner1.setItems(Namelist);
//                    mobilenumbertext.setText(getIntent().getStringExtra("mobile"));
//                }
//            }
//
//        else {
//            typecuslist=new ArrayList<>();
//            typecuslist.add("Retailer");
//            typecuslist.add("Mechanic");
//            typecuslist.add("Electrician");
//            typecuslist.add("End Customer");
//            typecuslist.add(0,"select Customer Type");
//            typespinner.setItems(typecuslist);
//        }

//
//        remarksp.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
//                remarkss=item.toString();
//                if (remarkss.equals("Next Visit")){
//                    ntdate.setVisibility(View.VISIBLE);
//                }
//            }
//        });

//        nextvisitdate.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                DialogFragment newFragment = new ExsitingvistorActivity.FromDatePickerFragment();
//                newFragment.show(getSupportFragmentManager(),"datePicker");
//            }
//        });
//
//        acctypess.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
//                accounttype=item.toString();
//            }
//        });
//        typespinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
//                cusomertype=item.toString();
//                if (accounttype.equals("New Customer")){
//                    Namelist=db.adhereDao().Getcusname(cusomertype);
//                    Namelist.add(0,"Select Customer");
//                    dealerspinner1.setItems(Namelist);
//                }
//                else {
//                    Namelist = db.adhereDao().Getdealername(cusomertype);
//                    Namelist.add(0,"Select Customer");
//                    dealerspinner1.setItems(Namelist);
//                }
//
//            }
//        });
//        dealerspinner1.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
//                customername=item.toString();
//                if (accounttype.equals("New Customer")){
//                    mob= db.adhereDao().Getmon(customername,cusomertype);
//                    mobilenumbertext.setText(mob);
//                }
//                else {
//                    mob= db.adhereDao().GetCustomermobile(cusomertype,customername);
//                    mobilenumbertext.setText(mob);
//                }
//
//
//            }
//        });

        title.setText("Report");
        SalesName=myshare.getString("name","");
        ordertype=getIntent().getStringExtra("custype");



        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new homevalues(ExsitingvistorActivityNew.this).home_io();
            }
        });

        db = getAppDatabase(ExsitingvistorActivityNew.this);
        cusomerlist = new ArrayList<>();
        mobilelist = new ArrayList<>();
        Namelist = new ArrayList<>();
        Date d1 = new Date();
        Calendar c = Calendar.getInstance();
        c.getTime();
        SimpleDateFormat df1 = new SimpleDateFormat("dd/MM/yyyy");
        formateddate = df1.format(c.getTime());
        date.setText(formateddate);
        getNetworkStatus();

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(typespinner.getText().toString().equals("")) {
                    typespinner.setError("Select Customer Type");
//                    StyleableToast st = new StyleableToast(ExsitingvistorActivityNew.this, "Select Customer Type", Toast.LENGTH_SHORT);
//                    st.setBackgroundColor(ExsitingvistorActivityNew.this.getResources().getColor(R.color.colorPrimary));
//                    st.setTextColor(Color.WHITE);
//                    st.setMaxAlpha();
//                    st.show();


                }else if(mobilenumber.getText().toString().equals("")){
                    mobilenumber.setError("Enter Mobile Number");
//                    StyleableToast st = new StyleableToast(ExsitingvistorActivityNew.this, "Enter Mobile Number", Toast.LENGTH_SHORT);
//                    st.setBackgroundColor(ExsitingvistorActivityNew.this.getResources().getColor(R.color.colorPrimary));
//                    st.setTextColor(Color.WHITE);
//                    st.setMaxAlpha();
//                    st.show();


                }else if(dealerspinner1.getText().toString().equals("")){
                    dealerspinner1.setError("Enter Customer Name");
//                    StyleableToast st = new StyleableToast(ExsitingvistorActivityNew.this, "Enter Customer Name", Toast.LENGTH_SHORT);
//                    st.setBackgroundColor(ExsitingvistorActivityNew.this.getResources().getColor(R.color.colorPrimary));
//                    st.setTextColor(Color.WHITE);
//                    st.setMaxAlpha();
//                    st.show();



                }else if(gprsaddress.getText().toString().equals("")){
                    gprsaddress.setError("Enter Address");
//                    StyleableToast st = new StyleableToast(ExsitingvistorActivityNew.this, "Enter Address", Toast.LENGTH_SHORT);
//                    st.setBackgroundColor(ExsitingvistorActivityNew.this.getResources().getColor(R.color.colorPrimary));
//                    st.setTextColor(Color.WHITE);
//                    st.setMaxAlpha();
//                    st.show();




                }else {
                    checkinternet();
                }


            }
        });

        NetworkConnection network = new NetworkConnection(ExsitingvistorActivityNew.this);
        if (network.CheckInternet()) {
            AskLocationPermission();
//            mProgressDialog= ProgressDialog.show(this,"Location","Getting Your Location",false,false);
        } else {
            Alertbox alert = new Alertbox(ExsitingvistorActivityNew.this);
            alert.showAlertbox("Kindly check your Internet Connection");
        }
//        GetCustomerData(db.adhereDao().GetCustomer_list());
        //new GetCustomer().execute();



//        customerspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//
//                customers = parent.getItemAtPosition(position).toString();
//                if(customers.equals("Select Customer Name")){
//                    Toast.makeText(getApplicationContext(),"Enter  Customer Name",Toast.LENGTH_SHORT).show();
//                }else {
//                    new GetMobile().execute();
//                }
//
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });

    }
//
//    private void GetMobileNumber(List<String> strings) {
//        mobilenumber.setAdapter(new ArrayAdapter<String>(ExsitingvistorActivityNew.this, android.R.layout.simple_dropdown_item_1line,strings));
//        mobilenumber.setThreshold(1);
//
//    }

    public static class FromDatePickerFragment extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            //FromDate.setText(day + "/" + (month + 1) + "/" + year);
            nextvisitdate.setText(day + "-" + (month + 1) + "-" + year);
//            nextvisitdate=year + "-" + (month + 1) + "-" + day;
            nextvisitdate.clearFocus();
        }
    }



    private void checkinternet() {
        NetworkConnection networkConnection=new NetworkConnection(ExsitingvistorActivityNew.this);
        if(networkConnection.CheckInternet()){
            GetVisit();
        }else {
            final AlertDialog alertDialog = new AlertDialog.Builder(ExsitingvistorActivityNew.this).create();
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.loginalert, null);
            alertDialog.setView(dialogView);
            TextView login =(TextView)dialogView.findViewById(R.id.login);
            login.setText("Please Check your Internet Connection !!");
            Button submit =(Button) dialogView.findViewById(R.id.submit);
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();

                }

            });
            alertDialog.show();
        }
    }

    private void GetVisit() {
        try {
            VisitReq visitReq = new VisitReq();
//            if ()
            visitReq.setSalesPersonName(SalesName);
            visitReq.setCustMobileNo(mobilenumber.getText().toString());
            visitReq.setCustomerName(dealerspinner1.getText().toString());
            visitReq.setPlaceOfVisit(gprsaddress.getText().toString());
            visitReq.setVisitedDate(date.getText().toString());
            visitReq.setCustomerType(customertype);
            visitReq.setNextVisitDate(nextvisitdate.getText().toString());
            visitReq.setRemark(remarksp.getText().toString());
            visitReq.setSalesPersonName(SalesName);
//            visitReq.setSalesPersonName(SalesName);
//            visitReq.setCustMobileNo(Long.valueOf(mobilenumbertext.getText().toString()));
//            visitReq.setCustomerName(customername);
//            visitReq.setPlaceOfVisit(gprsaddress.getText().toString());
//            visitReq.setVisitedDate(date.getText().toString());
//            visitReq.setCustomerType(cusomertype);
//            visitReq.setNextVisitDate(nextvisitdate.getText().toString());
//            visitReq.setRemark(remarkss);
//
//            visitReq.setSalesPersonName(SalesName);
//            visitReq.setCustMobileNo(Long.valueOf(mobilenumbertext.getText().toString()));
//            visitReq.setCustomerName(customername);
//            visitReq.setPlaceOfVisit(gprsaddress.getText().toString());
//            visitReq.setVisitedDate(date.getText().toString());
//            visitReq.setCustomerType(cusomertype);
//            visitReq.setNextVisitDate(nextvisitdate.getText().toString());
//            visitReq.setRemark(remarkss);



            final ProgressDialog loading = ProgressDialog.show(ExsitingvistorActivityNew.this, getResources().getString(R.string.app_name), "Loading...", false, false);
            APIService service = RetrofitClient.getApiService();
            Call<VisitData> call = service.visit(visitReq);
            call.enqueue(new Callback<VisitData>() {
                @Override
                public void onResponse(Call<VisitData> call, Response<VisitData> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success")) {

                            SuccessLogin(response.body().getData());

                        } else if(response.body().getResult().equals("InvalidUser")) {
                            final AlertDialog alertDialog = new AlertDialog.Builder(ExsitingvistorActivityNew.this).create();
                            LayoutInflater inflater = getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.loginalert, null);
                            alertDialog.setView(dialogView);
                            TextView login =(TextView)dialogView.findViewById(R.id.login);
                            login.setText("InvalidUser");
                            Button submit =(Button) dialogView.findViewById(R.id.submit);
                            submit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    alertDialog.dismiss();

                                }

                            });
                            alertDialog.show();
                        }else
                        {
                            final AlertDialog alertDialog = new AlertDialog.Builder(ExsitingvistorActivityNew.this).create();
                            LayoutInflater inflater = getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.loginalert, null);
                            alertDialog.setView(dialogView);
                            TextView login =(TextView)dialogView.findViewById(R.id.login);
                            login.setText("Something went wrong .Please try again later .");
                            Button submit =(Button) dialogView.findViewById(R.id.submit);
                            submit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    alertDialog.dismiss();

                                }

                            });
                            alertDialog.show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        final AlertDialog alertDialog = new AlertDialog.Builder(ExsitingvistorActivityNew.this).create();
                        LayoutInflater inflater = getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.loginalert, null);
                        alertDialog.setView(dialogView);
                        TextView login =(TextView)dialogView.findViewById(R.id.login);
                        login.setText("Something went wrong .Please try again later .");
                        Button submit =(Button) dialogView.findViewById(R.id.submit);
                        submit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                alertDialog.dismiss();

                            }

                        });
                        alertDialog.show();

                    }
                }

                @Override
                public void onFailure(Call<VisitData> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    final AlertDialog alertDialog = new AlertDialog.Builder(ExsitingvistorActivityNew.this).create();
                    LayoutInflater inflater = getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.loginalert, null);
                    alertDialog.setView(dialogView);
                    TextView login =(TextView)dialogView.findViewById(R.id.login);
                    login.setText("Something went wrong .Please try again later .");
                    Button submit =(Button) dialogView.findViewById(R.id.submit);
                    submit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();

                        }

                    });
                    alertDialog.show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }



    }

    private void SuccessLogin(Visit data) {
        final AlertDialog alertDialog = new AlertDialog.Builder(ExsitingvistorActivityNew.this).create();
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.loginalert, null);
        alertDialog.setView(dialogView);
        TextView login =(TextView)dialogView.findViewById(R.id.login);
        login.setText("Visit Report Added Successfully");
        Button submit =(Button) dialogView.findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ExsitingvistorActivityNew.this, HomeActivty.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);

            }

        });
        alertDialog.show();
//        final AlertDialog dialog = new AlertDialog.Builder(ExsitingvistorActivityNew.this).create();
//        dialog.setTitle(R.string.app_name);
//        dialog.setMessage("Visit Report Added Successfully");
//        dialog.setButton("Okay", new DialogInterface.OnClickListener() {
//
//            public void onClick(DialogInterface dialog, int which) {
//
//                Intent i = new Intent(ExsitingvistorActivityNew.this, HomeActivty.class);
//                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(i);
//            }
//        });
//        dialog.show();
    }


    private void AskLocationPermission() {
        if (EasyPermissions.hasPermissions(ExsitingvistorActivityNew.this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Have permission, do the thing!
            if (CheckLocationIsEnabled()) {
                // if location is enabled show place picker activity to use
                startLocationUpdates();

            } else {

                LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                        .addLocationRequest(locationRequest);

                PendingResult<LocationSettingsResult> result =
                        LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
                result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                    @SuppressLint("LongLogTag")
                    @Override
                    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
                        final Status status = locationSettingsResult.getStatus();
                        switch (status.getStatusCode()) {
                            case LocationSettingsStatusCodes.SUCCESS:
                                startLocationUpdates();
                                break;
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                try {
                                    status.startResolutionForResult(
                                            ExsitingvistorActivityNew.this,
                                            REQUEST_CHECK_SETTINGS_GPS);
                                } catch (IntentSender.SendIntentException e) {
                                    Log.e(TAG, "Exception : " + e.getMessage());
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                Log.e(TAG, "Location settings are not satisfied.");
                                break;
                        }
                    }
                });


            }
        } else {
            // Request one permission
            EasyPermissions.requestPermissions(this, getString(R.string.rationale_location), REQUEST_ID_MULTIPLE_PERMISSIONS, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION);

        }
    }

    @SuppressLint("LongLogTag")
    protected void startLocationUpdates() {
        Log.d(TAG, "startLocationUpdates: ");
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                    if (googleApiClient.isConnected()) {
                        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
                        mylocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                    }
//                    if (mylocation == null)
                    {
                        mProgressDialog = ProgressDialog.show(this, "Location", "Getting Your Location", false, false);
                        locationManager.requestLocationUpdates(
                                LocationManager.NETWORK_PROVIDER,
                                1000,
                                500, this);
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                1000,
                                500, this);
                    }
//                    else {
//                        mProgressDialog = ProgressDialog.show(this, "Location", "Getting Your Location", false, false);
//                        locationManager.requestLocationUpdates(
//                                LocationManager.NETWORK_PROVIDER,
//                                1000,
//                                500, this);
//                    }
//                CheckInternet();
//                mProgressDialog=ProgressDialog.show(this,"Location","Getting Your Location",false,false);
                    Log.d(TAG, "startLocationUpdates: ");
                } else {
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            REQUEST_ID_MULTIPLE_PERMISSIONS);
                    Log.d(TAG, "Permission Not Granted");
                }

            } else {
                locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                if (googleApiClient.isConnected()) {
                    Log.d(TAG, "startLocationUpdates: else");

                    LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
                    mylocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                }
//                if (mylocation == null)
                {
                    mProgressDialog = ProgressDialog.show(this, "Location", "Getting Your Location", false, false);
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            1000,
                            500, this);
                    locationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            1000,
                            500, this);
                }
//                else {
//                    mProgressDialog = ProgressDialog.show(this, "Location", "Getting Your Location", false, false);
//                    AskLocationPermission();
//                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean CheckLocationIsEnabled() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

        }
        if (googleApiClient != null)
            mylocation = LocationServices.FusedLocationApi
                    .getLastLocation(googleApiClient);

        if (mylocation == null) {
            return false;
        } else {
            return true;
        }
    }

    private void getNetworkStatus() {
        NetworkConnection network = new NetworkConnection(ExsitingvistorActivityNew.this);
        if (network.CheckInternet()) {
            setUpGClient();
            getLastLocation();
//            mProgressDialog= ProgressDialog.show(this,"Location","Getting Your Location",false,false);
        } else {
            Alertbox alert = new Alertbox(ExsitingvistorActivityNew.this);
            alert.showAlertbox("Kindly check your Internet Connection");

        }
    }

    public void getLastLocation() {
        int permissionLocation = ContextCompat.checkSelfPermission(ExsitingvistorActivityNew.this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
//            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
//            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//                    LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
//            mylocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
            locationRequest = new LocationRequest();
            locationRequest.setInterval(100);
            locationRequest.setFastestInterval(100);
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setSmallestDisplacement(10);
            loc = true;
        }
    }

    private synchronized void setUpGClient() {
        try {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .addApi(Places.GEO_DATA_API)
                    .addApi(Places.PLACE_DETECTION_API)
                    .enableAutoManage(this, this).build();
//            googleApiClient.connect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("LongLogTag")
    @Override
    protected void onPause() {
        super.onPause();
        try {
            if (googleApiClient != null)
                if (googleApiClient.isConnected()) {
                    googleApiClient.disconnect();
                }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("LongLogTag")
    @Override
    protected void onResume() {
        super.onResume();
        if (googleApiClient != null)
            if (!alreadyCalled && googleApiClient.isConnected()) {
                getNetworkStatus();
            }
        alreadyCalled = false;

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS_GPS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        startLocationUpdates();
                        break;
                    case Activity.RESULT_CANCELED:
                        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(ExsitingvistorActivityNew.this);
                        alertDialog.setMessage("If you don't enable GPS, your travel cannot be tracked. Do you want to close the app?");
                        alertDialog.setTitle("Pricol Sales Order");
                        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                finish();
                            }
                        });

                        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                AskLocationPermission();
                            }
                        });

                        alertDialog.show();
                        break;
                    default:
                        finish();
                        break;
                }
                break;
        }
    }

    @SuppressLint("LongLogTag")
    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop: ");
        if (googleApiClient != null)
            if (googleApiClient.isConnected()) {
                googleApiClient.disconnect();
            }
    }

    @SuppressLint("LongLogTag")
    private void CheckInternet() {

        Log.d(TAG, "CheckInternet: lat and long");
        NetworkConnection network = new NetworkConnection(ExsitingvistorActivityNew.this);
        if (network.CheckInternet()) {
            // relativeLayout.setVisibility(View.VISIBLE);
            Log.d(TAG, "CheckInternet: RelativeLayout");
            ShowCurrentLocationMarker();
        } else {
            Alertbox alert = new Alertbox(ExsitingvistorActivityNew.this);
            alert.showAlertbox("Kindly check your Internet Connection");
            //  retry.setVisibility(View.VISIBLE);
            //relativeLayout.setVisibility(View.GONE);
        }
    }

    @SuppressLint("LongLogTag")
    private void ShowCurrentLocationMarker() {
        if (mLastLocation != null) {
            Log.d(TAG, "ShowCurrentLocationMarker: latitude " + mLastLocation.getLatitude() + " logitude " + mLastLocation.getLongitude());

            new GeocodeAsyncTask().execute(mLastLocation.getLatitude(), mLastLocation.getLongitude());     // Sets the center of the map to location user
        }
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onLocationChanged(Location location) {
        mProgressDialog.dismiss();
        // check_button.setVisibility(View.VISIBLE);
        if (location != null) {
            mylocation = location;
            latitude = mylocation.getLatitude();
            longitude = mylocation.getLongitude();
            editor.putString("latitude", latitude.toString());
            editor.putString("longitude", longitude.toString());
            Log.d(TAG, "onLocationChanged: " + mylocation.getLatitude() + " long " + mylocation.getLongitude());
            editor.commit();
            new GeocodeAsyncTask().execute(mylocation.getLatitude(), mylocation.getLongitude());
            //Or Do whatever you want with your location
        } else if (mylocation != null) {
            latitude = mylocation.getLatitude();
            longitude = mylocation.getLongitude();
            editor.putString("latitude", latitude.toString());
            editor.putString("longitude", longitude.toString());
            Log.d(TAG, "onLocationChanged: " + mylocation.getLatitude() + " long " + mylocation.getLongitude());
            editor.commit();
            new GeocodeAsyncTask().execute(mylocation.getLatitude(), mylocation.getLongitude());
        }

    }


    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {

    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

    }

    private class GeocodeAsyncTask extends AsyncTask<Double, Void, Address> {

        String errorMessage = "";


        @SuppressLint("LongLogTag")
        @Override
        protected Address doInBackground(Double... latlang) {
            Geocoder geocoder = new Geocoder(ExsitingvistorActivityNew.this, Locale.getDefault());
            List<Address> addresses = null;
            if (geocoder.isPresent()) {
                try {
                    addresses = geocoder.getFromLocation(latlang[0], latlang[1], 1);
                    Log.d(TAG, "doInBackground: ************");
                } catch (IOException ioException) {
                    errorMessage = "Service Not Available";
                    Log.e(TAG, errorMessage, ioException);
                } catch (IllegalArgumentException illegalArgumentException) {
                    errorMessage = "Invalid Latitude or Longitude Used";
                    Log.e(TAG, errorMessage + ". " +
                            "Latitude = " + latlang[0] + ", Longitude = " +
                            latlang[1], illegalArgumentException);
                }

                if (addresses != null && addresses.size() > 0)
                    return addresses.get(0);
            } else {
                new GetGeoCodeAPIAsynchTask().execute(latlang[0], latlang[1]);
            }

            return null;
        }

        @SuppressLint("LongLogTag")
        protected void onPostExecute(Address addresss) {

            if (addresss == null) {
                new GetGeoCodeAPIAsynchTask().execute(mylocation.getLatitude(), mylocation.getLongitude());
                Log.d(TAG, "onPostExecute: *****");
            } else {
//                progressBar.setVisibility(View.GONE);
                address = addresss.getAddressLine(0);
                editor.putString("ToAddress", address);
                editor.commit();
                editor.apply();
                City = addresss.getLocality();
                Log.d(TAG, "onPostExecute: **************************" + City);
                String city = addresss.getLocality();
                String state = addresss.getAdminArea();
                //create your custom title
                String title = city + "-" + state;
                gprsaddress.setText(address +
                        "\n"
                        + title);
                editor.putString("FromAddress", address);
                Geocoder geocoder = new Geocoder(ExsitingvistorActivityNew.this);
                try {
                    ArrayList<Address> addresses = (ArrayList<Address>) geocoder.getFromLocationName("karur", 50);
                    for (Address address3 : addresses) {
                        double lat = address3.getLatitude();
                        double lon = address3.getLongitude();
//                        address2.setText(lat +
//                                "\n"
//                                + lon);

                    }
                } catch (IOException e) {
                    e.printStackTrace();

                }
                //create your custom title
//                String title = city + "-" + state;
//                Alertbox alertbox=new Alertbox(MainActivity.this);
//                alertbox.showAlertboxwithback("Your Current location is "+city);


            }
        }
    }

    private class GetGeoCodeAPIAsynchTask extends AsyncTask<Double, Void, String[]> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String[] doInBackground(Double... latlang) {
            String response;
            try {
                String URL = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + latlang[0] + "," + latlang[1] + "&key=" + APIKEY;
                Log.v("URL", URL);
                response = getLatLongByURL(URL);
                return new String[]{response};
            } catch (Exception e) {
                return new String[]{"error"};
            }
        }

        @SuppressLint("LongLogTag")
        @Override
        protected void onPostExecute(String... result) {
            try {
                JSONObject jsonObject = new JSONObject(result[0]);

                address = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONArray("address_components").getJSONObject(0).getString("long_name");

                editor.putString("FromAddress", address);
                editor.putString("ToAddress", address);
                editor.commit();
                editor.apply();

                City = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONArray("address_components").getJSONObject(2).getString("long_name");

                String state = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONArray("address_components").getJSONObject(4).getString("long_name");

                Log.d(TAG, "onPostExecute: " + City);

//                String title = city + "-" + state;

//                Alertbox alertbox=new Alertbox(MainActivity.this);
//                alertbox.showAlertbox("Your Current location is "+city);

//                if (fetchType == FROMADDRESS) {
//
//                    mPrimaryAddress.setText(address);
//                    mSecondaryAddress.setText(title);
//                    mDropText.requestFocus();
//                    fromaddress = address;
//                    mPickupText.setText(address);
//
//                    editor.putString("FromAddress", fromaddress);
//                    editor.apply();
//                    editor.commit();
//
//                    Log.i("FromAddress1", address);
//                    Log.i("FromAddress2", title);
//                } else {
//                    mPrimaryAddress.setText(address);
//                    mSecondaryAddress.setText(title);
//                    mDropText.setText(address);
//                    toaddress = address;
//                    editor.putString("ToAddress", toaddress).commit();
//
//                    Log.i("ToAddress1", address);
//                    Log.i("ToAddress2", title);
//
//                }


                Log.d("Address", "" + address);
            } catch (JSONException e) {
                e.printStackTrace();
            }
//            progressBar.setVisibility(View.GONE);

        }
    }

    public String getLatLongByURL(String requestURL) {
        URL url;
        String response = "";
        try {
            url = new URL(requestURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");
            conn.setDoOutput(true);
            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = "";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }


    private class GetCustomer extends AsyncTask<String, Void, String> {
        private ProgressDialog progressDialog;
        private Integer id;
        private SQLiteDatabase db;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(ExsitingvistorActivityNew.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.show();



//            descriptiondy = (EditText) newView.findViewById(R.id.descriptiondynamic);
//            quantitydy = (EditText) newView.findViewById(R.id.quantitydynamic);
//            productdy = (MaterialSpinner) newView.findViewById(R.id.productdynname);


        }

        @Override
        protected String doInBackground(String... strings) {
            ServerConnection serverConnection = new ServerConnection();
            try {
                connection = serverConnection.getConnection();
                /*statement = connection.prepareStatement(selectquery);*/
                statement = connection.createStatement();
                String selectquery = "SELECT * FROM Customer";
                Log.v("query", selectquery);
                resultSet = statement.executeQuery(selectquery);
                while (resultSet.next()) {
                    cusomerlist.add(resultSet.getString("Name"));


                }
                if (!cusomerlist.isEmpty()) {
                    statement.close();
                    connection.close();
                    resultSet.close();
                    return "success";
                } else {
                    statement.close();
                    connection.close();
                    resultSet.close();
                    return "empty";
                }

            } catch (Exception e) {
                e.printStackTrace();
                return "error_insertion";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();

            if (s.equals("success")) {
                cusomerlist.add(0, "Select Customer Name");
                ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(ExsitingvistorActivityNew.this, R.layout.simple_spinner_item, cusomerlist);
                spinnerAdapter.setNotifyOnChange(true);
                spinnerAdapter.setDropDownViewResource(R.layout.simple_spinner_item);
                customerspinner.setAdapter(spinnerAdapter);

            } else if (s.equals("empty")) {
                box.showAlertboxwithback("No data found!");
            } else {
                box.showAlertboxwithback("Kindly check your Internet Connection");
            }
        }
    }

    private class GetMobile extends AsyncTask<String, Void, String> {
        private ProgressDialog progressDialog;
        private Integer id;
        private SQLiteDatabase db;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(ExsitingvistorActivityNew.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.show();



//            descriptiondy = (EditText) newView.findViewById(R.id.descriptiondynamic);
//            quantitydy = (EditText) newView.findViewById(R.id.quantitydynamic);
//            productdy = (MaterialSpinner) newView.findViewById(R.id.productdynname);


        }

        @Override
        protected String doInBackground(String... strings) {
            ServerConnection serverConnection = new ServerConnection();
            try {
                connection = serverConnection.getConnection();
                /*statement = connection.prepareStatement(selectquery);*/
                statement = connection.createStatement();
                String selectquery = "select * from Customer where Name ='" + customers + "'";
                Log.v("query", selectquery);
                resultSet = statement.executeQuery(selectquery);
                while (resultSet.next()) {
                    mobilelist.add(resultSet.getString("Mobile"));
                }
                if (!cusomerlist.isEmpty()) {
                    statement.close();
                    connection.close();
                    resultSet.close();
                    return "success";
                } else {
                    statement.close();
                    connection.close();
                    resultSet.close();
                    return "success";
                }

            } catch (Exception e) {
                e.printStackTrace();
                return "error_insertion";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();

            if (s.equals("success")) {
                mobilenumbertext.setText(mobilelist.get(0));

            } else if (s.equals("empty")) {
                box.showAlertboxwithback("No data found!");
            } else {
//                box.showAlertboxwithback("Kindly check your Internet Connection");
            }
        }
    }
}


