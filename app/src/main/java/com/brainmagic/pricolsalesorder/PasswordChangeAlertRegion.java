package com.brainmagic.pricolsalesorder;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import alertbox.Alertbox;
import alertbox.PasswordChangeAlert;
import network.NetworkConnection;
import persistance.ServerConnection;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by SYSTEM10 on 5/20/2019.
 */

public class PasswordChangeAlertRegion {
    Context context;
    Connection connection;
    Statement statement;
    ResultSet resultSet;
    private ProgressDialog progress;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private String SalesPass,Old,New,Confirm;
    private int SalesId;

    //editor = myshare.edit();

    public PasswordChangeAlertRegion(Context con) {
        // TODO Auto-generated constructor stub
        this.context = con;
    }

//	editor = myshare.edit();

    public void showLoginbox()
    {

        myshare = context.getSharedPreferences("pricolsalesorder", MODE_PRIVATE);
        editor = myshare.edit();
        final AlertDialog alertDialog = new AlertDialog.Builder(
                context).create();
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.changepasswordalert, null);
        alertDialog.setView(dialogView);
        //TextView log = (TextView) dialogView.findViewById(R.id.loglabel);

        final EditText Old_password=(EditText)dialogView.findViewById(R.id.old_password);
        final EditText New_password=(EditText)dialogView.findViewById(R.id.new_password);
        final EditText Confirm_password=(EditText)dialogView.findViewById(R.id.confirm_password);
        Button OK=(Button) dialogView.findViewById(R.id.okay2);
        Button Cancel=(Button) dialogView.findViewById(R.id.cancel);
        Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });



        OK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Old = Old_password.getText().toString();
                New = New_password.getText().toString();
                Confirm = Confirm_password.getText().toString();
                SalesPass =  myshare.getString("password", "");
                SalesId =  myshare.getInt("idlistreg", 0);


                if(Old.equals("")){
                    Old_password.setError("Enter Current Password");
                } else if(New.equals("")){
                    New_password.setError("Enter New Password");
                } else if (Confirm.equals("")){
                    Confirm_password.setError("Enter Confirm Password");
                } else if(!SalesPass.equalsIgnoreCase(Old)) {
                    final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(context).create();
                    LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.loginalert, null);
                    alertDialog.setView(dialogView);
                    TextView login =(TextView)dialogView.findViewById(R.id.login);
                    login.setText("Current Password is incorrect!");
                    Button submit =(Button) dialogView.findViewById(R.id.submit);
                    submit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();

                        }

                    });
                    alertDialog.show();
                   // Toast.makeText(context,"Current Password is incorrect!", Toast.LENGTH_SHORT).show();
                } else if(!New.equalsIgnoreCase(Confirm)) {
                    final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(context).create();
                    LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.loginalert, null);
                    alertDialog.setView(dialogView);
                    TextView login =(TextView)dialogView.findViewById(R.id.login);
                    login.setText("New password & Confirm password doesn't match !");
                    Button submit =(Button) dialogView.findViewById(R.id.submit);
                    submit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();

                        }

                    });
                    alertDialog.show();

                   // Toast.makeText(context,"New password & Confirm password doesn't match !", Toast.LENGTH_SHORT).show();
                }
                else {
                    CheckInternet();
                    alertDialog.dismiss();
                }

            }
        });

        alertDialog.show();
    }


    private void CheckInternet() {
        try {
            NetworkConnection network = new NetworkConnection(context);
            if (network.CheckInternet()) {
                new changepassword().execute();
                //askForPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, WRITE_EXST);
            } else {
                final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(context).create();
                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.loginalert, null);
                alertDialog.setView(dialogView);
                TextView login =(TextView)dialogView.findViewById(R.id.login);
                login.setText("Please  check your Internet Connection !");
                Button submit =(Button) dialogView.findViewById(R.id.submit);
                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();

                    }

                });
                alertDialog.show();
//                Alertbox alert = new Alertbox(context);
//                alert.showAlertbox("Kindly check your Internet Connection");
            }

        }catch (Exception e) {
            e.printStackTrace();

        }
    }

    private class changepassword extends AsyncTask<String, Void, String> {
        int i;
        protected void onPreExecute() {
            super.onPreExecute();
            progress = new ProgressDialog(context);
            progress.setMessage("Loading...");
            progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progress.setCancelable(false);
            progress.show();
        }

        @Override
        protected String doInBackground(String... strings) {

            ServerConnection serverConnection = new ServerConnection();
            try {
                connection = serverConnection.getConnection();
                statement = connection.createStatement();
                String selectquery = "UPDATE Region_Manager SET password = '"+New+"' WHERE R_Id = '"+SalesId+"'";
                Log.v("query", selectquery);
                i=statement.executeUpdate(selectquery);
                if(i==1) {

                    connection.close();
                    statement.close();
                    return "updated";}
                else {
                    connection.close();
                    statement.close();
                    return "notupdated";
                }
            } catch (Exception e) {
                e.printStackTrace();
                return "error_insertion";
            }

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s.equals("updated")) {
                editor.putString("pass", New);
                editor.commit();
                final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(context).create();
                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.loginalert, null);
                alertDialog.setView(dialogView);
                TextView login =(TextView)dialogView.findViewById(R.id.login);
                login.setText("Password changed successfully !");
                Button submit =(Button) dialogView.findViewById(R.id.submit);
                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();

                    }

                });
                alertDialog.show();
//                Alertbox alert = new Alertbox(context);
//                alert.showAlertbox("Password changed successfully");
            } else {
                final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(context).create();
                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.loginalert, null);
                alertDialog.setView(dialogView);
                TextView login =(TextView)dialogView.findViewById(R.id.login);
                login.setText("Error occurred try again !");
                Button submit =(Button) dialogView.findViewById(R.id.submit);
                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();

                    }

                });
                alertDialog.show();
//                Alertbox alert = new Alertbox(context);
//                alert.showAlertbox("Error occurred try again");
            }
        }
    }
}
