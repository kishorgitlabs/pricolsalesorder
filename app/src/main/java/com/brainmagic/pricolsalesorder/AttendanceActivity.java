package com.brainmagic.pricolsalesorder;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.StrictMode;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Places;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.net.ssl.HttpsURLConnection;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import Home.homevalues;
import alertbox.Alertbox;
import api.models.attendance.AttendanceData;
import api.retrofit.APIService;
import api.retrofit.RetrofitClient;
import network.NetworkConnection;
import persistance.ServerConnection;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import service.BGServicenormal;

import static service.BGServicenormal.ACTION_START_FOREGROUND_SERVICE;
import static service.BGServicenormal.ACTION_STOP_FOREGROUND_SERVICE;

public class AttendanceActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, android.location.LocationListener, com.google.android.gms.location.LocationListener
        ,EasyPermissions.PermissionCallbacks,ResultCallback<LocationSettingsResult>
{
    private ImageView menu, logout, back, arrow, home;
    private TextView tittle, ename, designation, gprsaddress;
    public static final String ACTION_START_FOREGROUND_SERVICE = "ACTION_START_FOREGROUND_SERVICE";
    public static final String ACTION_STOP_FOREGROUND_SERVICE = "ACTION_STOP_FOREGROUND_SERVICE";
    private Button attendancebtn;
    SharedPreferences myshare;
    SharedPreferences.Editor editor;
    private String name, designationstring, gprsaddressstring;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;
    private boolean loc = false;
    private static final String TAG = "AttendanceActivity";
    private Location mylocation;
    private final static int REQUEST_CHECK_SETTINGS_GPS = 100;
    private final static int REQUEST_ID_MULTIPLE_PERMISSIONS = 0x2;
    private LocationManager locationManager;
    private ProgressDialog mProgressDialog;
    private boolean alreadyCalled = false;
    private Alertbox box = new Alertbox(AttendanceActivity.this);
    private Location mLastLocation;
    String address, address1, City, state, country, postalCode;
    private Double latitude, longitude;
    public static final String APIKEY = "AIzaSyBRmYkOOy9QhrI53Fp3h_Tt8t7amWNa4Q0";
    private boolean isCheckedIn = false;
    //    private ProgressDialog loading;
    private boolean istiming = false;
    private boolean servuce = false;
    private boolean islinit = false;
    private String currentdate, currentdating, formatdating, formatdate1;
    private String currentDate, formattedtime, formatedtime2,formattedDate,formattedtime1;
    private TextView day;
    private String code;
    private boolean distancelikes = false;
    private TextView disti;
    private Integer trackId;
    private int empid;
    private String regid;
    private String lat, lon;
    Float distance;
    String distancedouble;
    Float distancestring;
    private Connection connection;
    private Statement statement;
    private ResultSet resultSet;
    double valueRounded;

    // boolean flag to toggle periodic location updates
    private boolean mRequestingLocationUpdates = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance);
        myshare = getSharedPreferences("pricolsalesorder", MODE_PRIVATE);
        editor = myshare.edit();
        menu = (ImageView) findViewById(R.id.menu);
        logout = (ImageView) findViewById(R.id.logout);
        home = (ImageView) findViewById(R.id.home);
        arrow = (ImageView) findViewById(R.id.arrow);
       // home1 = (ImageView) findViewById(R.id.home1);
        ename = (TextView) findViewById(R.id.ename);
        designation = (TextView) findViewById(R.id.designation);
        gprsaddress = (TextView) findViewById(R.id.address);
        disti = (TextView) findViewById(R.id.disti);
        attendancebtn = (Button) findViewById(R.id.attendancebtn);
        tittle = (TextView) findViewById(R.id.title);
        day = (TextView) findViewById(R.id.day);
        tittle.setText("Attendance");
        distancelikes = myshare.getBoolean("distancelikes", distancelikes);
        ename.setText(myshare.getString("name", ""));
        designation.setText(myshare.getString("usertype", ""));
        isCheckedIn = myshare.getBoolean("isCheckedIn", isCheckedIn);
        istiming = myshare.getBoolean("istiming", istiming);
        servuce = myshare.getBoolean("service", servuce);
        islinit = myshare.getBoolean("islinit", islinit);
        code = myshare.getString("empcode", "");
        trackId = myshare.getInt("trackid", 0);
        regid = myshare.getString("regid", "");
        //date
        Calendar c1 = Calendar.getInstance();
        SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
        formattedDate = df1.format(c1.getTime());

        //Time
        Calendar cal1 = Calendar.getInstance();
        Date currentTime1 = cal1.getTime();
        SimpleDateFormat dftime1 = new SimpleDateFormat("HH:mm:ss");
        formattedtime = dftime1.format(currentTime1);
        //date
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        currentdating = df.format(c.getTime());
        formatdating = myshare.getString("formateddate", "");
        formatdate1 = formatdating;
        Calendar cal = Calendar.getInstance();
        Date currentTime = cal.getTime();
        SimpleDateFormat dftime = new SimpleDateFormat("HH:mm:ss");
        formattedtime = dftime.format(currentTime);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new homevalues(AttendanceActivity.this).home_io();
            }
        });

        if (formatdate1.equals(currentdating)) {
            if (istiming == false) {
                attendancebtn.setText("In Time");
            } else if (servuce == true) {
                attendancebtn.setText("In Time");
            } else {
                attendancebtn.setText("Out Time");

            }
        } else {
            attendancebtn.setText("In Time");
            isCheckedIn = false;
            editor.putBoolean("isCheckedIn", isCheckedIn);
            editor.commit();
        }
//        back.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                onBackPressed();
//            }
//        });
//        logout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                new logout(AttendanceActivity.this).log_out();
//            }
//        });
//        home1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                new homevalues(AttendanceActivity.this).home_io();
//            }
//        });
        arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(AttendanceActivity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu pop) {
                        pop.dismiss();
                    }

                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {

                            case R.id.view_Attendance:
                                Intent i2 = new Intent(getApplicationContext(), ViewAttendance.class);
                                startActivity(i2);
                                return true;
                            case R.id.Travelhistory:
                                Intent i5 = new Intent(getApplicationContext(), TravelHistoryActivity.class);
                                startActivity(i5);
                                return true;
                        }
                        return false;
                    }
                });
                popupMenu.inflate(R.menu.attendancemenu);
                popupMenu.show();
            }
        });
        attendancebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (gprsaddress.getText().toString().equals("")) {
                    final AlertDialog alertDialog = new AlertDialog.Builder(AttendanceActivity.this).create();
                    LayoutInflater inflater = getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.loginalertvaldiate, null);
                    alertDialog.setView(dialogView);
                    TextView login =(TextView)dialogView.findViewById(R.id.login);
                    login.setText("Not Address Found");
                    Button submit =(Button) dialogView.findViewById(R.id.submit);
                    submit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();


                        }

                    });
                    alertDialog.show();
                    /*StyleableToast st = new StyleableToast(AttendanceActivity.this, "Get The Address", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(AttendanceActivity.this.getResources().getColor(R.color.colorPrimary));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();*/
                    showAlertBox();
                } else {
                    checkinternet();
                }
            }
        });
        getNetworkStatus();
        NetworkConnection network = new NetworkConnection(AttendanceActivity.this);
        if (network.CheckInternet()) {
            AskLocationPermission();
//            mProgressDialog= ProgressDialog.show(this,"Location","Getting Your Location",false,false);
        } else {
            final AlertDialog alertDialog = new AlertDialog.Builder(AttendanceActivity.this).create();
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.loginalert, null);
            alertDialog.setView(dialogView);
            TextView login =(TextView)dialogView.findViewById(R.id.login);
            login.setText("Please Check your Internet Connection !!");
            Button submit =(Button) dialogView.findViewById(R.id.submit);
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();

                }

            });
            alertDialog.show();
        }
    }

    private void showAlertBox()
    {
        new AlertDialog.Builder(AttendanceActivity.this)
                .setTitle("Pricol Sales Executive Managment Application")
                .setMessage("GPS Failed to Fetch Your Location. Do you want to restart the process")

                // Specifying a listener allows you to take an action before dismissing the dialog.
                // The dialog is automatically dismissed when a dialog button is clicked.
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Continue with delete operation
                        NetworkConnection network = new NetworkConnection(AttendanceActivity.this);
                        if (network.CheckInternet()) {
                            AskLocationPermission();
//            mProgressDialog= ProgressDialog.show(this,"Location","Getting Your Location",false,false);
                        } else {
                            Alertbox alert = new Alertbox(AttendanceActivity.this);
                            alert.showAlertbox("Kindly check your Internet Connection");
                        }
                    }
                })

                // A null listener allows the button to dismiss the dialog and take no further action.
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .show();
    }

    private void checkinternet() {
        NetworkConnection networkConnection = new NetworkConnection(AttendanceActivity.this);
        if (networkConnection.CheckInternet()) {
            markAttendance();
        } else {
            final AlertDialog alertDialog = new AlertDialog.Builder(AttendanceActivity.this).create();
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.loginalert, null);
            alertDialog.setView(dialogView);
            TextView login =(TextView)dialogView.findViewById(R.id.login);
            login.setText("Please Check your Internet Connection !!");
            Button submit =(Button) dialogView.findViewById(R.id.submit);
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();

                }

            });
            alertDialog.show();
        }
    }

    private void markAttendance() {
        attendancebtn.setVisibility(View.GONE);
        if (isCheckedIn) {
//            new startAttendance().execute("stop", "");
            attendance("stop");
        } else {
//            new startAttendance().execute("start", "");
            attendance("start");
        }

        attendancebtn.setVisibility(View.VISIBLE);
    }

    private void AskLocationPermission() {
        if (EasyPermissions.hasPermissions(AttendanceActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Have permission, do the thing!
            if (CheckLocationIsEnabled()) {
                // if location is enabled show place picker activity to use
                startLocationUpdates();

            } else {

                LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                        .addLocationRequest(locationRequest);

                PendingResult<LocationSettingsResult> result =
                        LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
                result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                    @SuppressLint("LongLogTag")
                    @Override
                    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
                        final Status status = locationSettingsResult.getStatus();
                        switch (status.getStatusCode()) {
                            case LocationSettingsStatusCodes.SUCCESS:
                                startLocationUpdates();
                                break;
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                try {
                                    status.startResolutionForResult(
                                            AttendanceActivity.this,
                                            REQUEST_CHECK_SETTINGS_GPS);
                                } catch (IntentSender.SendIntentException e) {
                                    Log.e(TAG, "Exception : " + e.getMessage());
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                Log.e(TAG, "Location settings are not satisfied.");
                                break;
                        }
                    }
                });


            }
        } else {
            // Request one permission
            EasyPermissions.requestPermissions(this, getString(R.string.rationale_location), REQUEST_ID_MULTIPLE_PERMISSIONS, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION);

        }
    }

    @SuppressLint("LongLogTag")
    protected void startLocationUpdates() {
        Log.d(TAG, "startLocationUpdates: ");
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                    if (googleApiClient.isConnected()) {
                        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
                        mylocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                    }
//                    if (mylocation == null)
                    {
                        mProgressDialog = ProgressDialog.show(this, "Location", "Getting Your Location", false, false);
                        locationManager.requestLocationUpdates(
                                LocationManager.NETWORK_PROVIDER,
                                1000,
                                500, this);
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                1000,
                                500, this);
                    }
//                    else {
//                        mProgressDialog = ProgressDialog.show(this, "Location", "Getting Your Location", false, false);
//                        locationManager.requestLocationUpdates(
//                                LocationManager.NETWORK_PROVIDER,
//                                1000,
//                                500, this);
//                    }
//                CheckInternet();
//                mProgressDialog=ProgressDialog.show(this,"Location","Getting Your Location",false,false);
                    Log.d(TAG, "startLocationUpdates: ");
                } else {
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            REQUEST_ID_MULTIPLE_PERMISSIONS);
                    Log.d(TAG, "Permission Not Granted");
                }

            } else {
                locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                if (googleApiClient.isConnected()) {
                    Log.d(TAG, "startLocationUpdates: else");

                    LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
                    mylocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                }
//                if (mylocation == null)
                {
                    mProgressDialog = ProgressDialog.show(this, "Location", "Getting Your Location", false, false);
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            1000,
                            500, this);
                    locationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            1000,
                            500, this);
                }
//                else {
//                    mProgressDialog = ProgressDialog.show(this, "Location", "Getting Your Location", false, false);
//                    AskLocationPermission();
//                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private boolean CheckLocationIsEnabled() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

        }
        if (googleApiClient != null)
            mylocation = LocationServices.FusedLocationApi
                    .getLastLocation(googleApiClient);

        if (mylocation == null) {
            return false;
        } else {
            return true;
        }
    }

    private void getNetworkStatus() {
        NetworkConnection network = new NetworkConnection(AttendanceActivity.this);
        if (network.CheckInternet()) {
            setUpGClient();
            getLastLocation();
//            mProgressDialog= ProgressDialog.show(this,"Location","Getting Your Location",false,false);
        } else {
            final AlertDialog alertDialog = new AlertDialog.Builder(AttendanceActivity.this).create();
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.loginalert, null);
            alertDialog.setView(dialogView);
            TextView login =(TextView)dialogView.findViewById(R.id.login);
            login.setText("Please Check your Internet Connection !!");
            Button submit =(Button) dialogView.findViewById(R.id.submit);
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();

                }

            });
            alertDialog.show();

        }
    }


    @SuppressLint("LongLogTag")
    @Override
    protected void onPause() {
        super.onPause();
        try {
            if (googleApiClient != null)
                if (googleApiClient.isConnected()) {
                    googleApiClient.disconnect();
                }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("LongLogTag")
    @Override
    protected void onResume() {
        super.onResume();
        if (googleApiClient != null)
            if (!alreadyCalled && googleApiClient.isConnected()) {
                getNetworkStatus();
                startLocationUpdates();
            }
        alreadyCalled = false;

    }

    public void getLastLocation() {
        int permissionLocation = ContextCompat.checkSelfPermission(AttendanceActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
//            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
//            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//                    LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
//            mylocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
            locationRequest = new LocationRequest();
            locationRequest.setInterval(100);
            locationRequest.setFastestInterval(100);
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setSmallestDisplacement(10);
            loc = true;
        }
    }

    private synchronized void setUpGClient() {
        try {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .addApi(Places.GEO_DATA_API)
                    .addApi(Places.PLACE_DETECTION_API)
                    .enableAutoManage(this, this).build();
//            googleApiClient.connect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS_GPS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        startLocationUpdates();
                        break;
                    case Activity.RESULT_CANCELED:
                        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(AttendanceActivity.this);
                        alertDialog.setMessage("If you don't enable GPS, your travel cannot be tracked. Do you want to close the app?");
                        alertDialog.setTitle("Pricol Sales Order");
                        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                finish();
                            }
                        });

                        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                AskLocationPermission();
                            }
                        });

                        alertDialog.show();
                        break;
                    default:
                        finish();
                        break;
                }
                break;
        }
    }

    @SuppressLint("LongLogTag")
    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop: ");
        if (googleApiClient != null)
            if (googleApiClient.isConnected()) {
                googleApiClient.disconnect();
            }
    }

    @SuppressLint("LongLogTag")
    private void CheckInternet() {

        Log.d(TAG, "CheckInternet: lat and long");
        NetworkConnection network = new NetworkConnection(AttendanceActivity.this);
        if (network.CheckInternet()) {
            // relativeLayout.setVisibility(View.VISIBLE);
            Log.d(TAG, "CheckInternet: RelativeLayout");
            ShowCurrentLocationMarker();
        } else {
            final AlertDialog alertDialog = new AlertDialog.Builder(AttendanceActivity.this).create();
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.loginalert, null);
            alertDialog.setView(dialogView);
            TextView login =(TextView)dialogView.findViewById(R.id.login);
            login.setText("Please Check your Internet Connection !!");
            Button submit =(Button) dialogView.findViewById(R.id.submit);
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();

                }

            });
            alertDialog.show();
            //  retry.setVisibility(View.VISIBLE);
            //relativeLayout.setVisibility(View.GONE);
        }
    }

    @SuppressLint("LongLogTag")
    private void ShowCurrentLocationMarker() {
        if (mLastLocation != null) {
            Log.d(TAG, "ShowCurrentLocationMarker: latitude " + mLastLocation.getLatitude() + " logitude " + mLastLocation.getLongitude());

            new GeocodeAsyncTask().execute(mLastLocation.getLatitude(), mLastLocation.getLongitude());     // Sets the center of the map to location user
        }
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onLocationChanged(Location location) {
        if(mProgressDialog!=null)
            if(mProgressDialog.isShowing())
                mProgressDialog.dismiss();
        // check_button.setVisibility(View.VISIBLE);
        if (location != null) {
            mylocation = location;
            latitude = mylocation.getLatitude();
            longitude = mylocation.getLongitude();
            editor.putString("latitude", latitude.toString());
            editor.putString("longitude", longitude.toString());
            Log.d(TAG, "onLocationChanged: " + mylocation.getLatitude() + " long " + mylocation.getLongitude());
            editor.commit();
            new GeocodeAsyncTask().execute(mylocation.getLatitude(), mylocation.getLongitude());
            //Or Do whatever you want with your location
        } else if (mylocation != null) {
            latitude = mylocation.getLatitude();
            longitude = mylocation.getLongitude();
            editor.putString("latitude", latitude.toString());
            editor.putString("longitude", longitude.toString());
            Log.d(TAG, "onLocationChanged: " + mylocation.getLatitude() + " long " + mylocation.getLongitude());
            editor.commit();
            new GeocodeAsyncTask().execute(mylocation.getLatitude(), mylocation.getLongitude());
        }

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }


    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {

        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                // NO need to show the dialog;
                CheckInternet();
                break;

            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                //  Location settings are not satisfied. Show the user a dialog
                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    status.startResolutionForResult(AttendanceActivity.this, REQUEST_CHECK_SETTINGS_GPS);
                } catch (IntentSender.SendIntentException e) {
                    //failed to show
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                // Location settings are unavailable so not possible to show any dialog now
                break;
        }


    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        if (CheckLocationIsEnabled()) {
            // if location is enabled show place picker activity to user
            startLocationUpdates();
        } else {
            // if location is not enabled show request to enable location to user
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);
            builder.setAlwaysShow(true);
            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(
                            googleApiClient,
                            builder.build()

                    );
            result.setResultCallback(this);
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
//        if (mRequestingLocationUpdates) {
//            startLocationUpdates();
//        }

    }

    private class GeocodeAsyncTask extends AsyncTask<Double, Void, Address> {

        String errorMessage = "";
        @SuppressLint("LongLogTag")
        @Override
        protected Address doInBackground(Double... latlang) {
            Geocoder geocoder = new Geocoder(AttendanceActivity.this, Locale.getDefault());
            List<Address> addresses = null;
            if (geocoder.isPresent()) {
                try {
                    addresses = geocoder.getFromLocation(latlang[0], latlang[1], 1);
                    Log.d(TAG, "doInBackground: ************");
                } catch (IOException ioException) {
                    errorMessage = "Service Not Available";
                    Log.e(TAG, errorMessage, ioException);
                } catch (IllegalArgumentException illegalArgumentException) {
                    errorMessage = "Invalid Latitude or Longitude Used";
                    Log.e(TAG, errorMessage + ". " +
                            "Latitude = " + latlang[0] + ", Longitude = " +
                            latlang[1], illegalArgumentException);
                }

                if (addresses != null && addresses.size() > 0)
                    return addresses.get(0);
            } else {
                new GetGeoCodeAPIAsynchTask().execute(latlang[0], latlang[1]);
            }

            return null;
        }

        @SuppressLint("LongLogTag")
        protected void onPostExecute(Address addresss) {

            if (addresss == null) {
                new GetGeoCodeAPIAsynchTask().execute(mylocation.getLatitude(), mylocation.getLongitude());
                Log.d(TAG, "onPostExecute: *****");
            } else {
//                progressBar.setVisibility(View.GONE);
                address = addresss.getAddressLine(0);
                editor.putString("ToAddress", address);
                editor.commit();
                editor.apply();
                City = addresss.getLocality();
                Log.d(TAG, "onPostExecute: **************************" + City);
                String city = addresss.getLocality();
                String state = addresss.getAdminArea();
                //create your custom title
                String title = city + "-" + state;
                gprsaddress.setText(address +
                        "\n"
                        + title);
                editor.putString("FromAddress", address);
                Geocoder geocoder = new Geocoder(AttendanceActivity.this);
                try {
                    ArrayList<Address> addresses = (ArrayList<Address>) geocoder.getFromLocationName("karur", 50);
                    for (Address address3 : addresses) {
                        double lat = address3.getLatitude();
                        double lon = address3.getLongitude();
//                        address2.setText(lat +
//                                "\n"
//                                + lon);

                    }
                } catch (IOException e) {
                    e.printStackTrace();

                }
                //create your custom title
//                String title = city + "-" + state;
//                Alertbox alertbox=new Alertbox(MainActivity.this);
//                alertbox.showAlertboxwithback("Your Current location is "+city);


            }
        }
    }

    private class GetGeoCodeAPIAsynchTask extends AsyncTask<Double, Void, String[]> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String[] doInBackground(Double... latlang) {
            String response;
            try {
                String URL = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + latlang[0] + "," + latlang[1] + "&key=" + APIKEY;
                Log.v("URL", URL);
                response = getLatLongByURL(URL);
                return new String[]{response};
            } catch (Exception e) {
                return new String[]{"error"};
            }
        }

        @SuppressLint("LongLogTag")
        @Override
        protected void onPostExecute(String... result) {
            try {
                JSONObject jsonObject = new JSONObject(result[0]);

                address = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONArray("address_components").getJSONObject(0).getString("long_name");

                editor.putString("FromAddress", address);
                editor.putString("ToAddress", address);
                editor.commit();
                editor.apply();

                City = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONArray("address_components").getJSONObject(2).getString("long_name");

                String state = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONArray("address_components").getJSONObject(4).getString("long_name");

                Log.d(TAG, "onPostExecute: " + City);

//                String title = city + "-" + state;

//                Alertbox alertbox=new Alertbox(MainActivity.this);
//                alertbox.showAlertbox("Your Current location is "+city);

//                if (fetchType == FROMADDRESS) {
//
//                    mPrimaryAddress.setText(address);
//                    mSecondaryAddress.setText(title);
//                    mDropText.requestFocus();
//                    fromaddress = address;
//                    mPickupText.setText(address);
//
//                    editor.putString("FromAddress", fromaddress);
//                    editor.apply();
//                    editor.commit();
//
//                    Log.i("FromAddress1", address);
//                    Log.i("FromAddress2", title);
//                } else {
//                    mPrimaryAddress.setText(address);
//                    mSecondaryAddress.setText(title);
//                    mDropText.setText(address);
//                    toaddress = address;
//                    editor.putString("ToAddress", toaddress).commit();
//
//                    Log.i("ToAddress1", address);
//                    Log.i("ToAddress2", title);
//
//                }


                Log.d("Address", "" + address);
            } catch (JSONException e) {
                e.printStackTrace();
            }
//            progressBar.setVisibility(View.GONE);

        }
    }

    public String getLatLongByURL(String requestURL) {
        URL url;
        String response = "";
        try {
            url = new URL(requestURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");
            conn.setDoOutput(true);
            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = "";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public String getDistance(double lat1, double lon1, double lat2, double lon2) {
        distance = 0f;
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        System.out.println(lat1 + " " + lon1 + " " + lat2 + " " + lon2);
        // String url = "http://maps.google.com/maps/api/directions/xml?origin=" + lat1 + "," + lon1 + "&destination=" + lat2 + "," + lon2 + "&sensor=false&units=metric";
        String url = "https://maps.googleapis.com/maps/api/directions/xml?origin=" + lat1 + "," + lon1 + "&destination=" + lat2 + "," + lon2 + "&key=" + APIKEY + "";

        String tag[] = {"value"};  //will give distance as string e.g 1.2 km
        // or tag[] = {"value"} if you want to get distance in metre e.g. 1234
        Log.i("URL display == ", url);
        String address[] = {"end_address"};
        HttpResponse response = null;
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpContext localContext = new BasicHttpContext();
            HttpPost httpPost = new HttpPost(url);
            response = httpClient.execute(httpPost, localContext);
            InputStream is = response.getEntity().getContent();
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = builder.parse(is);

            if (doc != null) {
                NodeList nl, n2;
                ArrayList args = new ArrayList();
                ArrayList args1 = new ArrayList();
                for (String s : tag) {
                    nl = doc.getElementsByTagName(s);
                    if (nl.getLength() > 0) {
                        Node node = nl.item(nl.getLength() - 1);
                        args.add(node.getTextContent());
                    } else {
                        args.add("0");
                    }
                    for (String s1 : address) {
                        n2 = doc.getElementsByTagName(s1);
                        if (n2.getLength() > 0) {
                            Node node = n2.item(n2.getLength() - 1);
                            args1.add(node.getTextContent());
                        } else {
                            args1.add("no address");
                        }
                    }
                }

                String dis = String.format("%s", args.get(0));

                distance = Float.parseFloat(dis) / 1000;
                Log.d(TAG, "getDistance: Distance"+distance);
//                    end_address = args1.get(0).toString().replace("'", "");


            } else {
                System.out.print("Doc is null");
                Log.d(TAG, "getDistance: Doc is null ");

                if (lat1 != 0.0) {
                    Location locationA = new Location("point A");
                    locationA.setLatitude(lat1);
                    locationA.setLongitude(lon1);
                    Location locationB = new Location("point B");
                    locationB.setLatitude(lat2);
                    locationB.setLongitude(lon2);
                    distance = locationA.distanceTo(locationB) / 1000;
                    Log.d(TAG, "getDistance: small distance "+distance);
                }

            }

            if (distance == 0) {
                Log.d(TAG, "getDistance: inside distance is zero");
                if (lat1 != 0.0) {
                    Log.d(TAG, "getDistance: inside lat not one");
                    Location locationA = new Location("point A");
                    locationA.setLatitude(lat1);
                    locationA.setLongitude(lon1);
                    Location locationB = new Location("point B");
                    locationB.setLatitude(lat2);
                    locationB.setLongitude(lon2);
                    distance = locationA.distanceTo(locationB) / 1000;
                }
            }


        } catch (Exception e) {
            Log.d(TAG, "getDistance: inside exception in getDistance");
//            Location locationA = new Location("point A");
//            locationA.setLatitude(lat1);
//            locationA.setLongitude(lon1);
//            Location locationB = new Location("point B");
//            locationB.setLatitude(lat2);
//            locationB.setLongitude(lon2);
//            distance = locationA.distanceTo(locationB) / 1000;
            e.printStackTrace();
        }

        try {
            DecimalFormat df = new DecimalFormat();
            df.setMaximumFractionDigits(3);
            Log.d("distance from google **", Float.toString(distance));
            distance = Float.parseFloat(df.format(distance));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return "";
    }

    private void attendance(final String attendance)
    {
        final ProgressDialog loading1;
        final String name, formattedtime, formattedDate, description, locate, lat, lon, code, dayName, inlat, inlong, inaddress;
        int empid;
        loading1 = new ProgressDialog(AttendanceActivity.this);
        loading1.setMessage("Loading...");
        loading1.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        loading1.setCancelable(false);
        loading1.show();
        //date
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        formattedDate = df.format(c.getTime());

        //Time

        Calendar cal = Calendar.getInstance();
        Date currentTime = cal.getTime();
        SimpleDateFormat dftime = new SimpleDateFormat("HH:mm:ss");
        formattedtime = dftime.format(currentTime);

        //Day
        SimpleDateFormat sdf_ = new SimpleDateFormat("EEEE");
        Date date = new Date();
        dayName = sdf_.format(date);
//            loading = ProgressDialog.show(AttendanceActivity.this, "Loading", "Please wait", false);
        name = myshare.getString("name", "");
        description = myshare.getString("usertype", "");
        //address=myshare.getString("address","");
        // locate=myshare.getString("worklocation","");
        // locate=getIntent().getStringExtra("worklocation");
        empid = myshare.getInt("idlist", 0);
        lat = myshare.getString("latitude", "");
        lon = myshare.getString("longitude", "");
        code = myshare.getString("empcode", "");
        if (distancelikes) {
            inlat = myshare.getString("Inlatstring", "");
            inlong = myshare.getString("Inlongstring", "");
            inaddress = myshare.getString("inAddress", "");

//                Location inlcoation =new Location(inaddress);
//                Location outlocation=new Location(address);
//                distance=inlcoation.distanceTo(outlocation)/1000;

//                Location inlcoation =new Location("");
//                inlcoation.setLatitude(Double.parseDouble("12.9801"));
//                inlcoation.setLongitude(Double.parseDouble("80.2184"));
//                Location outlocation=new Location("");
//                outlocation.setLatitude(Double.parseDouble("13.0823"));
//                outlocation.setLongitude(Double.parseDouble("80.2755"));
//                distance=inlcoation.distanceTo(outlocation)/1000;

            Location inlcoation = new Location("");
            inlcoation.setLatitude(Double.parseDouble(inlat));
            inlcoation.setLongitude(Double.parseDouble(inlong));
            Location outlocation = new Location("");
            outlocation.setLatitude(Double.parseDouble(lat));
            outlocation.setLongitude(Double.parseDouble(lon));
            getDistance(inlcoation.getLatitude(),inlcoation.getLongitude(),outlocation.getLatitude(),outlocation.getLongitude());
//                distance = inlcoation.distanceTo(outlocation) / 1000;
            valueRounded = Math.round(distance * 100D) / 100D;
            // distancedouble=String.valueOf(distance);

//                Location inlocation=new Location("location in");
//                inlocation.setLatitude(Double.parseDouble(inlat));
//                inlocation.setLongitude(Double.parseDouble(inlong));
//                Location Outlocation=new Location("location Out");
//                Outlocation.setLatitude(Double.parseDouble(lat));
//                Outlocation.setLongitude(Double.parseDouble(lon));

//                Location inlocation=new Location("location in");
//                inlocation.setLatitude(Double.parseDouble("12.9801"));
//                inlocation.setLongitude(Double.parseDouble("80.2184"));
//                Location Outlocation=new Location("location Out");
//                Outlocation.setLatitude(Double.parseDouble("12.9830"));
//                Outlocation.setLongitude(Double.parseDouble("80.2594"));
//                distance=inlocation.distanceTo(Outlocation);
        }
        try {
            final APIService service = RetrofitClient.getApiService();
            Call<AttendanceData> call = service.attendnace(empid, name, formattedDate, formattedtime, address, description, address, lat, lon, lat, lon, code, dayName, valueRounded);
            call.enqueue(new Callback<AttendanceData>() {
                @Override
                public void onResponse(Call<AttendanceData> call, Response<AttendanceData> response) {
                    if (response.body().getResult().equals("Success")) {
                        loading1.dismiss();
                        editor.putInt("trackid", response.body().getData().getId());
                        editor.putInt("sid", response.body().getData().getEmpId());
                        editor.commit();
                        if (attendance.equals("start")) {
                            editor.putString("fingerType", "noFingerPrint");
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                ContextCompat.startForegroundService(   AttendanceActivity.this,new Intent(AttendanceActivity.this, BGServicenormal.class).setAction(ACTION_START_FOREGROUND_SERVICE));
                            }
                            else {
                                startService(new Intent(AttendanceActivity.this, BGServicenormal.class).setAction(ACTION_START_FOREGROUND_SERVICE));
                            }
                            showalertbox(formattedtime);
                            attendancebtn.setText("Out Time");
                            attendancebtn.setVisibility(View.VISIBLE);
                            istiming = true;
                            isCheckedIn = true;
                            servuce = false;
                            distancelikes = true;
                            editor.putBoolean("isCheckedIn", isCheckedIn);
                            editor.putBoolean("istiming", istiming);
                            editor.putBoolean("istiming", istiming);
                            editor.putString("formateddate", formattedDate);
                            editor.putString("formatedtime", formattedtime);
                            editor.putBoolean("service", servuce);
                            editor.putString("Inlatstring", lat);
                            editor.putString("Inlongstring", lon);
                            editor.putString("inAddress", address);
                            editor.putBoolean("distancelikes", distancelikes);
                            editor.commit();

//                     getNetworkStatus();

                        } else if (attendance.equals("stop")) {
                            loading1.dismiss();
                            stopService(new Intent(AttendanceActivity.this, BGServicenormal.class).setAction(ACTION_STOP_FOREGROUND_SERVICE));
                            inflateAlertBox(formattedtime);
                            attendancebtn.setText("InTime");
                            attendancebtn.setVisibility(View.VISIBLE);
                            isCheckedIn = false;
                            islinit = true;
                            istiming = false;
                            servuce = false;
                            distancelikes = false;
                            editor.putBoolean("isCheckedIn", isCheckedIn);
                            editor.putBoolean("istiming", istiming);
                            editor.putBoolean("islinit", islinit);
                            editor.putBoolean("service", servuce);
                            editor.putString("formateddate", formattedDate);
                            editor.putString("formatedtime", formattedtime);
                            editor.putString("outlat", lat);
                            editor.putString("outlong", lon);
                            editor.putBoolean("distancelikes", distancelikes);
                            editor.commit();
                        }

                    } else if (response.body().getResult().equals("Already Punched")) {
                        loading1.dismiss();
                        editor.putString("formateddate", formattedDate);
                        editor.commit();
                        showalert();
                        attendancebtn.setVisibility(View.VISIBLE);
                    } else {
                        attendancebtn.setVisibility(View.VISIBLE);
                        Toast.makeText(AttendanceActivity.this, "Please try again later...", Toast.LENGTH_SHORT).show();
                    }
                }


                @Override
                public void onFailure(Call<AttendanceData> call, Throwable t) {
                    loading1.dismiss();
                    attendancebtn.setVisibility(View.VISIBLE);
                    Toast.makeText(AttendanceActivity.this, "Attempt Failed. Please try again later...", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            loading1.dismiss();
            attendancebtn.setVisibility(View.VISIBLE);
//            Toast.makeText(AttendanceActivity.this, "Server not responding...", Toast.LENGTH_SHORT).show();
        }
        loading1.dismiss();
    }

    private class startAttendance extends AsyncTask<String, Void, String> {
        private ProgressDialog loading1;
        String name, formattedtime, formattedDate, description, locate, lat, lon, code, dayName, inlat, inlong, inaddress;
        int empid;


        @Override
        protected void onPreExecute() {
            loading1 = new ProgressDialog(AttendanceActivity.this);
            loading1.setMessage("Loading...");
            loading1.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            loading1.setCancelable(false);
            loading1.show();
            super.onPreExecute();
            //date
            Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            formattedDate = df.format(c.getTime());

            //Time

            Calendar cal = Calendar.getInstance();
            Date currentTime = cal.getTime();
            SimpleDateFormat dftime = new SimpleDateFormat("HH:mm:ss");
            formattedtime = dftime.format(currentTime);

            //Day
            SimpleDateFormat sdf_ = new SimpleDateFormat("EEEE");
            Date date = new Date();
            dayName = sdf_.format(date);
//            loading = ProgressDialog.show(AttendanceActivity.this, "Loading", "Please wait", false);
            name = myshare.getString("name", "");
            description = myshare.getString("usertype", "");
            //address=myshare.getString("address","");
            // locate=myshare.getString("worklocation","");
            // locate=getIntent().getStringExtra("worklocation");
            empid = myshare.getInt("idlist", 0);
            lat = myshare.getString("latitude", "");
            lon = myshare.getString("longitude", "");
            code = myshare.getString("empcode", "");
            if (distancelikes) {
                inlat = myshare.getString("Inlatstring", "");
                inlong = myshare.getString("Inlongstring", "");
                inaddress = myshare.getString("inAddress", "");

//                Location inlcoation =new Location(inaddress);
//                Location outlocation=new Location(address);
//                distance=inlcoation.distanceTo(outlocation)/1000;

//                Location inlcoation =new Location("");
//                inlcoation.setLatitude(Double.parseDouble("12.9801"));
//                inlcoation.setLongitude(Double.parseDouble("80.2184"));
//                Location outlocation=new Location("");
//                outlocation.setLatitude(Double.parseDouble("13.0823"));
//                outlocation.setLongitude(Double.parseDouble("80.2755"));
//                distance=inlcoation.distanceTo(outlocation)/1000;

                Location inlcoation = new Location("");
                inlcoation.setLatitude(Double.parseDouble(inlat));
                inlcoation.setLongitude(Double.parseDouble(inlong));
                Location outlocation = new Location("");
                outlocation.setLatitude(Double.parseDouble(lat));
                outlocation.setLongitude(Double.parseDouble(lon));
                getDistance(inlcoation.getLatitude(),inlcoation.getLongitude(),outlocation.getLatitude(),outlocation.getLongitude());
//                distance = inlcoation.distanceTo(outlocation) / 1000;
                valueRounded = Math.round(distance * 100D) / 100D;
                // distancedouble=String.valueOf(distance);

//                Location inlocation=new Location("location in");
//                inlocation.setLatitude(Double.parseDouble(inlat));
//                inlocation.setLongitude(Double.parseDouble(inlong));
//                Location Outlocation=new Location("location Out");
//                Outlocation.setLatitude(Double.parseDouble(lat));
//                Outlocation.setLongitude(Double.parseDouble(lon));

//                Location inlocation=new Location("location in");
//                inlocation.setLatitude(Double.parseDouble("12.9801"));
//                inlocation.setLongitude(Double.parseDouble("80.2184"));
//                Location Outlocation=new Location("location Out");
//                Outlocation.setLatitude(Double.parseDouble("12.9830"));
//                Outlocation.setLongitude(Double.parseDouble("80.2594"));
//                distance=inlocation.distanceTo(Outlocation);
            }


        }

        @Override
        protected String doInBackground(String... strings) {

            final String string = strings[0];
            try {
                final APIService service = RetrofitClient.getApiService();
                Call<AttendanceData> call = service.attendnace(empid, name, formattedDate, formattedtime, address, description, address, lat, lon, lat, lon, code, dayName, valueRounded);
                call.enqueue(new Callback<AttendanceData>() {
                    @Override
                    public void onResponse(Call<AttendanceData> call, Response<AttendanceData> response) {
                        if (response.body().getResult().equals("Success")) {

                            loading1.dismiss();
                            editor.putInt("trackid", response.body().getData().getId());
                            editor.putInt("sid", response.body().getData().getEmpId());
                            editor.commit();
                            if (string.equals("start")) {
                                editor.putString("fingerType", "noFingerPrint");

                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                    ContextCompat.startForegroundService(   AttendanceActivity.this,new Intent(AttendanceActivity.this, BGServicenormal.class).setAction(ACTION_START_FOREGROUND_SERVICE));
                                }
                                else {
                                    startService(new Intent(AttendanceActivity.this, BGServicenormal.class).setAction(ACTION_START_FOREGROUND_SERVICE));
                                }
                                showalertbox(formattedtime);
                                attendancebtn.setText("Out Time");
                                istiming = true;
                                isCheckedIn = true;
                                servuce = false;
                                distancelikes = true;
                                editor.putBoolean("isCheckedIn", isCheckedIn);
                                editor.putBoolean("istiming", istiming);
                                editor.putBoolean("istiming", istiming);
                                editor.putString("formateddate", formattedDate);
                                editor.putString("formatedtime", formattedtime);
                                editor.putBoolean("service", servuce);
                                editor.putString("Inlatstring", lat);
                                editor.putString("Inlongstring", lon);
                                editor.putString("inAddress", address);
                                editor.putBoolean("distancelikes", distancelikes);
                                editor.commit();

//                     getNetworkStatus();

                            } else if (string.equals("stop")) {
                                loading1.dismiss();
                                stopService(new Intent(AttendanceActivity.this, BGServicenormal.class).setAction(ACTION_STOP_FOREGROUND_SERVICE));
                                inflateAlertBox(formattedtime);
                                attendancebtn.setText("InTime");
                                isCheckedIn = false;
                                islinit = true;
                                istiming = false;
                                servuce = false;
                                distancelikes = false;
                                editor.putBoolean("isCheckedIn", isCheckedIn);
                                editor.putBoolean("istiming", istiming);
                                editor.putBoolean("islinit", islinit);
                                editor.putBoolean("service", servuce);
                                editor.putString("formateddate", formattedDate);
                                editor.putString("formatedtime", formattedtime);
                                editor.putString("outlat", lat);
                                editor.putString("outlong", lon);
                                editor.putBoolean("distancelikes", distancelikes);
                                editor.commit();
                            }

                        } else if (response.body().getResult().equals("Already Punched")) {
                            loading1.dismiss();
                            editor.putString("formateddate", formattedDate);
                            editor.commit();
                            showalert();
                        } else {
                            Toast.makeText(getApplicationContext(), "Please try again later...", Toast.LENGTH_SHORT).show();
                        }
                    }


                    @Override
                    public void onFailure(Call<AttendanceData> call, Throwable t) {
                        loading1.dismiss();
                        Toast.makeText(getApplicationContext(), "Attempt Failed. Please try again later...", Toast.LENGTH_SHORT).show();
                    }
                });
            } catch (Exception e) {
                loading1.dismiss();
    }
            loading1.dismiss();
            return "";
        }


    }

    private void showalert() {
        final AlertDialog alertDialog = new AlertDialog.Builder(
                AttendanceActivity.this).create();

        LayoutInflater inflater = ((Activity) AttendanceActivity.this).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alertboxouttime, null);
        alertDialog.setView(dialogView);

        TextView log = (TextView) dialogView.findViewById(R.id.textView1);
        Button okay = (Button) dialogView.findViewById(R.id.okay);
        log.setText("Already Punched Today");
        okay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent i = new Intent(AttendanceActivity.this, HomeActivty.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            }
        });
        alertDialog.show();

    }

    private void inflateAlertBox(String formattedtime) {
        try {
            AlertDialog alertDialog = new AlertDialog.Builder(
                    AttendanceActivity.this).create();

            LayoutInflater inflater = ((Activity) AttendanceActivity.this).getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.alertintime, null);
            alertDialog.setView(dialogView);
            TextView log = (TextView) dialogView.findViewById(R.id.textView1);
            Button okay = (Button) dialogView.findViewById(R.id.okay);
            log.setText("Your Current Time " + formattedtime + " is marked as your Out Time Attendance!");
            okay.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    // TODO Auto-generated method stub
                    Intent i = new Intent(AttendanceActivity.this, HomeActivty.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                }
            });
            alertDialog.show();
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    private void showalertbox(final String formattedtime) {

        try {
            final AlertDialog alertDialog = new AlertDialog.Builder(
                    AttendanceActivity.this).create();

            LayoutInflater inflater = ((Activity) AttendanceActivity.this).getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.alertintime, null);
            alertDialog.setView(dialogView);

            TextView log = (TextView) dialogView.findViewById(R.id.textView1);
            Button okay = (Button) dialogView.findViewById(R.id.okay);
            log.setText("Your Current Time " + formattedtime + " is marked as your In Time Attendance!");
            okay.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    name = myshare.getString("name", "");
                    //   description = myshare.getString("usertype", "");
                    //address=myshare.getString("address","");
                    // locate=myshare.getString("worklocation","");
                    // locate=getIntent().getStringExtra("worklocation");
                    empid = myshare.getInt("idlist", 0);
                    lat = myshare.getString("latitude", "");
                    lon = myshare.getString("longitude", "");
                    code = myshare.getString("empcode", "");
                    Calendar cal = Calendar.getInstance();
                    Date currentTime = cal.getTime();
                    SimpleDateFormat dftime = new SimpleDateFormat("HH:mm:ss");
                    formattedtime1 = dftime.format(currentTime);
                    Intent i = new Intent(AttendanceActivity.this, HomeActivty.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
//                new AddItems1().execute("insert into coordinate(TrackId,S_id,saname,latitude,langtitude,address,datetime,timedate,updateddate,distance,Regid) " +
//                        "values ('"+trackId+"' , '"+empid+"' ,'"+name+"', '"+latitude+"' ,'"+longitude+"' , '"+address+"','"+formatdate1+"','"+formattedtime+"','"+formattedtime1+"','"+ distance+"','"+regid+"' )");
                    // TODO Auto-generated method stub

                }
            });
            alertDialog.show();
        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    private class AddItems1 extends AsyncTask<String, Void, String> {
        private ProgressDialog progressDialog;
        //        private Integer id;
        private SQLiteDatabase db;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

//            descriptiondy = (EditText) newView.findViewById(R.id.descriptiondynamic);
//            quantitydy = (EditText) newView.findViewById(R.id.quantitydynamic);
//            productdy = (MaterialSpinner) newView.findViewById(R.id.productdynname);


        }

        @Override
        protected String doInBackground(String... strings) {
            ServerConnection serverConnection = new ServerConnection();
            try {
                connection = serverConnection.getConnection();
                /*statement = connection.prepareStatement(selectquery);*/
                statement = connection.createStatement();
                String selectquery = strings[0];
                int i= statement.executeUpdate(selectquery);
                if(i!=0)
                {

                    statement.close();
                    connection.close();
                    return "success";
                } else {
                    statement.close();
                    connection.close();
                    return "notsucess";
                }


            } catch (Exception e) {
                e.printStackTrace();
                return "error_insertion";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//            progressDialog.dismiss();

            if (s.equals("success")) {
                Intent i = new Intent(AttendanceActivity.this, HomeActivty.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);

            } else if (s.equals("empty")) {

            } else {

            }
        }
    }
}
