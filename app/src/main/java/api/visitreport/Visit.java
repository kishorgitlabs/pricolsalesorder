package api.visitreport;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class Visit {
  @SerializedName("CustMobileNo")
  @Expose
  private Long CustMobileNo;
  @SerializedName("VisitedDate")
  @Expose
  private String VisitedDate;
  @SerializedName("SalesPersonName")
  @Expose
  private String SalesPersonName;
  @SerializedName("PlaceOfVisit")
  @Expose
  private String PlaceOfVisit;
  @SerializedName("Time")
  @Expose
  private Object Time;
  @SerializedName("id")
  @Expose
  private Integer id;
  @SerializedName("CustomerName")
  @Expose
  private String CustomerName;
  @SerializedName("CustomerType")
  @Expose
  private String CustomerType;
  @SerializedName("Remark")
  @Expose
  private String Remark;
  public void setCustMobileNo(Long CustMobileNo){
   this.CustMobileNo=CustMobileNo;
  }
  public Long getCustMobileNo(){
   return CustMobileNo;
  }
  public void setVisitedDate(String VisitedDate){
   this.VisitedDate=VisitedDate;
  }
  public String getVisitedDate(){
   return VisitedDate;
  }
  public void setSalesPersonName(String SalesPersonName){
   this.SalesPersonName=SalesPersonName;
  }
  public String getSalesPersonName(){
   return SalesPersonName;
  }
  public void setPlaceOfVisit(String PlaceOfVisit){
   this.PlaceOfVisit=PlaceOfVisit;
  }
  public String getPlaceOfVisit(){
   return PlaceOfVisit;
  }
  public void setTime(Object Time){
   this.Time=Time;
  }
  public Object getTime(){
   return Time;
  }
  public void setId(Integer id){
   this.id=id;
  }
  public Integer getId(){
   return id;
  }
  public void setCustomerName(String CustomerName){
   this.CustomerName=CustomerName;
  }
  public String getCustomerName(){
   return CustomerName;
  }
  public void setCustomerType(String CustomerType){
   this.CustomerType=CustomerType;
  }
  public String getCustomerType(){
   return CustomerType;
  }
  public void setRemark(String Remark){
   this.Remark=Remark;
  }
  public String getRemark(){
   return Remark;
  }
}