package api.retrofit;

import api.models.AttendanceAllTimeData.AttendanceAllData;
import api.models.AttendanceReg1.AttendanceRegData;
import api.models.DetailsRegion.RegionSalesHoleData;
import api.models.GetSalesAttendanceReg.GetSalesAttRegData;
import api.models.GetSalesExecutive.SalesExecutiveData;
import api.models.GetViewStatus.GetViewOrderStatus;
import api.models.MultipleAttendancePage.MultipleAttendanceDataReg;
import api.models.amountdetailsview.Amountdata;
import api.models.approveddelaermenu.ApproveDealerMenuData;
import api.models.approvedealer.ApproveDealerData;
import api.models.attendance.AttendanceData;
import api.models.codedata.ExecutiveCodeData;
import api.models.datewisefilterregion.DateWiseFilterReData;
import api.models.datewiseorderSe.DateWiseSeData;
import api.models.forgotpassword.ForgotPasswordData;
import api.models.LoginData;
import api.models.master.master;
import api.models.orderexist.request.ExistOrderReq;
import api.models.orderexist.response.OrderResult;
import api.models.orderhistory.OrderHistoryData;
import api.models.regionPrimary.RegionPrimaryData;
import api.models.regionSecondary.RegionSecondaryData;
import api.models.regionneworder.RegionNewOrderData;
import api.models.rmdate.RmDateData;
import api.models.updateversion.UpdateData;
import api.models.viewOrder.ViewOrderStatusData;
import api.models.viewindividualhistory.ViewIndividual;
import api.models.vieworder1.ViewOrderStatusAct;
import api.models.viewregionsalesattendance.ViewSalesAttendanceRegData;
import api.models.viewvisitreport.viewvisitreportrm.ViewVisitReportRM;
import api.models.viewvisitreport.viewvisitreportse.ViewVisitReportSE;
import api.visitreport.VisitData;
import api.visitreport.response.VisitReq;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by SYSTEM10 on 4/17/2019.
 */

public interface APIService {
    @FormUrlEncoded
    @POST("api/Values/Login")
    public Call<LoginData> Login(
            @Field("Username") String UserName,
            @Field("Password") String Password);

    @FormUrlEncoded
    @POST("api/Values/AttendanceNew")
    public Call<AttendanceData> attendnace(
            @Field("EmpId") int id,
            @Field("Name") String Name,
            @Field("Date") String date,
            @Field("attendanceTime") String attendancetime,
            @Field("Address") String address,
            @Field("Designation") String designation,
            @Field("OutAddress") String outaddress,
            @Field("InLatitude") String inlat,
            @Field("InLongitude") String inlong,
            @Field("OutLatitude") String outlat,
            @Field("OutLongitude") String outlong,
            @Field("EmpCode") String code,
            @Field("AttendDay") String Day,
            @Field("Distance") Double distance
            );

    @FormUrlEncoded
    @POST("api/Values/SalesData")
    Call<master> GetMasterData(
            @Field("Exeid") int name);

    @POST("api/Values/PlaceOrderExisting")
    Call<OrderResult> EXIST_ORDERS_CALL(
            @Body ExistOrderReq existOrders);

    @POST("api/Values/VisitReport")
    public Call<VisitData> visit(
            @Body VisitReq visit);

    @FormUrlEncoded
    @POST("api/Values/Searchorderview ")
    Call<ViewOrderStatusData> GetOrderPreview(
            @Field("excuteid") int id,
            @Field("orderType") String type);

    @FormUrlEncoded
    @POST("api/Values/AttendenceSearchview")
    Call<AttendanceAllData> GetAttendanceAllData(
            @Field("Date") String date,
            @Field("EmpId") int id);

    @FormUrlEncoded
    @POST("api/Values/RegionPendingDealer")
    Call<ApproveDealerData> ApproveData(
            @Field("Regid") int date);

    @FormUrlEncoded
    @POST("api/Values/RegionApprovalDealer")
    Call<ApproveDealerMenuData> Approve(
            @Field("id") int id);

    @FormUrlEncoded
    @POST("api/Values/RegionAttendenceview")
    Call<ViewSalesAttendanceRegData> ViewSalesAttendanceRegData(
            @Field("Regid") int regi);

    @FormUrlEncoded
    @POST("api/Values/SearchorderviewNew")
    Call<ViewOrderStatusAct> GetOrderPreview1(
            @Field("excuteid") int id,
            @Field("orderType") String type);


    @FormUrlEncoded
    @POST("api/Values/Regionsearchorderview")
    Call<OrderHistoryData> orderhistory(
            @Field("Regid") int regid,
            @Field("orderType") String ordertype);

    @FormUrlEncoded
    @POST("api/Values/Regionsalesexecutivename")
    Call<SalesExecutiveData> GetSalesExecutive(
            @Field("Regid") int regid);

    @FormUrlEncoded
    @POST("api/Values/RegionPrimaryorderview")
    Call<RegionPrimaryData> regionprimary(
            @Field("Regid") int regid);

    @FormUrlEncoded
    @POST("api/Values/RegionSecondaryorderview")
    Call<RegionSecondaryData> regionsecondary(
            @Field("Regid") int regid);

    @FormUrlEncoded
    @POST("api/Values/RegionAttendenceSearchview1")
    Call<GetSalesAttRegData> GetSalesatt(
            @Field("EmpCode") String regid,
            @Field("fromdate") String fromdate,
            @Field("todate") String todate,
            @Field("Regid") int regid1);

    @FormUrlEncoded
    @POST("api/Values/Regionsearchorderview")
    Call<RegionSalesHoleData> regionhole(
            @Field("orderid") String regid,
            @Field("orderType") String ordertype);

    @FormUrlEncoded
    @POST("api/Values/ReginalAttendanceNew")
    public Call<AttendanceRegData> attendnace1(
            @Field("EmpId") int id,
            @Field("Name") String Name,
            @Field("Date") String date,
            @Field("attendanceTime") String attendancetime,
            @Field("Address") String address,
            @Field("Designation") String designation,
            @Field("OutAddress") String outaddress,
            @Field("InLatitude") String inlat,
            @Field("InLongitude") String inlong,
            @Field("OutLatitude") String outlat,
            @Field("OutLongitude") String outlong,
            @Field("EmpCode") String code,
            @Field("AttendDay") String Day,
            @Field("Distance") Double distance
    );

    @FormUrlEncoded
    @POST("api/Values/RegionalAttendenceSearchviewNew")
    Call<MultipleAttendanceDataReg> GetAttendanceAll(
            @Field("Date") String date,
            @Field("EmpId") int id);


    @FormUrlEncoded
    @POST("api/Values/Forgot")
    Call<ForgotPasswordData> forgot(
            @Field("EmailId") String email,
            @Field("UserType") String usertype);

    @FormUrlEncoded
    @POST("api/Values/searchorderviewDetails")
    Call<ViewIndividual> GetOrderPreviewvalues(
            @Field("orderid") String type);

    @FormUrlEncoded
    @POST("api/Values/searchorderviewMonth")
    Call<GetViewOrderStatus> GetOrderValues(
            @Field("excuteid") int id);

    @FormUrlEncoded
    @POST("api/Values/searchorderviewRMMonth")
    Call<RegionNewOrderData> regioncurrentmonthorder(
            @Field("Regid") int regid);

    @FormUrlEncoded
    @POST("api/Values/searchorderviewdate")
    Call<DateWiseSeData>GetSalesdatewise(
            @Field("excuteid") int regid,
            @Field("fromdate") String fromdate,
            @Field("todate") String todate,
            @Field("UserType") String usertype);

    @FormUrlEncoded
    @POST("api/Values/searchorderviewRMdate")
    Call<DateWiseFilterReData> GetSalesdatewisere(
            @Field("Regid") int regid,
            @Field("executivecode") String excode,
            @Field("orderType") String ordertype,
            @Field("fromdate") String fromdate,
            @Field("todate") String todate);

    @FormUrlEncoded
    @POST("api/Values/RegionsalesexecutiveCode")
    Call<ExecutiveCodeData> executivecode(
            @Field("Regid") int regid);

    @FormUrlEncoded
    @POST("api/Values/searchorderviewRMMonthDetails")
    Call<Amountdata> amount(
            @Field("Regid") int regid,
            @Field("executiveCode") String code);

    @FormUrlEncoded
    @POST("api/Values/searchorderviewRMdateDetails")
    Call<RmDateData> rmdate(
            @Field("Regid") int regid,
            @Field("executiveCode") String code,
            @Field("OrderedDate") String orderdate);

    @GET("api/Values/CheckVersion")
    Call<UpdateData> update(
            @Query("version") int version);


    @FormUrlEncoded
    @POST("Api/values/viewVisitReport")
    Call<ViewVisitReportSE> viewVisitReportSE(
            @Field("SalesPersonName") String  salesPerson);

    @FormUrlEncoded
    @POST("Api/values/viewVisitReportDatewise")
    Call<ViewVisitReportSE> viewVisitReportSEBySearch(
            @Field("fromdate") String  fromDate,
            @Field("todate") String  toDate,
            @Field("ExecutiveName") String  salesPerson,
            @Field("UserType") String  userType
    );

    @FormUrlEncoded
    @POST("Api/values/viewRegionalVisitReport1")
    Call<ViewVisitReportRM> viewVisitReportRM(
            @Field("Regid") int  regId);

}
