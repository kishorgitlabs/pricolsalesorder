package api.models.regionSecondary;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;
/**
 * Awesome Pojo Generator
 * */
public class RegionSecondaryData{
  @SerializedName("result")
  @Expose
  private String result;
  @SerializedName("data")
  @Expose
  private List<RegionSecondary> data;
  public void setResult(String result){
   this.result=result;
  }
  public String getResult(){
   return result;
  }
  public void setData(List<RegionSecondary> data){
   this.data=data;
  }
  public List<RegionSecondary> getData(){
   return data;
  }
}