package api.models.regionSecondary;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class RegionSecondary {
  @SerializedName("date")
  @Expose
  private String date;
  @SerializedName("orderType")
  @Expose
  private String orderType;
  @SerializedName("dealermobile")
  @Expose
  private Long dealermobile;
  @SerializedName("Address")
  @Expose
  private Object Address;
  @SerializedName("dealerid")
  @Expose
  private Object dealerid;
  @SerializedName("executivename")
  @Expose
  private String executivename;
  @SerializedName("inserteddate")
  @Expose
  private String inserteddate;
  @SerializedName("discount")
  @Expose
  private Object discount;
  @SerializedName("Grandtotal")
  @Expose
  private Integer Grandtotal;
  @SerializedName("unitprice")
  @Expose
  private String unitprice;
  @SerializedName("Reg_mobile")
  @Expose
  private Object Reg_mobile;
  @SerializedName("package")
  @Expose
  private Object package1;
  @SerializedName("LrNo")
  @Expose
  private Object LrNo;
  @SerializedName("PartNo")
  @Expose
  private Integer PartNo;
  @SerializedName("DistributorName")
  @Expose
  private Object DistributorName;
  @SerializedName("Color")
  @Expose
  private Object Color;
  @SerializedName("CusName")
  @Expose
  private String CusName;
  @SerializedName("Branchcode")
  @Expose
  private Object Branchcode;
  @SerializedName("Unpackage")
  @Expose
  private Object Unpackage;
  @SerializedName("descode")
  @Expose
  private Object descode;
  @SerializedName("Regional_manager")
  @Expose
  private String Regional_manager;
  @SerializedName("dealerregion")
  @Expose
  private String dealerregion;
  @SerializedName("Id")
  @Expose
  private Integer Id;
  @SerializedName("Regid")
  @Expose
  private Integer Regid;
  @SerializedName("dealerstate")
  @Expose
  private String dealerstate;
  @SerializedName("ReferenceNo")
  @Expose
  private Object ReferenceNo;
  @SerializedName("status")
  @Expose
  private String status;
  @SerializedName("shiptocity")
  @Expose
  private Object shiptocity;
  @SerializedName("Description")
  @Expose
  private Object Description;
  @SerializedName("Segment")
  @Expose
  private String Segment;
  @SerializedName("dealername")
  @Expose
  private String dealername;
  @SerializedName("Number")
  @Expose
  private Object Number;
  @SerializedName("productname")
  @Expose
  private String productname;
  @SerializedName("Approve")
  @Expose
  private Object Approve;
  @SerializedName("distrubutor")
  @Expose
  private Object distrubutor;
  @SerializedName("ModeofTransport")
  @Expose
  private Object ModeofTransport;
  @SerializedName("amount")
  @Expose
  private Integer amount;
  @SerializedName("quantity")
  @Expose
  private Integer quantity;
  @SerializedName("orderid")
  @Expose
  private String orderid;
  @SerializedName("OrderMode")
  @Expose
  private Object OrderMode;
  @SerializedName("Disid")
  @Expose
  private Object Disid;
  @SerializedName("excuteid")
  @Expose
  private Integer excuteid;
  @SerializedName("Dis_Mobile")
  @Expose
  private Object Dis_Mobile;
  @SerializedName("package2")
  @Expose
  private Object package2;
  @SerializedName("EmpCode")
  @Expose
  private Object EmpCode;
  @SerializedName("Voucher")
  @Expose
  private Object Voucher;
  @SerializedName("CusCode")
  @Expose
  private String CusCode;
  @SerializedName("dealercity")
  @Expose
  private String dealercity;
  @SerializedName("dealercode")
  @Expose
  private Object dealercode;
  @SerializedName("remarks")
  @Expose
  private Object remarks;
  @SerializedName("dealeraddress")
  @Expose
  private String dealeraddress;
  @SerializedName("dealeremail")
  @Expose
  private String dealeremail;
  public void setDate(String date){
   this.date=date;
  }
  public String getDate(){
   return date;
  }
  public void setOrderType(String orderType){
   this.orderType=orderType;
  }
  public String getOrderType(){
   return orderType;
  }
  public void setDealermobile(Long dealermobile){
   this.dealermobile=dealermobile;
  }
  public Long getDealermobile(){
   return dealermobile;
  }
  public void setAddress(Object Address){
   this.Address=Address;
  }
  public Object getAddress(){
   return Address;
  }
  public void setDealerid(Object dealerid){
   this.dealerid=dealerid;
  }
  public Object getDealerid(){
   return dealerid;
  }
  public void setExecutivename(String executivename){
   this.executivename=executivename;
  }
  public String getExecutivename(){
   return executivename;
  }
  public void setInserteddate(String inserteddate){
   this.inserteddate=inserteddate;
  }
  public String getInserteddate(){
   return inserteddate;
  }
  public void setDiscount(Object discount){
   this.discount=discount;
  }
  public Object getDiscount(){
   return discount;
  }
  public void setGrandtotal(Integer Grandtotal){
   this.Grandtotal=Grandtotal;
  }
  public Integer getGrandtotal(){
   return Grandtotal;
  }
  public void setUnitprice(String unitprice){
   this.unitprice=unitprice;
  }
  public String getUnitprice(){
   return unitprice;
  }
  public void setReg_mobile(Object Reg_mobile){
   this.Reg_mobile=Reg_mobile;
  }
  public Object getReg_mobile(){
   return Reg_mobile;
  }
  public void setPackage(Object package1){
   this.package1=package1;
  }
  public Object getPackage(){
   return package1;
  }
  public void setLrNo(Object LrNo){
   this.LrNo=LrNo;
  }
  public Object getLrNo(){
   return LrNo;
  }
  public void setPartNo(Integer PartNo){
   this.PartNo=PartNo;
  }
  public Integer getPartNo(){
   return PartNo;
  }
  public void setDistributorName(Object DistributorName){
   this.DistributorName=DistributorName;
  }
  public Object getDistributorName(){
   return DistributorName;
  }
  public void setColor(Object Color){
   this.Color=Color;
  }
  public Object getColor(){
   return Color;
  }
  public void setCusName(String CusName){
   this.CusName=CusName;
  }
  public String getCusName(){
   return CusName;
  }
  public void setBranchcode(Object Branchcode){
   this.Branchcode=Branchcode;
  }
  public Object getBranchcode(){
   return Branchcode;
  }
  public void setUnpackage(Object Unpackage){
   this.Unpackage=Unpackage;
  }
  public Object getUnpackage(){
   return Unpackage;
  }
  public void setDescode(Object descode){
   this.descode=descode;
  }
  public Object getDescode(){
   return descode;
  }
  public void setRegional_manager(String Regional_manager){
   this.Regional_manager=Regional_manager;
  }
  public String getRegional_manager(){
   return Regional_manager;
  }
  public void setDealerregion(String dealerregion){
   this.dealerregion=dealerregion;
  }
  public String getDealerregion(){
   return dealerregion;
  }
  public void setId(Integer Id){
   this.Id=Id;
  }
  public Integer getId(){
   return Id;
  }
  public void setRegid(Integer Regid){
   this.Regid=Regid;
  }
  public Integer getRegid(){
   return Regid;
  }
  public void setDealerstate(String dealerstate){
   this.dealerstate=dealerstate;
  }
  public String getDealerstate(){
   return dealerstate;
  }
  public void setReferenceNo(Object ReferenceNo){
   this.ReferenceNo=ReferenceNo;
  }
  public Object getReferenceNo(){
   return ReferenceNo;
  }
  public void setStatus(String status){
   this.status=status;
  }
  public String getStatus(){
   return status;
  }
  public void setShiptocity(Object shiptocity){
   this.shiptocity=shiptocity;
  }
  public Object getShiptocity(){
   return shiptocity;
  }
  public void setDescription(Object Description){
   this.Description=Description;
  }
  public Object getDescription(){
   return Description;
  }
  public void setSegment(String Segment){
   this.Segment=Segment;
  }
  public String getSegment(){
   return Segment;
  }
  public void setDealername(String dealername){
   this.dealername=dealername;
  }
  public String getDealername(){
   return dealername;
  }
  public void setNumber(Object Number){
   this.Number=Number;
  }
  public Object getNumber(){
   return Number;
  }
  public void setProductname(String productname){
   this.productname=productname;
  }
  public String getProductname(){
   return productname;
  }
  public void setApprove(Object Approve){
   this.Approve=Approve;
  }
  public Object getApprove(){
   return Approve;
  }
  public void setDistrubutor(Object distrubutor){
   this.distrubutor=distrubutor;
  }
  public Object getDistrubutor(){
   return distrubutor;
  }
  public void setModeofTransport(Object ModeofTransport){
   this.ModeofTransport=ModeofTransport;
  }
  public Object getModeofTransport(){
   return ModeofTransport;
  }
  public void setAmount(Integer amount){
   this.amount=amount;
  }
  public Integer getAmount(){
   return amount;
  }
  public void setQuantity(Integer quantity){
   this.quantity=quantity;
  }
  public Integer getQuantity(){
   return quantity;
  }
  public void setOrderid(String orderid){
   this.orderid=orderid;
  }
  public String getOrderid(){
   return orderid;
  }
  public void setOrderMode(Object OrderMode){
   this.OrderMode=OrderMode;
  }
  public Object getOrderMode(){
   return OrderMode;
  }
  public void setDisid(Object Disid){
   this.Disid=Disid;
  }
  public Object getDisid(){
   return Disid;
  }
  public void setExcuteid(Integer excuteid){
   this.excuteid=excuteid;
  }
  public Integer getExcuteid(){
   return excuteid;
  }
  public void setDis_Mobile(Object Dis_Mobile){
   this.Dis_Mobile=Dis_Mobile;
  }
  public Object getDis_Mobile(){
   return Dis_Mobile;
  }
  public void setPackage2(Object package2){
   this.package2=package2;
  }
  public Object getPackage2(){
   return package2;
  }
  public void setEmpCode(Object EmpCode){
   this.EmpCode=EmpCode;
  }
  public Object getEmpCode(){
   return EmpCode;
  }
  public void setVoucher(Object Voucher){
   this.Voucher=Voucher;
  }
  public Object getVoucher(){
   return Voucher;
  }
  public void setCusCode(String CusCode){
   this.CusCode=CusCode;
  }
  public String getCusCode(){
   return CusCode;
  }
  public void setDealercity(String dealercity){
   this.dealercity=dealercity;
  }
  public String getDealercity(){
   return dealercity;
  }
  public void setDealercode(Object dealercode){
   this.dealercode=dealercode;
  }
  public Object getDealercode(){
   return dealercode;
  }
  public void setRemarks(Object remarks){
   this.remarks=remarks;
  }
  public Object getRemarks(){
   return remarks;
  }
  public void setDealeraddress(String dealeraddress){
   this.dealeraddress=dealeraddress;
  }
  public String getDealeraddress(){
   return dealeraddress;
  }
  public void setDealeremail(String dealeremail){
   this.dealeremail=dealeremail;
  }
  public String getDealeremail(){
   return dealeremail;
  }
}