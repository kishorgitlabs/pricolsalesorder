package api.models.master;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class master{
  @SerializedName("result")
  @Expose
  private String result;
  @SerializedName("data")
  @Expose
  private MasterData data;
  public void setResult(String result){
   this.result=result;
  }
  public String getResult(){
   return result;
  }
  public void setData(MasterData data){
   this.data=data;
  }
  public MasterData getData(){
   return data;
  }
}