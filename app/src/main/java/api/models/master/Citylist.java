package api.models.master;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Awesome Pojo Generator
 * */
@Entity(tableName = "city")
public class Citylist{
    @ColumnInfo(name = "Id")
    @PrimaryKey(autoGenerate = true)
    private int id;
    @ColumnInfo(name = "S_id")
    private String S_id;
    @ColumnInfo(name = "deleteflag")
    private String deleteflag;
    @ColumnInfo(name = "town1")
    private String town1;
    @ColumnInfo(name = "state")
    private String state;
    public void setS_id(String S_id){
        this.S_id=S_id;
    }
    public String getS_id(){
        return S_id;
    }
    public void setDeleteflag(String deleteflag){
        this.deleteflag=deleteflag;
    }
    public String getDeleteflag(){
        return deleteflag;
    }
    public void setTown1(String town1){
        this.town1=town1;
    }
    public String getTown1(){
        return town1;
    }
    public void setId(int id){
        this.id=id;
    }
    public int getId(){
        return id;
    }
    public void setState(String state){
        this.state=state;
    }
    public String getState(){
        return state;
    }
}

