package api.models.master;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Awesome Pojo Generator
 * */
@Entity(tableName = "products")
public class Productlist{
    @ColumnInfo(name = "Id")
    @PrimaryKey(autoGenerate = true)
    private int Product_Id;
    @ColumnInfo(name = "S_Id")
    private String S_Id;
    @ColumnInfo(name = "whatsnew")
    private String whatsnew;
    @ColumnInfo(name = "Customername")
    private String Customername;
    @ColumnInfo(name = "description")
    private String description;
    @ColumnInfo(name = "Instrument_Dia")
    private String Instrument_Dia;
    @ColumnInfo(name = "type")
    private String type;
    @ColumnInfo(name = "segments")
    private String segments;
    @ColumnInfo(name = "listprice")
    private String listprice;
    @ColumnInfo(name = "insertdatetime")
    private String insertdatetime;
    @ColumnInfo(name = "model")
    private String model;
    @ColumnInfo(name = "partno")
    private String partno;
    @ColumnInfo(name = "Proca_Id")
    private String Proca_Id;
    @ColumnInfo(name = "image")
    private String image;
    @ColumnInfo(name = "deleteflag")
    private String deleteflag;
    @ColumnInfo(name = "C_Id")
    private String C_Id;
    @ColumnInfo(name = "mrp")
    private String mrp;
    @ColumnInfo(name = "V_Id")
    private String V_Id;
    @ColumnInfo(name = "Range")
    private String Range;
    @ColumnInfo(name = "qty")
    private String qty;
    @ColumnInfo(name = "KeyFeature")
    private String KeyFeature;
    @ColumnInfo(name = "category")
    private String category;
    @ColumnInfo(name = "subcategory")
    private String subcategory;
    @ColumnInfo(name = "updatetime")
    private String updatetime;
    @ColumnInfo(name = "partimage")
    private String partimage;
    @ColumnInfo(name = "oempartno")
    private String oempartno;
    public void setS_Id(String S_Id){
        this.S_Id=S_Id;
    }
    public String getS_Id(){
        return S_Id;
    }
    public void setWhatsnew(String whatsnew){
        this.whatsnew=whatsnew;
    }
    public String getWhatsnew(){
        return whatsnew;
    }
    public void setCustomername(String Customername){
        this.Customername=Customername;
    }
    public String getCustomername(){
        return Customername;
    }
    public void setDescription(String description){
        this.description=description;
    }
    public String getDescription(){
        return description;
    }
    public void setInstrument_Dia(String Instrument_Dia){
        this.Instrument_Dia=Instrument_Dia;
    }
    public String getInstrument_Dia(){
        return Instrument_Dia;
    }
    public void setType(String type){
        this.type=type;
    }
    public String getType(){
        return type;
    }
    public void setSegments(String segments){
        this.segments=segments;
    }
    public String getSegments(){
        return segments;
    }
    public void setListprice(String listprice){
        this.listprice=listprice;
    }
    public String getListprice(){
        return listprice;
    }
    public void setInsertdatetime(String insertdatetime){
        this.insertdatetime=insertdatetime;
    }
    public String getInsertdatetime(){
        return insertdatetime;
    }
    public void setModel(String model){
        this.model=model;
    }
    public String getModel(){
        return model;
    }
    public void setPartno(String partno){
        this.partno=partno;
    }
    public String getPartno(){
        return partno;
    }
    public void setProca_Id(String Proca_Id){
        this.Proca_Id=Proca_Id;
    }
    public String getProca_Id(){
        return Proca_Id;
    }
    public void setImage(String image){
        this.image=image;
    }
    public String getImage(){
        return image;
    }
    public void setDeleteflag(String deleteflag){
        this.deleteflag=deleteflag;
    }
    public String getDeleteflag(){
        return deleteflag;
    }
    public void setProduct_Id(int Product_Id){
        this.Product_Id=Product_Id;
    }
    public int getProduct_Id(){
        return Product_Id;
    }
    public void setC_Id(String C_Id){
        this.C_Id=C_Id;
    }
    public String getC_Id(){
        return C_Id;
    }
    public void setMrp(String mrp){
        this.mrp=mrp;
    }
    public String getMrp(){
        return mrp;
    }
    public void setV_Id(String V_Id){
        this.V_Id=V_Id;
    }
    public String getV_Id(){
        return V_Id;
    }
    public void setRange(String Range){
        this.Range=Range;
    }
    public String getRange(){
        return Range;
    }
    public void setQty(String qty){
        this.qty=qty;
    }
    public String getQty(){
        return qty;
    }
    public void setKeyFeature(String KeyFeature){
        this.KeyFeature=KeyFeature;
    }
    public String getKeyFeature(){
        return KeyFeature;
    }
    public void setCategory(String category){
        this.category=category;
    }
    public String getCategory(){
        return category;
    }
    public void setSubcategory(String subcategory){
        this.subcategory=subcategory;
    }
    public String getSubcategory(){
        return subcategory;
    }
    public void setUpdatetime(String updatetime){
        this.updatetime=updatetime;
    }
    public String getUpdatetime(){
        return updatetime;
    }
    public void setPartimage(String partimage){
        this.partimage=partimage;
    }
    public String getPartimage(){
        return partimage;
    }
    public void setOempartno(String oempartno){
        this.oempartno=oempartno;
    }
    public String getOempartno(){
        return oempartno;
    }
}
