package api.models.master;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Awesome Pojo Generator
 * */
@Entity(tableName = "setting")
public class Setting{
    @ColumnInfo(name = "id")
    @PrimaryKey(autoGenerate = true)
    private int id;
    @ColumnInfo(name = "UpdateDate")
  private String UpdateDate;
    @ColumnInfo(name = "flag")
  private String flag;
    @ColumnInfo(name = "InsertDate")
  private String InsertDate;
    @ColumnInfo(name = "Value")
  private String Value;
  public void setUpdateDate(String UpdateDate){
   this.UpdateDate=UpdateDate;
  }
  public String getUpdateDate(){
   return UpdateDate;
  }
  public void setFlag(String flag){
   this.flag=flag;
  }
  public String getFlag(){
   return flag;
  }
  public void setInsertDate(String InsertDate){
   this.InsertDate=InsertDate;
  }
  public String getInsertDate(){
   return InsertDate;
  }
  public void setValue(String Value){
   this.Value=Value;
  }
  public String getValue(){
   return Value;
  }
  public void setId(Integer id){
   this.id=id;
  }
  public Integer getId(){
   return id;
  }
}