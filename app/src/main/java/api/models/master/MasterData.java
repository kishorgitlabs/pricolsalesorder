package api.models.master;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

import api.models.deal;

/**
 * Awesome Pojo Generator
 * */
public class MasterData {
    @SerializedName("deal")
    @Expose
    private List<api.models.deal> deal;
    @SerializedName("Customer_list")
    @Expose
    private List<Customer_list> Customer_list;
    @SerializedName("statelist")
    @Expose
    private List<Statelist> statelist;
    @SerializedName("citylist")
    @Expose
    private List<Citylist> citylist;
    @SerializedName("distributor_list")
    @Expose
    private List<Distributor_list> distributor_list;
    @SerializedName("productlist")
    @Expose
    private List<Productlist> productlist;
    @SerializedName("setting")
    @Expose private List<Setting> settingDetails;
    public List<Setting> getSettingDetails() {return settingDetails;}
    public void setSettingDetails(List<Setting> settingDetails) {this.settingDetails = settingDetails;}
    public void setDeal(List<deal> deal){
        this.deal=deal;
    }
    public List<deal> getDeal(){
        return deal;
    }
    public void setCustomer_list(List<Customer_list> Customer_list){
        this.Customer_list=Customer_list;
    }
    public List<Customer_list> getCustomer_list(){
        return Customer_list;
    }
    public void setStatelist(List<Statelist> statelist){
        this.statelist=statelist;
    }
    public List<Statelist> getStatelist(){
        return statelist;
    }
    public void setCitylist(List<Citylist> citylist){
        this.citylist=citylist;
    }
    public List<Citylist> getCitylist(){
        return citylist;
    }
    public void setDistributor_list(List<Distributor_list> distributor_list){
        this.distributor_list=distributor_list;
    }
    public List<Distributor_list> getDistributor_list(){
        return distributor_list;
    }
    public void setProductlist(List<Productlist> productlist){
        this.productlist=productlist;
    }
    public List<Productlist> getProductlist(){
        return productlist;
    }
}




