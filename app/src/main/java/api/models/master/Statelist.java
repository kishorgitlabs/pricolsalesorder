package api.models.master;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Awesome Pojo Generator
 * */
@Entity(tableName = "state")
public class Statelist{
    @ColumnInfo(name = "Id")
    @PrimaryKey(autoGenerate = true)
    private int ID;
    @ColumnInfo(name = "deleteflag")
    private String deleteflag;
    @ColumnInfo(name = "D_Id")
    private String D_Id;
    @ColumnInfo(name = "state1")
    private String state1;
    @ColumnInfo(name = "Region")
    private String Region;
    public void setDeleteflag(String deleteflag){
        this.deleteflag=deleteflag;
    }
    public String getDeleteflag(){
        return deleteflag;
    }
    public void setD_Id(String D_Id){
        this.D_Id=D_Id;
    }
    public String getD_Id(){
        return D_Id;
    }
    public void setID(int ID){
        this.ID=ID;
    }
    public int getID(){
        return ID;
    }
    public void setState1(String state1){
        this.state1=state1;
    }
    public String getState1(){
        return state1;
    }
    public String getRegion() {return Region;}
    public void setRegion(String region) {Region = region;}
}