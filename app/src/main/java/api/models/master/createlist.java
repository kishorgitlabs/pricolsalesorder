package api.models.master;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Created by SYSTEM10 on 5/30/2019.
 */
@Entity(tableName = "createlist")
public class createlist {
    @ColumnInfo(name = "Id")
    @PrimaryKey(autoGenerate = true)
    private int id;
    @ColumnInfo(name = "typeofacc")
    private String typeofacc;
    @ColumnInfo(name = "region")
    private String region;
    @ColumnInfo(name = "state")
    private String state;
    @ColumnInfo(name = "city")
    private String city;
    @ColumnInfo(name = "mobilenumber")
    private String mobilenumber;
    @ColumnInfo(name = "address")
    private String address;
    @ColumnInfo(name = "name")
    private String name;
    @ColumnInfo(name = "shopname")
    private String shopname;
    @ColumnInfo(name = "gst")
    private String gst;
    @ColumnInfo(name = "contactperson")
    private String contactperson;
    @ColumnInfo(name = "sid")
    private int sid;
    @ColumnInfo(name = "emaild")
    private String emaild;
    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTypeofacc() {
        return typeofacc;
    }

    public void setTypeofacc(String typeofacc) {
        this.typeofacc = typeofacc;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getMobilenumber() {
        return mobilenumber;
    }

    public void setMobilenumber(String mobilenumber) {
        this.mobilenumber = mobilenumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShopname() {
        return shopname;
    }

    public void setShopname(String shopname) {
        this.shopname = shopname;
    }

    public String getGst() {
        return gst;
    }

    public void setGst(String gst) {
        this.gst = gst;
    }

    public String getContactperson() {
        return contactperson;
    }

    public void setContactperson(String contactperson) {
        this.contactperson = contactperson;
    }
    public String getEmaild() {return emaild;}
    public void setEmaild(String emaild) {this.emaild = emaild;}
}
