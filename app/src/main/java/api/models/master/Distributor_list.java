package api.models.master;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Awesome Pojo Generator
 * */
@Entity(tableName = "Distributor")
public class Distributor_list{
    @ColumnInfo(name = "Id")
    @PrimaryKey(autoGenerate = true)
    private int P_Id;
    @ColumnInfo(name = "deleteflag")
    private String deleteflag;
    @ColumnInfo(name = "address")
    private String address;
    @ColumnInfo(name = "Cus_No")
    private String Cus_No;
    @ColumnInfo(name = "cname")
    private String cname;
    @ColumnInfo(name = "townnme")
    private String townnme;
    @ColumnInfo(name = "emailid")
    private String emailid;
    @ColumnInfo(name = "statename")
    private String statename;
    @ColumnInfo(name = "mobileno")
    private String mobileno;
    @ColumnInfo(name = "Cus_type")
    private String Cus_type;
    @ColumnInfo(name = "password")
    private String password;
    @ColumnInfo(name = "protype")
    private String protype;
    @ColumnInfo(name = "Reg_email")
    private String Reg_email;
    @ColumnInfo(name = "Reg_mobile")
    private String Reg_mobile;
    @ColumnInfo(name = "Regional_manager")
    private String Regional_manager;
    @ColumnInfo(name = "insertdatetime")
    private String insertdatetime;
    @ColumnInfo(name = "Region")
    private String Region;
    @ColumnInfo(name = "Reg_code")
    private String Reg_code;
    @ColumnInfo(name = "landline")
    private String landline;
    @ColumnInfo(name = "updatetime")
    private String updatetime;
    @ColumnInfo(name = "disname")
    private String disname;
    @ColumnInfo(name = "placepassword")
    private String placepassword;
    @ColumnInfo(name = "username")
    private String username;
    public void setDeleteflag(String deleteflag){
        this.deleteflag=deleteflag;
    }
    public String getDeleteflag(){
        return deleteflag;
    }
    public void setAddress(String address){
        this.address=address;
    }
    public String getAddress(){
        return address;
    }
    public void setCus_No(String Cus_No){
        this.Cus_No=Cus_No;
    }
    public String getCus_No(){
        return Cus_No;
    }
    public void setCname(String cname){
        this.cname=cname;
    }
    public String getCname(){
        return cname;
    }
    public void setTownnme(String townnme){
        this.townnme=townnme;
    }
    public String getTownnme(){
        return townnme;
    }
    public void setEmailid(String emailid){
        this.emailid=emailid;
    }
    public String getEmailid(){
        return emailid;
    }
    public void setStatename(String statename){
        this.statename=statename;
    }
    public String getStatename(){
        return statename;
    }
    public void setMobileno(String mobileno){
        this.mobileno=mobileno;
    }
    public String getMobileno(){
        return mobileno;
    }
    public void setCus_type(String Cus_type){
        this.Cus_type=Cus_type;
    }
    public String getCus_type(){
        return Cus_type;
    }
    public void setP_Id(int P_Id){
        this.P_Id=P_Id;
    }
    public int getP_Id(){
        return P_Id;
    }
    public void setPassword(String password){
        this.password=password;
    }
    public String getPassword(){
        return password;
    }
    public void setProtype(String protype){
        this.protype=protype;
    }
    public String getProtype(){
        return protype;
    }
    public void setReg_email(String Reg_email){
        this.Reg_email=Reg_email;
    }
    public String getReg_email(){
        return Reg_email;
    }
    public void setReg_mobile(String Reg_mobile){
        this.Reg_mobile=Reg_mobile;
    }
    public String getReg_mobile(){
        return Reg_mobile;
    }
    public void setRegional_manager(String Regional_manager){
        this.Regional_manager=Regional_manager;
    }
    public String getRegional_manager(){
        return Regional_manager;
    }
    public void setInsertdatetime(String insertdatetime){
        this.insertdatetime=insertdatetime;
    }
    public String getInsertdatetime(){
        return insertdatetime;
    }
    public void setRegion(String Region){
        this.Region=Region;
    }
    public String getRegion(){
        return Region;
    }
    public void setReg_code(String Reg_code){
        this.Reg_code=Reg_code;
    }
    public String getReg_code(){
        return Reg_code;
    }
    public void setLandline(String landline){
        this.landline=landline;
    }
    public String getLandline(){
        return landline;
    }
    public void setUpdatetime(String updatetime){
        this.updatetime=updatetime;
    }
    public String getUpdatetime(){
        return updatetime;
    }
    public void setDisname(String disname){
        this.disname=disname;
    }
    public String getDisname(){
        return disname;
    }
    public void setPlacepassword(String placepassword){
        this.placepassword=placepassword;
    }
    public String getPlacepassword(){
        return placepassword;
    }
    public void setUsername(String username){
        this.username=username;
    }
    public String getUsername(){
        return username;
    }
}

