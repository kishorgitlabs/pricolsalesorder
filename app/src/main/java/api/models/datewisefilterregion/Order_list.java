package api.models.datewisefilterregion;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class Order_list{
  @SerializedName("OrderedDate")
  @Expose
  private String OrderedDate;
  @SerializedName("orderType")
  @Expose
  private String orderType;
  @SerializedName("amount")
  @Expose
  private String amount;
  @SerializedName("executiveCode")
  @Expose
  private String executiveCode;
  @SerializedName("executivename")
  @Expose
  private String executivename;
  @SerializedName("excuteid")
  @Expose
  private String excuteid;
  @SerializedName("Regid")
  @Expose
  private String Regid;
  public void setOrderedDate(String OrderedDate){
   this.OrderedDate=OrderedDate;
  }
  public String getOrderedDate(){
   return OrderedDate;
  }
  public void setOrderType(String orderType){
   this.orderType=orderType;
  }
  public String getOrderType(){
   return orderType;
  }
  public void setAmount(String amount){
   this.amount=amount;
  }
  public String getAmount(){
   return amount;
  }
  public void setExecutiveCode(String executiveCode){
   this.executiveCode=executiveCode;
  }
  public String getExecutiveCode(){
   return executiveCode;
  }
  public void setExecutivename(String executivename){
   this.executivename=executivename;
  }
  public String getExecutivename(){
   return executivename;
  }
  public void setExcuteid(String excuteid){
   this.excuteid=excuteid;
  }
  public String getExcuteid(){
   return excuteid;
  }
  public void setRegid(String Regid){
   this.Regid=Regid;
  }
  public String getRegid(){
   return Regid;
  }
}