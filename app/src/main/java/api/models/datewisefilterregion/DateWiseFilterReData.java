package api.models.datewisefilterregion;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class DateWiseFilterReData{
  @SerializedName("result")
  @Expose
  private String result;
  @SerializedName("data")
  @Expose
  private DateWiseReFilt data;
  public void setResult(String result){
   this.result=result;
  }
  public String getResult(){
   return result;
  }
  public void setData(DateWiseReFilt data){
   this.data=data;
  }
  public DateWiseReFilt getData(){
   return data;
  }
}