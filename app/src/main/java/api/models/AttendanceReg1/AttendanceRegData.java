package api.models.AttendanceReg1;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class AttendanceRegData{
  @SerializedName("result")
  @Expose
  private String result;
  @SerializedName("data")
  @Expose
  private AttendanceReg data;
  public void setResult(String result){
   this.result=result;
  }
  public String getResult(){
   return result;
  }
  public void setData(AttendanceReg data){
   this.data=data;
  }
  public AttendanceReg getData(){
   return data;
  }
}