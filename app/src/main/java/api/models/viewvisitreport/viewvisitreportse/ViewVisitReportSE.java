
package api.models.viewvisitreport.viewvisitreportse;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ViewVisitReportSE {

    @SerializedName("data")
    private List<ViewVisitReportListSE> mData;
    @SerializedName("result")
    private String mResult;

    public List<ViewVisitReportListSE> getData() {
        return mData;
    }

    public void setData(List<ViewVisitReportListSE> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
