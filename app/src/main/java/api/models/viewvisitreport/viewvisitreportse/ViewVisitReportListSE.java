
package api.models.viewvisitreport.viewvisitreportse;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class   ViewVisitReportListSE {

    @SerializedName("CustMobileNo")
    private String mCustMobileNo;
    @SerializedName("CustomerName")
    private String mCustomerName;
    @SerializedName("CustomerType")
    private String mCustomerType;
    @SerializedName("id")
    private Long mId;
    @SerializedName("NextVisitDate")
    private String mNextVisitDate;
    @SerializedName("PlaceOfVisit")
    private String mPlaceOfVisit;
    @SerializedName("Remark")
    private String mRemark;
    @SerializedName("SalesPersonName")
    private String mSalesPersonName;
    @SerializedName("Time")
    private Object mTime;
    @SerializedName("VisitedDate")
    private String mVisitedDate;

    public String getCustMobileNo() {

        return mCustMobileNo;
    }

    public void setCustMobileNo(String custMobileNo) {
        mCustMobileNo = custMobileNo;
    }

    public String getCustomerName() {
        return mCustomerName;
    }

    public void setCustomerName(String customerName) {
        mCustomerName = customerName;
    }

    public String getCustomerType() {
        return mCustomerType;
    }

    public void setCustomerType(String customerType) {
        mCustomerType = customerType;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getNextVisitDate() {
        return mNextVisitDate;
    }

    public void setNextVisitDate(String nextVisitDate) {
        mNextVisitDate = nextVisitDate;
    }

    public String getPlaceOfVisit() {
        return mPlaceOfVisit;
    }

    public void setPlaceOfVisit(String placeOfVisit) {
        mPlaceOfVisit = placeOfVisit;
    }

    public String getRemark() {
        return mRemark;
    }

    public void setRemark(String remark) {
        mRemark = remark;
    }

    public String getSalesPersonName() {
        return mSalesPersonName;
    }

    public void setSalesPersonName(String salesPersonName) {
        mSalesPersonName = salesPersonName;
    }

    public Object getTime() {
        return mTime;
    }

    public void setTime(Object time) {
        mTime = time;
    }

    public String getVisitedDate() {
        return mVisitedDate;
    }

    public void setVisitedDate(String visitedDate) {
        mVisitedDate = visitedDate;
    }

}
