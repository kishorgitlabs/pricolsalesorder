
package api.models.viewvisitreport.viewvisitreportrm;

import java.util.List;
import com.google.gson.annotations.SerializedName;

import api.models.viewvisitreport.viewvisitreportse.ViewVisitReportListSE;

@SuppressWarnings("unused")
public class ViewViewVisitReportList {

    @SerializedName("BranchCode")
    private Object mBranchCode;
    @SerializedName("BranchId")
    private Object mBranchId;
    @SerializedName("BranchName")
    private Object mBranchName;
    @SerializedName("Createddate")
    private Object mCreateddate;
    @SerializedName("CustomerCode")
    private Object mCustomerCode;
    @SerializedName("EmailId")
    private String mEmailId;
    @SerializedName("EmailPassword")
    private Object mEmailPassword;
    @SerializedName("EmailSignature")
    private Object mEmailSignature;
    @SerializedName("id")
    private Long mId;
    @SerializedName("LastLogin")
    private Object mLastLogin;
    @SerializedName("LocationCode")
    private Object mLocationCode;
    @SerializedName("Locationname")
    private Object mLocationname;
    @SerializedName("MobileNo")
    private String mMobileNo;
    @SerializedName("Name")
    private String mName;
    @SerializedName("Password")
    private String mPassword;
    @SerializedName("paymentemail")
    private Object mPaymentemail;
    @SerializedName("paymentsms")
    private Object mPaymentsms;
    @SerializedName("Regid")
    private Long mRegid;
    @SerializedName("Region")
    private String mRegion;
    @SerializedName("RegionalManagername")
    private String mRegionalManagername;
    @SerializedName("regioncode")
    private Object mRegioncode;
    @SerializedName("report")
    private List<ViewVisitReportListSE> mReport;
    @SerializedName("salesemail")
    private Object mSalesemail;
    @SerializedName("salessms")
    private Object mSalessms;
    @SerializedName("Status")
    private Object mStatus;
    @SerializedName("type")
    private Object mType;
    @SerializedName("updatedate")
    private Object mUpdatedate;
    @SerializedName("UserName")
    private String mUserName;
    @SerializedName("UserType")
    private Object mUserType;

    public Object getBranchCode() {
        return mBranchCode;
    }

    public void setBranchCode(Object branchCode) {
        mBranchCode = branchCode;
    }

    public Object getBranchId() {
        return mBranchId;
    }

    public void setBranchId(Object branchId) {
        mBranchId = branchId;
    }

    public Object getBranchName() {
        return mBranchName;
    }

    public void setBranchName(Object branchName) {
        mBranchName = branchName;
    }

    public Object getCreateddate() {
        return mCreateddate;
    }

    public void setCreateddate(Object createddate) {
        mCreateddate = createddate;
    }

    public Object getCustomerCode() {
        return mCustomerCode;
    }

    public void setCustomerCode(Object customerCode) {
        mCustomerCode = customerCode;
    }

    public String getEmailId() {
        return mEmailId;
    }

    public void setEmailId(String emailId) {
        mEmailId = emailId;
    }

    public Object getEmailPassword() {
        return mEmailPassword;
    }

    public void setEmailPassword(Object emailPassword) {
        mEmailPassword = emailPassword;
    }

    public Object getEmailSignature() {
        return mEmailSignature;
    }

    public void setEmailSignature(Object emailSignature) {
        mEmailSignature = emailSignature;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public Object getLastLogin() {
        return mLastLogin;
    }

    public void setLastLogin(Object lastLogin) {
        mLastLogin = lastLogin;
    }

    public Object getLocationCode() {
        return mLocationCode;
    }

    public void setLocationCode(Object locationCode) {
        mLocationCode = locationCode;
    }

    public Object getLocationname() {
        return mLocationname;
    }

    public void setLocationname(Object locationname) {
        mLocationname = locationname;
    }

    public String getMobileNo() {
        return mMobileNo;
    }

    public void setMobileNo(String mobileNo) {
        mMobileNo = mobileNo;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public Object getPaymentemail() {
        return mPaymentemail;
    }

    public void setPaymentemail(Object paymentemail) {
        mPaymentemail = paymentemail;
    }

    public Object getPaymentsms() {
        return mPaymentsms;
    }

    public void setPaymentsms(Object paymentsms) {
        mPaymentsms = paymentsms;
    }

    public Long getRegid() {
        return mRegid;
    }

    public void setRegid(Long regid) {
        mRegid = regid;
    }

    public String getRegion() {
        return mRegion;
    }

    public void setRegion(String region) {
        mRegion = region;
    }

    public String getRegionalManagername() {
        return mRegionalManagername;
    }

    public void setRegionalManagername(String regionalManagername) {
        mRegionalManagername = regionalManagername;
    }

    public Object getRegioncode() {
        return mRegioncode;
    }

    public void setRegioncode(Object regioncode) {
        mRegioncode = regioncode;
    }

    public List<ViewVisitReportListSE> getReport() {
        return mReport;
    }

    public void setReport(List<ViewVisitReportListSE> report) {
        mReport = report;
    }

    public Object getSalesemail() {
        return mSalesemail;
    }

    public void setSalesemail(Object salesemail) {
        mSalesemail = salesemail;
    }

    public Object getSalessms() {
        return mSalessms;
    }

    public void setSalessms(Object salessms) {
        mSalessms = salessms;
    }

    public Object getStatus() {
        return mStatus;
    }

    public void setStatus(Object status) {
        mStatus = status;
    }

    public Object getType() {
        return mType;
    }

    public void setType(Object type) {
        mType = type;
    }

    public Object getUpdatedate() {
        return mUpdatedate;
    }

    public void setUpdatedate(Object updatedate) {
        mUpdatedate = updatedate;
    }

    public String getUserName() {
        return mUserName;
    }

    public void setUserName(String userName) {
        mUserName = userName;
    }

    public Object getUserType() {
        return mUserType;
    }

    public void setUserType(Object userType) {
        mUserType = userType;
    }

    @Override
    public String toString() {
        return mName;
    }
}
