package api.models.attendance;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class Data{
  @SerializedName("OutLongitude")
  @Expose
  private Object OutLongitude;
  @SerializedName("Designation")
  @Expose
  private String Designation;
  @SerializedName("flag")
  @Expose
  private Integer flag;
  @SerializedName("Address")
  @Expose
  private String Address;
  @SerializedName("attendanceTime")
  @Expose
  private String attendanceTime;
  @SerializedName("OutTime")
  @Expose
  private Object OutTime;
  @SerializedName("Disid")
  @Expose
  private Object Disid;
  @SerializedName("EmpCode")
  @Expose
  private String EmpCode;
  @SerializedName("InLongitude")
  @Expose
  private Double InLongitude;
  @SerializedName("Date")
  @Expose
  private Object Date;
  @SerializedName("InLatitude")
  @Expose
  private Double InLatitude;
  @SerializedName("Name")
  @Expose
  private String Name;
  @SerializedName("TotalDuration")
  @Expose
  private Object TotalDuration;
  @SerializedName("OutAddress")
  @Expose
  private Object OutAddress;
  @SerializedName("AttendDay")
  @Expose
  private String AttendDay;
  @SerializedName("InTime")
  @Expose
  private String InTime;
  @SerializedName("OutLatitude")
  @Expose
  private Object OutLatitude;
  @SerializedName("CreatedDate")
  @Expose
  private Object CreatedDate;
  @SerializedName("id")
  @Expose
  private Integer id;
  @SerializedName("EmpId")
  @Expose
  private Integer EmpId;
  @SerializedName("Regid")
  @Expose
  private Object Regid;
  public void setOutLongitude(Object OutLongitude){
   this.OutLongitude=OutLongitude;
  }
  public Object getOutLongitude(){
   return OutLongitude;
  }
  public void setDesignation(String Designation){
   this.Designation=Designation;
  }
  public String getDesignation(){
   return Designation;
  }
  public void setFlag(Integer flag){
   this.flag=flag;
  }
  public Integer getFlag(){
   return flag;
  }
  public void setAddress(String Address){
   this.Address=Address;
  }
  public String getAddress(){
   return Address;
  }
  public void setAttendanceTime(String attendanceTime){
   this.attendanceTime=attendanceTime;
  }
  public String getAttendanceTime(){
   return attendanceTime;
  }
  public void setOutTime(Object OutTime){
   this.OutTime=OutTime;
  }
  public Object getOutTime(){
   return OutTime;
  }
  public void setDisid(Object Disid){
   this.Disid=Disid;
  }
  public Object getDisid(){
   return Disid;
  }
  public void setEmpCode(String EmpCode){
   this.EmpCode=EmpCode;
  }
  public String getEmpCode(){
   return EmpCode;
  }
  public void setInLongitude(Double InLongitude){
   this.InLongitude=InLongitude;
  }
  public Double getInLongitude(){
   return InLongitude;
  }
  public void setDate(Object Date){
   this.Date=Date;
  }
  public Object getDate(){
   return Date;
  }
  public void setInLatitude(Double InLatitude){
   this.InLatitude=InLatitude;
  }
  public Double getInLatitude(){
   return InLatitude;
  }
  public void setName(String Name){
   this.Name=Name;
  }
  public String getName(){
   return Name;
  }
  public void setTotalDuration(Object TotalDuration){
   this.TotalDuration=TotalDuration;
  }
  public Object getTotalDuration(){
   return TotalDuration;
  }
  public void setOutAddress(Object OutAddress){
   this.OutAddress=OutAddress;
  }
  public Object getOutAddress(){
   return OutAddress;
  }
  public void setAttendDay(String AttendDay){
   this.AttendDay=AttendDay;
  }
  public String getAttendDay(){
   return AttendDay;
  }
  public void setInTime(String InTime){
   this.InTime=InTime;
  }
  public String getInTime(){
   return InTime;
  }
  public void setOutLatitude(Object OutLatitude){
   this.OutLatitude=OutLatitude;
  }
  public Object getOutLatitude(){
   return OutLatitude;
  }
  public void setCreatedDate(Object CreatedDate){
   this.CreatedDate=CreatedDate;
  }
  public Object getCreatedDate(){
   return CreatedDate;
  }
  public void setId(Integer id){
   this.id=id;
  }
  public Integer getId(){
   return id;
  }
  public void setEmpId(Integer EmpId){
   this.EmpId=EmpId;
  }
  public Integer getEmpId(){
   return EmpId;
  }
  public void setRegid(Object Regid){
   this.Regid=Regid;
  }
  public Object getRegid(){
   return Regid;
  }
}