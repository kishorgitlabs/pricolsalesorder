package api.models.MultipleAttendancePage;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;
/**
 * Awesome Pojo Generator
 * */
public class MultipleAttendanceDataReg{
  @SerializedName("result")
  @Expose
  private String result;
  @SerializedName("data")
  @Expose
  private List<MultipleDataAttendance> data;
  public void setResult(String result){
   this.result=result;
  }
  public String getResult(){
   return result;
  }
  public void setData(List<MultipleDataAttendance> data){
   this.data=data;
  }
  public List<MultipleDataAttendance> getData(){
   return data;
  }
}