package api.models.GetSalesAttendanceReg;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;
/**
 * Awesome Pojo Generator
 * */
public class GetSalesAttRegData{
  @SerializedName("result")
  @Expose
  private String result;
  @SerializedName("data")
  @Expose
  private List<GetSalesAtt> data;
  public void setResult(String result){
   this.result=result;
  }
  public String getResult(){
   return result;
  }
  public void setData(List<GetSalesAtt> data){
   this.data=data;
  }
  public List<GetSalesAtt> getData(){
   return data;
  }
}