package api.models.GetSalesAttendanceReg;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class GetSalesAtt {
  @SerializedName("OutLongitude")
  @Expose
  private String OutLongitude;
  @SerializedName("Designation")
  @Expose
  private String Designation;
  @SerializedName("flag")
  @Expose
  private String flag;
  @SerializedName("Address")
  @Expose
  private String Address;
  @SerializedName("OutTime")
  @Expose
  private String OutTime;
  @SerializedName("Disid")
  @Expose
  private String Disid;
  @SerializedName("EmpCode")
  @Expose
  private String EmpCode;
  @SerializedName("InLongitude")
  @Expose
  private String InLongitude;
  @SerializedName("Date")
  @Expose
  private String Date;
  @SerializedName("InLatitude")
  @Expose
  private String InLatitude;
  @SerializedName("Name")
  @Expose
  private String Name;
  @SerializedName("TotalDuration")
  @Expose
  private String TotalDuration;
  @SerializedName("OutAddress")
  @Expose
  private String OutAddress;
  @SerializedName("AttendDay")
  @Expose
  private String AttendDay;
  @SerializedName("InTime")
  @Expose
  private String InTime;
  @SerializedName("OutLatitude")
  @Expose
  private String OutLatitude;
  @SerializedName("RegName")
  @Expose
  private String RegName;
  @SerializedName("CreatedDate")
  @Expose
  private String CreatedDate;
  @SerializedName("id")
  @Expose
  private Integer id;
  @SerializedName("EmpId")
  @Expose
  private Integer EmpId;
  @SerializedName("Regid")
  @Expose
  private Integer Regid;
  @SerializedName("Distance")
  @Expose
  private String Distance;
  public void setOutLongitude(String OutLongitude){
   this.OutLongitude=OutLongitude;
  }
  public String getOutLongitude(){
   return OutLongitude;
  }
  public void setDesignation(String Designation){
   this.Designation=Designation;
  }
  public String getDesignation(){
   return Designation;
  }
  public void setFlag(String flag){
   this.flag=flag;
  }
  public String getFlag(){
   return flag;
  }
  public void setAddress(String Address){
   this.Address=Address;
  }
  public String getAddress(){
   return Address;
  }
  public void setOutTime(String OutTime){
   this.OutTime=OutTime;
  }
  public String getOutTime(){
   return OutTime;
  }
  public void setDisid(String Disid){
   this.Disid=Disid;
  }
  public String getDisid(){
   return Disid;
  }
  public void setEmpCode(String EmpCode){
   this.EmpCode=EmpCode;
  }
  public String getEmpCode(){
   return EmpCode;
  }
  public void setInLongitude(String InLongitude){
   this.InLongitude=InLongitude;
  }
  public String getInLongitude(){
   return InLongitude;
  }
  public void setDate(String Date){
   this.Date=Date;
  }
  public String getDate(){
   return Date;
  }
  public void setInLatitude(String InLatitude){
   this.InLatitude=InLatitude;
  }
  public String getInLatitude(){
   return InLatitude;
  }
  public void setName(String Name){
   this.Name=Name;
  }
  public String getName(){
   return Name;
  }
  public void setTotalDuration(String TotalDuration){
   this.TotalDuration=TotalDuration;
  }
  public String getTotalDuration(){
   return TotalDuration;
  }
  public void setOutAddress(String OutAddress){
   this.OutAddress=OutAddress;
  }
  public String getOutAddress(){
   return OutAddress;
  }
  public void setAttendDay(String AttendDay){
   this.AttendDay=AttendDay;
  }
  public String getAttendDay(){
   return AttendDay;
  }
  public void setInTime(String InTime){
   this.InTime=InTime;
  }
  public String getInTime(){
   return InTime;
  }
  public void setOutLatitude(String OutLatitude){
   this.OutLatitude=OutLatitude;
  }
  public String getOutLatitude(){
   return OutLatitude;
  }
  public void setRegName(String RegName){
   this.RegName=RegName;
  }
  public String getRegName(){
   return RegName;
  }
  public void setCreatedDate(String CreatedDate){
   this.CreatedDate=CreatedDate;
  }
  public String getCreatedDate(){
   return CreatedDate;
  }
  public void setId(Integer id){
   this.id=id;
  }
  public Integer getId(){
   return id;
  }
  public void setEmpId(Integer EmpId){
   this.EmpId=EmpId;
  }
  public Integer getEmpId(){
   return EmpId;
  }
  public void setRegid(Integer Regid){
   this.Regid=Regid;
  }
  public Integer getRegid(){
   return Regid;
  }
  public void setDistance(String Distance){
   this.Distance=Distance;
  }
  public String getDistance(){
   return Distance;
  }
}