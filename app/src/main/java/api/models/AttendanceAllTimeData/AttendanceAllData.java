package api.models.AttendanceAllTimeData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;
/**
 * Awesome Pojo Generator
 * */
public class AttendanceAllData{
  @SerializedName("result")
  @Expose
  private String result;
  @SerializedName("data")
  @Expose
  private List<AttendanceAll> data;
  public void setResult(String result){
   this.result=result;
  }
  public String getResult(){
   return result;
  }
  public void setData(List<AttendanceAll> data){
   this.data=data;
  }
  public List<AttendanceAll> getData(){
   return data;
  }
}