package api.models.AttendanceAllTimeData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class AttendanceAll {
  @SerializedName("OutLongitude")
  @Expose
  private Double OutLongitude;
  @SerializedName("Designation")
  @Expose
  private String Designation;
  @SerializedName("flag")
  @Expose
  private Object flag;
  @SerializedName("Address")
  @Expose
  private String Address;
  @SerializedName("OutTime")
  @Expose
  private String OutTime;
  @SerializedName("Disid")
  @Expose
  private Object Disid;
  @SerializedName("EmpCode")
  @Expose
  private Object EmpCode;
  @SerializedName("InLongitude")
  @Expose
  private Double InLongitude;
  @SerializedName("Date")
  @Expose
  private String Date;
  @SerializedName("InLatitude")
  @Expose
  private Double InLatitude;
  @SerializedName("Name")
  @Expose
  private String Name;
  @SerializedName("TotalDuration")
  @Expose
  private Object TotalDuration;
  @SerializedName("OutAddress")
  @Expose
  private String OutAddress;
  @SerializedName("AttendDay")
  @Expose
  private String AttendDay;
  @SerializedName("InTime")
  @Expose
  private String InTime;
  @SerializedName("OutLatitude")
  @Expose
  private Double OutLatitude;
  @SerializedName("CreatedDate")
  @Expose
  private String CreatedDate;
  @SerializedName("id")
  @Expose
  private Integer id;
  @SerializedName("EmpId")
  @Expose
  private Integer EmpId;
  @SerializedName("Regid")
  @Expose
  private Object Regid;
  @SerializedName("Distance")
  @Expose
  private String Distance;
  public void setOutLongitude(Double OutLongitude){
   this.OutLongitude=OutLongitude;
  }
  public Double getOutLongitude(){
   return OutLongitude;
  }
  public void setDesignation(String Designation){
   this.Designation=Designation;
  }
  public String getDesignation(){
   return Designation;
  }
  public void setFlag(Object flag){
   this.flag=flag;
  }
  public Object getFlag(){
   return flag;
  }
  public void setAddress(String Address){
   this.Address=Address;
  }
  public String getAddress(){
   return Address;
  }
  public void setOutTime(String OutTime){
   this.OutTime=OutTime;
  }
  public String getOutTime(){
   return OutTime;
  }
  public void setDisid(Object Disid){
   this.Disid=Disid;
  }
  public Object getDisid(){
   return Disid;
  }
  public void setEmpCode(Object EmpCode){
   this.EmpCode=EmpCode;
  }
  public Object getEmpCode(){
   return EmpCode;
  }
  public void setInLongitude(Double InLongitude){
   this.InLongitude=InLongitude;
  }
  public Double getInLongitude(){
   return InLongitude;
  }
  public void setDate(String Date){
   this.Date=Date;
  }
  public String getDate(){
   return Date;
  }
  public void setInLatitude(Double InLatitude){
   this.InLatitude=InLatitude;
  }
  public Double getInLatitude(){
   return InLatitude;
  }
  public void setName(String Name){
   this.Name=Name;
  }
  public String getName(){
   return Name;
  }
  public void setTotalDuration(Object TotalDuration){
   this.TotalDuration=TotalDuration;
  }
  public Object getTotalDuration(){
   return TotalDuration;
  }
  public void setOutAddress(String OutAddress){
   this.OutAddress=OutAddress;
  }
  public String getOutAddress(){
   return OutAddress;
  }
  public void setAttendDay(String AttendDay){
   this.AttendDay=AttendDay;
  }
  public String getAttendDay(){
   return AttendDay;
  }
  public void setInTime(String InTime){
   this.InTime=InTime;
  }
  public String getInTime(){
   return InTime;
  }
  public void setOutLatitude(Double OutLatitude){
   this.OutLatitude=OutLatitude;
  }
  public Double getOutLatitude(){
   return OutLatitude;
  }
  public void setCreatedDate(String CreatedDate){
   this.CreatedDate=CreatedDate;
  }
  public String getCreatedDate(){
   return CreatedDate;
  }
  public void setId(Integer id){
   this.id=id;
  }
  public Integer getId(){
   return id;
  }
  public void setEmpId(Integer EmpId){
   this.EmpId=EmpId;
  }
  public Integer getEmpId(){
   return EmpId;
  }
  public void setRegid(Object Regid){
   this.Regid=Regid;
  }
  public Object getRegid(){
   return Regid;
  }
  public String getDistance() {return Distance;}
  public void setDistance(String distance) {Distance = distance;}
}