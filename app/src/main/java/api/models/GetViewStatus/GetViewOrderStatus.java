package api.models.GetViewStatus;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class GetViewOrderStatus{
  @SerializedName("result")
  @Expose
  private String result;
  @SerializedName("data")
  @Expose
  private GetStatus data;
  public void setResult(String result){
   this.result=result;
  }
  public String getResult(){
   return result;
  }
  public void setData(GetStatus data){
   this.data=data;
  }
  public GetStatus getData(){
   return data;
  }
}