package api.models.GetViewStatus;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;
/**
 * Awesome Pojo Generator
 * */
public class GetStatus {
  @SerializedName("tot")
  @Expose
  private Integer tot;
  @SerializedName("Order_list")
  @Expose
  private List<Order_list> Order_list;
  public void setTot(Integer tot){
   this.tot=tot;
  }
  public Integer getTot(){
   return tot;
  }
  public void setOrder_list(List<Order_list> Order_list){
   this.Order_list=Order_list;
  }
  public List<Order_list> getOrder_list(){
   return Order_list;
  }
}