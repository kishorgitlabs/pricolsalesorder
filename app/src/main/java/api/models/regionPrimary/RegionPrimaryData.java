package api.models.regionPrimary;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;
/**
 * Awesome Pojo Generator
 * */
public class RegionPrimaryData{
  @SerializedName("result")
  @Expose
  private String result;
  @SerializedName("data")
  @Expose
  private List<RegionPrimary> data;
  public void setResult(String result){
   this.result=result;
  }
  public String getResult(){
   return result;
  }
  public void setData(List<RegionPrimary> data){
   this.data=data;
  }
  public List<RegionPrimary> getData(){
   return data;
  }
}