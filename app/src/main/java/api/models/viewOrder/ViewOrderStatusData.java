package api.models.viewOrder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;
/**
 * Awesome Pojo Generator
 * */
public class ViewOrderStatusData{
  @SerializedName("result")
  @Expose
  private String result;
  @SerializedName("data")
  @Expose
  private List<ViewOrder> data;
  public void setResult(String result){
   this.result=result;
  }
  public String getResult(){
   return result;
  }
  public void setData(List<ViewOrder> data){
   this.data=data;
  }
  public List<ViewOrder> getData(){
   return data;
  }
}