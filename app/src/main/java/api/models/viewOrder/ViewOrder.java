package api.models.viewOrder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class ViewOrder {
  @SerializedName("date")
  @Expose
  private String date;
  @SerializedName("orderType")
  @Expose
  private String orderType;
  @SerializedName("dealermobile")
  @Expose
  private String dealermobile;
  @SerializedName("Address")
  @Expose
  private String Address;
  @SerializedName("dealerid")
  @Expose
  private String dealerid;
  @SerializedName("executivename")
  @Expose
  private String executivename;
  @SerializedName("inserteddate")
  @Expose
  private String inserteddate;
  @SerializedName("discount")
  @Expose
  private String discount;
  @SerializedName("Grandtotal")
  @Expose
  private String Grandtotal;
  @SerializedName("unitprice")
  @Expose
  private String unitprice;
  @SerializedName("Reg_mobile")
  @Expose
  private String Reg_mobile;
  @SerializedName("package")
  @Expose
  private String package1;
  @SerializedName("LrNo")
  @Expose
  private String LrNo;
  @SerializedName("PartNo")
  @Expose
  private String PartNo;
  @SerializedName("DistributorName")
  @Expose
  private String DistributorName;
  @SerializedName("Color")
  @Expose
  private String Color;
  @SerializedName("CusName")
  @Expose
  private String CusName;
  @SerializedName("Branchcode")
  @Expose
  private String Branchcode;
  @SerializedName("Unpackage")
  @Expose
  private String Unpackage;
  @SerializedName("descode")
  @Expose
  private String descode;
  @SerializedName("Regional_manager")
  @Expose
  private String Regional_manager;
  @SerializedName("dealerregion")
  @Expose
  private String dealerregion;
  @SerializedName("Id")
  @Expose
  private String Id;
  @SerializedName("Regid")
  @Expose
  private String Regid;
  @SerializedName("dealerstate")
  @Expose
  private String dealerstate;
  @SerializedName("ReferenceNo")
  @Expose
  private String ReferenceNo;
  @SerializedName("status")
  @Expose
  private String status;
  @SerializedName("shiptocity")
  @Expose
  private String shiptocity;
  @SerializedName("Description")
  @Expose
  private String Description;
  @SerializedName("Segment")
  @Expose
  private String Segment;
  @SerializedName("dealername")
  @Expose
  private String dealername;
  @SerializedName("Number")
  @Expose
  private String Number;
  @SerializedName("productname")
  @Expose
  private String productname;
  @SerializedName("Approve")
  @Expose
  private String Approve;
  @SerializedName("distrubutor")
  @Expose
  private String distrubutor;
  @SerializedName("ModeofTransport")
  @Expose
  private String ModeofTransport;
  @SerializedName("amount")
  @Expose
  private String amount;
  @SerializedName("quantity")
  @Expose
  private String quantity;
  @SerializedName("orderid")
  @Expose
  private String orderid;
  @SerializedName("OrderMode")
  @Expose
  private String OrderMode;
  @SerializedName("Disid")
  @Expose
  private String Disid;
  @SerializedName("excuteid")
  @Expose
  private String excuteid;
  @SerializedName("Dis_Mobile")
  @Expose
  private String Dis_Mobile;
  @SerializedName("package2")
  @Expose
  private String package2;
  @SerializedName("EmpCode")
  @Expose
  private String EmpCode;
  @SerializedName("Voucher")
  @Expose
  private String Voucher;
  @SerializedName("CusCode")
  @Expose
  private String CusCode;
  @SerializedName("dealercity")
  @Expose
  private String dealercity;
  @SerializedName("dealercode")
  @Expose
  private String dealercode;
  @SerializedName("remarks")
  @Expose
  private String remarks;
  @SerializedName("dealeraddress")
  @Expose
  private String dealeraddress;
  @SerializedName("dealeremail")
  @Expose
  private String dealeremail;
  public void setDate(String date){
   this.date=date;
  }
  public String getDate(){
   return date;
  }
  public void setOrderType(String orderType){
   this.orderType=orderType;
  }
  public String getOrderType(){
   return orderType;
  }
  public void setDealermobile(String dealermobile){
   this.dealermobile=dealermobile;
  }
  public String getDealermobile(){
   return dealermobile;
  }
  public void setAddress(String Address){
   this.Address=Address;
  }
  public String getAddress(){
   return Address;
  }
  public void setDealerid(String dealerid){
   this.dealerid=dealerid;
  }
  public String getDealerid(){
   return dealerid;
  }
  public void setExecutivename(String executivename){
   this.executivename=executivename;
  }
  public String getExecutivename(){
   return executivename;
  }
  public void setInserteddate(String inserteddate){
   this.inserteddate=inserteddate;
  }
  public String getInserteddate(){
   return inserteddate;
  }
  public void setDiscount(String discount){
   this.discount=discount;
  }
  public String getDiscount(){
   return discount;
  }
  public void setGrandtotal(String Grandtotal){
   this.Grandtotal=Grandtotal;
  }
  public String getGrandtotal(){
   return Grandtotal;
  }
  public void setUnitprice(String unitprice){
   this.unitprice=unitprice;
  }
  public String getUnitprice(){
   return unitprice;
  }
  public void setReg_mobile(String Reg_mobile){
   this.Reg_mobile=Reg_mobile;
  }
  public String getReg_mobile(){
   return Reg_mobile;
  }
  public void setPackage(String package1){
   this.package1=package1;
  }
  public String getPackage(){
   return package1;
  }
  public void setLrNo(String LrNo){
   this.LrNo=LrNo;
  }
  public String getLrNo(){
   return LrNo;
  }
  public void setPartNo(String PartNo){
   this.PartNo=PartNo;
  }
  public String getPartNo(){
   return PartNo;
  }
  public void setDistributorName(String DistributorName){
   this.DistributorName=DistributorName;
  }
  public String getDistributorName(){
   return DistributorName;
  }
  public void setColor(String Color){
   this.Color=Color;
  }
  public String getColor(){
   return Color;
  }
  public void setCusName(String CusName){
   this.CusName=CusName;
  }
  public String getCusName(){
   return CusName;
  }
  public void setBranchcode(String Branchcode){
   this.Branchcode=Branchcode;
  }
  public String getBranchcode(){
   return Branchcode;
  }
  public void setUnpackage(String Unpackage){
   this.Unpackage=Unpackage;
  }
  public String getUnpackage(){
   return Unpackage;
  }
  public void setDescode(String descode){
   this.descode=descode;
  }
  public String getDescode(){
   return descode;
  }
  public void setRegional_manager(String Regional_manager){
   this.Regional_manager=Regional_manager;
  }
  public String getRegional_manager(){
   return Regional_manager;
  }
  public void setDealerregion(String dealerregion){
   this.dealerregion=dealerregion;
  }
  public String getDealerregion(){
   return dealerregion;
  }
  public void setId(String Id){
   this.Id=Id;
  }
  public String getId(){
   return Id;
  }
  public void setRegid(String Regid){
   this.Regid=Regid;
  }
  public String getRegid(){
   return Regid;
  }
  public void setDealerstate(String dealerstate){
   this.dealerstate=dealerstate;
  }
  public String getDealerstate(){
   return dealerstate;
  }
  public void setReferenceNo(String ReferenceNo){
   this.ReferenceNo=ReferenceNo;
  }
  public String getReferenceNo(){
   return ReferenceNo;
  }
  public void setStatus(String status){
   this.status=status;
  }
  public String getStatus(){
   return status;
  }
  public void setShiptocity(String shiptocity){
   this.shiptocity=shiptocity;
  }
  public String getShiptocity(){
   return shiptocity;
  }
  public void setDescription(String Description){
   this.Description=Description;
  }
  public String getDescription(){
   return Description;
  }
  public void setSegment(String Segment){
   this.Segment=Segment;
  }
  public String getSegment(){
   return Segment;
  }
  public void setDealername(String dealername){
   this.dealername=dealername;
  }
  public String getDealername(){
   return dealername;
  }
  public void setNumber(String Number){
   this.Number=Number;
  }
  public String getNumber(){
   return Number;
  }
  public void setProductname(String productname){
   this.productname=productname;
  }
  public String getProductname(){
   return productname;
  }
  public void setApprove(String Approve){
   this.Approve=Approve;
  }
  public String getApprove(){
   return Approve;
  }
  public void setDistrubutor(String distrubutor){
   this.distrubutor=distrubutor;
  }
  public String getDistrubutor(){
   return distrubutor;
  }
  public void setModeofTransport(String ModeofTransport){
   this.ModeofTransport=ModeofTransport;
  }
  public String getModeofTransport(){
   return ModeofTransport;
  }
  public void setAmount(String amount){
   this.amount=amount;
  }
  public String getAmount(){
   return amount;
  }
  public void setQuantity(String quantity){
   this.quantity=quantity;
  }
  public String getQuantity(){
   return quantity;
  }
  public void setOrderid(String orderid){
   this.orderid=orderid;
  }
  public String getOrderid(){
   return orderid;
  }
  public void setOrderMode(String OrderMode){
   this.OrderMode=OrderMode;
  }
  public String getOrderMode(){
   return OrderMode;
  }
  public void setDisid(String Disid){
   this.Disid=Disid;
  }
  public String getDisid(){
   return Disid;
  }
  public void setExcuteid(String excuteid){
   this.excuteid=excuteid;
  }
  public String getExcuteid(){
   return excuteid;
  }
  public void setDis_Mobile(String Dis_Mobile){
   this.Dis_Mobile=Dis_Mobile;
  }
  public String getDis_Mobile(){
   return Dis_Mobile;
  }
  public void setPackage2(String package2){
   this.package2=package2;
  }
  public String getPackage2(){
   return package2;
  }
  public void setEmpCode(String EmpCode){
   this.EmpCode=EmpCode;
  }
  public String getEmpCode(){
   return EmpCode;
  }
  public void setVoucher(String Voucher){
   this.Voucher=Voucher;
  }
  public String getVoucher(){
   return Voucher;
  }
  public void setCusCode(String CusCode){
   this.CusCode=CusCode;
  }
  public String getCusCode(){
   return CusCode;
  }
  public void setDealercity(String dealercity){
   this.dealercity=dealercity;
  }
  public String getDealercity(){
   return dealercity;
  }
  public void setDealercode(String dealercode){
   this.dealercode=dealercode;
  }
  public String getDealercode(){
   return dealercode;
  }
  public void setRemarks(String remarks){
   this.remarks=remarks;
  }
  public String getRemarks(){
   return remarks;
  }
  public void setDealeraddress(String dealeraddress){
   this.dealeraddress=dealeraddress;
  }
  public String getDealeraddress(){
   return dealeraddress;
  }
  public void setDealeremail(String dealeremail){
   this.dealeremail=dealeremail;
  }
  public String getDealeremail(){
   return dealeremail;
  }
}