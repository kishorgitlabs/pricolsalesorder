
package api.models.vieworder1;
import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class ViewOrderStatusD {

    @SerializedName("date")
    private String mDate;
    @SerializedName("dealername")
    private String mDealername;
    @SerializedName("DistributorName")
    private String mDistributorName;
    @SerializedName("executivename")
    private String mExecutivename;
    @SerializedName("orderType")
    private String mOrderType;
    @SerializedName("orderid")
    private String mOrderid;
    @SerializedName("unitprice")
    private String unitprice;

    public String getUnitprice() {
        return unitprice;
    }

    public void setUnitprice(String unitprice) {
        this.unitprice = unitprice;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getDealername() {
        return mDealername;
    }

    public void setDealername(String dealername) {
        mDealername = dealername;
    }

    public String getDistributorName() {
        return mDistributorName;
    }

    public void setDistributorName(String distributorName) {
        mDistributorName = distributorName;
    }

    public String getExecutivename() {
        return mExecutivename;
    }

    public void setExecutivename(String executivename) {
        mExecutivename = executivename;
    }

    public String getOrderType() {
        return mOrderType;
    }

    public void setOrderType(String orderType) {
        mOrderType = orderType;
    }

    public String getOrderid() {
        return mOrderid;
    }

    public void setOrderid(String orderid) {
        mOrderid = orderid;
    }

}
