
package api.models.vieworder1;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ViewOrderStatusAct {

    @SerializedName("data")
    private List<ViewOrderStatusD> mData;
    @SerializedName("result")
    private String mResult;

    public List<ViewOrderStatusD> getData() {
        return mData;
    }

    public void setData(List<ViewOrderStatusD> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
