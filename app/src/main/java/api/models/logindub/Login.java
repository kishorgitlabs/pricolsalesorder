package api.models.logindub;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Awesome Pojo Generator
 * */
public class Login {
  @SerializedName("EmailSignature")
  @Expose
  private String EmailSignature;
  @SerializedName("EmailPassword")
  @Expose
  private String EmailPassword;
  @SerializedName("Createddate")
  @Expose
  private String Createddate;
  @SerializedName("type")
  @Expose
  private String type;
  @SerializedName("Name")
  @Expose
  private String Name;
  @SerializedName("updatedate")
  @Expose
  private String updatedate;
  @SerializedName("RegionalManagername")
  @Expose
  private String RegionalManagername;
  @SerializedName("id")
  @Expose
  private int id;
  @SerializedName("BranchName")
  @Expose
  private String BranchName;
  @SerializedName("Password")
  @Expose
  private String Password;
  @SerializedName("MobileNo")
  @Expose
  private String MobileNo;
  @SerializedName("Status")
  @Expose
  private String Status;
  @SerializedName("EmailId")
  @Expose
  private String EmailId;
  @SerializedName("UserName")
  @Expose
  private String UserName;
  @SerializedName("paymentemail")
  @Expose
  private String paymentemail;
  @SerializedName("CustomerCode")
  @Expose
  private String CustomerCode;
  @SerializedName("LocationCode")
  @Expose
  private String LocationCode;
  @SerializedName("BranchId")
  @Expose
  private String BranchId;
  @SerializedName("salesemail")
  @Expose
  private String salesemail;
  @SerializedName("paymentsms")
  @Expose
  private String paymentsms;
  @SerializedName("LastLogin")
  @Expose
  private String LastLogin;
  @SerializedName("salessms")
  @Expose
  private String salessms;
  @SerializedName("Region")
  @Expose
  private String Region;
  @SerializedName("Regid")
  @Expose
  private String Regid;
  @SerializedName("Locationname")
  @Expose
  private String Locationname;
  @SerializedName("UserType")
  @Expose
  private String UserType;
  @SerializedName("BranchCode")
  @Expose
  private String BranchCode;
  public void setEmailSignature(String EmailSignature){
   this.EmailSignature=EmailSignature;
  }
  public String getEmailSignature(){
   return EmailSignature;
  }
  public void setEmailPassword(String EmailPassword){
   this.EmailPassword=EmailPassword;
  }
  public String getEmailPassword(){
   return EmailPassword;
  }
  public void setCreateddate(String Createddate){
   this.Createddate=Createddate;
  }
  public String getCreateddate(){
   return Createddate;
  }
  public void setType(String type){
   this.type=type;
  }
  public String getType(){
   return type;
  }
  public void setName(String Name){
   this.Name=Name;
  }
  public String getName(){
   return Name;
  }
  public void setUpdatedate(String updatedate){
   this.updatedate=updatedate;
  }
  public String getUpdatedate(){
   return updatedate;
  }
  public void setRegionalManagername(String RegionalManagername){
   this.RegionalManagername=RegionalManagername;
  }
  public String getRegionalManagername(){
   return RegionalManagername;
  }
  public void setId(int id){
   this.id=id;
  }
  public int getId(){
   return id;
  }
  public void setBranchName(String BranchName){
   this.BranchName=BranchName;
  }
  public String getBranchName(){
   return BranchName;
  }
  public void setPassword(String Password){
   this.Password=Password;
  }
  public String getPassword(){
   return Password;
  }
  public void setMobileNo(String MobileNo){
   this.MobileNo=MobileNo;
  }
  public String getMobileNo(){
   return MobileNo;
  }
  public void setStatus(String Status){
   this.Status=Status;
  }
  public String getStatus(){
   return Status;
  }
  public void setEmailId(String EmailId){
   this.EmailId=EmailId;
  }
  public String getEmailId(){
   return EmailId;
  }
  public void setUserName(String UserName){
   this.UserName=UserName;
  }
  public String getUserName(){
   return UserName;
  }
  public void setPaymentemail(String paymentemail){
   this.paymentemail=paymentemail;
  }
  public String getPaymentemail(){
   return paymentemail;
  }
  public void setCustomerCode(String CustomerCode){
   this.CustomerCode=CustomerCode;
  }
  public String getCustomerCode(){
   return CustomerCode;
  }
  public void setLocationCode(String LocationCode){
   this.LocationCode=LocationCode;
  }
  public String getLocationCode(){
   return LocationCode;
  }
  public void setBranchId(String BranchId){
   this.BranchId=BranchId;
  }
  public String getBranchId(){
   return BranchId;
  }
  public void setSalesemail(String salesemail){
   this.salesemail=salesemail;
  }
  public String getSalesemail(){
   return salesemail;
  }
  public void setPaymentsms(String paymentsms){
   this.paymentsms=paymentsms;
  }
  public String getPaymentsms(){
   return paymentsms;
  }
  public void setLastLogin(String LastLogin){
   this.LastLogin=LastLogin;
  }
  public String getLastLogin(){
   return LastLogin;
  }
  public void setSalessms(String salessms){
   this.salessms=salessms;
  }
  public String getSalessms(){
   return salessms;
  }
  public void setRegion(String Region){
   this.Region=Region;
  }
  public String getRegion(){
   return Region;
  }
  public void setRegid(String Regid){
   this.Regid=Regid;
  }
  public String getRegid(){
   return Regid;
  }
  public void setLocationname(String Locationname){
   this.Locationname=Locationname;
  }
  public String getLocationname(){
   return Locationname;
  }
  public void setUserType(String UserType){
   this.UserType=UserType;
  }
  public String getUserType(){
   return UserType;
  }
  public void setBranchCode(String BranchCode){
   this.BranchCode=BranchCode;
  }
  public String getBranchCode(){
   return BranchCode;
  }
}