package api.models;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class login {
  @SerializedName("user_exist1")
  @Expose
  private User_exist1 user_exist1;
  @SerializedName("user_exist")
  @Expose
  private User_exist user_exist;
  public void setUser_exist1(User_exist1 user_exist1){
   this.user_exist1=user_exist1;
  }
  public User_exist1 getUser_exist1(){
   return user_exist1;
  }
  public void setUser_exist(User_exist user_exist){
   this.user_exist=user_exist;
  }
  public User_exist getUser_exist(){
   return user_exist;
  }
}