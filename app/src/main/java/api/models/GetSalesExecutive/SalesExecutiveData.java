package api.models.GetSalesExecutive;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;
/**
 * Awesome Pojo Generator
 * */
public class SalesExecutiveData{
  @SerializedName("result")
  @Expose
  private String result;
  @SerializedName("data")
  @Expose
  private List<String> data1;
  public void setResult(String result){
    this.result=result;
  }
  public String getResult(){
    return result;
  }
  public void setData(List<String> data){
    this.data1=data1;
  }
  public List<String> getData(){
    return data1;
  }
}