package api.models.approvedealer;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;
/**
 * Awesome Pojo Generator
 * */
public class ApproveDealerData{
  @SerializedName("result")
  @Expose
  private String result;
  @SerializedName("data")
  @Expose
  private List<ApproveDealer> data;
  public void setResult(String result){
   this.result=result;
  }
  public String getResult(){
   return result;
  }
  public void setData(List<ApproveDealer> data){
   this.data=data;
  }
  public List<ApproveDealer> getData(){
   return data;
  }
}