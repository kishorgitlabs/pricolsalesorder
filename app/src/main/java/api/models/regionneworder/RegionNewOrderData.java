package api.models.regionneworder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class RegionNewOrderData{
  @SerializedName("result")
  @Expose
  private String result;
  @SerializedName("data")
  @Expose
  private RegionNewOrder data;
  public void setResult(String result){
   this.result=result;
  }
  public String getResult(){
   return result;
  }
  public void setData(RegionNewOrder data){
   this.data=data;
  }
  public RegionNewOrder getData(){
   return data;
  }
}