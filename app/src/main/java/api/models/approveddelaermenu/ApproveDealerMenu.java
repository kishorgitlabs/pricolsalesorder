package api.models.approveddelaermenu;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class ApproveDealerMenu {
  @SerializedName("DealerEmail")
  @Expose
  private String DealerEmail;
  @SerializedName("Status")
  @Expose
  private String Status;
  @SerializedName("deleteflag")
  @Expose
  private String deleteflag;
  @SerializedName("DealerAddress")
  @Expose
  private String DealerAddress;
  @SerializedName("Insertdate")
  @Expose
  private String Insertdate;
  @SerializedName("DealerCity")
  @Expose
  private String DealerCity;
  @SerializedName("UpdateDate")
  @Expose
  private Object UpdateDate;
  @SerializedName("DealerMobile")
  @Expose
  private Long DealerMobile;
  @SerializedName("DealerState")
  @Expose
  private String DealerState;
  @SerializedName("RegName")
  @Expose
  private String RegName;
  @SerializedName("id")
  @Expose
  private Integer id;
  @SerializedName("Regid")
  @Expose
  private Integer Regid;
  @SerializedName("DealerName")
  @Expose
  private String DealerName;
  @SerializedName("Approve")
  @Expose
  private String Approve;
  @SerializedName("DaelerRegion")
  @Expose
  private String DaelerRegion;
  public void setDealerEmail(String DealerEmail){
   this.DealerEmail=DealerEmail;
  }
  public String getDealerEmail(){
   return DealerEmail;
  }
  public void setStatus(String Status){
   this.Status=Status;
  }
  public String getStatus(){
   return Status;
  }
  public void setDeleteflag(String deleteflag){
   this.deleteflag=deleteflag;
  }
  public String getDeleteflag(){
   return deleteflag;
  }
  public void setDealerAddress(String DealerAddress){
   this.DealerAddress=DealerAddress;
  }
  public String getDealerAddress(){
   return DealerAddress;
  }
  public void setInsertdate(String Insertdate){
   this.Insertdate=Insertdate;
  }
  public String getInsertdate(){
   return Insertdate;
  }
  public void setDealerCity(String DealerCity){
   this.DealerCity=DealerCity;
  }
  public String getDealerCity(){
   return DealerCity;
  }
  public void setUpdateDate(Object UpdateDate){
   this.UpdateDate=UpdateDate;
  }
  public Object getUpdateDate(){
   return UpdateDate;
  }
  public void setDealerMobile(Long DealerMobile){
   this.DealerMobile=DealerMobile;
  }
  public Long getDealerMobile(){
   return DealerMobile;
  }
  public void setDealerState(String DealerState){
   this.DealerState=DealerState;
  }
  public String getDealerState(){
   return DealerState;
  }
  public void setRegName(String RegName){
   this.RegName=RegName;
  }
  public String getRegName(){
   return RegName;
  }
  public void setId(Integer id){
   this.id=id;
  }
  public Integer getId(){
   return id;
  }
  public void setRegid(Integer Regid){
   this.Regid=Regid;
  }
  public Integer getRegid(){
   return Regid;
  }
  public void setDealerName(String DealerName){
   this.DealerName=DealerName;
  }
  public String getDealerName(){
   return DealerName;
  }
  public void setApprove(String Approve){
   this.Approve=Approve;
  }
  public String getApprove(){
   return Approve;
  }
  public void setDaelerRegion(String DaelerRegion){
   this.DaelerRegion=DaelerRegion;
  }
  public String getDaelerRegion(){
   return DaelerRegion;
  }
}