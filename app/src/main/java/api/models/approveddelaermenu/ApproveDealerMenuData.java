package api.models.approveddelaermenu;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class ApproveDealerMenuData{
  @SerializedName("result")
  @Expose
  private String result;
  @SerializedName("data")
  @Expose
  private ApproveDealerMenu data;
  public void setResult(String result){
   this.result=result;
  }
  public String getResult(){
   return result;
  }
  public void setData(ApproveDealerMenu data){
   this.data=data;
  }
  public ApproveDealerMenu getData(){
   return data;
  }
}