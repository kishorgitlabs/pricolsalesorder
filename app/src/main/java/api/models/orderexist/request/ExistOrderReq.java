package api.models.orderexist.request;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;
/**
 * Awesome Pojo Generator
 * */
public class ExistOrderReq{
  @SerializedName("Exist_Orders_List")
  @Expose
  private List<Exist_Orders_List> Exist_Orders_List;
  public void setExist_Orders_List(List<Exist_Orders_List> Exist_Orders_List){
   this.Exist_Orders_List=Exist_Orders_List;
  }
  public List<Exist_Orders_List> getExist_Orders_List(){
   return Exist_Orders_List;
  }
}