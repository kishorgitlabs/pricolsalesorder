package api.models;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class User_exist1{
  @SerializedName("EmailId")
  @Expose
  private String EmailId;
  @SerializedName("LandlineNumber")
  @Expose
  private String LandlineNumber;
  @SerializedName("managertype")
  @Expose
  private String managertype;
  @SerializedName("deleteflag")
  @Expose
  private String deleteflag;
  @SerializedName("employeetype")
  @Expose
  private String employeetype;
  @SerializedName("UserName")
  @Expose
  private String UserName;
  @SerializedName("Address")
  @Expose
  private String Address;
  @SerializedName("City")
  @Expose
  private String City;
  @SerializedName("deletedate")
  @Expose
  private String deletedate;
  @SerializedName("R_Id")
  @Expose
  private Integer R_Id;
  @SerializedName("MobileNumber")
  @Expose
  private String MobileNumber;
  @SerializedName("Managername")
  @Expose
  private String Managername;
  @SerializedName("State")
  @Expose
  private String State;
  @SerializedName("Region")
  @Expose
  private String Region;
  @SerializedName("insertdate")
  @Expose
  private String insertdate;
  @SerializedName("Managercode")
  @Expose
  private String Managercode;
  @SerializedName("Password")
  @Expose
  private String Password;
  public void setEmailId(String EmailId){
   this.EmailId=EmailId;
  }
  public String getEmailId(){
   return EmailId;
  }
  public void setLandlineNumber(String LandlineNumber){
   this.LandlineNumber=LandlineNumber;
  }
  public String getLandlineNumber(){
   return LandlineNumber;
  }
  public void setManagertype(String managertype){
   this.managertype=managertype;
  }
  public String getManagertype(){
   return managertype;
  }
  public void setDeleteflag(String deleteflag){
   this.deleteflag=deleteflag;
  }
  public String getDeleteflag(){
   return deleteflag;
  }
  public void setEmployeetype(String employeetype){
   this.employeetype=employeetype;
  }
  public String getEmployeetype(){
   return employeetype;
  }
  public void setUserName(String UserName){
   this.UserName=UserName;
  }
  public String getUserName(){
   return UserName;
  }
  public void setAddress(String Address){
   this.Address=Address;
  }
  public String getAddress(){
   return Address;
  }
  public void setCity(String City){
   this.City=City;
  }
  public String getCity(){
   return City;
  }
  public void setDeletedate(String deletedate){
   this.deletedate=deletedate;
  }
  public Object getDeletedate(){
   return deletedate;
  }
  public void setR_Id(Integer R_Id){
   this.R_Id=R_Id;
  }
  public Integer getR_Id(){
   return R_Id;
  }
  public void setMobileNumber(String MobileNumber){
   this.MobileNumber=MobileNumber;
  }
  public String getMobileNumber(){
   return MobileNumber;
  }
  public void setManagername(String Managername){
   this.Managername=Managername;
  }
  public String getManagername(){
   return Managername;
  }
  public void setState(String State){
   this.State=State;
  }
  public String getState(){
   return State;
  }
  public void setRegion(String Region){
   this.Region=Region;
  }
  public String getRegion(){
   return Region;
  }
  public void setInsertdate(String insertdate){
   this.insertdate=insertdate;
  }
  public String getInsertdate(){
   return insertdate;
  }
  public void setManagercode(String Managercode){
   this.Managercode=Managercode;
  }
  public String getManagercode(){
   return Managercode;
  }
  public void setPassword(String Password){
   this.Password=Password;
  }
  public String getPassword(){
   return Password;
  }
}