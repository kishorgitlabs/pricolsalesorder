package api.models.DetailsRegion;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;
/**
 * Awesome Pojo Generator
 * */
public class RegionSalesHoleData{
  @SerializedName("result")
  @Expose
  private String result;
  @SerializedName("data")
  @Expose
  private List<RegionSalesHole> data;
  public void setResult(String result){
   this.result=result;
  }
  public String getResult(){
   return result;
  }
  public void setData(List<RegionSalesHole> data){
   this.data=data;
  }
  public List<RegionSalesHole> getData(){
   return data;
  }
}