package api.models;



import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
@Entity(tableName = "Dealer")
public class deal {

    @ColumnInfo(name = "Approve")
    @SerializedName("Approve")
    private String mApprove;
    @ColumnInfo(name = "ContactPersonname")
    @SerializedName("ContactPersonname")
    private String mContactPersonname;
    @ColumnInfo(name = "DaelerRegion")
    @SerializedName("DaelerRegion")
    private String mDaelerRegion;
    @ColumnInfo(name = "DealerAddress")
    @SerializedName("DealerAddress")
    private String mDealerAddress;
    @ColumnInfo(name = "DealerCity")
    @SerializedName("DealerCity")
    private String mDealerCity;
    @ColumnInfo(name = "DealerEmail")
    @SerializedName("DealerEmail")
    private String mDealerEmail;
    @ColumnInfo(name = "DealerMobile")
    @SerializedName("DealerMobile")
    private String mDealerMobile;
    @ColumnInfo(name = "DealerName")
    @SerializedName("DealerName")
    private String mDealerName;
    @ColumnInfo(name = "DealerState")
    @SerializedName("DealerState")
    private String mDealerState;
    @ColumnInfo(name = "deleteflag")
    @SerializedName("deleteflag")
    private String mDeleteflag;
    @ColumnInfo(name = "Id")
    @PrimaryKey(autoGenerate = true)
    @SerializedName("id")
    private Long mId;
    @ColumnInfo(name = "Insertdate")
    @SerializedName("Insertdate")
    private String mInsertdate;
    @ColumnInfo(name = "RegName")
    @SerializedName("RegName")
    private String mRegName;
    @ColumnInfo(name = "Regid")
    @SerializedName("Regid")
    private String mRegid;
    @ColumnInfo(name = "ShopName")
    @SerializedName("ShopName")
    private String mShopName;
    @ColumnInfo(name = "Status")
    @SerializedName("Status")
    private String mStatus;
    @ColumnInfo(name = "UpdateDate")
    @SerializedName("UpdateDate")
    private String mUpdateDate;
    @ColumnInfo(name = "UserType")
    @SerializedName("UserType")
    private String mUserType;
    @ColumnInfo(name = "ExecutId")
    @SerializedName("ExecutId")
    private String ExecutId;
    @ColumnInfo(name = "GST")
    @SerializedName("GST")
    private String GST;
    public String getGST() {return GST;}
    public void setGST(String GST) {this.GST = GST;}
    public String getExecutId() {
        return ExecutId;
    }
    public void setExecutId(String executId) {
        ExecutId = executId;
    }
    public String getApprove() {
        return mApprove;
    }

    public void setApprove(String approve) {
        mApprove = approve;
    }

    public String getContactPersonname() {
        return mContactPersonname;
    }

    public void setContactPersonname(String contactPersonname) {
        mContactPersonname = contactPersonname;
    }

    public String getDaelerRegion() {
        return mDaelerRegion;
    }

    public void setDaelerRegion(String daelerRegion) {
        mDaelerRegion = daelerRegion;
    }

    public String getDealerAddress() {
        return mDealerAddress;
    }

    public void setDealerAddress(String dealerAddress) {
        mDealerAddress = dealerAddress;
    }

    public String getDealerCity() {
        return mDealerCity;
    }

    public void setDealerCity(String dealerCity) {
        mDealerCity = dealerCity;
    }

    public String getDealerEmail() {
        return mDealerEmail;
    }

    public void setDealerEmail(String dealerEmail) {
        mDealerEmail = dealerEmail;
    }

    public String getDealerMobile() {
        return mDealerMobile;
    }

    public void setDealerMobile(String dealerMobile) {
        mDealerMobile = dealerMobile;
    }

    public String getDealerName() {
        return mDealerName;
    }

    public void setDealerName(String dealerName) {
        mDealerName = dealerName;
    }

    public String getDealerState() {
        return mDealerState;
    }

    public void setDealerState(String dealerState) {
        mDealerState = dealerState;
    }

    public String getDeleteflag() {
        return mDeleteflag;
    }

    public void setDeleteflag(String deleteflag) {
        mDeleteflag = deleteflag;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getInsertdate() {
        return mInsertdate;
    }

    public void setInsertdate(String insertdate) {
        mInsertdate = insertdate;
    }

    public String getRegName() {
        return mRegName;
    }

    public void setRegName(String regName) {
        mRegName = regName;
    }

    public String getRegid() {
        return mRegid;
    }

    public void setRegid(String regid) {
        mRegid = regid;
    }

    public String getShopName() {
        return mShopName;
    }

    public void setShopName(String shopName) {
        mShopName = shopName;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getUpdateDate() {
        return mUpdateDate;
    }

    public void setUpdateDate(String updateDate) {
        mUpdateDate = updateDate;
    }

    public String getUserType() {
        return mUserType;
    }

    public void setUserType(String userType) {
        mUserType = userType;
    }

}
