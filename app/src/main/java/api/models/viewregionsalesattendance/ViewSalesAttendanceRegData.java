package api.models.viewregionsalesattendance;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;
/**
 * Awesome Pojo Generator
 * */
public class ViewSalesAttendanceRegData{
  @SerializedName("result")
  @Expose
  private String result;
  @SerializedName("data")
  @Expose
  private List<ViewSalesAttendancereg> data;
  public void setResult(String result){
   this.result=result;
  }
  public String getResult(){
   return result;
  }
  public void setData(List<ViewSalesAttendancereg> data){
   this.data=data;
  }
  public List<ViewSalesAttendancereg> getData(){
   return data;
  }
}