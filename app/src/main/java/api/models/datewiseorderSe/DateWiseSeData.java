package api.models.datewiseorderSe;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class DateWiseSeData{
  @SerializedName("result")
  @Expose
  private String result;
  @SerializedName("data")
  @Expose
  private DateWiseSe data;
  public void setResult(String result){
   this.result=result;
  }
  public String getResult(){
   return result;
  }
  public void setData(DateWiseSe data){
   this.data=data;
  }
  public DateWiseSe getData(){
   return data;
  }
}