package api.models.datewiseorderSe;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class Order_list{
  @SerializedName("OrderedDate")
  @Expose
  private String OrderedDate;
  @SerializedName("orderType")
  @Expose
  private String orderType;
  @SerializedName("dealername")
  @Expose
  private String dealername;
  @SerializedName("orderid")
  @Expose
  private String orderid;
  @SerializedName("executivename")
  @Expose
  private String executivename;
  @SerializedName("DistributorName")
  @Expose
  private String DistributorName;
  @SerializedName("unitprice")
  @Expose
  private String unitprice;

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    @SerializedName("quantity")
  @Expose
  private String quantity;


  public void setOrderedDate(String OrderedDate){
   this.OrderedDate=OrderedDate;
  }
  public String getOrderedDate(){
   return OrderedDate;
  }
  public void setOrderType(String orderType){
   this.orderType=orderType;
  }
  public String getOrderType(){
   return orderType;
  }
  public void setDealername(String dealername){
   this.dealername=dealername;
  }
  public String getDealername(){
   return dealername;
  }
  public void setOrderid(String orderid){
   this.orderid=orderid;
  }
  public String getOrderid(){
   return orderid;
  }
  public void setExecutivename(String executivename){
   this.executivename=executivename;
  }
  public String getExecutivename(){
   return executivename;
  }
  public void setDistributorName(String DistributorName){
   this.DistributorName=DistributorName;
  }
  public String getDistributorName(){
   return DistributorName;
  }
  public void setUnitprice(String unitprice){
   this.unitprice=unitprice;
  }
  public String getUnitprice(){
   return unitprice;
  }
}