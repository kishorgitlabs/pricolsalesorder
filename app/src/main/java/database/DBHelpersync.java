package database;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.brainmagic.pricolsalesorder.SalesOrderExistingActivity;

import SQL_Fields.SQLITE;

public class DBHelpersync extends SQLiteOpenHelper {
	public Context context;
	SQLiteDatabase sqLiteDatabase;
	private static final int DATABASE_VERSION = 2;



	@SuppressLint({"SdCardPath"})
	public DBHelpersync(Context context) {

		super(context, SQLITE.DATABASE_NAME, null, DATABASE_VERSION);
		this.sqLiteDatabase = null;
		this.context = context;
	}

	//createcoocrdinates table
	public void onCreate(SQLiteDatabase sqLiteDatabase) {
		/*String CREATE_TABLE_CORDINATES = "CREATE TABLE IF NOT EXISTS " +
				TABLE_CORDINATES + "("
				+ SQLITE.COLUMN_S_id +" INTEGER  , "+ SQLITE.COLUMN_saname + " TEXT , "+ SQLITE.COLUMN_latitude+" TEXT , "+ SQLITE.COLUMN_langtitude + " TEXT , "
				+ SQLITE.COLUMN_cityname + " TEXT , "+ SQLITE.COLUMN_description + " TEXT , "+ SQLITE.COLUMN_datetime + " TEXT , "+ SQLITE.COLUMN_timedate + " TEXT "
				+")";
		Log.v("CREATE_TABLE_CORDINATES",CREATE_TABLE_CORDINATES);
		sqLiteDatabase.execSQL(CREATE_TABLE_CORDINATES);*/
	}




//	public void CreateCustomerProfile(SQLiteDatabase sqLiteDatabase)
//	{
//
//		String CREATE_CUSTOMER_TABLE = "CREATE TABLE IF NOT EXISTS " +
//				SQLITE.TABLE_Create_Profile + "("
//				+SQLITE.COLUMN_Create_cusname+ " TEXT  ,"+ SQLITE.COLUMN_Create_address+ " TEXT ,"+ SQLITE.COLUMN_Create_city + " TEXT," +SQLITE.COLUMN_Create_state
//				+ " TEXT ,"+SQLITE.COLUMN_Create_pincode + " TEXT ,"+ SQLITE.COLUMN_Create_contact + " TEXT ,"+ SQLITE.COLUMN_Create_contactname
//				+ " TEXT ,"+SQLITE.COLUMN_Create_email + " TEXT ,"+ SQLITE.COLUMN_Create_gst + " TEXT ,"+ SQLITE.COLUMN_Create_pan + " TEXT ,"+ SQLITE.COLUMN_Create_deliveryaddress
//				+ " TEXT ,"+ SQLITE.COLUMN_Create_custype + " TEXT "
//				+")";
//		sqLiteDatabase.execSQL(CREATE_CUSTOMER_TABLE);
//	}





	public void CreateAddorderTable(SQLiteDatabase sqLiteDatabase)
	{
		String CREATE_NEW_ADDITEM_TABLE = "CREATE TABLE IF NOT EXISTS " +
				SQLITE.TABLE_Additems + "("+SQLITE.COLUMN_Addid+ " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE ,"
				+SQLITE.COLUMN_Addexecutive+ " TEXT  ,"+ SQLITE.COLUMN_AddcustomerType+ " TEXT ,"+ SQLITE.COLUMN_Productcat + " TEXT,"+ SQLITE.COLUMN_Addproductname + " TEXT," +SQLITE.COLUMN_Addpackage
				+ " TEXT ,"+SQLITE.COLUMN_Addquantity + " TEXT ,"+ SQLITE.COLUMN_Adddiscount + " TEXT ,"+ SQLITE.COLUMN_AddOrderType
				+ " TEXT ,"+SQLITE.COLUMN_AddMobileNO + " TEXT ,"+SQLITE.COLUMN_AddNames+ " TEXT ,"+ SQLITE.COLUMN_Addstring_mail +" TEXT ,"+ SQLITE.COLUMN_AddRemarks
				+ " TEXT ,"+ SQLITE.COLUMN_Addcurrentdate + " TEXT ,"+ SQLITE.COLUMN_Addstring_ordermode + " TEXT , "+ SQLITE.COLUMN_Addstring_ordernumber + " TEXT , "+ SQLITE.COLUMN_Addstring_ordercolors + " TEXT , "+SQLITE.COLUMN_Unpackage + " TEXT ,"+ SQLITE.COLUMN_RetState + " TEXT , "+ SQLITE.COLUMN_RetCity + " TEXT , "+ SQLITE.COLUMN_RetAddress + " TEXT , "+ SQLITE.COLUMN_RetEmail + " TEXT , "+ SQLITE.COLUMN_RetRegion + " TEXT ,"+ SQLITE.COLUMN_CUSTOMERNAME + " TEXT ,"+ SQLITE.COLUMN_Addcustomercode + " TEXT " +")";
		       sqLiteDatabase.execSQL(CREATE_NEW_ADDITEM_TABLE);

	}

	public void CreateExistingCustomerAddorderTable(SQLiteDatabase sqLiteDatabase)
	{
		String CREATE_NEW_ADDITEM_TABLE = "CREATE TABLE IF NOT EXISTS " +
				SalesOrderExistingActivity.TABLE_Additem + "("+SQLITE.COLUMN_Addid+ " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE ,"
				+SQLITE.COLUMN_Addexecutive+ " TEXT  ,"+ SQLITE.COLUMN_AddcustomerType+ " TEXT ,"+ SQLITE.COLUMN_Productcat + " TEXT,"+ SQLITE.COLUMN_Addproductname + " TEXT," +SQLITE.COLUMN_Addpackage
				+ " TEXT ,"+SQLITE.COLUMN_Addquantity + " TEXT ,"+ SQLITE.COLUMN_Adddiscount + " TEXT ,"+ SQLITE.COLUMN_AddOrderType
				+ " TEXT ,"+SQLITE.COLUMN_AddMobileNO + " TEXT ,"+SQLITE.COLUMN_AddNames+ " TEXT ,"+ SQLITE.COLUMN_Addstring_mail +" TEXT ,"+ SQLITE.COLUMN_AddRemarks
				+ " TEXT ,"+ SQLITE.COLUMN_Addcurrentdate + " TEXT ,"+ SQLITE.COLUMN_Addstring_ordermode + " TEXT , "+ SQLITE.COLUMN_Addstring_ordernumber + " TEXT , "+ SQLITE.COLUMN_Addstring_ordercolors + " TEXT , "+SQLITE.COLUMN_Unpackage + " TEXT ,"+ SQLITE.COLUMN_RetState + " TEXT , "+ SQLITE.COLUMN_RetCity + " TEXT , "+ SQLITE.COLUMN_RetAddress + " TEXT , "+ SQLITE.COLUMN_RetEmail + " TEXT , "+ SQLITE.COLUMN_RetRegion + " TEXT ,"+ SQLITE.COLUMN_CUSTOMERNAME + " TEXT ,"+ SQLITE.COLUMN_Addcustomercode + " TEXT " +")";
		sqLiteDatabase.execSQL(CREATE_NEW_ADDITEM_TABLE);

	}
	public void Createsalesordernewtable(SQLiteDatabase sqLiteDatabase)
	{

		String CREATE_NEW_SALES_TABLE = "CREATE TABLE IF NOT EXISTS " +
				SQLITE.TABLE_NewPartsOrder + "("+SQLITE.COLUMN_Newid+ " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE ,"
				+SQLITE.COLUMN_executivename+ " TEXT  ,"+ SQLITE.COLUMN_dealername + " TEXT," +SQLITE.COLUMN_address
				+ " TEXT ,"+SQLITE.COLUMN_city + " TEXT ,"+ SQLITE.COLUMN_state + " TEXT ,"+ SQLITE.COLUMN_pincode + " TEXT ,"+ SQLITE.COLUMN_contact
				+ " TEXT ,"+SQLITE.COLUMN_contactname + " TEXT ,"+ SQLITE.COLUMN_email + " TEXT ,"+ SQLITE.COLUMN_gst
				+ " TEXT ,"+SQLITE.COLUMN_pan + " TEXT ,"+ SQLITE.COLUMN_custype + " TEXT ,"+ SQLITE.COLUMN_prodname + " TEXT ,"+ SQLITE.COLUMN_pack
				+ " TEXT ,"+SQLITE.COLUMN_quant + " TEXT ,"+ SQLITE.COLUMN_discounts + " TEXT ,"+ SQLITE.COLUMN_orderType + " TEXT ,"+ SQLITE.COLUMN_remark
				+ " TEXT ,"+ SQLITE.COLUMN_date + " TEXT ,"+ SQLITE.COLUMN_deliveryaddress + " TEXT ,"+ SQLITE.COLUMN_ordermode + " TEXT , "+ SQLITE.COLUMN_ordernumber + " TEXT , "+ SQLITE.COLUMN_ordercolours + " TEXT ,"+ SQLITE.COLUMN_Productsubname + " TEXT ,"+ SQLITE.Coloumn_umpackage + " TEXT "
				+")";
		sqLiteDatabase.execSQL(CREATE_NEW_SALES_TABLE);
	}
	@Override
	public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
		/*sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_CORDINATES);
		onCreate(sqLiteDatabase);*/
	}
















	public Boolean updateproduct(int i, ContentValues values, SQLiteDatabase db) {
		// TODO Auto-generated method stub
		//return db.update(SQLITE.TABLE_CORDINATES, values, SQLITE.COLUMN_S_id + "=" + i, null) > 0;
		return null;
	}
}

