package database;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.core.view.accessibility.AccessibilityNodeInfoCompat;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import SQL_Fields.SQLITE;

public class DBHelper extends SQLiteOpenHelper {
    private static final String DATABASENAME = "pricol";
    //@SuppressLint({"SdCardPath"})
    private static String DATABASE_PATH = null;
    private static final String FILENAME = "pricol";
    private static int version=1;
    private String INPUT_DB_PATH;
    private Context context;
    SQLiteDatabase sqLiteDatabase;


    @SuppressLint({"SdCardPath"})
    public DBHelper(Context context) {
        super(context, FILENAME, null, version);
        this.sqLiteDatabase = null;
        this.context = context;
        DATABASE_PATH = "/data/data/" + context.getPackageName() + "/Pricol/";
    }

    public SQLiteDatabase readDataBase() {
        try {
            InputStream in = this.context.getAssets().open(FILENAME);
            Log.e("sample", "Starting copying");
            File databaseFile = new File("/data/data/" + context.getPackageName() + "/Pricol/");
            if (!databaseFile.exists()) {
                boolean check = databaseFile.mkdirs();
                Log.v("Folder", "Created");
                Log.v("Folder", Boolean.toString(check));

                OutputStream out = new FileOutputStream(DATABASE_PATH + FILENAME);
                byte[] buffer = new byte[AccessibilityNodeInfoCompat.ACTION_NEXT_HTML_ELEMENT];
                while (true) {
                    int length = in.read(buffer);
                    if (length <= 0) {
                        break;
                    }
                    out.write(buffer, 0, length);
                }
                Log.e("sample", "Completed");
                out.flush();
                out.close();
                in.close();
            }
            this.sqLiteDatabase = SQLiteDatabase.openDatabase(DATABASE_PATH + FILENAME, null, 268435472);
        } catch (Exception e) {
            Log.v("errorr", e.getMessage());
        }
        return this.sqLiteDatabase;
    }

    public void onCreate(SQLiteDatabase sqLiteDatabase) {
    }

    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
    }

    public void CreateRetailerProfile(SQLiteDatabase sqLiteDatabase)
    {

        String CREATE_CUSTOMER_TABLE = "CREATE TABLE IF NOT EXISTS " +
                SQLITE.TABLE_Create_Profile + "("
                +SQLITE.COLUMN_Create_retname+ " TEXT  ,"+ SQLITE.COLUMN_Create_retstate+ " TEXT ,"+ SQLITE.COLUMN_Create_retcity + " TEXT," +SQLITE.COLUMN_Create_retregion
                + " TEXT ,"+SQLITE.COLUMN_Create_retaddress+ " TEXT ,"+ SQLITE.COLUMN_Create_retmobile+ " TEXT ,"+ SQLITE.COLUMN_Create_retemail + " TEXT ,"+ SQLITE.COLUMN_Create_retsalesid + " TEXT "+")";

        sqLiteDatabase.execSQL(CREATE_CUSTOMER_TABLE);
    }


}
