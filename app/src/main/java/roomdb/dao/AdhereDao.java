package roomdb.dao;



import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import api.models.deal;
import api.models.master.Citylist;
import api.models.master.Customer_list;
import api.models.master.Distributor_list;
import api.models.master.Productlist;
import api.models.master.Setting;
import api.models.master.Statelist;
import api.models.master.createlist;
import roomdb.entity.ExistingCustomerList;
import roomdb.entity.SecondaryOrderDetails;


@Dao
public interface AdhereDao {
    // Deteting data
    @Query("DELETE FROM Customer")
    void DeleteAllCustomer();

    @Query("DELETE FROM city")
    void DeleteAllCity();

    @Query("DELETE FROM products")
    void DeleteAllProduct();

    @Query("DELETE FROM state")
    void DeleteAllstate();

    @Query("DELETE FROM Distributor")
    void DeleteAllDistreibut();

    @Query("DELETE FROM Dealer")
    void DeleteAllDeal();

    @Query("DELETE FROM setting")
    void DeleteAllSetting();

    @Query("DELETE FROM CustomerDetails")
    void DeleteAllcustomer();

    @Query("DELETE FROM CustomerDetails where MobileNo=:number")
            void DeleteCustomer(String number);



    // Inserting data
    @Insert
    void insertAllCustomer(List<Customer_list> customer);

    @Insert
    void insertCustomerDetails(SecondaryOrderDetails secondaryOrderDetails);
    @Insert
    void insertExistingCustomer(ExistingCustomerList existingCustomerList);

    @Query("SELECT * FROM CustomerDetails")
    List<SecondaryOrderDetails> customerDetails();

    @Query("SELECT * FROM ExistingCustomerlist")
    List<ExistingCustomerList> existingCustomerDetails();

    @Insert
    void insertAllSetting(List<Setting> setting);

    @Insert
    void insertAllDistributor(List<Distributor_list> distributor);

    @Insert
    void insertAllstate(List<Statelist> state);

    @Insert
    void insertAllcity(List<Citylist> city);

    @Insert
    void insertAllproduct(List<Productlist> product);

    @Insert
    void insertAllDeal(List<deal> deal);

    @Insert
    void insertAllregion(createlist create);

    // Getting data
    @Query("SELECT * FROM Customer")
    List<Customer_list> GetCustomer_list();

    @Query("SELECT * FROM state")
    List<Statelist> GetStatelist();



    @Query("SELECT state1 FROM state where Region = :region")
    List<String> sGetRegion(String region);

    @Query("SELECT * FROM city")
    List<Citylist> GetCitylist();



    // Getting data
    @Query("SELECT * FROM Customer where Name = :customername")
    List<Customer_list> GetCustomer_list_Name(String customername);


    // Getting data
    @Query("SELECT * FROM Distributor where statename = :states AND townnme = :city")
    List<Distributor_list> GetDistributorlist(String states,String city);



    // Getting data
    @Query("SELECT Name  FROM Customer")
    List<String> GetCustomer_name_list();

    // Getting data
    @Query("SELECT distinct segments  FROM products")
    List<String> GetProdutlist();

    @Query("SELECT distinct category FROM products where segments = :segments")
    List<String> GetProductcat(String segments);

    @Query("SELECT * FROM products where segments = :segments AND category = :category")
    List<Productlist> Getpartno(String segments, String category);

    @Query("SELECT * FROM Distributor")
    List<Distributor_list> GetDistilist();

    @Query("SELECT distinct statename FROM Distributor where protype = :protype")
    List<String> GetStateDistilist(String protype);

    @Query("SELECT * FROM Dealer")
    List<deal> GetDeallist();

    @Query("SELECT * FROM products")
    List<Productlist> GetPartlist();

    @Query("SELECT distinct partno  FROM products where type = :type")
    List<String> GetPartnolist(String type);

    @Query("SELECT  * FROM products where partno = :partno")
    List<Productlist> Getprducts(String partno);

    @Query("SELECT * FROM Dealer where UserType = :usertype")
    List<deal> GetDeallist(String usertype );

    // Getting data
    @Query("SELECT *  FROM city where state = :state")
    List<Citylist> GetCitystring(String state);

    // Getting data
    @Query("SELECT *  FROM createlist where typeofacc = :types")
    List<createlist> GetCrestestring(String types);

    // Getting data
    @Query("SELECT *  FROM createlist where mobilenumber = :mobiles")
    List<createlist> GetCrestemobile(String mobiles);


    // Getting data
    @Query("SELECT *  FROM Dealer where DealerMobile = :mobiles")
    List<deal> GetCrestemobile1(String mobiles);

    @Query("SELECT DealerMobile  FROM Dealer")
    List<String> Getalreadycusmobile();



// Getting locallycreated dealer mobile

    @Query("SELECT mobilenumber  FROM createlist")
    List<String> Getlocallycreateddealer();


    @Query("SELECT * FROM Distributor where statename = :state ")
    List<Distributor_list> GetDistilist1(String state);

    @Query("SELECT distinct townnme FROM Distributor where statename = :state ")
    List<String> GetCityDistilist1(String state);

    @Query("SELECT distinct * FROM Distributor where townnme = :city ")
    List<Distributor_list> GetNameDistilist1(String city);

    @Query("SELECT distinct Id FROM Distributor where disname = :name ")
    List<Integer> GetIdDistilist1(String name);


    // Getting data
    @Query("SELECT *  FROM createlist")
    List<createlist> GetCustomer_field();
    // Getting data
    @Query("DELETE FROM createlist where mobilenumber = :mobilenumber")
    void DeleteAllCreate(String mobilenumber);

    @Query("DELETE FROM CustomerDetails")
    void DeleteAllsecondary();

//    @Query("DELETE FROM SecondaryOrderDetails")
//    void DeleteAllCustomerdetails();

    @Query("DELETE FROM ExistingCustomerlist")
    void DeleteAllExistingCustomer();

    // Getting data
    @Query("SELECT DealerName  FROM dealer")
    List<String> GetCustomersie();

    @Query("SELECT * FROM Dealer")

    List<deal> GetDeal();

    @Query("SELECT * FROM dealer where DealerName = :dealername")

    List<deal> GetCustomerlist(String dealername);

    // Getting data
    @Query("SELECT name FROM createlist where typeofacc=:type")
    List<String> Getcusname(String type);



//getting dealer name

    @Query("SELECT DealerName  FROM Dealer where UserType=:type")

    List<String> Getdealername(String type);


// Getting data

    @Query("SELECT mobilenumber FROM createlist where name =:name AND typeofacc=:types")

    String Getmon(String name,String types);

//getting dealer name

    @Query("SELECT DealerMobile  FROM Dealer  where DealerName=:name AND UserType=:type")

    String GetCustomermobile(String type,String name);

    @Query("SELECT DealerMobile  FROM Dealer  where  UserType=:type")
    List<String>GetCustomermobileexisting(String type);


    @Query("SELECT DealerName  FROM Dealer  where  DealerMobile=:mobile")
    String GetShopname(String mobile);

    @Query("SELECT ContactPersonname  FROM Dealer  where  DealerMobile=:mobile")
    String GetCpn(String mobile);

    @Query("SELECT DealerState  FROM Dealer  where  DealerMobile=:mobile")
    String GetState(String mobile);

    @Query("SELECT DealerCity  FROM Dealer  where  DealerMobile=:mobile")
    String GetCity(String mobile);

    @Query("SELECT DealerEmail  FROM Dealer  where  DealerMobile=:mobile")
    String Getemail(String mobile);

    @Query("SELECT DealerAddress  FROM Dealer  where  DealerMobile=:mobile")
    String GetAddress(String mobile);

    @Query("SELECT GST  FROM Dealer  where  DealerMobile=:mobile")
    String GetGst(String mobile);





//    @Query("SELECT DealerAddress  FROM Dealer  where  DealerMobile=:mobile")
//    String GetAddress(String mobile);


}
