package roomdb.entity;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Created by SYSTEM10 on 6/1/2019.
 */

@Entity(tableName = "ExistingCustomerlist")
public class ExistingCustomerList {


    @ColumnInfo(name = "Id")
    @PrimaryKey(autoGenerate = true)
    private int id;
    @ColumnInfo(name = "OrderType")
    String OrderType;
    @ColumnInfo(name = "CustomerType")
    String CustomerType;
    @ColumnInfo(name = "CustomerName")
    String CustomerName;
    @ColumnInfo(name = "MobileNo")
    String MobileNo;

    @ColumnInfo(name = "state")
    String state;
    @ColumnInfo(name = "city")
    String city;
    @ColumnInfo(name = "address")
    String address;
    @ColumnInfo(name = "region")
    String region;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    public String getOrderType() {
        return OrderType;
    }

    public void setOrderType(String orderType) {
        OrderType = orderType;
    }

    public String getCustomerType() {
        return CustomerType;
    }

    public void setCustomerType(String customerType) {
        CustomerType = customerType;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getMobileNo() {
        return MobileNo;
    }

    public void setMobileNo(String mobileNo) {
        MobileNo = mobileNo;
    }
}
