package roomdb.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Awesome Pojo Generator
 */
/*@Entity(foreignKeys = {
        @ForeignKey(entity = Cust.class,
                parentColumns = "id",
                childColumns = "CustID"),
        @ForeignKey(entity = Site.class,
                parentColumns = "id",
                childColumns = "SiteId")
})*/
@Entity
public class Allocate {


    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private  int id;
    @ColumnInfo(name = "Site")
    private String Site;
    @ColumnInfo(name = "SiteId")
    private String SiteId;
    @ColumnInfo(name = "SiteIncharge")
    private String SiteIncharge;
    @ColumnInfo(name = "InsertDate")
    private String InsertDate;
    @ColumnInfo(name = "Inverter5")
    private String Inverter5;
    @ColumnInfo(name = "Inverter6")
    private String Inverter6;
    @ColumnInfo(name = "Inverter3")
    private String Inverter3;
    @ColumnInfo(name = "Inverter4")
    private String Inverter4;
    @ColumnInfo(name = "Inverter9")
    private String Inverter9;
    @ColumnInfo(name = "Inverter7")
    private String Inverter7;
    @ColumnInfo(name = "Inverter8")
    private String Inverter8;
    @ColumnInfo(name = "Inverter11")
    private String Inverter11;
    @ColumnInfo(name = "SiteInchargeMobile")
    private String SiteInchargeMobile;
    @ColumnInfo(name = "Inverter10")
    private String Inverter10;
    @ColumnInfo(name = "UpdateDate")
    private String UpdateDate;
    @ColumnInfo(name = "Plant")
    private String Plant;
    @ColumnInfo(name = "Inverter1")
    private String Inverter1;
    @ColumnInfo(name = "Inverter15")
    private String Inverter15;
    @ColumnInfo(name = "Inverter2")
    private String Inverter2;
    @ColumnInfo(name = "Inverter14")
    private String Inverter14;
    @ColumnInfo(name = "Inverter13")
    private String Inverter13;
    @ColumnInfo(name = "Inverter12")
    private String Inverter12;
    @ColumnInfo(name = "Inverter19")
    private String Inverter19;
    @ColumnInfo(name = "Inverter18")
    private String Inverter18;
    @ColumnInfo(name = "Inverter17")
    private String Inverter17;
    @ColumnInfo(name = "CustomerName")
    private String CustomerName;
    @ColumnInfo(name = "Inverter16")
    private String Inverter16;
    @ColumnInfo(name = "Status")
    private String Status;
    @ColumnInfo(name = "CustomerMobile")
    private String CustomerMobile;
    @ColumnInfo(name = "SiteInchargeID")
    private String SiteInchargeID;
    @ColumnInfo(name = "Block_Id")
    private String Block_Id;
    @ColumnInfo(name = "Inverter20")
    private String Inverter20;
    @ColumnInfo(name = "CustID")
    private String CustID;
    @ColumnInfo(name = "Block")
    private String Block;

    public void setSite(String Site) {
        this.Site = Site;
    }

    public String getSite() {
        return Site;
    }

    public void setSiteId(String SiteId) {
        this.SiteId = SiteId;
    }

    public String getSiteId() {
        return SiteId;
    }

    public void setSiteIncharge(String SiteIncharge) {
        this.SiteIncharge = SiteIncharge;
    }

    public String getSiteIncharge() {
        return SiteIncharge;
    }

    public void setInsertDate(String InsertDate) {
        this.InsertDate = InsertDate;
    }

    public String getInsertDate() {
        return InsertDate;
    }

    public void setInverter5(String Inverter5) {
        this.Inverter5 = Inverter5;
    }

    public String getInverter5() {
        return Inverter5;
    }

    public void setInverter6(String Inverter6) {
        this.Inverter6 = Inverter6;
    }

    public String getInverter6() {
        return Inverter6;
    }

    public void setInverter3(String Inverter3) {
        this.Inverter3 = Inverter3;
    }

    public String getInverter3() {
        return Inverter3;
    }

    public void setInverter4(String Inverter4) {
        this.Inverter4 = Inverter4;
    }

    public String getInverter4() {
        return Inverter4;
    }

    public void setInverter9(String Inverter9) {
        this.Inverter9 = Inverter9;
    }

    public String getInverter9() {
        return Inverter9;
    }

    public void setInverter7(String Inverter7) {
        this.Inverter7 = Inverter7;
    }

    public String getInverter7() {
        return Inverter7;
    }

    public void setInverter8(String Inverter8) {
        this.Inverter8 = Inverter8;
    }

    public String getInverter8() {
        return Inverter8;
    }

    public void setInverter11(String Inverter11) {
        this.Inverter11 = Inverter11;
    }

    public String getInverter11() {
        return Inverter11;
    }

    public void setSiteInchargeMobile(String SiteInchargeMobile) {
        this.SiteInchargeMobile = SiteInchargeMobile;
    }

    public String getSiteInchargeMobile() {
        return SiteInchargeMobile;
    }

    public void setInverter10(String Inverter10) {
        this.Inverter10 = Inverter10;
    }

    public String getInverter10() {
        return Inverter10;
    }

    public void setUpdateDate(String UpdateDate) {
        this.UpdateDate = UpdateDate;
    }

    public String getUpdateDate() {
        return UpdateDate;
    }

    public void setPlant(String Plant) {
        this.Plant = Plant;
    }

    public String getPlant() {
        return Plant;
    }

    public void setInverter1(String Inverter1) {
        this.Inverter1 = Inverter1;
    }

    public String getInverter1() {
        return Inverter1;
    }

    public void setInverter15(String Inverter15) {
        this.Inverter15 = Inverter15;
    }

    public String getInverter15() {
        return Inverter15;
    }

    public void setInverter2(String Inverter2) {
        this.Inverter2 = Inverter2;
    }

    public String getInverter2() {
        return Inverter2;
    }

    public void setInverter14(String Inverter14) {
        this.Inverter14 = Inverter14;
    }

    public String getInverter14() {
        return Inverter14;
    }

    public void setInverter13(String Inverter13) {
        this.Inverter13 = Inverter13;
    }

    public String getInverter13() {
        return Inverter13;
    }

    public void setInverter12(String Inverter12) {
        this.Inverter12 = Inverter12;
    }

    public String getInverter12() {
        return Inverter12;
    }

    public void setInverter19(String Inverter19) {
        this.Inverter19 = Inverter19;
    }

    public String getInverter19() {
        return Inverter19;
    }

    public void setInverter18(String Inverter18) {
        this.Inverter18 = Inverter18;
    }

    public String getInverter18() {
        return Inverter18;
    }

    public void setInverter17(String Inverter17) {
        this.Inverter17 = Inverter17;
    }

    public String getInverter17() {
        return Inverter17;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setCustomerName(String CustomerName) {
        this.CustomerName = CustomerName;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setInverter16(String Inverter16) {
        this.Inverter16 = Inverter16;
    }

    public String getInverter16() {
        return Inverter16;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getStatus() {
        return Status;
    }

    public void setCustomerMobile(String CustomerMobile) {
        this.CustomerMobile = CustomerMobile;
    }

    public String getCustomerMobile() {
        return CustomerMobile;
    }

    public void setSiteInchargeID(String SiteInchargeID) {
        this.SiteInchargeID = SiteInchargeID;
    }

    public String getSiteInchargeID() {
        return SiteInchargeID;
    }

    public void setBlock_Id(String Block_Id) {
        this.Block_Id = Block_Id;
    }

    public String getBlock_Id() {
        return Block_Id;
    }

    public void setInverter20(String Inverter20) {
        this.Inverter20 = Inverter20;
    }

    public String getInverter20() {
        return Inverter20;
    }

    public void setCustID(String CustID) {
        this.CustID = CustID;
    }

    public String getCustID() {
        return CustID;
    }

    public void setBlock(String Block) {
        this.Block = Block;
    }

    public String getBlock() {
        return Block;
    }
}