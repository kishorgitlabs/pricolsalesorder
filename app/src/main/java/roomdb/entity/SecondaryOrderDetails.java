package roomdb.entity;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Created by SYSTEM10 on 6/1/2019.
 */

@Entity(tableName = "CustomerDetails")
public class SecondaryOrderDetails {


    @ColumnInfo(name = "Id")
    @PrimaryKey(autoGenerate = true)
    private int id;
    @ColumnInfo(name = "OrderType")
    String OrderType;
    @ColumnInfo(name = "CustomerType")
    String CustomerType;
    @ColumnInfo(name = "CustomerName")
    String CustomerName;
    @ColumnInfo(name = "MobileNo")
    String MobileNo;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    public String getOrderType() {
        return OrderType;
    }

    public void setOrderType(String orderType) {
        OrderType = orderType;
    }

    public String getCustomerType() {
        return CustomerType;
    }

    public void setCustomerType(String customerType) {
        CustomerType = customerType;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getMobileNo() {
        return MobileNo;
    }

    public void setMobileNo(String mobileNo) {
        MobileNo = mobileNo;
    }
}
