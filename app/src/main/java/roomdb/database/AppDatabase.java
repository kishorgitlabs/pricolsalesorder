package roomdb.database;



import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import api.models.deal;
import api.models.master.Citylist;
import api.models.master.Customer_list;
import api.models.master.Distributor_list;
import api.models.master.Productlist;
import api.models.master.Setting;
import api.models.master.Statelist;
import api.models.master.createlist;
import roomdb.dao.AdhereDao;
import roomdb.entity.ExistingCustomerList;
import roomdb.entity.SecondaryOrderDetails;

@Database(entities = {Customer_list.class, Distributor_list.class, Statelist.class, Citylist.class,Productlist.class, deal.class,createlist.class,Setting.class,SecondaryOrderDetails.class,ExistingCustomerList.class},version = 36,exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase INSTANCE;
    public abstract AdhereDao adhereDao();

    public static synchronized AppDatabase getAppDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "pricolsales_database")
                            // allow queries on the main thread.
                            // Don't do this on a real app! See PersistenceBasicSample for an example.
                            .allowMainThreadQueries()
                            .fallbackToDestructiveMigration()
                            .build();

        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }
}
