package alertbox;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.brainmagic.pricolsalesorder.R;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import network.NetworkConnection;
import persistance.ServerConnection;

import static android.content.Context.MODE_PRIVATE;

public class PrimaryOrder {

    Context context;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;

    //editor = myshare.edit();

    public PrimaryOrder(Context con) {
        // TODO Auto-generated constructor stub
        this.context = con;
    }

//	editor = myshare.edit();

    public void showLoginboxtoggle()
    {

        myshare = context.getSharedPreferences("pricolsalesorder", MODE_PRIVATE);
        editor = myshare.edit();
        final AlertDialog alertDialog = new AlertDialog.Builder(
                context).create();
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.primaryordertoggle, null);

        final ToggleButton primaryorder=dialogView.findViewById(R.id.primaryorder);
        primaryorder.setText(myshare.getString("toggletext","Disabled"));;
        primaryorder.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

//                if(myshare.getBoolean("primaryorder",false))
                {
                    primaryorder.setChecked(myshare.getBoolean("primaryorder",false));
                }
//                else {
//                    primaryorder.setChecked(true);
//                }

//                if (isChecked) {
//                    primaryorder.setChecked(true);
//                    editor.putString("toggletext","Enabled");
//                    editor.putBoolean("primaryorder",true);
//                    editor.commit();
//                    editor.apply();
//                }else {
//                    primaryorder.setChecked(false);
//                    editor.putString("toggletext","Disabled");
//                    editor.putBoolean("primaryorder",false);
//                    editor.commit();
//                    editor.apply();
//                }
            }
        });
        alertDialog.setView(dialogView);


        alertDialog.show();
    }




}
