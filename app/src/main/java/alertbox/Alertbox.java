package alertbox;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.brainmagic.pricolsalesorder.R;

/**
 * Created by SYSTEM10 on 4/17/2019.
 */

public class Alertbox {
    Context context;

    public Alertbox(Context con) {
        // TODO Auto-generated constructor stub
        this.context = con;
    }


    public void showAlertbox(String msg)
    {
        final AlertDialog alertDialog = new AlertDialog.Builder(
                context).create();

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alertbox, null);
        alertDialog.setView(dialogView);

        TextView log = (TextView) dialogView.findViewById(R.id.textView1);
        Button okay = (Button)  dialogView.findViewById(R.id.okay);
        log.setText(msg);
        okay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }


    public void showAlertboxwithback(String msg)
    {
        final AlertDialog alertDialog = new AlertDialog.Builder(
                context).create();

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alertbox, null);
        alertDialog.setView(dialogView);

        TextView log = (TextView) dialogView.findViewById(R.id.textView1);
        Button okay = (Button)  dialogView.findViewById(R.id.okay);
        log.setText(msg);
        okay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                alertDialog.dismiss();
//                ((Activity) context).onBackPressed();
            }
        });

        alertDialog.show();
    }

    public void showAlertBoxWithBack(String msg)
    {
        final AlertDialog alertDialog = new AlertDialog.Builder(
                context).create();

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alertbox, null);
        alertDialog.setView(dialogView);

        TextView log = (TextView) dialogView.findViewById(R.id.textView1);
        Button okay = (Button)  dialogView.findViewById(R.id.okay);
        log.setText(msg);
        okay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                alertDialog.dismiss();
                ((Activity) context).onBackPressed();
            }
        });

        alertDialog.show();
    }
}
