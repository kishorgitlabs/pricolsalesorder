package adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.brainmagic.pricolsalesorder.R;

import java.util.List;

import api.models.viewindividualhistory.ViewInd;

/**
 * Created by SYSTEM10 on 6/2/2019.
 */

public class ViewindivdualAdapterreg extends ArrayAdapter {
    private Context context;
    private List<ViewInd> data;

    public ViewindivdualAdapterreg(@NonNull Context context, List<ViewInd> data) {
        super(context, R.layout.viewpreview, data);
        this.context = context;
        this.data = data;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        convertView = null;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.viewpreview, parent, false);
            try {


                //  TextView sno=(TextView)convertView.findViewById(R.id.sno1);
                //   TextView name = (TextView) convertView.findViewById(R.id.namehis);
                TextView sno = (TextView) convertView.findViewById(R.id.sno);
                TextView partno = (TextView) convertView.findViewById(R.id.partno);
                TextView productname = (TextView) convertView.findViewById(R.id.productname);
                TextView segment = (TextView) convertView.findViewById(R.id.segment);
                TextView qty = (TextView) convertView.findViewById(R.id.qty);
                TextView amount = (TextView) convertView.findViewById(R.id.amount);
                TextView granttotal = (TextView) convertView.findViewById(R.id.granttotal);
//                TextView quantity = (TextView) convertView.findViewById(R.id.Quantity);
//                TextView mrp = (TextView) convertView.findViewById(R.id.total);
//                TextView partno = (TextView) convertView.findViewById(R.id.partno);
//                TextView ordervalues = (TextView) convertView.findViewById(R.id.ordervalues);
//                TextView disti = (TextView) convertView.findViewById(R.id.disti);
                sno.setText(Integer.toString(position + 1));
                partno.setText(data.get(position).getPartNo());
                productname.setText(data.get(position).getProductname());
                segment.setText(data.get(position).getSegment());
                qty.setText(data.get(position).getQuantity());
                amount.setText(data.get(position).getAmount());
                granttotal.setText(data.get(position).getGrandtotal());

//                productseg.setText(data.get(position).getSegment());
//                productcat.setText(data.get(position).getProductname());
//                quantity.setText(data.get(position).getQuantity());
//                partno.setText(data.get(position).getPartNo());
//                mrp.setText("₹"+" "+data.get(position).getAmount());
//                disti.setText(data.get(position).getDistributorName());
//                ordervalues.setText("₹"+" "+data.get(position).getGrandtotal());
          /*   name.setText(namelist.get(position));*/


            } catch (Exception e) {
                e.printStackTrace();
            }


        }


        return convertView;
    }

    private class Jobcarddetailsfumi {

        public TextView sno;
        public TextView name;
        public TextView date;
        public TextView intime;
        public TextView outtime;


    }

    @Override
    public int getCount() {
        return data.size();
    }
}
