package adapter;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import androidx.core.app.ActivityCompat;

import com.brainmagic.pricolsalesorder.R;

import java.util.List;

import api.models.deal;
import api.models.master.Customer_list;


/**
 * Created by system01 on 2/12/2017.
 */

public class CuslistAdapter2 extends ArrayAdapter<Customer_list> {

    private Context context;

    private List<deal> Cuslist;


    public CuslistAdapter2(Context context, List<deal> Cuslist) {
        super(context, R.layout.view_adapter_customer);
        this.context = context;
        this.Cuslist = Cuslist;



    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        convertView = null;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.view_adapter_customer, parent, false);

            TextView Text1 = (TextView) convertView.findViewById(R.id.text1);
           // TextView Text3 = (TextView) convertView.findViewById(R.id.text3);
            TextView Text4 = (TextView) convertView.findViewById(R.id.text4);
            TextView Text5 = (TextView) convertView.findViewById(R.id.text5);
            //TextView Text6 = (TextView) convertView.findViewById(R.id.text6);
            TextView Text7 = (TextView) convertView.findViewById(R.id.text7);
            //TextView customercode = (TextView) convertView.findViewById(R.id.customercode);
            //convertView.setTag(convertView);
            Text1.setText(Cuslist.get(position).getDealerName()+"-"+Cuslist.get(position).getDealerCity());
            //Text3.setText(Cuslist.get(position).getBranchCode());
            Text4.setText(Cuslist.get(position).getDealerMobile());
            Text5.setText(Cuslist.get(position).getDealerEmail());
          //  Text6.setText(Cuslist.get(position).get);
            Text7.setText(Cuslist.get(position).getDealerCity());
           // customercode.setText(Cuslist.get(position).getCustomerCode());

            //Text4.setTag(position);
            Text4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + Cuslist.get(position).getDealerMobile()));
                    // callIntent.setNewOrder(Uri.parse(MobileList.get(position)));
                    //callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    context.startActivity(callIntent);
                }
            });
        }

        return convertView;
    }

    @Override
    public int getCount() {
        return Cuslist.size();
    }
}
