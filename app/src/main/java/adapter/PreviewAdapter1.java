package adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.brainmagic.pricolsalesorder.R;

import java.util.ArrayList;
import java.util.List;

import database.DBHelpersync;
import roomdb.database.AppDatabase;

import static android.content.Context.MODE_PRIVATE;
import static roomdb.database.AppDatabase.getAppDatabase;

/**
 * Created by SYSTEM10 on 4/25/2019.
 */

public class PreviewAdapter1 extends ArrayAdapter<String> {

    private Context context;
    private List<String> customerlist, productseglist, productcatagorylist, datelist, quantitylist, packagelsist, ordertypelist, discountlist, unpacklist, distlist,dealerlist,customlist,customertypelist,productseglist1;
    private ArrayList<Integer> IDList;
    private int totalvalues, quantityedit, mrp;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    DBHelpersync dbhelper ;
    private AppDatabase db1;
    private String salesType;
    public TotalValuesString totalValue;
    public deletevaluesString deletevaluesString;

    public PreviewAdapter1(Context context, ArrayList<String> customerlist, ArrayList<String> productseglist, ArrayList<String> productcatagorylist, ArrayList<String> datelist, ArrayList<String> quantitylist, ArrayList<String> packagelsist, ArrayList<String> ordertypelist, ArrayList<Integer> IDList, ArrayList<String> discountlist, ArrayList<String> unpacklist, ArrayList<String> distlist,ArrayList<String> dealerlist,ArrayList<String> customlist,ArrayList<String> customertypelist,String salesType) {
        super(context, R.layout.preview_adapter1, customerlist);
        this.context = context;
        this.customerlist = customerlist;
        this.productseglist = productseglist;
        this.productcatagorylist = productcatagorylist;
        this.datelist = datelist;
        this.quantitylist = quantitylist;
        this.packagelsist = packagelsist;
        this.ordertypelist = ordertypelist;
        this.IDList = IDList;
        this.discountlist = discountlist;
        this.unpacklist = unpacklist;
        this.distlist = distlist;
        this.dealerlist = dealerlist;
        this.customlist = customlist;
        this.customertypelist = customertypelist;
        myshare = context.getSharedPreferences("pricolsalesorder", MODE_PRIVATE);
        editor = myshare.edit();
        dbhelper = new DBHelpersync(context);
        db1=getAppDatabase(context);
        this.productseglist1=new ArrayList<>();
        this.salesType=salesType;
        this.totalValue= (TotalValuesString) context;
        this.deletevaluesString= (deletevaluesString) context;
    }



    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        // TODO Auto-generated method stub
        convertView = null;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.preview_adapter1, parent, false);

            try {
                ImageView delete = (ImageView) convertView.findViewById(R.id.delete);
                ImageView edit = (ImageView) convertView.findViewById(R.id.update);
                TextView SNo = (TextView) convertView.findViewById(R.id.sno);
                TextView dealerename = (TextView) convertView.findViewById(R.id.dealerName);
                // TextView productseg = (TextView) convertView.findViewById(R.id.productseg);
                TextView productcat = (TextView) convertView.findViewById(R.id.productcat);
                TextView Quantity = (TextView) convertView.findViewById(R.id.Quantity);
                TextView cutomertype = (TextView) convertView.findViewById(R.id.cutomertype);
                //  TextView text5 = (TextView) convertView.findViewById(R.id.text5);
                TextView date = (TextView) convertView.findViewById(R.id.date);
                TextView cname = (TextView) convertView.findViewById(R.id.cname);
                final TextView partno = (TextView) convertView.findViewById(R.id.partno);
                final TextView ordervalue = (TextView) convertView.findViewById(R.id.ordervalue);


                convertView.setTag(convertView);
                SNo.setText(Integer.toString(position + 1));
                dealerename.setText(dealerlist.get(position));
                // productseg.setText(productseglist.get(position));
                productcat.setText(productcatagorylist.get(position));
                Quantity.setText(quantitylist.get(position));
                //   text5.setText(DisList.get(position));
                date.setText(unpacklist.get(position));
                partno.setText(packagelsist.get(position) + " - " + productseglist.get(position));
                cname.setText(customerlist.get(position));
                ordervalue.setText(discountlist.get(position));
                cutomertype.setText(customertypelist.get(position)+" - "+customerlist.get(position));
                // text9.setText(Colorlist.get(position));


            delete.setTag(position);
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String query = "select * from Orderpreview where OrderType ='" + ordertypelist.get(position) + "'";
                    List<String> list = selectlist(query);
                    if (list.size() == 1) {

                        AlertDialog.Builder adb = new AlertDialog.Builder(context);
                        adb.setTitle("Delete?");
                        adb.setMessage("Are you sure you want to delete " + packagelsist.get(position)+"?");
                        adb.setNegativeButton("Cancel", null);
                        adb.setPositiveButton("Ok", new AlertDialog.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

//                                try {
                                    deletelist(IDList.get(position));
//                                remove(productseglist.get(position));
                                    IDList.remove(position);
                                    customerlist.remove(position);
                                    productseglist.remove(position);
                                    productcatagorylist.remove(position);
                                    // DisList.remove(position);
                                    dealerlist.remove(position);
                                    datelist.remove(position);
                                    ordertypelist.remove(position);
                                    quantitylist.remove(position);
                                    packagelsist.remove(position);
                                    unpacklist.remove(position);
                                    customertypelist.remove(position);
                                    editor.putString("customertypestring", "");
                                    editor.commit();
                                    discountlist.remove(position);
                                    deletevaluesString.deletevalues(discountlist);
                                    if(getCartQuantity()<=0)
                                        db1.adhereDao().DeleteAllcustomer();

//                                }catch (Exception e)
//                                {
//                                    e.printStackTrace();
//                                }
                                   // db1.adhereDao().DeleteAllExistingCustomer();
                                //  Colorlist.remove(position);
                                notifyDataSetChanged();
                            }
                        });
                        adb.show();
                    }else {
                        AlertDialog.Builder adb = new AlertDialog.Builder(context);
                        adb.setTitle("Delete?");
                        adb.setMessage("Are you sure you want to delete " + packagelsist.get(position) +"?");
                        adb.setNegativeButton("Cancel", null);
                        adb.setPositiveButton("Ok", new AlertDialog.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {


                                deletelist(IDList.get(position));
//                                remove(productseglist.get(position));
                                IDList.remove(position);
                                customerlist.remove(position);
                                productseglist.remove(position);
                                productcatagorylist.remove(position);
                                dealerlist.remove(position);
                                // DisList.remove(position);
                                datelist.remove(position);
                                packagelsist.remove(position);
                                ordertypelist.remove(position);
                                quantitylist.remove(position);
                                discountlist.remove(position);
                                unpacklist.remove(position);
                                deletevaluesString.deletevalues(discountlist);
                                if(getCartQuantity()<=0)
                                    db1.adhereDao().DeleteAllcustomer();
                                //  Colorlist.remove(position);
                                notifyDataSetChanged();
                            }
                        });
                        adb.show();
                    }
                }
            });
            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showAlertbox(IDList.get(position), position, customerlist.get(position), productseglist.get(position), productcatagorylist.get(position), quantitylist.get(position), ordertypelist.get(position), discountlist.get(position), unpacklist.get(position),packagelsist.get(position),productseglist.get(position));
                    notifyDataSetChanged();
                }
            });
            }catch (Exception e)
            {
                e.printStackTrace();
            }

        }

        return convertView;
    }

    private List<String> selectlist(String query) {
        SQLiteDatabase db =dbhelper.getWritableDatabase();
        Cursor cursor;

        cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
//                        customerlist.add(cursor.getString(cursor.getColumnIndex("CustomerName")));
                productseglist1.add(cursor.getString(cursor.getColumnIndex("ProductName")));
//                productcatagorylist.add(cursor.getString(cursor.getColumnIndex("ProductCatagory")));
//                datelist.add(cursor.getString(cursor.getColumnIndex("formattedDate")));
//                quantitylist.add(cursor.getString(cursor.getColumnIndex("quantity")));
//                packagelsist.add(cursor.getString(cursor.getColumnIndex("Package")));
//                ordertypelist.add(cursor.getString(cursor.getColumnIndex("OrderType")));
//                IDList.add(cursor.getInt(cursor.getColumnIndex("id")));
//                discountlist.add(cursor.getString(cursor.getColumnIndex("discount")));
//                unpacklist.add(cursor.getString(cursor.getColumnIndex("Unpackage")));
//                distlist.add(cursor.getString(cursor.getColumnIndex("AddNames")));
            } while (cursor.moveToNext());
            cursor.close();
            db.close();
        } else {
            cursor.close();
            db.close();
        }
        return productseglist;
    }

    private void deletelist(int orderid) {

        SQLiteDatabase db = null;
        DBHelpersync dbhelper = new DBHelpersync(context);
        db = dbhelper.getWritableDatabase();
        db.execSQL("delete from Orderpreview where id='" + orderid + "'");
        db.close();
    }

    private int getCartQuantity()
    {
        SQLiteDatabase db = null;
        DBHelpersync dbhelper = new DBHelpersync(context);
        db = dbhelper.getWritableDatabase();
        Cursor cursor =db.rawQuery("Select * from Orderpreview",null);
        cursor.moveToNext();
        db.close();
        return cursor.getCount();
    }

    public void showAlertbox(final int id, final int pos, String s, String name, String productcat, final String quantity, String type, final String mrp1, final String omrp, final String partno1, final String segment) {
        final AlertDialog alertDialog = new AlertDialog.Builder(
                context).create();

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.update_order, null);
        alertDialog.setView(dialogView);

        TextView productsegment = (TextView) dialogView.findViewById(R.id.productsegment);
        TextView partno = (TextView) dialogView.findViewById(R.id.partno);
//        TextView Pack = (TextView) dialogView.findViewById(R.id.pack);
        TextView Type = (TextView) dialogView.findViewById(R.id.type);
//        TextView Mode = (TextView) dialogView.findViewById(R.id.mode);
        // TextView Colour=(TextView)dialogView.findViewById(R.id.color);
        // final EditText Discount = (EditText) dialogView.findViewById(R.id.discount);
        final EditText Quantity = (EditText) dialogView.findViewById(R.id.quantity);

        Button Save = (Button) dialogView.findViewById(R.id.save);
        Button Cancel = (Button) dialogView.findViewById(R.id.cancel);
        partno.setText(partno1);
        productsegment.setText(segment);

        Type.setText(type);

        //  Discount.setText(discount);
        Quantity.setText(quantity);
        quantityedit = Integer.parseInt(Quantity.getText().toString());
        mrp = Integer.parseInt(omrp);


        //  Colour.setText(color);

        Save.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if(Quantity.getText().toString().equals("")){
                    Quantity.setError("Enter Quantity");

                }else {
                    SQLiteDatabase db = null;
                    DBHelpersync dbhelper = new DBHelpersync(context);
                    db = dbhelper.getWritableDatabase();
                    totalvalues = Integer.parseInt(String.valueOf(mrp * Integer.parseInt(Quantity.getText().toString())));
                    String query = "update Orderpreview set quantity = '" + Quantity.getText().toString() + "',discount ='" + totalvalues + "'  where id = '" + id + "'";
                    //update Orderpreview set quantity = 5, discount = 10 where id = 1
                    db.execSQL(query);
                    db.close();

                    quantitylist.remove(pos);
                    quantitylist.add(pos, Quantity.getText().toString());
                    discountlist.remove(pos);
                    discountlist.add(pos, String.valueOf(totalvalues));
                    totalValue.totalvalues(discountlist,pos);
                    //DisList.add(pos,Discount.getText().toString());

//                QuantityList.replace(QuantityList.get(pos),Quantity.getText().toString());
//                DisList.get(pos).replace(DisList.get(pos),Discount.getText().toString());
                    notifyDataSetChanged();
                    alertDialog.dismiss();
                }
                // TODO Auto-generated method stub


            }
        });
        alertDialog.show();
        Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });


    }
    public interface TotalValuesString{
        void totalvalues(List<String> value, int pos);

    }

    public interface deletevaluesString{
        void deletevalues(List<String> value);
    }

}


