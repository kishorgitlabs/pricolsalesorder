package adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.brainmagic.pricolsalesorder.R;

import java.util.List;

import api.models.regionneworder.Order_list;

/**
 * Created by SYSTEM10 on 5/8/2019.
 */

public class OrderRegionDealerAdapter extends ArrayAdapter {
    private Context context;
    private List<Order_list> data;
//    private List<RegionSecondary> dataSize;


    public OrderRegionDealerAdapter(@NonNull Context context, List<Order_list> data) {
        super(context, R.layout.orderregiondealer, data);
        this.context = context;
        this.data = data;
//        dataSize=new ArrayList<RegionSecondary>();
//        this.dataSize.addAll(data);


    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        convertView = null;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.orderregiondealer, parent, false);
            TextView sno = (TextView) convertView.findViewById(R.id.sno);
            TextView dealerName = (TextView) convertView.findViewById(R.id.dealerName);
            TextView amount = (TextView) convertView.findViewById(R.id.amount);
          //  TextView dealtext = (TextView) convertView.findViewById(R.id.dealtext);
            sno.setText(Integer.toString(position + 1));
            dealerName.setText(data.get(position).getExecutivename());
            amount.setText(data.get(position).getAmount());

        }


        return convertView;
    }

//    public void filter(String s) {
//        s = s.toLowerCase(Locale.getDefault());
//        data.clear();
//
//        if (s.length() == 0) {
//            data.addAll(dataSize);
//
//        } else {
//            for (int i = 0; i < dataSize.size(); i++) {
//                String wp = String.valueOf(dataSize.get(i).getOrderid().toLowerCase());
//
//                // String id=String.valueOf(arraylistSize.get(i).getEmployeeId().toLowerCase());
//                //   String id=arraylistSize.get(i).getEmployeeId().toLowerCase();
//                //String mobile=arraylistSize.get(i).getMobileNo().toLowerCase();
//                if (wp.toLowerCase(Locale.getDefault()).contains(s.toLowerCase())) {
//                    data.add(dataSize.get(i));
//                    notifyDataSetChanged();
//
//                }
//            }
//
//        }

}
