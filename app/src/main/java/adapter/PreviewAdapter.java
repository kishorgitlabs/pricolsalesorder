package adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.brainmagic.pricolsalesorder.LoginActivity;
import com.brainmagic.pricolsalesorder.R;
import com.brainmagic.pricolsalesorder.SalesConfirmOrder;
import com.brainmagic.pricolsalesorder.SalesSecondaryActivity;
import com.muddzdev.styleabletoastlibrary.StyleableToast;

import java.util.ArrayList;
import java.util.List;

import database.DBHelpersync;
import roomdb.database.AppDatabase;

import static android.content.Context.MODE_PRIVATE;
import static roomdb.database.AppDatabase.getAppDatabase;

/**
 * Created by SYSTEM10 on 4/17/2019.
 */

public class PreviewAdapter extends ArrayAdapter<String> {

    private Context context;
    private List<String> customerlist, productseglist, productcatagorylist, datelist, quantitylist, packagelsist,ordertypelist,discountlist,unpacklist,distlist;
    private ArrayList<Integer> IDList;
    private int totalvalues,quantityedit,mrp;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private SQLiteDatabase db;
    DBHelpersync dbhelper ;
    Cursor cursor;
    private AppDatabase db1;

    public PreviewAdapter(Context context, ArrayList<String> customerlist, ArrayList<String> productseglist, ArrayList<String> productcatagorylist, ArrayList<String> datelist, ArrayList<String> quantitylist, ArrayList<String> packagelsist,ArrayList<String>ordertypelist,ArrayList<Integer>IDList,ArrayList<String>discountlist,ArrayList<String>unpacklist,ArrayList<String>distlist) {
        super(context, R.layout.preview_adapter, productseglist);
        this.context = context;
        this.customerlist = customerlist;
        this.productseglist = productseglist;
        this.productcatagorylist = productcatagorylist;
        this.datelist = datelist;
        this.quantitylist = quantitylist;
        this.packagelsist = packagelsist;
        this.ordertypelist = ordertypelist;
        this.IDList = IDList;
        this.discountlist = discountlist;
        this.unpacklist = unpacklist;
        this.distlist = distlist;
        myshare = context.getSharedPreferences("pricolsalesorder", MODE_PRIVATE);
        editor = myshare.edit();
        dbhelper = new DBHelpersync(context);
        db = dbhelper.getWritableDatabase();
        db1=getAppDatabase(context);



    }


    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        // TODO Auto-generated method stub
        convertView = null;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.preview_adapter, parent, false);

            ImageView delete = (ImageView) convertView.findViewById(R.id.delete);
            ImageView edit = (ImageView) convertView.findViewById(R.id.update);
            TextView SNo = (TextView) convertView.findViewById(R.id.sno);
            TextView dealerename = (TextView) convertView.findViewById(R.id.dealerName);
            TextView productseg = (TextView) convertView.findViewById(R.id.productseg);
            TextView productcat = (TextView) convertView.findViewById(R.id.productcat);
            TextView Quantity = (TextView) convertView.findViewById(R.id.Quantity);
            //  TextView text5 = (TextView) convertView.findViewById(R.id.text5);
            TextView date = (TextView) convertView.findViewById(R.id.date);
            TextView Ordervalue = (TextView) convertView.findViewById(R.id.Ordervalue);
            final TextView partno = (TextView) convertView.findViewById(R.id.partno);


            convertView.setTag(convertView);
            SNo.setText(Integer.toString(position + 1));
            dealerename.setText(distlist.get(position));
            productseg.setText(productseglist.get(position));
            productcat.setText(productcatagorylist.get(position));
            Quantity.setText(quantitylist.get(position));
            //   text5.setText(DisList.get(position));
            date.setText("₹"+" "+ unpacklist.get(position));
            partno.setText(packagelsist.get(position));
            Ordervalue.setText("₹"+" "+ discountlist.get(position));

            // text9.setText(Colorlist.get(position));


            delete.setTag(position);
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String query = "select * from Orderpreview where OrderType ='" + ordertypelist.get(position) + "'";
                    List<String> list = selectlist(query);
                    if (list.size() == 1) {

                        AlertDialog.Builder adb = new AlertDialog.Builder(context);
                        adb.setTitle("Delete?");
                        adb.setMessage("Are you sure you want to delete " + productseglist.get(position));
                        adb.setNegativeButton("Cancel", null);
                        adb.setPositiveButton("Ok", new AlertDialog.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                deletelist(IDList.get(position));
                                remove(productseglist.get(position));
                                IDList.remove(position);
//                            customerlist.remove(position);
                                productseglist.remove(position);
                                productcatagorylist.remove(position);
                                // DisList.remove(position);
                                datelist.remove(position);
                                ordertypelist.remove(position);
                                quantitylist.remove(position);
                                editor.putString("customertypestring", "");
                                editor.commit();
                                db1.adhereDao().DeleteAllcustomer();
                                //  Colorlist.remove(position);
                                notifyDataSetChanged();
                            }
                        });
                        adb.show();
                    }else {
                        AlertDialog.Builder adb = new AlertDialog.Builder(context);
                        adb.setTitle("Delete?");
                        adb.setMessage("Are you sure you want to delete " + productseglist.get(position));
                        adb.setNegativeButton("Cancel", null);
                        adb.setPositiveButton("Ok", new AlertDialog.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                deletelist(IDList.get(position));
                                remove(productseglist.get(position));
                                IDList.remove(position);
//                            customerlist.remove(position);
                                productseglist.remove(position);
                                productcatagorylist.remove(position);
                                // DisList.remove(position);
                                datelist.remove(position);
                                ordertypelist.remove(position);
                                quantitylist.remove(position);
                                //  Colorlist.remove(position);
                                notifyDataSetChanged();
                            }
                        });
                        adb.show();
                    }
                }
            });
            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showAlertbox(IDList.get(position), position, "", productseglist.get(position), productcatagorylist.get(position), quantitylist.get(position), ordertypelist.get(position),discountlist.get(position),unpacklist.get(position));
                    notifyDataSetChanged();
                }
            });

        }

        return convertView;
    }

    private List<String> selectlist(String query){
        cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
//                        customerlist.add(cursor.getString(cursor.getColumnIndex("CustomerName")));
                productseglist.add(cursor.getString(cursor.getColumnIndex("ProductName")));
                productcatagorylist.add(cursor.getString(cursor.getColumnIndex("ProductCatagory")));
                datelist.add(cursor.getString(cursor.getColumnIndex("formattedDate")));
                quantitylist.add(cursor.getString(cursor.getColumnIndex("quantity")));
                packagelsist.add(cursor.getString(cursor.getColumnIndex("Package")));
                ordertypelist.add(cursor.getString(cursor.getColumnIndex("OrderType")));
                IDList.add(cursor.getInt(cursor.getColumnIndex("id")));
                discountlist.add(cursor.getString(cursor.getColumnIndex("discount")));
                unpacklist.add(cursor.getString(cursor.getColumnIndex("Unpackage")));
                distlist.add(cursor.getString(cursor.getColumnIndex("AddNames")));
            } while (cursor.moveToNext());
            cursor.close();
            db.close();
        } else {
            cursor.close();
            db.close();
        }
        return productseglist;
    }

    private void deletelist(int orderid) {

        SQLiteDatabase db = null;
        DBHelpersync dbhelper = new DBHelpersync(context);
        db = dbhelper.getWritableDatabase();
        db.execSQL("delete from Orderpreview where id='" + orderid + "'");
        db.close();
    }

    @Override
    public int getCount() {
        return productseglist.size();
    }

    public void showAlertbox(final int id, final int pos, String s, String name, String productcat, String quantity, String type, final String mrp1, final String omrp) {
        final AlertDialog alertDialog = new AlertDialog.Builder(
                context).create();

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.update_order, null);
        alertDialog.setView(dialogView);

        TextView Name = (TextView) dialogView.findViewById(R.id.customer);
        TextView Product = (TextView) dialogView.findViewById(R.id.product);
//        TextView Pack = (TextView) dialogView.findViewById(R.id.pack);
        TextView Type = (TextView) dialogView.findViewById(R.id.type);
//        TextView Mode = (TextView) dialogView.findViewById(R.id.mode);
        // TextView Colour=(TextView)dialogView.findViewById(R.id.color);
        // final EditText Discount = (EditText) dialogView.findViewById(R.id.discount);
        final EditText Quantity = (EditText) dialogView.findViewById(R.id.quantity);

        Button Save = (Button) dialogView.findViewById(R.id.save);
        Button Cancel = (Button) dialogView.findViewById(R.id.cancel);
        Name.setText(name);
        Product.setText(productcat);

        Type.setText(type);

        //  Discount.setText(discount);
        Quantity.setText(quantity);
        quantityedit= Integer.parseInt(Quantity.getText().toString());
        mrp= Integer.parseInt(omrp);


        //  Colour.setText(color);

        Save.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if(Quantity.getText().toString().equals("")){
                    StyleableToast st = new StyleableToast(context, "Enter Quantity", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }else {
                    SQLiteDatabase db = null;
                    DBHelpersync dbhelper = new DBHelpersync(context);
                    db = dbhelper.getWritableDatabase();
                    totalvalues=Integer.parseInt(String.valueOf(mrp * Integer.parseInt(Quantity.getText().toString())));
                    String query = "update Orderpreview set quantity = '" + Quantity.getText().toString() + "',discount ='"+totalvalues+"'  where id = '" + id + "'";
                    //update Orderpreview set quantity = 5, discount = 10 where id = 1
                    db.execSQL(query);
                    db.close();
                    quantitylist.remove(pos);
                    quantitylist.add(pos, Quantity.getText().toString());
                    discountlist.add(pos, String.valueOf(totalvalues));
                    //DisList.add(pos,Discount.getText().toString());

//                QuantityList.replace(QuantityList.get(pos),Quantity.getText().toString());
//                DisList.get(pos).replace(DisList.get(pos),Discount.getText().toString());
                    notifyDataSetChanged();
                    alertDialog.dismiss();
                }



            }
        });
        alertDialog.show();
        Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
    }
}
