package adapter;

import android.content.Context;
import android.content.Intent;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.brainmagic.pricolsalesorder.R;
import com.brainmagic.pricolsalesorder.Regionorderidsttusactivity;
import androidx.annotation.NonNull;
import java.util.List;

import api.models.regionneworder.Order_list;

/**
 * Created by SYSTEM10 on 5/8/2019.
 */

public class OrderRegionAdapter  extends ArrayAdapter {
    private Context context;
    private List<Order_list> data;
//    private List<RegionPrimary> dataSize;


    public OrderRegionAdapter(@NonNull Context context,
                              List<Order_list> data) {
        super(context, R.layout.orderregion, data);
        this.context = context;
        this.data = data;
//        dataSize=new ArrayList<RegionPrimary>();
//        this.dataSize.addAll(data);


    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        convertView = null;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.orderregioncurrentmonth, parent, false);
            TextView sno = (TextView) convertView.findViewById(R.id.sno);
            TextView dealerName = (TextView) convertView.findViewById(R.id.dealerName);
            TextView amount = (TextView) convertView.findViewById(R.id.amount);
            sno.setText(Integer.toString(position + 1));
           // orderid.setText(data.get(position).getOrderid());
            dealerName.setText(data.get(position).getExecutivename());
            amount.setText(data.get(position).getAmount());
            amount.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    Intent i = new Intent(context, RegionOrderStatusDetailsActivity.class).putExtra("executecode",data.get(position).getExecutiveCode());
//                    context.startActivity(i);

                    Intent i = new Intent(context, Regionorderidsttusactivity.class).putExtra("executecode",data.get(position).getExecutiveCode());
                    context.startActivity(i);
                }
            });



        }


        return convertView;
    }

//    public void filter(String s) {
//        s = s.toLowerCase(Locale.getDefault());
//        data.clear();
//
//        if (s.length() == 0) {
//            data.addAll(dataSize);
//
//        } else {
//            for (int i = 0; i < dataSize.size(); i++) {
//                String wp = String.valueOf(dataSize.get(i).getOrderid().toLowerCase());
//                // String id=String.valueOf(arraylistSize.get(i).getEmployeeId().toLowerCase());
//                //   String id=arraylistSize.get(i).getEmployeeId().toLowerCase();
//                //String mobile=arraylistSize.get(i).getMobileNo().toLowerCase();
//                if (wp.toLowerCase(Locale.getDefault()).contains(s.toLowerCase())) {
//                    data.add(dataSize.get(i));
//                    notifyDataSetChanged();
//
//                }
//            }
//
//

}
