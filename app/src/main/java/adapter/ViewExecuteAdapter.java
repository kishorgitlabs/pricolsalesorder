package adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.brainmagic.pricolsalesorder.MapsActivity;
import com.brainmagic.pricolsalesorder.R;
import com.brainmagic.pricolsalesorder.VIewMultipleExecutiveActivity;
import com.brainmagic.pricolsalesorder.ViewCoordinate;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import api.models.GetSalesAttendanceReg.GetSalesAtt;

/**
 * Created by SYSTEM10 on 5/8/2019.
 */

public class  ViewExecuteAdapter extends ArrayAdapter {
    private Context context;
    private List<GetSalesAtt>data;
    private String[] date1,dateacc;
    private List<GetSalesAtt> dataSize;


    public ViewExecuteAdapter(Context context, List<GetSalesAtt>data) {
        super(context, R.layout.viewsalesattendancereg, data);
        this.context = context;
        this.data = data;
        dataSize=new ArrayList<GetSalesAtt>();
        this.dataSize.addAll(data);

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        convertView = null;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.viewsalesattendancereg, parent, false);


            try {

                //  TextView sno=(TextView)convertView.findViewById(R.id.sno1);
                //  TextView name=(TextView)convertView.findViewById(R.id.name1);
                TextView sno = (TextView) convertView.findViewById(R.id.sno);
                TextView salename = (TextView) convertView.findViewById(R.id.salename);
                TextView salesdate = (TextView) convertView.findViewById(R.id.salesdate);
                TextView salescode = (TextView) convertView.findViewById(R.id.salescode);
                TextView intimesales = (TextView) convertView.findViewById(R.id.intimesales);
                TextView outtimesales = (TextView) convertView.findViewById(R.id.outtimesales);
                TextView inaddress = (TextView) convertView.findViewById(R.id.inaddress);
                TextView outaddress = (TextView) convertView.findViewById(R.id.outaddress);
                TextView distance = (TextView) convertView.findViewById(R.id.distance);
                 Button map = (Button) convertView.findViewById(R.id.map);
                sno.setText(Integer.toString(position + 1));
                salename.setText(data.get(position).getName());
                if(data.get(position).getDate() != null){
                    date1 = data.get(position).getDate().split("T");
                    salesdate.setText(date1[0]);
                }
                salescode.setText((CharSequence) data.get(position).getEmpCode());
                intimesales.setText(data.get(position).getInTime());
                outtimesales.setText(data.get(position).getOutTime());
                inaddress.setText(data.get(position).getAddress());
                outaddress.setText(data.get(position).getOutAddress());
                distance.setText(data.get(position).getDistance());
                salesdate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i =new Intent(context,VIewMultipleExecutiveActivity.class).putExtra("Datereg",data.get(position).getDate()).putExtra("empid",data.get(position).getEmpId());
                        context.startActivity(i);
                    }
                });

                intimesales.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        date1 = data.get(position).getDate().split("T");
                        Intent i=new Intent(context, ViewCoordinate.class).putExtra("Date",date1[0]).putExtra("idCordinate",String.valueOf(data.get(position).getEmpId()));
                        context.startActivity(i);
                    }
                });
                map.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dateacc=data.get(position).getDate().split("T");
                        Intent i=new Intent(context, MapsActivity.class).putExtra("fromadd",data.get(position).getAddress()).putExtra("toadd",data.get(position).getOutAddress())
                                .putExtra("fromlatitude",data.get(position).getInLatitude()).putExtra("fromlongitude",data.get(position).getInLongitude())
                                .putExtra("tolatitude",data.get(position).getOutLatitude()).putExtra("tolongitude",data.get(position).getOutLongitude()).putExtra("Date",dateacc[0]).putExtra("idCordinate",String.valueOf(data.get(position).getEmpId()));
                        context.startActivity(i);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        return convertView;
    }

    public void filter(String s) {
        s = s.toLowerCase(Locale.getDefault());
        data.clear();

        if (s.length() == 0) {
            data.addAll(dataSize);

        } else {
            for (int i = 0; i < dataSize.size(); i++) {
                String wp = String.valueOf(dataSize.get(i).getEmpCode());
                String wp1=String.valueOf(dataSize.get(i).getName().toLowerCase());
                // String id=String.valueOf(arraylistSize.get(i).getEmployeeId().toLowerCase());
                //   String id=arraylistSize.get(i).getEmployeeId().toLowerCase();
                //String mobile=arraylistSize.get(i).getMobileNo().toLowerCase();
                if (wp.toLowerCase(Locale.getDefault()).contains(s.toLowerCase())|| wp1.toLowerCase(Locale.getDefault()).contains(s.toLowerCase())) {
                    data.add(dataSize.get(i));
                    notifyDataSetChanged();

                }
            }

        }
    }
}
