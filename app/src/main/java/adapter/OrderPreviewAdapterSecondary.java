package adapter;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.brainmagic.pricolsalesorder.R;

import java.util.List;

import api.models.viewOrder.ViewOrder;

/**
 * Created by SYSTEM10 on 5/15/2019.
 */

public class OrderPreviewAdapterSecondary extends ArrayAdapter {
    private Context context;
    List<ViewOrder> data;


    public OrderPreviewAdapterSecondary(@NonNull Context context, List<ViewOrder> data) {
        super(context, R.layout.orderpreviewsecondary, data);
        this.context = context;
        this.data = data;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        convertView = null;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.orderpreviewsecondary, parent, false);
            try {


                //  TextView sno=(TextView)convertView.findViewById(R.id.sno1);
                //   TextView name = (TextView) convertView.findViewById(R.id.namehis);
                TextView sno = (TextView) convertView.findViewById(R.id.sno);
                TextView orderid = (TextView) convertView.findViewById(R.id.orderid);
                TextView productseg = (TextView) convertView.findViewById(R.id.productsegment);
                TextView productcat = (TextView) convertView.findViewById(R.id.productcatagory);
                TextView quantity = (TextView) convertView.findViewById(R.id.Quantity);
                TextView mrp = (TextView) convertView.findViewById(R.id.total);
                TextView partno = (TextView) convertView.findViewById(R.id.partno);
                TextView ordervalues = (TextView) convertView.findViewById(R.id.ordervalues);
                TextView customername = (TextView) convertView.findViewById(R.id.customername);
                sno.setText(Integer.toString(position + 1));
                orderid.setText(data.get(position).getOrderid());
                productseg.setText(data.get(position).getSegment());
                productcat.setText(data.get(position).getProductname());
                quantity.setText(data.get(position).getQuantity());
                partno.setText(data.get(position).getPartNo());
                mrp.setText("₹"+" "+data.get(position).getAmount());
                ordervalues.setText("₹"+" "+data.get(position).getGrandtotal());
                customername.setText(data.get(position).getCusName());
          /*   name.setText(namelist.get(position));*/


            } catch (Exception e) {
                e.printStackTrace();
            }


        }


        return convertView;
    }

    private class Jobcarddetailsfumi {

        public TextView sno;
        public TextView name;
        public TextView date;
        public TextView intime;
        public TextView outtime;


    }

    @Override
    public int getCount() {
        return data.size();
    }
}
