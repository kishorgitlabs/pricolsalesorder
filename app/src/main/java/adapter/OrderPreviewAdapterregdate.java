package adapter;

import android.content.Context;
import android.content.Intent;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.brainmagic.pricolsalesorder.R;
import com.brainmagic.pricolsalesorder.RegionOrderStatusDetailsActivity;

import java.util.List;

import api.models.rmdate.RmDate;


/**
 * Created by SYSTEM10 on 4/29/2019.
 */

public class OrderPreviewAdapterregdate extends ArrayAdapter {
    private Context context;
    private List<RmDate>data;
    private TextView orderid;

    public OrderPreviewAdapterregdate(@NonNull Context context, List<RmDate>data) {
        super(context, R.layout.orderpreview, data);
        this.context = context;
        this.data = data;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        convertView = null;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.orderpreview, parent, false);
            try {


                //  TextView sno=(TextView)convertView.findViewById(R.id.sno1);
                //   TextView name = (TextView) convertView.findViewById(R.id.namehis);
                TextView sno = (TextView) convertView.findViewById(R.id.sno);
                orderid = (TextView) convertView.findViewById(R.id.orderids);
                TextView orderdate = (TextView) convertView.findViewById(R.id.orderdate);
                TextView shopname = (TextView) convertView.findViewById(R.id.shopname);
                TextView amount = (TextView) convertView.findViewById(R.id.amount);
//                TextView quantity = (TextView) convertView.findViewById(R.id.Quantity);
//                TextView mrp = (TextView) convertView.findViewById(R.id.total);
//                TextView partno = (TextView) convertView.findViewById(R.id.partno);
//                TextView ordervalues = (TextView) convertView.findViewById(R.id.ordervalues);
//                TextView disti = (TextView) convertView.findViewById(R.id.disti);
                sno.setText(Integer.toString(position + 1));
                orderid.setText(data.get(position).getOrderid());
              //  executivename.setText(data.get(position).getExecutivename());
               if( data.get(position).getOrderedDate() != null){
                   String[] date1 =data.get(position).getOrderedDate().split("T");
                   orderdate.setText(date1[0]);
               }

                shopname.setText(data.get(position).getDealername());
                amount.setText(data.get(position).getUnitprice());

                orderid.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i =new Intent(context, RegionOrderStatusDetailsActivity.class).putExtra("orderid",data.get(position).getOrderid());
                        context.startActivity(i);
                    }
                });
//                productseg.setText(data.get(position).getSegment());
//                productcat.setText(data.get(position).getProductname());
//                quantity.setText(data.get(position).getQuantity());
//                partno.setText(data.get(position).getPartNo());
//                mrp.setText("₹"+" "+data.get(position).getAmount());
//                disti.setText(data.get(position).getDistributorName());
//                ordervalues.setText("₹"+" "+data.get(position).getGrandtotal());
          /*   name.setText(namelist.get(position));*/


            } catch (Exception e) {
                e.printStackTrace();
            }


        }


        return convertView;
    }

    private class Jobcarddetailsfumi {

        public TextView sno;
        public TextView name;
        public TextView date;
        public TextView intime;
        public TextView outtime;


    }

    @Override
    public int getCount() {
        return data.size();
    }
}
