package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.brainmagic.pricolsalesorder.R;

import java.util.List;

import api.models.AttendanceAllTimeData.AttendanceAll;

/**
 * Created by SYSTEM10 on 5/2/2019.
 */

public class ViewAllItemAdapter extends ArrayAdapter {
    private Context context;
    private List<AttendanceAll> data;


    public ViewAllItemAdapter(Context context, List<AttendanceAll> data) {
        super(context, R.layout.viewattendanceadapter,data);
        this.context = context;
        this.data = data ;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        convertView = null;
        if (convertView == null)
        {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.viewattendanceadapter, parent, false);
            try{

                //  TextView sno=(TextView)convertView.findViewById(R.id.sno1);
                //  TextView name=(TextView)convertView.findViewById(R.id.name1);
                TextView date=(TextView)convertView.findViewById(R.id.date1);
                TextView intime=(TextView)convertView.findViewById(R.id.intime);
                TextView outtime=(TextView)convertView.findViewById(R.id.outtime);
                TextView day1=(TextView)convertView.findViewById(R.id.day1);
                TextView distance=(TextView)convertView.findViewById(R.id.distance);
                // sno.setText(Integer.toString(position +1));
                // name.setText(namelist.get(position));
                day1.setText(data.get(position).getAttendDay());
//                distance.setText(data.get(position).getDistance()+"KM");
                if(data.get(position).getDate() != null){
                    String[] date1 =data.get(position).getDate().split("T");
                    date.setText(date1[0]);

                }

                if(data.get(position).getInTime()!= null){
                    String[] intime1=data.get(position).getInTime().split("\\.");
                    intime.setText(intime1[0]);

                }else{
                    intime.setText("");
                }
                if(data.get(position).getOutTime() != null){
                    String[] outtime1=data.get(position).getOutTime().split("\\.");
                    outtime.setText(outtime1[0]);

                }else{
                    outtime.setText("");
                }


            }catch (Exception e){
                e.printStackTrace();
            }

        }

        return convertView;
    }

}
