package adapter;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import com.brainmagic.pricolsalesorder.R;
import com.brainmagic.pricolsalesorder.ViewAttendanceContext;
import com.brainmagic.pricolsalesorder.ViewAttendanceContextReg;

import java.util.List;

/**
 * Created by SYSTEM10 on 10/23/2018.
 */

public class ViewAttendanceAdapter extends ArrayAdapter<String> {
    private Context context;
    private List<String> namelist, DateList, Intimelist, outtimelist,daylist,Distancelist,Designationlist;

    public ViewAttendanceAdapter(@NonNull Context context, List<String> namelist, List<String> DateList, List<String> Intimelist, List<String> outtimelist ,List<String> daylist,List<String> Distancelist,List<String> Designationlist) {
        super(context, R.layout.viewattendance,namelist);
        this.context = context;
        this.namelist = namelist ;
        this.DateList = DateList ;
        this.Intimelist = Intimelist ;
        this.outtimelist = outtimelist ;
        this.daylist = daylist ;
        this.Distancelist = Distancelist ;
        this.Designationlist = Designationlist ;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        convertView = null;
        if (convertView == null)
        {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.viewattendance, parent, false);
            try{

                //  TextView sno=(TextView)convertView.findViewById(R.id.sno1);
              //  TextView name=(TextView)convertView.findViewById(R.id.name1);
                TextView date=(TextView)convertView.findViewById(R.id.date1);
                TextView intime=(TextView)convertView.findViewById(R.id.intime);
                TextView outtime=(TextView)convertView.findViewById(R.id.outtime);
                TextView day1=(TextView)convertView.findViewById(R.id.day1);
                TextView distance=(TextView)convertView.findViewById(R.id.distance);
              // sno.setText(Integer.toString(position +1));
               // name.setText(namelist.get(position));
                day1.setText(daylist.get(position));
//                distance.setText(Distancelist.get(position)+"KM");
                if(DateList.get(position) != null){
                    String[] date1 =DateList.get(position).split(" ");
                    date.setText(date1[0]);

                }

                if(Intimelist.get(position).toString() != null){
                    String[] intime1=Intimelist.get(position).split("\\.");
                    intime.setText(intime1[0]);

                }else{
                    intime.setText("");
                }
                if(outtimelist.get(position)!=null)
                if(outtimelist.get(position).toString() != null){
                    String[] outtime1=outtimelist.get(position).split("\\.");
                    outtime.setText(outtime1[0]);

                }else{
                      outtime.setText("");
                }

                date.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(Designationlist.get(position).equals("SalesExecutive")){
                            Intent i=new Intent(context,ViewAttendanceContext.class).putExtra("date",DateList.get(position));
                            context.startActivity(i);
                        }else {
                            Intent i=new Intent(context,ViewAttendanceContextReg.class).putExtra("date",DateList.get(position));
                            context.startActivity(i);
                        }

                    }
                });
            }catch (Exception e){
                e.printStackTrace();
            }

        }

        return convertView;
    }
    private class Jobcarddetailsfumi {

        public TextView sno;
        public TextView name;
        public TextView date;
        public TextView intime;
        public TextView outtime;
        public TextView day1;


    }

    }
