package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.brainmagic.pricolsalesorder.R;

import java.util.List;

import api.models.master.Distributor_list;

/**
 * Created by SYSTEM10 on 4/23/2019.
 */

public class DistributorAdapter extends ArrayAdapter<Distributor_list> {

    private Context context;

    private List<Distributor_list> distribut;
    private SelectValue selectvalue;
    private String mobile ="";


    public DistributorAdapter(Context context, List<Distributor_list> distribut) {
        super(context, R.layout.slectdistribut,distribut);
        this.context = context;
        this.distribut = distribut;
        selectvalue=(SelectValue)context;


    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        convertView = null;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.slectdistribut, parent, false);

              TextView distiname=(TextView)convertView.findViewById(R.id.nameview);
              TextView distimobile=(TextView)convertView.findViewById(R.id.mobileview);
              TextView distiaddress=(TextView)convertView.findViewById(R.id.addressview);
              final CheckBox checkBox=(CheckBox)convertView.findViewById(R.id.radiobuttonadapter);

              distiname.setText(distribut.get(position).getDisname());
              distimobile.setText(distribut.get(position).getMobileno());
              distiaddress.setText(distribut.get(position).getAddress());
//            if(mobile.equals(distribut.get(position).getMobileno()))
//            {
//                checkBox.setChecked(true);
//            }
              checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                  @Override
                  public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                      if(checked){
                          compoundButton.setChecked(checked);
                          selectvalue.selectid(String.valueOf(distribut.get(position).getP_Id()));
                          selectvalue.selectname(String.valueOf(distribut.get(position).getDisname()));
                          selectvalue.selectmobile(String.valueOf(distribut.get(position).getMobileno()));
                          selectvalue.selectadress(String.valueOf(distribut.get(position).getAddress()));
                         // notifyDataSetChanged();
                      }
                  }
              });
        }

        return convertView;
    }


    @Override
    public int getCount() {
        return distribut.size();
    }

    public interface SelectValue {
        void selectid(String id);
        void selectname(String name);
        void selectmobile(String mobile);
        void selectadress(String addreess);

    }


}
