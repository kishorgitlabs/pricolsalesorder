package adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import android.widget.ArrayAdapter;
import android.widget.Filter;

import com.brainmagic.pricolsalesorder.R;

import java.util.ArrayList;
import java.util.List;

import api.models.viewvisitreport.viewvisitreportrm.ViewViewVisitReportList;

public class ViewVisitAdapter<T> extends ArrayAdapter<ViewViewVisitReportList> {

    List<ViewViewVisitReportList> fullList;
    List<ViewViewVisitReportList> mOriginalValues;
    ArrayFilter mFilter;
    Context context;

    public ViewVisitAdapter(@NonNull Context context, List<ViewViewVisitReportList> fullList) {
        super(context, R.layout.simple_spinner_item,fullList);
        this.fullList=fullList;
        mOriginalValues=new ArrayList<>(fullList);
        this.context=context;
    }

//    @NonNull
//    @Override
//    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
//        View view = LayoutInflater.from(context).inflate(R.layout.simple_spinner_item,parent,false);
//        TextView name=view.findViewById(R.id.simple_list_id);
//        name.setText(fullList.get(position).toString());
//
//        return view;
//    }

    @Override
    public ViewViewVisitReportList getItem(int position) {
        return (ViewViewVisitReportList)fullList.get(position);
    }


    @Override
    public Filter getFilter() {
        if (mFilter == null) {
            mFilter = new ArrayFilter();
        }
        return mFilter;
    }

    private void setText(String name){
        for(ViewViewVisitReportList list:fullList)
        {
            if(list.toString().contains(name))
            {

            }
        }
    }

    private class ArrayFilter extends Filter {
        private Object lock;

        @Override
        protected FilterResults performFiltering(CharSequence prefix) {
            FilterResults results = new FilterResults();

            if (mOriginalValues == null) {
                synchronized (lock) {
                    mOriginalValues = new ArrayList<ViewViewVisitReportList>(fullList);
                }
            }

            if (prefix == null || prefix.length() == 0) {
                synchronized (lock) {
                    ArrayList<ViewViewVisitReportList> list = new ArrayList<ViewViewVisitReportList>(mOriginalValues);
                    results.values = list;
                    results.count = list.size();
                }
            } else {
                final String prefixString = prefix.toString().toLowerCase();

                List<ViewViewVisitReportList> values = mOriginalValues;
                int count = values.size();

                ArrayList<String> newValues = new ArrayList<String>(count);

                for (int i = 0; i < count; i++) {
                    String item = values.get(i).toString();
                    if (item.toLowerCase().contains(prefixString)) {
                        newValues.add(item);
                    }

                }

                results.values = newValues;
                results.count = newValues.size();
            }

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            if (results.values != null) {
                fullList = (ArrayList<ViewViewVisitReportList>) results.values;
            } else {
                fullList = new ArrayList<ViewViewVisitReportList>();
            }
            if (results.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }
    }
}
