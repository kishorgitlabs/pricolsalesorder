package adapter;

import android.content.Context;
import android.content.Intent;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.brainmagic.pricolsalesorder.R;
import com.brainmagic.pricolsalesorder.RegionorderDate;

import java.util.List;

/**
 * Created by SYSTEM10 on 5/8/2019.
 */

public class OrderRegionAdapterReg extends ArrayAdapter {
    private Context context;
    private List<api.models.datewisefilterregion.Order_list> data;
//    private List<RegionPrimary> dataSize;


    public OrderRegionAdapterReg(@NonNull Context context, List<api.models.datewisefilterregion.Order_list> data) {
        super(context, R.layout.orderregion, data);
        this.context = context;
        this.data = data;
//        dataSize=new ArrayList<RegionPrimary>();
//        this.dataSize.addAll(data);


    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        convertView = null;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.orderregion, parent, false);
            TextView sno = (TextView) convertView.findViewById(R.id.sno);
            TextView dealerName = (TextView) convertView.findViewById(R.id.dealerName);
            TextView amount = (TextView) convertView.findViewById(R.id.amount);
            TextView date = (TextView) convertView.findViewById(R.id.date);
            sno.setText(Integer.toString(position + 1));
           // orderid.setText(data.get(position).getOrderid());
            dealerName.setText(data.get(position).getExecutivename());
            amount.setText(data.get(position).getAmount());
            String[] orderdate = data.get(position).getOrderedDate().split("T");
            if(data.get(position).getOrderedDate()!=null){
                date.setText(orderdate[0]);
            }

            amount.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String[] dateTemp= null;
                    if(data.get(position).getOrderedDate()!=null)
                    {
                        dateTemp= data.get(position).getOrderedDate().split("T");
                    }
                    if(dateTemp[0]!=null) {
                        Intent i = new Intent(context, RegionorderDate.class).putExtra("executecode",data.get(position).getExecutiveCode()).putExtra("orderdate",dateTemp[0]);
                        context.startActivity(i);
                    }else {
                        Intent i = new Intent(context, RegionorderDate.class).putExtra("executecode",data.get(position).getExecutiveCode()).putExtra("orderdate","");
                        context.startActivity(i);
                    }

                }
            });
//            orderid.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    Intent i = new Intent(context, RegionOrderStatusDetailsActivity.class).putExtra("orderid", data.get(position).getOrderid()).putExtra("ordertype", data.get(position).getOrderType());
//                    context.startActivity(i);
//                }
//            });


        }


        return convertView;
    }

//    public void filter(String s) {
//        s = s.toLowerCase(Locale.getDefault());
//        data.clear();
//
//        if (s.length() == 0) {
//            data.addAll(dataSize);
//
//        } else {
//            for (int i = 0; i < dataSize.size(); i++) {
//                String wp = String.valueOf(dataSize.get(i).getOrderid().toLowerCase());
//                // String id=String.valueOf(arraylistSize.get(i).getEmployeeId().toLowerCase());
//                //   String id=arraylistSize.get(i).getEmployeeId().toLowerCase();
//                //String mobile=arraylistSize.get(i).getMobileNo().toLowerCase();
//                if (wp.toLowerCase(Locale.getDefault()).contains(s.toLowerCase())) {
//                    data.add(dataSize.get(i));
//                    notifyDataSetChanged();
//
//                }
//            }
//
//

}
