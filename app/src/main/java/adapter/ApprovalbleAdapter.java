package adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.brainmagic.pricolsalesorder.R;
import com.brainmagic.pricolsalesorder.RegionHomeActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import alertbox.Alertbox;
import api.models.approveddelaermenu.ApproveDealerMenuData;
import api.models.approvedealer.ApproveDealer;
import api.retrofit.APIService;
import api.retrofit.RetrofitClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by SYSTEM10 on 5/8/2019.
 */

public class ApprovalbleAdapter extends ArrayAdapter {
    private Context context;
   private List<ApproveDealer> data;
    private Alertbox box =new Alertbox(context);
    private List<ApproveDealer> dataSize;




    public ApprovalbleAdapter(@NonNull Context context, List<ApproveDealer> data) {
        super(context, R.layout.approvedealer, data);
        this.context = context;
        this.data = data;
        dataSize=new ArrayList<ApproveDealer>();
        this.dataSize.addAll(data);

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        convertView = null;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.approvedealer, parent, false);
            try {


                //  TextView sno=(TextView)convertView.findViewById(R.id.sno1);
                //   TextView name = (TextView) convertView.findViewById(R.id.namehis);
                TextView sno = (TextView) convertView.findViewById(R.id.sno);
                TextView dealername = (TextView) convertView.findViewById(R.id.dealname);
                TextView dealemail = (TextView) convertView.findViewById(R.id.dealemail);
                TextView dealaddress = (TextView) convertView.findViewById(R.id.dealaddress);
                TextView dealstate = (TextView) convertView.findViewById(R.id.dealstate);
                TextView dealcity = (TextView) convertView.findViewById(R.id.dealcity);
                TextView dealmobile = (TextView) convertView.findViewById(R.id.dealmobile);
                ImageView update = (ImageView) convertView.findViewById(R.id.update);
                ImageView delete = (ImageView) convertView.findViewById(R.id.delete);
                sno.setText(Integer.toString(position + 1));
                dealername.setText(data.get(position).getDealerName());
                dealemail.setText(data.get(position).getDealerEmail());
                dealaddress.setText(data.get(position).getDealerAddress());
                dealstate.setText(data.get(position).getDealerState());
                dealcity.setText(data.get(position).getDealerCity());
                dealmobile.setText(data.get(position).getDealerMobile());

                update.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new AlertDialog.Builder(context)
                                .setTitle("Pricol Sales Order")
                                .setMessage("Are you sure want to Approve this ?")
                                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Approved(data.get(position).getId());
                                    }
                                })
                                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                })
                                .show();

                    }
                });

          /*   name.setText(namelist.get(position));*/


            } catch (Exception e) {
                e.printStackTrace();
            }


        }


        return convertView;
    }

    private void Approved(Integer id) {
        try {

            final ProgressDialog loading = ProgressDialog.show(context, context.getResources().getString(R.string.app_name), "Loading...", false, false);
            APIService service = RetrofitClient.getApiService();
            Call<ApproveDealerMenuData> call = service.Approve(id);
            call.enqueue(new Callback<ApproveDealerMenuData>() {
                @Override

                public void onResponse(Call<ApproveDealerMenuData> call, Response<ApproveDealerMenuData> response) {
                    try {

                        if (response.body().getResult().equals("Success")) {
                            loading.dismiss();
                            final AlertDialog dialog = new AlertDialog.Builder(context).create();
                            dialog.setTitle(R.string.app_name);
                            dialog.setMessage("Dealer Approved successfully !");
                            dialog.setButton("Okay", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent i = new Intent(context, RegionHomeActivity.class);
                                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_NEW_TASK);
                                    context.startActivity(i);
                                }
                            });
                            dialog.show();
                        } else if(response.body().getResult().equals("NotSuccess")) {
                            box.showAlertbox("No New Dealer Found");
                        }else
                        {
                            box.showAlertbox(context.getResources().getString(R.string.server_error));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        box.showAlertbox(context.getResources().getString(R.string.server_error));

                    }
                }

                @Override
                public void onFailure(Call<ApproveDealerMenuData> call, Throwable t) {
                    t.printStackTrace();
                    loading.dismiss();
                    box.showAlertbox(context.getResources().getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void filter(String s) {
        s = s.toLowerCase(Locale.getDefault());
        data.clear();

        if (s.length() == 0) {
            data.addAll(dataSize);

        } else {
            for (int i = 0; i < dataSize.size(); i++) {
                String wp = String.valueOf(dataSize.get(i).getDealerName().toLowerCase());
                String wp1=String.valueOf(dataSize.get(i).getDealerMobile().toLowerCase());
                // String id=String.valueOf(arraylistSize.get(i).getEmployeeId().toLowerCase());
                //   String id=arraylistSize.get(i).getEmployeeId().toLowerCase();
                //String mobile=arraylistSize.get(i).getMobileNo().toLowerCase();
                if (wp.toLowerCase(Locale.getDefault()).contains(s.toLowerCase())|| wp1.toLowerCase(Locale.getDefault()).contains(s.toLowerCase())) {
                    data.add(dataSize.get(i));
                    notifyDataSetChanged();

                }
            }

        }
    }

    private class Jobcarddetailsfumi {

        public TextView sno;
        public TextView name;
        public TextView date;
        public TextView intime;
        public TextView outtime;


    }

    @Override
    public int getCount() {
        return data.size();
    }
}
