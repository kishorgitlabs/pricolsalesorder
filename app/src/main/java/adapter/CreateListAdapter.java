package adapter;

import android.content.Context;
import android.content.Intent;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.brainmagic.pricolsalesorder.R;
import com.brainmagic.pricolsalesorder.SalesSecondaryActivity;

import java.util.List;

import api.models.master.createlist;

/**
 * Created by SYSTEM10 on 5/31/2019.
 */

public class CreateListAdapter extends ArrayAdapter {
    private Context context;
    private List<createlist> createlists;
    private int Salesid;


    public CreateListAdapter(@NonNull Context context, List<createlist> createlists, int Salesid) {
        super(context, R.layout.saleslistview, createlists);
        this.context = context;
        this.createlists = createlists;
        this.Salesid = Salesid;



    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        convertView = null;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.saleslistview, parent, false);
            TextView sno=(TextView)convertView.findViewById(R.id.sno);
            TextView customertype=(TextView)convertView.findViewById(R.id.customertype);
            TextView customername=(TextView)convertView.findViewById(R.id.customername);
            TextView state=(TextView)convertView.findViewById(R.id.state);
            //  TextView city=(TextView)convertView.findViewById(R.id.city);
            TextView mobile=(TextView)convertView.findViewById(R.id.mobile);
            ImageView addtocart=(ImageView)convertView.findViewById(R.id.addtocart);
            sno.setText(Integer.toString(position + 1));

            if(createlists.get(position).getTypeofacc().equals("Retailer")){
                customertype.setText(createlists.get(position).getTypeofacc()+"/"+createlists.get(position).getShopname());
                //customername.setText(createlists.get(position).getShopname());
            }else {
                customertype.setText(createlists.get(position).getTypeofacc()+"/"+createlists.get(position).getName());
                //  customername.setText(createlists.get(position).getName());
            }
            state.setText(createlists.get(position).getState());
            // city.setText(createlists.get(position).getCity());
            mobile.setText(createlists.get(position).getMobilenumber());

            addtocart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i =new Intent(context,SalesSecondaryActivity.class).putExtra("ordertype",createlists.get(position).getTypeofacc()).putExtra("executeid",Salesid)
                            .putExtra("states",createlists.get(position).getState()).putExtra("shops","").putExtra("citys",createlists.get(position).getCity()).putExtra("shopname",createlists.get(position).getShopname()).putExtra("mobilestring",createlists.get(position).getMobilenumber())
                            .putExtra("gst",createlists.get(position).getGst()).putExtra("regionstring",createlists.get(position).getRegion()).putExtra("name",createlists.get(position).getName()).putExtra("chooseactivity","fromchoosed").putExtra("address",createlists.get(position).getAddress());
//                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(i);
                }

            });

        }


        return convertView;
    }
}
