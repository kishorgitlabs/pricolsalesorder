package Logout;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;
import com.brainmagic.pricolsalesorder.LoginActivity;
import SQL_Fields.SQLITE;
import database.DBHelper;
import database.DBHelpersync;
import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Systems02 on 30-Oct-17.
 */

public class logout {
    SharedPreferences myshare;
    SharedPreferences.Editor editor;
    private Context context;
   // private Boolean isLogin;

    public logout(Context context) {
        this.context = context;
    }


    public void log_out()
    {
        myshare = context.getSharedPreferences("pricolsalesorder", MODE_PRIVATE);
        editor = myshare.edit();

        if(myshare.getBoolean("islogin",true)) {
            editor.clear();
            editor.putBoolean("isLogin", false);
            editor.putBoolean("synchronize", false);
            editor.apply();
        }
        DBHelpersync dbHelper = new DBHelpersync(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        DBHelper dbHelper1=new DBHelper(context);
        SQLiteDatabase db1=dbHelper1.getWritableDatabase();
        try {
            db.execSQL("DROP TABLE IF EXISTS "+ SQLITE.TABLE_Additems);
            db1.execSQL("DROP TABLE  IF EXISTS "+ SQLITE.TABLE_Create_Profile);
            db.execSQL("DROP TABLE  IF EXISTS "+ SQLITE.TABLE_NewPartsOrder);
            db.close();
        }
        catch (Exception e)
        {
            db.close();
            Log.v("Deletetable","Delete");
        }

        context.startActivity(new Intent(context, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
        Toast.makeText(context, "Logged out successfully ", Toast.LENGTH_SHORT).show();



    }

    public void log_outReg()
    {
        myshare = context.getSharedPreferences("pricolsalesorder", MODE_PRIVATE);
        editor = myshare.edit();

        if(myshare.getBoolean("isLoginreg",true)) {
            editor.clear();
            editor.putBoolean("isLoginreg", false);
            editor.putBoolean("synchronize", false);
            editor.apply();
        }


        context.startActivity(new Intent(context, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
        Toast.makeText(context, "Logged out successfully ", Toast.LENGTH_SHORT).show();



    }





}
