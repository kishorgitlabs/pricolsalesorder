package Home;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;


import com.brainmagic.pricolsalesorder.HomeActivty;
import com.brainmagic.pricolsalesorder.LoginActivity;
import com.brainmagic.pricolsalesorder.RegionHomeActivity;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by SYSTEM10 on 4/26/2019.
 */

public class homevalues {

    private Context context;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private Boolean islogin =true;

    public homevalues(Context context) {
        this.context = context;
        myshare = context.getSharedPreferences("pricolsalesorder", MODE_PRIVATE);
        editor = myshare.edit();
        islogin=myshare.getBoolean("isLogin",islogin);
    }

    public void home_io(){
        if(islogin){
            context.startActivity(new Intent(context, HomeActivty.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
        }else {
            context.startActivity(new Intent(context, RegionHomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
        }
    }
}
